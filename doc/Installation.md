# Installation

Clone the repository 

```bash
git clone git@gitlab.com:Lyssal/collectolys.git
```

Install the project (dev environment)

```bash
make install-dev
```

Install the Git hook

```bash
mv git/hooks/pre-commit .git/hooks/pre-commit
```
