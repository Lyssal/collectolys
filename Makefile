server:
	symfony server:start

install-dev:
	composer install
	yarn install
	make assets-dev

update-dev:
	composer update
	yarn install
	make assets-dev

install-prod:
	composer install
	composer dump-autoload --optimize
	yarn install
	make assets-prod

update-prod:
	composer install
	bin/console doctrine:migration:migrate
	composer dump-autoload --optimize
	yarn install
	make assets-prod

cache-clear-dev:
	bin/console cache:clear --no-warmup --env=dev
	bin/console cache:warmup --env=dev

cache-clear-prod:
	bin/console cache:clear --no-warmup --env=prod
	bin/console cache:warmup --env=prod

assets-dev:
	bin/console elfinder:install
	bin/console assets:install --symlink
	yarn encore dev

assets-watch:
	bin/console assets:install --symlink
	yarn encore dev --watch

assets-prod:
	bin/console elfinder:install
	bin/console assets:install --env=prod
	yarn encore production
