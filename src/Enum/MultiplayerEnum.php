<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Enum;

/**
 * The multiplayer possibilities.
 *
 * @category Enum
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class MultiplayerEnum
{
    /**
     * Solo.
     *
     * @var string
     */
    const SOLO = 'solo';

    /**
     * Multiplayer on a single system.
     *
     * @var string
     */
    const MULTI_SINGLE_SYSTEM = 'multi_single_system';

    /**
     * Multiplayer on local.
     *
     * @var string
     */
    const MULTI_LOCAL = 'multi_local';

    /**
     * Multiplayer online.
     *
     * @var string
     */
    const MULTI_ONLINE = 'multi_online';

    /**
     * MMO.
     *
     * @var string
     */
    const MASSIVELY_MULTI_ONLINE = 'massively_multi_online';

    /**
     * Cooperative gameplay.
     *
     * @var string
     */
    const COOPERATIVE = 'cooperative';

    /**
     * Competitive gameplay.
     *
     * @var string
     */
    const COMPETITIVE = 'competitive';

    /**
     * The values.
     *
     * @var array<string, string>
     */
    const VALUES = [
        self::SOLO => 'multiplayer.solo',
        self::MULTI_SINGLE_SYSTEM => 'multiplayer.single_system',
        self::COOPERATIVE => 'multiplayer.cooperative',
        self::COMPETITIVE => 'multiplayer.competitive',
        self::MULTI_LOCAL => 'multiplayer.local',
        self::MULTI_ONLINE => 'multiplayer.online',
        self::MASSIVELY_MULTI_ONLINE => 'multiplayer.massively_multi_online',
    ];
}
