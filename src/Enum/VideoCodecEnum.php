<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Enum;

/**
 * The video codecs.
 *
 * @category Enum
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class VideoCodecEnum
{
    /**
     * DivX.
     *
     * @var string
     */
    const DIVX = 'DIVX';

    /**
     * H.264.
     *
     * @var string
     */
    const H_264 = 'H264';

    /**
     * H.265.
     *
     * @var string
     */
    const H_265 = 'H265';

    /**
     * AV1.
     *
     * @var string
     */
    const AV1 = 'AV1';

    /**
     * The values.
     *
     * @var array<string, string>
     */
    const VALUES = [
        self::DIVX => 'DivX',
        self::H_264 => 'H.264 (MPEG-4 AVC)',
        self::H_265 => 'H.265 (MPEG-H HEVC)',
        self::AV1 => 'AV1',
    ];
}
