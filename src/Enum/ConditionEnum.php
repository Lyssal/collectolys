<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Enum;

/**
 * The condition possibilities.
 *
 * @category Enum
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ConditionEnum
{
    /**
     * New.
     *
     * @var int
     */
    const NEW = 5;

    /**
     * Very good.
     *
     * @var int
     */
    const VERY_GOOD = 4;

    /**
     * Good.
     *
     * @var int
     */
    const GOOD = 3;

    /**
     * Fair.
     *
     * @var int
     */
    const FAIR = 2;

    /**
     * Bad.
     *
     * @var int
     */
    const BAD = 1;

    /**
     * Unusable.
     *
     * @var int
     */
    const UNUSABLE = 0;

    /**
     * The values.
     *
     * @var array<string, string>
     */
    const VALUES = [
        self::NEW => 'condition.new',
        self::VERY_GOOD => 'condition.very_good',
        self::GOOD => 'condition.good',
        self::FAIR => 'condition.fair',
        self::BAD => 'condition.bad',
        self::UNUSABLE => 'condition.unusable',
    ];
}
