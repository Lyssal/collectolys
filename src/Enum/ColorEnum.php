<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Enum;

/**
 * The video color types.
 *
 * @category Enum
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ColorEnum
{
    /**
     * Black and white.
     *
     * @var string
     */
    const BLACK_AND_WHITE = 'black_and_white';

    /**
     * Colorized.
     *
     * @var string
     */
    const COLORIZED = 'colorized';

    /**
     * The values.
     *
     * @var array<string, string>
     */
    const VALUES = [
        self::BLACK_AND_WHITE => 'color.black_and_white',
        self::COLORIZED => 'color.colorized',
    ];
}
