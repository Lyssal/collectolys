<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Enum;

use Psr\Log\InvalidArgumentException;

/**
 * The video types.
 *
 * @category Enum
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class VideoTypeEnum
{
    /**
     * Film.
     *
     * @var string
     */
    const FILM = 'film';

    /**
     * Television film.
     *
     * @var string
     */
    const TELEVISION_FILM = 'tv_film';

    /**
     * Television show.
     *
     * @var string
     */
    const TELEVISION_SHOW = 'tv_show';

    /**
     * Mini series.
     *
     * @var string
     */
    const MINI_SERIES = 'mini_series';

    /**
     * Television series.
     *
     * @var string
     */
    const TELEVISION_SERIES = 'tv_series';

    /**
     * Non-animated television series.
     *
     * @var string
     */
    const NON_ANIMATED_TELEVISION_SERIES = 'non_animated_tv_series';

    /**
     * Animated series.
     *
     * @var string
     */
    const ANIMATED_SERIES = 'animated_series';

    /**
     * Short film.
     *
     * @var string
     */
    const SHORT_FILM = 'short_film';

    /**
     * The duration max in seconds for a short film.
     *
     * @var int
     */
    const SHORT_FILM_DURATION_MAX = 60 * 40;

    /**
     * The values.
     *
     * @var array<string, string>
     */
    const VALUES = [
        self::FILM => 'video.type.film',
        self::TELEVISION_FILM => 'video.type.television_film',
        self::TELEVISION_SHOW => 'video.type.television_show',
        self::MINI_SERIES => 'video.type.mini_series',
        self::TELEVISION_SERIES => 'video.type.television_series',
    ];

    /**
     * Get the video type label.
     *
     * @param string $videoType The video type
     *
     * @return string The label
     */
    public static function getLabel(string $videoType): string
    {
        switch ($videoType) {
            case self::NON_ANIMATED_TELEVISION_SERIES:
                return 'video.type.television_series';
            case self::ANIMATED_SERIES:
                return 'video.type.animated_series';
            case self::SHORT_FILM:
                return 'video.type.short_film';
        }

        if (!isset(self::VALUES[$videoType])) {
            throw new InvalidArgumentException('The video type label has not been found for "'.$videoType.'".');
        }

        return self::VALUES[$videoType];
    }
}
