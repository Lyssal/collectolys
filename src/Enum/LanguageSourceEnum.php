<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Enum;

/**
 * The language sources.
 *
 * @category Enum
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class LanguageSourceEnum
{
    /**
     * Text.
     *
     * @var string
     */
    const TEXT = 'text';

    /**
     * Audio.
     *
     * @var string
     */
    const AUDIO = 'audio';

    /**
     * The values.
     *
     * @var array<string, string>
     */
    const VALUES = [
        self::TEXT => 'text',
        self::AUDIO => 'audio',
    ];
}
