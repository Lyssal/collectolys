<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Enum;

/**
 * The audio codecs.
 *
 * @category Enum
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class AudioCodecEnum
{
    /**
     * AAC.
     *
     * @var string
     */
    const AAC = 'AAC';

    /**
     * AC3.
     *
     * @var string
     */
    const AC3 = 'AC3';

    /**
     * FLAC.
     *
     * @var string
     */
    const FLAC = 'FLAC';

    /**
     * DTS.
     *
     * @var string
     */
    const DTS = 'DTS';

    /**
     * MP3.
     *
     * @var string
     */
    const MP3 = 'MP3';

    /**
     * Vorbis.
     *
     * @var string
     */
    const VORBIS = 'VORBIS';

    /**
     * Opus.
     *
     * @var string
     */
    const OPUS = 'OPUS';

    /**
     * The values.
     *
     * @var array<string, string>
     */
    const VALUES = [
        self::AAC => 'AAC',
        self::AC3 => 'AC3',
        self::DTS => 'DTS',
        self::FLAC => 'FLAC',
        self::MP3 => 'MP3',
        self::VORBIS => 'Vorbis',
        self::OPUS => 'Opus',
    ];
}
