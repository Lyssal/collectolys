<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Search;

use App\Entity\Company\Company;
use App\Entity\Person\Person;
use App\Entity\Type;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Lyssal\Text\Slug;
use Symfony\Component\HttpFoundation\Request;

/**
 * The search service.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class Searcher
{
    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    private $entityAdministratorManager;

    /**
     * The view parameter service.
     *
     * @var \App\Search\ViewParameter
     */
    private $viewParameter;

    /**
     * Constructor.
     *
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     * @param \App\Search\ViewParameter                                     $viewParameter              The view parameter service
     */
    public function __construct(EntityAdministratorManager $entityAdministratorManager, ViewParameter $viewParameter)
    {
        $this->entityAdministratorManager = $entityAdministratorManager;
        $this->viewParameter = $viewParameter;
    }

    /**
     * Get the view parameters for the current search.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request The current request
     * @param int                                       $page    The page
     *
     * @throws \Lyssal\Entity\Decorator\Exception\DecoratorException If a decorator entity is invalid
     *
     * @return array The parameters
     */
    public function getViewParameters(Request $request, int $page): array
    {
        $q = $request->query->get('q');
        $object = $request->query->get('object');
        $conditions = ['set' => false];

        if ($request->query->has('types')) {
            $typeManager = $this->entityAdministratorManager->get(Type::class);
            $types = $typeManager->findBy(['id' => $request->query->get('types', [])]);

            if (\count($types) > 1) {
                $conditions['types'] = $types;
            } elseif (1 === \count($types)) {
                $conditions['type'] = $types[0];
            } else {
                $conditions['types'] = $typeManager->findAll();
            }
        }

        switch ($object) {
            case 'simple':
                $conditions['query_simple'] = $q;
                break;
            case 'advanced':
                $conditions['query_advanced'] = $q;
                break;
            case 'company':
                $querySlug = new Slug($q);
                $querySlug->minify('_', false);
                $companies = $this->entityAdministratorManager->get(Company::class)->findLikeBy(['slug' => '%'.$querySlug->getText().'%']);
                if (\count($companies) > 1) {
                    $conditions['companies'] = $companies;
                } elseif (1 === \count($companies)) {
                    $conditions['company'] = $companies[0];
                } else { // Any result, force 0 result
                    $conditions['company'] = null;
                }
                break;
            case 'person':
                $querySlug = new Slug($q);
                $querySlug->minify('_', false);
                $persons = $this->entityAdministratorManager->get(Person::class)->findLikeBy(['slug' => '%'.$querySlug->getText().'%']);
                if (\count($persons) > 1) {
                    $conditions['people'] = $persons;
                } elseif (1 === \count($persons)) {
                    $conditions['person'] = $persons[0];
                } else { // Any result, force 0 result
                    $conditions['person'] = null;
                }
                break;
            case 'year':
                $conditions['year'] = $q;
                break;
        }

        return array_merge(
            $this->viewParameter->get($conditions, [], $page, false),
            [
                'search_query' => $q,
                'search_object' => $object,
            ]
        );
    }
}
