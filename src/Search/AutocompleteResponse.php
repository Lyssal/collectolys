<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Search;

use App\Entity\Company\Company;
use App\Entity\Element\Element;
use App\Entity\Keyword;
use Doctrine\ORM\QueryBuilder;
use Lyssal\Doctrine\Orm\QueryBuilder as LyssalQueryBuilder;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Lyssal\EntityBundle\Appellation\AppellationManager;
use Lyssal\Text\Slug;
use Symfony\Component\HttpFoundation\Request;

/**
 * The type controller.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class AutocompleteResponse
{
    /**
     * The result count by page.
     *
     * @var int
     */
    const RESULT_COUNT = 10;

    /**
     * @var \Lyssal\EntityBundle\Appellation\AppellationManager
     */
    private $appellationManager;

    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    private $entityAdministratorManager;

    /**
     * Constructor.
     *
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     */
    public function __construct(AppellationManager $appellationManager, EntityAdministratorManager $entityAdministratorManager)
    {
        $this->appellationManager = $appellationManager;
        $this->entityAdministratorManager = $entityAdministratorManager;
    }

    /**
     * Get the element JSON.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request The request
     *
     * @return array The JSON
     */
    public function getElementJson(Request $request): array
    {
        $query = $request->query->get('query');
        $querySlug = new Slug($query);
        $querySlug->minify('_');
        $typeId = $request->query->getInt('type');
        $hasType = $typeId > 0;
        $exceptElementId = $request->query->get('except');
        $page = $request->query->getInt('page', 1);

        $conditions = [
            LyssalQueryBuilder::OR_WHERE => [
                [LyssalQueryBuilder::WHERE_LIKE => ['slug' => '%'.$querySlug->getText().'%']],
                [LyssalQueryBuilder::WHERE_LIKE => ['name.slug' => '%'.$querySlug->getText().'%']],
            ],
        ];

        if ($hasType) {
            $conditions['type'] = $typeId;
        }

        if (null !== $exceptElementId) {
            $conditions[LyssalQueryBuilder::WHERE_NOT_EQUAL] = [
                'id' => $exceptElementId,
            ];
        }

        $queryBuilder = $this->entityAdministratorManager->get(Element::class)->getRepository()->getQueryBuilderFindBy(
            $conditions,
            [],
            null,
            null,
            [
                LyssalQueryBuilder::LEFT_JOINS => [
                    'names' => 'name',
                ],
            ]
        );

        // Conditions are in a subquery because of the join (else the LIMIT will not work)
        $mainQueryBuilder = $this->entityAdministratorManager->get(Element::class)->getRepository()->createQueryBuilder('mainElement')
            ->orderBy('mainElement.slug');
        $mainQueryBuilder
            ->where($mainQueryBuilder->expr()->in('mainElement', $queryBuilder->getDQL()))
            ->setParameters($queryBuilder->getParameters())
        ;

        return $this->getJson($mainQueryBuilder, $page);
    }

    /**
     * Get the set JSON.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request The request
     *
     * @return array The JSON
     */
    public function getSetJson(Request $request): array
    {
        $query = $request->query->get('query');
        $querySlug = new Slug($query);
        $querySlug->minify('_');
        $typeId = $request->query->getInt('type');
        $page = $request->query->getInt('page', 1);

        $queryBuilder = $this->entityAdministratorManager->get(Element::class)->getRepository()->getQueryBuilderFindBy([
            'type' => $typeId,
            'set' => true,
            LyssalQueryBuilder::WHERE_LIKE => [
                'slug' => '%'.$querySlug->getText().'%',
            ],
        ], [
            'slug',
        ]);

        return $this->getJson($queryBuilder, $page);
    }

    /**
     * Get the company JSON.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request The request
     *
     * @return array The JSON
     */
    public function getCompanyJson(Request $request): array
    {
        $query = $request->query->get('query');
        $querySlug = new Slug($query);
        $querySlug->minify('_');
        $page = $request->query->getInt('page', 1);

        $queryBuilder = $this->entityAdministratorManager->get(Company::class)->getRepository()->getQueryBuilderFindBy([
            LyssalQueryBuilder::WHERE_LIKE => [
                'slug' => '%'.$querySlug->getText().'%',
            ],
        ], [
            'name',
        ]);

        return $this->getJson($queryBuilder, $page);
    }

    /**
     * Get the keyword JSON.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request The request
     *
     * @return array The JSON
     */
    public function getKeywordJson(Request $request): array
    {
        $query = $request->query->get('query');
        $querySlug = new Slug($query);
        $querySlug->minify('_');
        $page = $request->query->getInt('page', 1);

        $queryBuilder = $this->entityAdministratorManager->get(Keyword::class)->getRepository()->getQueryBuilderFindBy([
            LyssalQueryBuilder::WHERE_LIKE => [
                'slug' => '%'.$querySlug->getText().'%',
            ],
        ], [
            'name',
        ]);

        return $this->getJson($queryBuilder, $page);
    }

    /**
     * Get the JSON result for autocompletion.
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder The query buider
     * @param int                        $page         The page
     *
     * @return array The JSON
     */
    private function getJson(QueryBuilder $queryBuilder, int $page = 1): array
    {
        $queryBuilder
            ->setMaxResults(self::RESULT_COUNT + 1)
            ->setFirstResult(($page - 1) * self::RESULT_COUNT)
        ;

        $entities = $queryBuilder->getQuery()->getResult();

        foreach ($entities as $i => $entity) {
            if ($i < self::RESULT_COUNT) {
                $location = method_exists($entity, 'getLocation')
                    ? $entity->getLocation()
                    : (method_exists($entity, 'getNationality') ? $entity->getNationality() : null)
                ;
                $locationText = (null !== $location && null !== $location->getEmoji() ? $location->getEmoji().' ' : '');
                $typeText = (method_exists($entity, 'getType') ? $entity->getType()->getEmoji().' ' : '');
                $year = method_exists($entity, 'getYear') ? $entity->getYear() : null;
                $yearText = (null !== $year ? ' ('.$year.')' : '');

                $json['results'][] = [
                    'id' => $entity->getId(),
                    'text' => $locationText.$typeText.$this->appellationManager->appellation($entity).$yearText,
                ];
            }
        }

        $json['has_next_page'] = \count($entities) > self::RESULT_COUNT;

        return $json;
    }
}
