<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Search;

use App\Element\ElementSearcher;
use App\Element\Sorter;
use Lyssal\Entity\Decorator\DecoratorManager;
use Symfony\Component\Security\Core\Security;

/**
 * The type controller.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ViewParameter
{
    /**
     * The security service.
     *
     * @var \Symfony\Component\Security\Core\Security
     */
    private $security;

    /**
     * The decorator.
     *
     * @var \Lyssal\Entity\Decorator\DecoratorManager
     */
    private $decorator;

    /**
     * The sorter service.
     *
     * @var \App\Element\Sorter
     */
    private $sorter;

    /**
     * The Element searcher.
     *
     * @var \App\Element\ElementSearcher
     */
    private $elementSearcher;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Security\Core\Security $security        The security service
     * @param \Lyssal\Entity\Decorator\DecoratorManager $decorator       The decorator
     * @param \App\Element\Sorter                       $sorter          The sorter service
     * @param \App\Element\ElementSearcher              $elementSearcher The Element manager The Element searcher
     */
    public function __construct(Security $security, DecoratorManager $decorator, Sorter $sorter, ElementSearcher $elementSearcher)
    {
        $this->security = $security;
        $this->decorator = $decorator;
        $this->sorter = $sorter;
        $this->elementSearcher = $elementSearcher;
    }

    /**
     * Get the view parameters.
     *
     * @param array $conditions     The conditions
     * @param array $orderBy        The order
     * @param int   $page           The page number
     * @param bool  $aggregateBySet If the elements are grouped by set
     *
     * @throws \Lyssal\Entity\Decorator\Exception\DecoratorException If a decorator entity is invalid
     *
     * @return array The parameters
     */
    public function get(array $conditions = [], array $orderBy = [], int $page = 1, bool $aggregateBySet = true): array
    {
        $sort = empty($orderBy) ? $this->sorter->getChosenSort() : null;
        $orderBy = array_merge($orderBy, $this->sorter->getOrderBy());
        $resultsPagerfanta = $this->elementSearcher->getPagerfantaFind($this->security->getUser(), $conditions, $orderBy, $page, $aggregateBySet);
        $results = $this->decorator->get($resultsPagerfanta);

        return array_merge($conditions, [
            'sort' => $sort,
            'aggregate_by_set' => $aggregateBySet,
            'result_pagerfanta' => $resultsPagerfanta,
            'results' => $results,
        ]);
    }
}
