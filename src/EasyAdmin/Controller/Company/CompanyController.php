<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\EasyAdmin\Controller\Company;

use App\Doctrine\Manager\Company\CompanyManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

/**
 * The Company EasyAdmin controller.
 *
 * @category EasyAdmin
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class CompanyController extends EasyAdminController
{
    /**
     * @var \App\Doctrine\Manager\Company\CompanyManager
     */
    private $companyManager;

    public function __construct(CompanyManager $companyManager)
    {
        $this->companyManager = $companyManager;
    }

    /**
     * {@inheritdoc}
     *
     * @param \App\Entity\Company\Company $company
     */
    protected function persistEntity($company)
    {
        if (null !== $company->getImage()) {
            $company->getImage()->uploadFile();
        }

        $this->companyManager->save($company);
    }

    /**
     * {@inheritdoc}
     *
     * @param \App\Entity\Company\Company $company
     */
    protected function updateEntity($company)
    {
        $this->persistEntity($company);
    }
}
