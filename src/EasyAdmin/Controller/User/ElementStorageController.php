<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\EasyAdmin\Controller\User;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

/**
 * The ElementStorage EasyAdmin controller.
 *
 * @category EasyAdmin
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ElementStorageController extends EasyAdminController
{
    /**
     * {@inheritdoc}
     */
    public function createNewEntity()
    {
        $elementStorage = parent::createNewEntity();
        $elementStorage->setUser($this->getUser());

        return $elementStorage;
    }
}
