<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\EasyAdmin\Controller\Element;

use App\Doctrine\Manager\Element\ElementManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

/**
 * The Element EasyAdmin controller.
 *
 * @category EasyAdmin
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ElementController extends EasyAdminController
{
    /**
     * @var \App\Doctrine\Manager\Element\ElementManager
     */
    private $elementManager;

    public function __construct(ElementManager $elementManager)
    {
        $this->elementManager = $elementManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function persistEntity($entity)
    {
        $this->elementManager->save($entity);
    }

    /**
     * {@inheritdoc}
     */
    protected function updateEntity($entity)
    {
        $this->elementManager->save($entity);
    }
}
