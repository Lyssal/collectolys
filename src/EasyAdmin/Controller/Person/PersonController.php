<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\EasyAdmin\Controller\Person;

use App\Doctrine\Manager\Person\PersonManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

/**
 * The Person EasyAdmin controller.
 *
 * @category EasyAdmin
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class PersonController extends EasyAdminController
{
    /**
     * @var \App\Doctrine\Manager\Person\PersonManager
     */
    private $personManager;

    public function __construct(PersonManager $personManager)
    {
        $this->personManager = $personManager;
    }

    /**
     * {@inheritdoc}
     *
     * @param \App\Entity\Person\Person $person
     */
    protected function persistEntity($person)
    {
        if (null !== $person->getImage()) {
            $person->getImage()->uploadFile();
        }

        $this->personManager->save($person);
    }

    /**
     * {@inheritdoc}
     *
     * @param \App\Entity\Person\Person $person
     */
    protected function updateEntity($person)
    {
        $this->persistEntity($person);
    }
}
