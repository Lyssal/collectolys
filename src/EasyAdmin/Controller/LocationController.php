<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\EasyAdmin\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

/**
 * The Location EasyAdmin controller.
 *
 * @category EasyAdmin
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class LocationController extends EasyAdminController
{
    /**
     * {@inheritDoc}
     *
     * @param \App\Entity\Location $location
     */
    protected function persistEntity($location)
    {
        if (
            null !== $location->getIcon()
            && null === $location->getIcon()->getFilename()
            && null === $location->getIcon()->getUploadedFile()
        ) {
            $location->setIcon(null);
        }

        $this->getDoctrine()->getManager()->persist($location);
        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * {@inheritDoc}
     *
     * @param \App\Entity\Location $location
     */
    protected function updateEntity($location)
    {
        $this->persistEntity($location);
    }
}
