<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\EasyAdmin\Controller;

use App\Doctrine\Manager\GenreManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Lyssal\Doctrine\Orm\QueryBuilder;

/**
 * The Genre EasyAdmin controller.
 *
 * @category EasyAdmin
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class GenreController extends EasyAdminController
{
    /**
     * @var \App\Doctrine\Manager\GenreManager
     */
    private $genreManager;

    public function __construct(GenreManager $genreManager)
    {
        $this->genreManager = $genreManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function createEntityFormBuilder($entity, $view)
    {
        $builder = parent::createEntityFormBuilder($entity, $view);

        /**
         * @var \App\Entity\Genre
         */
        $genre = $entity;
        $genreAlreadyExists = $this->genreManager->exists($genre);

        $builder
            ->add('type', null, [
                'label' => 'collection',
                'disabled' => $genreAlreadyExists,
            ])
        ;

        if ($genreAlreadyExists) {
            $parents = $this->genreManager->findBy([
                'type' => $genre->getType(),
                QueryBuilder::WHERE_NOT_EQUAL => [QueryBuilder::ALIAS => $genre],
            ]);

            $builder->add('parent', null, [
                'choices' => $parents,
            ]);
        } else {
            $builder->remove('parent');
        }

        return $builder;
    }
}
