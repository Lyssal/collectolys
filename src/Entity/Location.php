<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity;

use App\Doctrine\Decorator\Element\ElementDecorator;
use App\Entity\Element\Element;
use App\Entity\File\Icon;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Lyssal\Exception\InvalidArgumentException;
use Lyssal\SimpleLocationBundle\Entity\Location as LyssalLocation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The location.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\LocationRepository")
 */
class Location extends LyssalLocation
{
    /**
     * The icon path.
     *
     * @var string
     */
    const ICON_PATH = 'locations';

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     *
     * @Gedmo\Slug(fields={"name"}, style="camel", separator="_")
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $emoji;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Icon", cascade={"persist", "remove"}, fetch="EAGER")
     *
     * @Assert\Valid()
     */
    private $icon;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     */
    private $currency;

    /**
     * @var \Doctrine\Common\Collections\Collection|\App\Entity\LocationVersion[]
     *
     * @ORM\OneToMany(targetEntity="LocationVersion", mappedBy="location", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"startDate":"ASC", "endDate":"ASC"})
     *
     * @Assert\Valid()
     */
    private $versions;

    public function __construct()
    {
        parent::__construct();
        $this->versions = new ArrayCollection();
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getEmoji(): ?string
    {
        return $this->emoji;
    }

    public function setEmoji(?string $emoji): self
    {
        $this->emoji = $emoji;

        return $this;
    }

    public function getIcon(): ?Icon
    {
        return $this->icon;
    }

    public function setIcon(?Icon $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|\App\Entity\LocationVersion[]
     */
    public function getVersions(): Collection
    {
        return $this->versions;
    }

    public function addVersion(LocationVersion $version): self
    {
        if (!$this->versions->contains($version)) {
            $this->versions[] = $version;
            $version->setLocation($this);
        }

        return $this;
    }

    public function removeVersion(LocationVersion $version): self
    {
        if ($this->versions->contains($version)) {
            $this->versions->removeElement($version);
            // set the owning side to null (unless already changed)
            if ($version->getLocation() === $this) {
                $version->setLocation(null);
            }
        }

        return $this;
    }

    public function getVersionName(?DateTimeInterface $date): ?string
    {
        if (null === $date) {
            return $this->name;
        }

        foreach ($this->versions as $version) {
            if ($version->dateIsIncluded($date) && null !== $version->getName()) {
                return $version->getName();
            }
        }

        return $this->name;
    }

    public function getVersionEmojiOrIcon(?DateTimeInterface $date)
    {
        if (null === $date) {
            return $this->emoji ?? $this->icon;
        }

        foreach ($this->versions as $version) {
            if ($version->dateIsIncluded($date) && (null !== $version->getEmoji() || null !== $version->getIcon())) {
                return $version->getEmoji() ?? $version->getIcon();
            }
        }

        return $this->emoji ?? $this->icon;
    }

    public function getVersionNameByElement($element): ?string
    {
        if (!$element instanceof Element && !$element instanceof ElementDecorator) {
            throw new InvalidArgumentException('The element has to be an Element or ElementDecorator.');
        }

        $elementDate = $element->getOldestElementDate();

        if (null === $elementDate) {
            return $this->name;
        }

        return $this->getVersionName($elementDate->getDateTime());
    }
}
