<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\File;

use Doctrine\ORM\Mapping as ORM;
use Lyssal\File\Directory;
use Lyssal\File\Image as LyssalImage;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An image.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Image extends File
{
    /**
     * The max image width / height.
     *
     * @var int
     */
    const MAX_SIZE = 1024;

    /**
     * The minimum file size to reduce. If the file size is greater, the file will be reduced.
     *
     * @var int
     */
    const MIN_FILE_SIZE_TO_REDUCE = 1024 * 100; // 100 Ko

    /**
     * The max file count in a sub directory.
     *
     * @var int
     */
    const MAX_FILES_IN_DIRECTORY = 500;

    /**
     * {@inheritdoc}
     *
     * @Assert\Image(detectCorrupted=true)
     */
    protected $uploadedFile;

    /**
     * @see \Lyssal\Entity\Traits\UploadedFileTrait::getUploadedFileDirectory()
     */
    public function getUploadedFileDirectory()
    {
        return 'images'.\DIRECTORY_SEPARATOR.$this->path;
    }

    /**
     * @see \Lyssal\EntityBundle\Traits\UploadedFileTrait::uploadFile()
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFile($filename = null): void
    {
        if (!$this->uploadedFileIsValid()) {
            return;
        }

        // Delete the old file if existing
        $this->deleteFile();

        if (null === $this->filename) {
            $path = $this->getUploadedFileDirectory();

            $subDirectory = 1;
            while (!self::canAddFileInDirectory($path.\DIRECTORY_SEPARATOR.$subDirectory)) {
                ++$subDirectory;
            }

            $this->path .= '/'.$subDirectory;
        } else {
            // Delete the old file if existing
            $this->deleteFile();
        }

        // Save the file in the server
        $this->saveUploadedFile();

        // Here our image in the server
        $image = new LyssalImage($this->getUploadedFilePathname());
        // We minify the image name to remove special characters,
        // specify a maxlength for the database
        // and to not replace an existing file
        $image->minify(null, null, true, 255, false);

        // We verify that the image format is managed
        if ($image->formatIsManaged() && $image->getSize() > self::MIN_FILE_SIZE_TO_REDUCE) {
            // Convert image into WEBP
            $image->convert(\IMAGETYPE_WEBP);
            // We proportionally reduce the image
            $image->resizeProportionallyByMaxSize(static::MAX_SIZE, static::MAX_SIZE);
        }

        // We get the new filename
        $this->filename = $image->getFilename();
    }

    /**
     * Determine if we can add an other file in the directory.
     *
     * @param string $path The directory path
     *
     * @throws \Lyssal\Exception\IoException If we can create the directory
     *
     * @return bool If it can be added
     */
    private static function canAddFileInDirectory(string $path): bool
    {
        $directory = new Directory($path);

        if (!$directory->exists()) {
            $directory->create();

            return true;
        }

        return $directory->getFileCount() < self::MAX_FILES_IN_DIRECTORY;
    }
}
