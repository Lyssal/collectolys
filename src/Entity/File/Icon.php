<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\File;

use Doctrine\ORM\Mapping as ORM;
use Lyssal\File\Image as LyssalImage;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An icon.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Icon extends File
{
    /**
     * The max image width / height.
     *
     * @var int
     */
    const MAX_SIZE = 528;

    /**
     * {@inheritdoc}
     *
     * @Assert\File(mimeTypes={"image/*", "image/svg+xml"})
     */
    protected $uploadedFile;

    /**
     * @see \Lyssal\Entity\Traits\UploadedFileTrait::getUploadedFileDirectory()
     */
    public function getUploadedFileDirectory()
    {
        return 'images'.\DIRECTORY_SEPARATOR.'icons'.\DIRECTORY_SEPARATOR.$this->path;
    }

    /**
     * @see \Lyssal\EntityBundle\Traits\UploadedFileTrait::uploadFile()
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFile($filename = null): void
    {
        if (!$this->uploadedFileIsValid()) {
            return;
        }

        // Delete the old file if existing
        $this->deleteFile();
        // Save the file in the server
        $this->saveUploadedFile();

        // Here our image in the server
        $image = new LyssalImage($this->getUploadedFilePathname());
        // We minify the image name to remove special characters,
        // specify a maxlength for the database
        // and to not replace an existing file
        $image->minify(null, null, true, 255, true);
        // We get the new filename
        $this->filename = $image->getFilename();

        // We verify that the image format is managed
        if ($image->formatIsManaged()) {
            // We proportionally reduce the image
            $image->resizeProportionallyByMaxSize(static::MAX_SIZE, static::MAX_SIZE);
        }
    }
}
