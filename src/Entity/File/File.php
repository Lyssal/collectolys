<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\File;

use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;
use Lyssal\EntityBundle\Entity\Traits\UpdatedAtTrait;
use Lyssal\EntityBundle\Traits\UploadedFileTrait;
use Lyssal\File\File as LyssalFile;

/**
 * A file.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks()
 */
abstract class File
{
    use IdTrait;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $filename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $path;

    use UpdatedAtTrait;

    /**
     * The file to upload.
     *
     * @var \Symfony\Component\HttpFoundation\File\File
     */
    protected $uploadedFile;

    use UploadedFileTrait;

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @see UploadedFileTrait::setUploadedFile()
     */
    public function setUploadedFile($uploadedFile = null)
    {
        $this->uploadedFile = $uploadedFile;
        // Force the event if no other property is modified
        $this->initUpdatedAt();

        return $this;
    }

    /**
     * Get the filename.
     */
    public function __toString(): string
    {
        return (string) $this->filename;
    }

    /**
     * Get the file URL.
     */
    public function getUrl(): string
    {
        return str_replace(\DIRECTORY_SEPARATOR, '/', $this->getPathname());
    }

    /**
     * Get the file path.
     */
    public function getPathname(): string
    {
        return $this->getUploadedFileDirectory().\DIRECTORY_SEPARATOR.$this->filename;
    }

    /**
     * Delete the image.
     *
     * @ORM\PostRemove()
     */
    public function deleteFile(): void
    {
        if (null !== $this->filename) {
            $file = new LyssalFile($this->getPathname());
            $file->delete();
        }
    }
}
