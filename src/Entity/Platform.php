<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity;

use App\Entity\File\IconableInterface;
use App\Entity\Traits\IconTrait;
use App\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * An element platform.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\PlatformRepository")
 */
class Platform implements IconableInterface
{
    /**
     * The icon path.
     *
     * @var string
     */
    const ICON_PATH = 'platforms';

    use IdTrait;

    use IconTrait;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     *
     * @Gedmo\Slug(fields={"name"}, style="camel", separator="_")
     */
    private $slug;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get the name.
     *
     * @return string The name
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
