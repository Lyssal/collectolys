<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Traits;

use App\Entity\File\Icon;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Add an icon property.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
trait IconTrait
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Icon", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\Valid()
     */
    private $icon;

    public function getIcon(): ?Icon
    {
        return $this->icon;
    }

    public function setIcon(Icon $icon): self
    {
        $this->icon = $icon;

        return $this;
    }
}
