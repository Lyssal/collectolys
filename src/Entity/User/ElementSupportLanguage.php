<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\User;

use App\Entity\Language;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PositionTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A user element support language.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 *
 * @UniqueEntity(fields={"elementSupport", "language"})
 */
class ElementSupportLanguage
{
    use IdTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\ElementSupport", inversedBy="languages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $elementSupport;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     * @ORM\JoinColumn(nullable=false)
     */
    private $language;

    /**
     * @ORM\Column(type="simple_array")
     *
     * @Assert\Count(min=1)
     */
    private $sources = [];

    use PositionTrait;

    public function getElementSupport(): ?ElementSupport
    {
        return $this->elementSupport;
    }

    public function setElementSupport(?ElementSupport $elementSupport): self
    {
        $this->elementSupport = $elementSupport;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getSources(): ?array
    {
        return $this->sources;
    }

    public function setSources(array $sources): self
    {
        $this->sources = $sources;

        return $this;
    }
}
