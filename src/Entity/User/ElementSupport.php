<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\User;

use App\Entity\Support\Support;
use App\Entity\Traits\IdTrait;
use App\Entity\Type;
use App\Entity\User\ElementSupport\Audio;
use App\Entity\User\ElementSupport\Video;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A user element support.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\User\ElementSupportRepository")
 * @ORM\InheritanceType("JOINED")
 */
class ElementSupport
{
    use IdTrait;

    /**
     * @var \App\Entity\User\UserElement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User\UserElement", inversedBy="supports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\ElementStorage")
     */
    private $storage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\ElementStorage")
     */
    private $storageBackup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Support\Support")
     */
    private $support;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $version;

    /**
     * @ORM\Column(type="smallint", name="support_condition", nullable=true)
     */
    private $condition;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $webSource;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\User\ElementSupportLanguage",
     *     mappedBy="elementSupport",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     *
     * @Assert\Valid()
     */
    private $languages;

    public function __construct()
    {
        $this->languages = new ArrayCollection();
    }

    public function getElement(): ?UserElement
    {
        return $this->element;
    }

    public function setElement(?UserElement $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getStorage(): ?ElementStorage
    {
        return $this->storage;
    }

    public function setStorage(?ElementStorage $storage): self
    {
        $this->storage = $storage;

        return $this;
    }

    public function getStorageBackup(): ?ElementStorage
    {
        return $this->storageBackup;
    }

    public function setStorageBackup(?ElementStorage $storageBackup): self
    {
        $this->storageBackup = $storageBackup;

        return $this;
    }

    public function getSupport(): ?Support
    {
        return $this->support;
    }

    public function setSupport(?Support $support): self
    {
        $this->support = $support;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getCondition(): ?int
    {
        return $this->condition;
    }

    public function setCondition(?int $condition): self
    {
        $this->condition = $condition;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getWebSource(): ?string
    {
        return $this->webSource;
    }

    public function setWebSource(?string $webSource): self
    {
        $this->webSource = $webSource;

        return $this;
    }

    /**
     * @return Collection|ElementSupportLanguage[]
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }

    public function addLanguage(ElementSupportLanguage $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
            $language->setElementSupport($this);
        }

        return $this;
    }

    public function removeLanguage(ElementSupportLanguage $language): self
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
            // set the owning side to null (unless already changed)
            if ($language->getElementSupport() === $this) {
                $language->setElementSupport(null);
            }
        }

        return $this;
    }

    public function getWebSourceHostname(): ?string
    {
        if (null === $this->webSource) {
            return null;
        }

        return parse_url($this->webSource, \PHP_URL_HOST);
    }

    /**
     * Determine if the support is an audio.
     *
     * @return bool If audio
     */
    public function isAudio(): bool
    {
        return $this instanceof Audio;
    }

    /**
     * Determine if the support is a video.
     *
     * @return bool If video
     */
    public function isVideo(): bool
    {
        return $this instanceof Video;
    }

    /**
     * Get the element support class name for a type.
     *
     * @param \App\Entity\Type $type The type
     *
     * @return string The class
     */
    public static function getClassByType(Type $type): string
    {
        switch ($type->getId()) {
            case Type::AUDIO:
                return Audio::class;
            case Type::VIDEO:
                return Video::class;
        }

        return self::class;
    }
}
