<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\User;

use App\Entity\Traits\IdTrait;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An element use date.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\User\ElementUseRepository")
 * @ORM\Table(indexes={@ORM\Index(columns={"date"})})
 *
 * @UniqueEntity(fields={"element", "date"})
 */
class ElementUse
{
    use IdTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\UserElement", inversedBy="uses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    /**
     * @ORM\Column(type="date")
     *
     * @Assert\NotNull()
     */
    private $date;

    public function getElement(): ?UserElement
    {
        return $this->element;
    }

    public function setElement(?UserElement $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
