<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\User\ElementSupport;

use App\Entity\Traits\IdTrait;
use App\Entity\User\ElementSupport;
use Doctrine\ORM\Mapping as ORM;

/**
 * An audio user element support.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(name="element_support_audio")
 */
class Audio extends ElementSupport
{
    use IdTrait;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     */
    private $bitrate;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $codec;

    public function getBitrate(): ?int
    {
        return $this->bitrate;
    }

    public function setBitrate(?int $bitrate): self
    {
        $this->bitrate = $bitrate;

        return $this;
    }

    public function getCodec(): ?string
    {
        return $this->codec;
    }

    public function setCodec(?string $codec): self
    {
        $this->codec = $codec;

        return $this;
    }
}
