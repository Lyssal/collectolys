<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\User\ElementSupport;

use App\Entity\Traits\IdTrait;
use App\Entity\User\ElementSupport;
use Doctrine\ORM\Mapping as ORM;

/**
 * A video user element support.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(name="element_support_video")
 */
class Video extends ElementSupport
{
    use IdTrait;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     */
    private $videoBitrate;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     */
    private $audioBitrate;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $videoCodec;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $audioCodec;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $resolutionWidth;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"unsigned"=true})
     */
    private $resolutionHeight;

    /**
     * @ORM\Column(type="boolean")
     */
    private $threeDimensional;

    public function getVideoBitrate(): ?int
    {
        return $this->videoBitrate;
    }

    public function setVideoBitrate(?int $videoBitrate): self
    {
        $this->videoBitrate = $videoBitrate;

        return $this;
    }

    public function getAudioBitrate(): ?int
    {
        return $this->audioBitrate;
    }

    public function setAudioBitrate(?int $audioBitrate): self
    {
        $this->audioBitrate = $audioBitrate;

        return $this;
    }

    public function getVideoCodec(): ?string
    {
        return $this->videoCodec;
    }

    public function setVideoCodec(?string $videoCodec): self
    {
        $this->videoCodec = $videoCodec;

        return $this;
    }

    public function getAudioCodec(): ?string
    {
        return $this->audioCodec;
    }

    public function setAudioCodec(?string $audioCodec): self
    {
        $this->audioCodec = $audioCodec;

        return $this;
    }

    public function getResolutionWidth(): ?int
    {
        return $this->resolutionWidth;
    }

    public function setResolutionWidth(?int $resolutionWidth): self
    {
        $this->resolutionWidth = $resolutionWidth;

        return $this;
    }

    public function getResolutionHeight(): ?int
    {
        return $this->resolutionHeight;
    }

    public function setResolutionHeight(?int $resolutionHeight): self
    {
        $this->resolutionHeight = $resolutionHeight;

        return $this;
    }

    public function getThreeDimensional(): ?bool
    {
        return $this->threeDimensional;
    }

    public function setThreeDimensional(bool $threeDimensional): self
    {
        $this->threeDimensional = $threeDimensional;

        return $this;
    }
}
