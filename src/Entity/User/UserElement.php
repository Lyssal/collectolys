<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\User;

use App\Entity\Element\Element;
use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A user element.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 *
 * @UniqueEntity(fields={"user", "element"})
 */
class UserElement
{
    use IdTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Element\Element")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @Assert\Range(min=1, max=10)
     */
    private $note;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private bool $wish = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $wishDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\User\ElementUse",
     *     mappedBy="element",
     *     cascade={"persist"},
     *     orphanRemoval=true)
     * @ORM\OrderBy({"date": "DESC"})
     *
     * @Assert\Valid()
     */
    private $uses;

    /**
     * @ORM\OneToMany(targetEntity="ElementSupport", mappedBy="element", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @Assert\Valid()
     */
    private $supports;

    public function __construct()
    {
        $this->supports = new ArrayCollection();
        $this->uses = new ArrayCollection();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getWish(): bool
    {
        return $this->wish;
    }

    public function setWish(bool $wish): self
    {
        if ($wish && !$this->wish) {
            $this->wishDate = new \DateTime();
        }

        $this->wish = $wish;

        return $this;
    }

    public function getWishDate(): ?\DateTime
    {
        return $this->wishDate;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|ElementUse[]
     */
    public function getUses(): Collection
    {
        return $this->uses;
    }

    public function addUse(ElementUse $use): self
    {
        if (!$this->uses->contains($use)) {
            $this->uses[] = $use;
            $use->setElement($this);
        }

        return $this;
    }

    public function removeUse(ElementUse $use): self
    {
        if ($this->uses->contains($use)) {
            $this->uses->removeElement($use);
            // set the owning side to null (unless already changed)
            if ($use->getElement() === $this) {
                $use->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementSupport[]
     */
    public function getSupports(): Collection
    {
        return $this->supports;
    }

    public function addSupport(ElementSupport $support): self
    {
        if (!$this->supports->contains($support)) {
            $this->supports[] = $support;
            $support->setElement($this);
        }

        return $this;
    }

    public function removeSupport(ElementSupport $support): self
    {
        if ($this->supports->contains($support)) {
            $this->supports->removeElement($support);
            // set the owning side to null (unless already changed)
            if ($support->getElement() === $this) {
                $support->setElement(null);
            }
        }

        return $this;
    }
}
