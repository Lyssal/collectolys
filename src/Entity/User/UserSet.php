<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\User;

use App\Entity\Traits\IdTrait;
use App\Entity\Type;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class UserSet
{
    use IdTrait;

    /**
     * @ORM\Column(type="string", length=64)
     *
     * @Assert\NotBlank()
     */
    private string $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     */
    private Type $type;

    /**
     * @ORM\Column(type="json")
     */
    private array $elementIds = [];

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setElementIds(array $elementIds): self
    {
        $this->elementIds = $elementIds;

        return $this;
    }

    public function getElementIds(): ?array
    {
        return $this->elementIds;
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }
}
