<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Person;

use App\Entity\File\Image;
use App\Entity\File\ImageableInterface;
use App\Entity\Location;
use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Lyssal\EntityBundle\Entity\RoutableInterface;
use Lyssal\Text\Slug;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * A person.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\Person\PersonRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity(fields={"firstName", "lastName"})
 */
class Person implements ImageableInterface, RoutableInterface
{
    use IdTrait;

    /**
     * The image path.
     *
     * @var string
     */
    const IMAGE_PATH = 'persons';

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $slug;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Image", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Location")
     */
    private $nationalities;

    public function __construct()
    {
        $this->nationalities = new ArrayCollection();
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * {@inheritdoc}
     *
     * @return \App\Entity\Person\Person
     */
    public function setImage(?Image $image): ImageableInterface
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getNationalities(): Collection
    {
        return $this->nationalities;
    }

    public function addNationality(Location $nationality): self
    {
        if (!$this->nationalities->contains($nationality)) {
            $this->nationalities[] = $nationality;
        }

        return $this;
    }

    public function removeNationality(Location $nationality): self
    {
        if ($this->nationalities->contains($nationality)) {
            $this->nationalities->removeElement($nationality);
        }

        return $this;
    }

    /**
     * Get the name.
     */
    public function getName(): string
    {
        return trim($this->firstName.' '.$this->lastName);
    }

    /**
     * @see \App\Entity\Person\Person::getName()
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteProperties()
    {
        return [
            'person_person_show',
            ['person' => $this->slug],
        ];
    }

    /**
     * Init the slug.
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function initSlug()
    {
        $personSlug = new Slug($this->firstName.' '.$this->lastName);
        $personSlug->minify('_', false);

        $this->slug = $personSlug->getText();
    }
}
