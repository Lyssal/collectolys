<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Company;

use App\Entity\File\Image;
use App\Entity\File\ImageableInterface;
use App\Entity\Location;
use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Lyssal\Entity\Model\Breadcrumb\BreadcrumbableInterface;
use Lyssal\EntityBundle\Entity\RoutableInterface;

/**
 * A company.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\Company\CompanyRepository")
 */
class Company implements ImageableInterface, RoutableInterface, BreadcrumbableInterface
{
    use IdTrait;

    /**
     * The image path.
     *
     * @var string
     */
    const IMAGE_PATH = 'companies';

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     *
     * @Gedmo\Slug(fields={"name"}, style="camel", separator="_")
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location")
     */
    private $nationality;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $foundedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Image", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="subsidiaries")
     */
    private $headquarter;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Company\Company", mappedBy="headquarter")
     * @ORM\OrderBy({"foundedAt": "DESC"})
     */
    private $subsidiaries;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Company\Company", inversedBy="nextCompany", cascade={"persist", "remove"})
     */
    private $previousCompany;

    /**
     * @ORM\OneToOne(
     *     targetEntity="App\Entity\Company\Company",
     *     mappedBy="previousCompany",
     *     cascade={"persist", "remove"}
     * )
     */
    private $nextCompany;

    public function __construct()
    {
        $this->subsidiaries = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getNationality(): ?Location
    {
        return $this->nationality;
    }

    public function setNationality(?Location $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getFoundedAt(): ?\DateTimeInterface
    {
        return $this->foundedAt;
    }

    public function setFoundedAt(?\DateTimeInterface $foundedAt): self
    {
        $this->foundedAt = $foundedAt;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * {@inheritdoc}
     *
     * @return \App\Entity\Company\Company
     */
    public function setImage(?Image $image): ImageableInterface
    {
        $this->image = $image;

        return $this;
    }

    public function getHeadquarter(): ?self
    {
        return $this->headquarter;
    }

    public function setHeadquarter(?self $headquarter): self
    {
        $this->headquarter = $headquarter;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getSubsidiaries(): Collection
    {
        return $this->subsidiaries;
    }

    public function addSubsidiary(self $subsidiary): self
    {
        if (!$this->subsidiaries->contains($subsidiary)) {
            $this->subsidiaries[] = $subsidiary;
            $subsidiary->setHeadquarter($this);
        }

        return $this;
    }

    public function removeSubsidiary(self $subsidiary): self
    {
        if ($this->subsidiaries->contains($subsidiary)) {
            $this->subsidiaries->removeElement($subsidiary);
            // set the owning side to null (unless already changed)
            if ($subsidiary->getHeadquarter() === $this) {
                $subsidiary->setHeadquarter(null);
            }
        }

        return $this;
    }

    public function getPreviousCompany(): ?self
    {
        return $this->previousCompany;
    }

    public function setPreviousCompany(?self $previousCompany): self
    {
        $this->previousCompany = $previousCompany;

        return $this;
    }

    public function getNextCompany(): ?self
    {
        return $this->nextCompany;
    }

    public function setNextCompany(?self $nextCompany): self
    {
        $this->nextCompany = $nextCompany;

        // set (or unset) the owning side of the relation if necessary
        $newPreviousCompany = null === $nextCompany ? null : $this;
        if ($newPreviousCompany !== $nextCompany->getPreviousCompany()) {
            $nextCompany->setPreviousCompany($newPreviousCompany);
        }

        return $this;
    }

    /**
     * Get the name.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteProperties()
    {
        return [
            'company_company_show',
            ['company' => $this->slug],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbParent()
    {
        if (null !== $this->nextCompany) {
            return $this->nextCompany;
        }

        if (null !== $this->headquarter) {
            return $this->headquarter;
        }

        return null;
    }
}
