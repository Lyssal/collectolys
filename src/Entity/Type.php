<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity;

use App\Entity\Element\SpecialSet;
use App\Entity\File\IconableInterface;
use App\Entity\Support\Support;
use App\Entity\Traits\IconTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PositionTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Lyssal\Color\Color;
use Lyssal\Entity\Model\Breadcrumb\BreadcrumbableInterface;
use Lyssal\EntityBundle\Entity\RoutableInterface;

/**
 * An element type.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\TypeRepository")
 */
class Type implements IconableInterface, RoutableInterface, BreadcrumbableInterface
{
    use IconTrait;
    use IdTrait;
    use PositionTrait;

    /**
     * The audio ID.
     *
     * @var int
     */
    const AUDIO = 1;

    /**
     * The video ID.
     *
     * @var int
     */
    const VIDEO = 2;

    /**
     * The software ID.
     *
     * @var int
     */
    const SOFTWARE = 3;

    /**
     * The video game ID.
     *
     * @var int
     */
    const VIDEO_GAME = 4;

    /**
     * The book ID.
     *
     * @var int
     */
    const BOOK = 5;

    /**
     * The periodical ID.
     *
     * @var int
     */
    const PERIODICAL = 6;

    /**
     * The cooking recipe ID.
     *
     * @var int
     */
    const COOKING_RECIPE = 7;

    /**
     * The icon path.
     *
     * @var string
     */
    const ICON_PATH = 'types';

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     *
     * @Gedmo\Slug(fields={"name"}, style="camel", separator="_")
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $emoji;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $elementName;

    /**
     * @ORM\Column(type="string", length=7)
     */
    private $color;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Genre", mappedBy="type", orphanRemoval=true)
     * @ORM\OrderBy({"slug": "ASC"})
     */
    private $genres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Element\SpecialSet", mappedBy="type", orphanRemoval=true)
     * @ORM\OrderBy({"position": "ASC", "slug": "ASC"})
     */
    private $specialSets;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Support\Support", mappedBy="types")
     * @ORM\OrderBy({"name": "ASC"})
     */
    private $supports;

    /**
     * @ORM\Column(type="simple_array")
     */
    private $defaultLanguageSources = [];

    public function __construct()
    {
        $this->genres = new ArrayCollection();
        $this->supports = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getEmoji(): ?string
    {
        return $this->emoji;
    }

    public function setEmoji(string $emoji): self
    {
        $this->emoji = $emoji;

        return $this;
    }

    public function getElementName(): ?string
    {
        return $this->elementName;
    }

    public function setElementName(string $elementName): self
    {
        $this->elementName = $elementName;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres[] = $genre;
            $genre->setType($this);
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        if ($this->genres->contains($genre)) {
            $this->genres->removeElement($genre);
            // set the owning side to null (unless already changed)
            if ($genre->getType() === $this) {
                $genre->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|\App\Entity\Element\SpecialSet[]
     */
    public function getSpecialSets(): Collection
    {
        return $this->specialSets;
    }

    public function addSpecialSet(SpecialSet $specialSet): self
    {
        if (!$this->specialSets->contains($specialSet)) {
            $this->specialSets[] = $specialSet;
            $specialSet->setType($this);
        }

        return $this;
    }

    public function removeSpecialSet(SpecialSet $specialSet): self
    {
        if ($this->specialSets->contains($specialSet)) {
            $this->specialSets->removeElement($specialSet);

            // set the owning side to null (unless already changed)
            if ($specialSet->getType() === $this) {
                $specialSet->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Support[]
     */
    public function getSupports(): Collection
    {
        return $this->supports;
    }

    public function addSupport(Support $support): self
    {
        if (!$this->supports->contains($support)) {
            $this->supports[] = $support;
            $support->addType($this);
        }

        return $this;
    }

    public function removeSupport(Support $support): self
    {
        if ($this->supports->contains($support)) {
            $this->supports->removeElement($support);
            $support->removeType($this);
        }

        return $this;
    }

    public function getDefaultLanguageSources(): ?array
    {
        return $this->defaultLanguageSources;
    }

    public function setDefaultLanguageSources(array $defaultLanguageSources): self
    {
        $this->defaultLanguageSources = $defaultLanguageSources;

        return $this;
    }

    /**
     * Get the name.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteProperties()
    {
        return [
            'type_show',
            ['type' => $this->slug],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbParent()
    {
        return null;
    }

    /**
     * Get the light color.
     *
     * @throws \Lyssal\Exception\LyssalException
     *
     * @return string The color
     */
    public function getLightColor(): string
    {
        $color = new Color($this->color);
        $color->lighten(20);

        return $color->getHexadecimalColor();
    }

    /**
     * Get the dark color.
     *
     * @throws \Lyssal\Exception\LyssalException
     *
     * @return string The color
     */
    public function getDarkColor(): string
    {
        $color = new Color($this->color);
        $color->darken(60);

        return $color->getHexadecimalColor();
    }

    /**
     * Get the genres without parent.
     *
     * @return \Doctrine\Common\Collections\Collection|\App\Entity\Genre[] The genres
     */
    public function getGenreParents(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->isNull('parent'))
            ->orderBy(['name' => Criteria::ASC]);

        return $this->genres->matching($criteria);
    }

    /**
     * Determine if we can add platforms with this type.
     *
     * @return bool If we can have platforms
     */
    public function canHavePlatforms(): bool
    {
        return \in_array($this->id, [self::SOFTWARE, self::VIDEO_GAME], true);
    }
}
