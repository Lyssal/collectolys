<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element;

use App\Entity\File\Image;
use App\Entity\File\ImageableInterface;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PositionTrait;
use App\Entity\Type;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Lyssal\Entity\Model\Breadcrumb\BreadcrumbableInterface;
use Lyssal\EntityBundle\Entity\RoutableInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * A special set.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2020 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"slug"})
 * })
 * @UniqueEntity(fields={"name", "type"})
 */
class SpecialSet implements ImageableInterface, RoutableInterface, BreadcrumbableInterface
{
    use IdTrait;
    use PositionTrait;

    /**
     * The image path.
     *
     * @var string
     */
    const IMAGE_PATH = 'special_sets';

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64)
     *
     * @Gedmo\Slug(fields={"name"}, style="camel", separator="_")
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Type", inversedBy="specialSets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Image", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\Column(type="json")
     */
    private $ids = [];

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    /**
     * {@inheritdoc}
     *
     * @return \App\Entity\Element\SpecialSet
     */
    public function setImage(?Image $image): ImageableInterface
    {
        $this->image = $image;

        return $this;
    }

    public function getIds(): ?array
    {
        return $this->ids;
    }

    public function setIds(array $ids): self
    {
        $this->ids = $ids;

        return $this;
    }

    /**
     * Get the name.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteProperties()
    {
        return [
            'element_specialset_show',
            ['specialSet' => $this->slug],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbParent()
    {
        return $this->type;
    }

    /**
     * @return int[]
     */
    public function getFormattedIds(): array
    {
        return array_map(function (string $id) {
            return (int) $id;
        }, $this->ids);
    }
}
