<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element\Type;

use App\Entity\Element\Element;
use App\Entity\Traits\AgeRangeTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A book.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(name="element_type_book")
 */
class Book extends Element
{
    use AgeRangeTrait;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pageCount;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     *
     * @Assert\Isbn()
     */
    private $isbn;

    public function getPageCount(): ?int
    {
        return $this->pageCount;
    }

    public function setPageCount(?int $pageCount): self
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        if (null !== $isbn) {
            $isbn = preg_replace('/[^0-9]+/', '', $isbn);
        }

        $this->isbn = $isbn;

        return $this;
    }

    public function getFormattedIsbn(): ?string
    {
        if (null === $this->isbn) {
            return null;
        }

        if (13 === \mb_strlen($this->isbn)) {
            return mb_substr($this->isbn, 0, 3)
                .'-'.mb_substr($this->isbn, 3, 1)
                .'-'.mb_substr($this->isbn, 4, 2)
                .'-'.mb_substr($this->isbn, 6, 6)
                .'-'.mb_substr($this->isbn, 12, 1)
            ;
        }

        if (10 === \mb_strlen($this->isbn)) {
            return mb_substr($this->isbn, 0, 1)
                .'-'.mb_substr($this->isbn, 1, 4)
                .'-'.mb_substr($this->isbn, 5, 4)
                .'-'.mb_substr($this->isbn, 9, 1)
            ;
        }

        return $this->isbn;
    }
}
