<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element\Type;

use App\Entity\Element\Element;
use App\Entity\Platform;
use App\Entity\Traits\AgeRangeTrait;
use App\Enum\MultiplayerEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Generator;

/**
 * A video game.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(name="element_type_videogame")
 */
class VideoGame extends Element
{
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Platform")
     */
    private $platforms;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cheating;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $multiplayer = [];

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $maxPlayerCount;

    use AgeRangeTrait;

    public function __construct()
    {
        parent::__construct();
        $this->platforms = new ArrayCollection();
    }

    /**
     * @return Collection|Platform[]
     */
    public function getPlatforms(): Collection
    {
        return $this->platforms;
    }

    public function addPlatform(Platform $platform): self
    {
        if (!$this->platforms->contains($platform)) {
            $this->platforms[] = $platform;
        }

        return $this;
    }

    public function removePlatform(Platform $platform): self
    {
        if ($this->platforms->contains($platform)) {
            $this->platforms->removeElement($platform);
        }

        return $this;
    }

    public function getCheating(): ?string
    {
        return $this->cheating;
    }

    public function setCheating(?string $cheating): self
    {
        $this->cheating = $cheating;

        return $this;
    }

    public function getMultiplayer(): ?array
    {
        return $this->multiplayer;
    }

    public function setMultiplayer(?array $multiplayer): self
    {
        $this->multiplayer = $multiplayer;

        return $this;
    }

    public function getMaxPlayerCount(): ?int
    {
        return $this->maxPlayerCount;
    }

    public function setMaxPlayerCount(?int $maxPlayerCount): self
    {
        $this->maxPlayerCount = $maxPlayerCount;

        return $this;
    }

    /**
     * Get the multiplayer types.
     *
     * @return \Generator The multiplayer array
     */
    public function getMultiplayerNames(): Generator
    {
        foreach ($this->multiplayer as $player) {
            yield MultiplayerEnum::VALUES[$player];
        }
    }
}
