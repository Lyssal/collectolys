<?php

namespace App\Entity\Element\Type;

use App\Entity\Element\Element;
use Doctrine\ORM\Mapping as ORM;

/**
 * A sound.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(name="element_type_audio")
 */
class Audio extends Element
{
    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $duration;

    public function getDuration(): ?\DateTimeInterface
    {
        return $this->duration;
    }

    public function setDuration(?\DateTimeInterface $duration): self
    {
        $this->duration = $duration;

        return $this;
    }
}
