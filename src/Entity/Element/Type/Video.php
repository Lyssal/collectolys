<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element\Type;

use App\Entity\Element\Element;
use App\Entity\Traits\AgeRangeTrait;
use App\Enum\VideoTypeEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * A video.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(name="element_type_video")
 */
class Video extends Element
{
    /**
     * @ORM\Column(type="string", length=16, options={"default":VideoTypeEnum::FILM})
     */
    private $videoType = VideoTypeEnum::FILM;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(type="time", nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $color;

    use AgeRangeTrait;

    public function getVideoType(): ?string
    {
        return $this->videoType;
    }

    public function setVideoType(?string $videoType): self
    {
        $this->videoType = $videoType;

        return $this;
    }

    public function getDuration(): ?\DateTimeInterface
    {
        return $this->duration;
    }

    public function setDuration(?\DateTimeInterface $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Determines if the video is a TV series.
     *
     * @return bool If It is a TV series
     */
    public function isTelevisionSeries(): bool
    {
        return VideoTypeEnum::TELEVISION_SERIES === $this->videoType;
    }

    /**
     * Get the duration in seconds.
     *
     * @return int|null The duration in seconds
     */
    public function getDurationInSeconds(): ?int
    {
        if (null === $this->duration) {
            return null;
        }

        return (int) $this->duration->format('Z') + $this->duration->getTimestamp();
    }
}
