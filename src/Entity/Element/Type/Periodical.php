<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element\Type;

use App\Entity\Element\Element;
use App\Entity\Traits\AgeRangeTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * A periodical.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(name="element_type_periodical")
 */
class Periodical extends Element
{
    use AgeRangeTrait;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $pageCount;

    public function getPageCount(): ?int
    {
        return $this->pageCount;
    }

    public function setPageCount(?int $pageCount): self
    {
        $this->pageCount = $pageCount;

        return $this;
    }
}
