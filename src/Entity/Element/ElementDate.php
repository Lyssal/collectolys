<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element;

use App\Entity\Location;
use App\Entity\Traits\IdTrait;
use App\Validator\Constraints as AppAssert;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The relation entity between an element and a date.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\Element\ElementDateRepository")
 * @ORM\Table(indexes={@ORM\Index(columns={"year"})})
 *
 * @AppAssert\CoherentDateYear()
 */
class ElementDate
{
    use IdTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Element\Element", inversedBy="dates")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location")
     * @ORM\JoinColumn(nullable=true)
     */
    private $location;

    /**
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="smallint")
     *
     * @Assert\NotNull()
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $precisions;

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getPrecisions(): ?string
    {
        return $this->precisions;
    }

    public function setPrecisions(?string $precisions): self
    {
        $this->precisions = $precisions;

        return $this;
    }

    public function getDateTime(): DateTimeInterface
    {
        if (null !== $this->date) {
            return $this->date;
        }

        return (new DateTime())->setTimestamp($this->getTimestamp());
    }

    public function getTimestamp(): int
    {
        if (null !== $this->date) {
            return $this->date->getTimestamp();
        }

        $oneYearTimestamp = DateTime::createFromFormat('Y-m-d', '1971-01-01')
            ->setTimezone(new DateTimeZone('UTC'))
            ->setTime(0, 0)
            ->getTimestamp()
        ;

        return ($this->getYear() - 1970) * $oneYearTimestamp;
    }

    /**
     * Get the day.
     *
     * @return int|null The day
     */
    public function getDay(): ?int
    {
        if (null === $this->date) {
            return null;
        }

        return (int) $this->date->format('d');
    }

    /**
     * Get the month.
     *
     * @return int|null The month
     */
    public function getMonth(): ?int
    {
        if (null === $this->date) {
            return null;
        }

        return (int) $this->date->format('m');
    }

    /**
     * Get the element age.
     *
     * @return int|null The age
     */
    public function getAge(): ?int
    {
        $year = $this->getYear();

        if (null === $year) {
            return null;
        }

        $todayYear = (int) date('Y');

        return $todayYear - $year;
    }
}
