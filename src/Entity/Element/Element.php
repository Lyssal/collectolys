<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element;

use App\Entity\Element\Type\Audio;
use App\Entity\Element\Type\Book;
use App\Entity\Element\Type\CookingRecipe;
use App\Entity\Element\Type\Periodical;
use App\Entity\Element\Type\Software;
use App\Entity\Element\Type\Video;
use App\Entity\Element\Type\VideoGame;
use App\Entity\File\Image;
use App\Entity\Genre;
use App\Entity\Keyword;
use App\Entity\Location;
use App\Entity\Traits\IdTrait;
use App\Entity\Type;
use App\Entity\Universe;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Lyssal\Entity\Model\Breadcrumb\BreadcrumbableInterface;
use Lyssal\EntityBundle\Entity\RoutableInterface;
use Lyssal\EntityBundle\Entity\Traits\CreatedAtTrait;
use Lyssal\EntityBundle\Entity\Traits\UpdatedAtTrait;
use Lyssal\Text\Slug;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An element.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity(repositoryClass="App\Doctrine\Repository\Element\ElementRepository")
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"slug"}),
 *     @ORM\Index(columns={"number_in_set"}),
 *     @ORM\Index(columns={"position_in_set"}),
 *     @ORM\Index(columns={"set_special_issue"})
 * })
 * @ORM\InheritanceType("JOINED")
 * @ORM\HasLifecycleCallbacks()
 */
abstract class Element implements RoutableInterface, BreadcrumbableInterface
{
    use CreatedAtTrait;
    use IdTrait;
    use UpdatedAtTrait;

    /**
     * The images path.
     *
     * @var string
     */
    const IMAGES_PATH = 'elements';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $type;

    /**
     * @var \Doctrine\Common\Collections\Collection|\App\Entity\Element\ElementName[]
     *
     * @ORM\OneToMany(targetEntity="ElementName", mappedBy="element", fetch="EAGER", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position":"ASC"})
     *
     * @Assert\Valid()
     */
    protected $names;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $slug;

    /**
     * @ORM\Column(name="series", type="boolean", options={"default": false})
     */
    private $set = false;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $setSpecialIssue = false;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $setAggregated = false;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $setDisplayInElementTitle = false;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $setElementName;

    /**
     * @var \App\Entity\Element\Element|null
     *
     * @ORM\ManyToOne(targetEntity="Element")
     */
    private $elementSet;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $numberInSet;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $positionInSet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Image", cascade={"persist", "remove"})
     *
     * @Assert\Valid()
     */
    protected $illustrationRecto;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Image", cascade={"persist", "remove"})
     *
     * @Assert\Valid()
     */
    protected $illustrationVerso;

    /**
     * @ORM\ManyToMany(targetEntity="Element", inversedBy="children")
     * @ORM\JoinTable(name="element_parent")
     */
    protected $parents;

    /**
     * @ORM\ManyToMany(targetEntity="Element", mappedBy="parents")
     * @ORM\OrderBy({"positionInSet": "ASC"})
     */
    protected $children;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Element\Element", inversedBy="referencedElements")
     * @ORM\JoinTable(name="element_reference")
     */
    private $references;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Element\Element", mappedBy="references")
     */
    private $referencedElements;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Element\Element", inversedBy="nextElements")
     */
    private $previousElement;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Element\Element", mappedBy="previousElement")
     * @ORM\OrderBy({"positionInSet": "ASC"})
     */
    private $nextElements;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Element\Element", inversedBy="otherSimilarElements")
     * @ORM\JoinTable(name="element_similar")
     */
    private $similarElements;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Element\Element", mappedBy="similarElements")
     */
    private $otherSimilarElements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Element\SubElement", mappedBy="element", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position":"ASC"})
     *
     * @Assert\Valid()
     */
    private $subElements;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $orderedSubElements = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Universe")
     */
    protected $universes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Genre")
     */
    private $genres;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Keyword")
     */
    private $keywords;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Location")
     */
    protected $origins;

    /**
     * @var \Doctrine\Common\Collections\Collection|\App\Entity\Element\ElementDate[]
     *
     * @ORM\OneToMany(targetEntity="ElementDate", mappedBy="element", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"year": "ASC", "date": "ASC"})
     *
     * @Assert\Valid()
     */
    protected $dates;

    /**
     * @var \Doctrine\Common\Collections\Collection|\App\Entity\Element\ElementCompany[]
     *
     * @ORM\OneToMany(targetEntity="ElementCompany", mappedBy="element", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position": "ASC"})
     *
     * @Assert\Valid()
     */
    protected $companies;

    /**
     * @var \Doctrine\Common\Collections\Collection|\App\Entity\Element\ElementPerson[]
     *
     * @ORM\OneToMany(targetEntity="ElementPerson", mappedBy="element", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position": "ASC"})
     *
     * @Assert\Valid()
     */
    protected $people;

    /**
     * @ORM\OneToMany(targetEntity="ElementPrice", mappedBy="element", cascade={"persist"}, orphanRemoval=true)
     *
     * @Assert\Valid()
     */
    protected $prices;

    /**
     * @ORM\OneToMany(targetEntity="ElementIllustration", mappedBy="element", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"position": "ASC"})
     *
     * @Assert\Valid()
     */
    protected $illustrations;

    public function __construct()
    {
        $this->names = new ArrayCollection();
        $this->parents = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->references = new ArrayCollection();
        $this->referencedElements = new ArrayCollection();
        $this->universes = new ArrayCollection();
        $this->genres = new ArrayCollection();
        $this->keywords = new ArrayCollection();
        $this->origins = new ArrayCollection();
        $this->dates = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->people = new ArrayCollection();
        $this->illustrations = new ArrayCollection();
        $this->subElements = new ArrayCollection();
        $this->nextElements = new ArrayCollection();
        $this->similarElements = new ArrayCollection();
        $this->otherSimilarElements = new ArrayCollection();
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|ElementName[]
     */
    public function getNames(): Collection
    {
        return $this->names;
    }

    public function addName(ElementName $name): self
    {
        if (!$this->names->contains($name)) {
            $this->names[] = $name;
            $name->setElement($this);
        }

        return $this;
    }

    public function removeName(ElementName $name): self
    {
        if ($this->names->contains($name)) {
            $this->names->removeElement($name);
            // set the owning side to null (unless already changed)
            if ($name->getElement() === $this) {
                $name->setElement(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function isSet(): ?bool
    {
        return $this->set;
    }

    public function setSet(bool $set): self
    {
        $this->set = $set;

        return $this;
    }

    public function isSetSpecialIssue(): ?bool
    {
        return $this->setSpecialIssue;
    }

    public function setSetSpecialIssue(bool $setSpecialIssue): self
    {
        $this->setSpecialIssue = $setSpecialIssue;

        return $this;
    }

    public function isSetAggregated(): ?bool
    {
        return $this->setAggregated;
    }

    public function setSetAggregated(bool $setAggregated): self
    {
        $this->setAggregated = $setAggregated;

        return $this;
    }

    public function isSetDisplayInElementTitle(): ?bool
    {
        return $this->setDisplayInElementTitle;
    }

    public function setSetDisplayInElementTitle(bool $setDisplayInElementTitle): self
    {
        $this->setDisplayInElementTitle = $setDisplayInElementTitle;

        return $this;
    }

    public function getSetElementName(): ?string
    {
        return $this->setElementName;
    }

    public function setSetElementName(?string $elementName): self
    {
        $this->setElementName = $elementName;

        return $this;
    }

    public function getElementSet(): ?self
    {
        return $this->elementSet;
    }

    public function setElementSet(?self $elementSet): self
    {
        $this->elementSet = $elementSet;

        return $this;
    }

    public function getNumberInSet(): ?string
    {
        return $this->numberInSet;
    }

    public function setNumberInSet(?string $numberInSet): self
    {
        $this->numberInSet = $numberInSet;

        return $this;
    }

    public function getPositionInSet(): ?int
    {
        return $this->positionInSet;
    }

    public function setPositionInSet(?int $positionInSet): self
    {
        $this->positionInSet = $positionInSet;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIllustrationRecto(): ?Image
    {
        return $this->illustrationRecto;
    }

    public function setIllustrationRecto(?Image $illustrationRecto): self
    {
        $this->illustrationRecto = $illustrationRecto;

        return $this;
    }

    public function getIllustrationVerso(): ?Image
    {
        return $this->illustrationVerso;
    }

    public function setIllustrationVerso(?Image $illustrationVerso): self
    {
        $this->illustrationVerso = $illustrationVerso;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getParents(): Collection
    {
        return $this->parents;
    }

    public function addParent(self $parent): self
    {
        if (!$this->parents->contains($parent)) {
            $this->parents[] = $parent;
        }

        return $this;
    }

    public function removeParent(self $parent): self
    {
        if ($this->parents->contains($parent)) {
            $this->parents->removeElement($parent);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->addParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            $child->removeParent($this);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getReferences(): Collection
    {
        return $this->references;
    }

    public function addReference(self $reference): self
    {
        if (!$this->references->contains($reference)) {
            $this->references[] = $reference;
        }

        return $this;
    }

    public function removeReference(self $reference): self
    {
        if ($this->references->contains($reference)) {
            $this->references->removeElement($reference);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getReferencedElements(): Collection
    {
        return $this->referencedElements;
    }

    public function addReferencedElement(self $referencedElement): self
    {
        if (!$this->referencedElements->contains($referencedElement)) {
            $this->referencedElements[] = $referencedElement;
            $referencedElement->addReference($this);
        }

        return $this;
    }

    public function removeReferencedElement(self $referencedElement): self
    {
        if ($this->referencedElements->contains($referencedElement)) {
            $this->referencedElements->removeElement($referencedElement);
            $referencedElement->removeReference($this);
        }

        return $this;
    }

    public function getPreviousElement(): ?self
    {
        return $this->previousElement;
    }

    public function setPreviousElement(?self $previousElement): self
    {
        $this->previousElement = $previousElement;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getNextElements(): Collection
    {
        return $this->nextElements;
    }

    public function addNextElement(self $nextElement): self
    {
        if (!$this->nextElements->contains($nextElement)) {
            $this->nextElements[] = $nextElement;
            $nextElement->setPreviousElement($this);
        }

        return $this;
    }

    public function removeNextElement(self $nextElement): self
    {
        if ($this->nextElements->contains($nextElement)) {
            $this->nextElements->removeElement($nextElement);
            // set the owning side to null (unless already changed)
            if ($nextElement->getPreviousElement() === $this) {
                $nextElement->setPreviousElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getSimilarElements(): Collection
    {
        return $this->similarElements;
    }

    public function addSimilarElement(self $similarElement): self
    {
        if (!$this->similarElements->contains($similarElement)) {
            $this->similarElements[] = $similarElement;
        }

        return $this;
    }

    public function removeSimilarElement(self $similarElement): self
    {
        if ($this->similarElements->contains($similarElement)) {
            $this->similarElements->removeElement($similarElement);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getOtherSimilarElements(): Collection
    {
        return $this->otherSimilarElements;
    }

    public function addOtherSimilarElement(self $otherSimilarElement): self
    {
        if (!$this->otherSimilarElements->contains($otherSimilarElement)) {
            $this->otherSimilarElements[] = $otherSimilarElement;
            $otherSimilarElement->addSimilarElement($this);
        }

        return $this;
    }

    public function removeOtherSimilarElement(self $otherSimilarElement): self
    {
        if ($this->otherSimilarElements->contains($otherSimilarElement)) {
            $this->otherSimilarElements->removeElement($otherSimilarElement);
            $otherSimilarElement->removeSimilarElement($this);
        }

        return $this;
    }

    /**
     * @return Collection|SubElement[]
     */
    public function getSubElements(): Collection
    {
        return $this->subElements;
    }

    public function addSubElement(SubElement $subElement): self
    {
        if (!$this->subElements->contains($subElement)) {
            $this->subElements[] = $subElement;
            $subElement->setElement($this);
        }

        return $this;
    }

    public function removeSubElement(SubElement $subElement): self
    {
        if ($this->subElements->contains($subElement) && -1 === $subElement->getLevel()) {
            $this->subElements->removeElement($subElement);
            // set the owning side to null (unless already changed)
            if ($subElement->getElement() === $this) {
                $subElement->setElement(null);
            }
        }

        return $this;
    }

    public function getOrderedSubElements(): ?bool
    {
        return $this->orderedSubElements;
    }

    public function setOrderedSubElements(bool $orderedSubElements): self
    {
        $this->orderedSubElements = $orderedSubElements;

        return $this;
    }

    /**
     * @return Collection|Universe[]
     */
    public function getUniverses(): Collection
    {
        return $this->universes;
    }

    public function addUniverse(Universe $universe): self
    {
        if (!$this->universes->contains($universe)) {
            $this->universes[] = $universe;
        }

        return $this;
    }

    public function removeUniverse(Universe $universe): self
    {
        if ($this->universes->contains($universe)) {
            $this->universes->removeElement($universe);
        }

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        if ($this->genres->contains($genre)) {
            $this->genres->removeElement($genre);
        }

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getKeywords(): Collection
    {
        return $this->keywords;
    }

    public function addKeyword(Keyword $keyword): self
    {
        if (!$this->keywords->contains($keyword)) {
            $this->keywords[] = $keyword;
        }

        return $this;
    }

    public function removeKeyword(Keyword $keyword): self
    {
        if ($this->keywords->contains($keyword)) {
            $this->keywords->removeElement($keyword);
        }

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getOrigins(): Collection
    {
        return $this->origins;
    }

    public function addOrigin(Location $origin): self
    {
        if (!$this->origins->contains($origin)) {
            $this->origins[] = $origin;
        }

        return $this;
    }

    public function removeOrigin(Location $origin): self
    {
        if ($this->origins->contains($origin)) {
            $this->origins->removeElement($origin);
        }

        return $this;
    }

    /**
     * @return Collection|ElementDate[]
     */
    public function getDates(): Collection
    {
        return $this->dates;
    }

    public function addDate(ElementDate $elementDate): self
    {
        if (!$this->dates->contains($elementDate)) {
            $this->dates[] = $elementDate;
            $elementDate->setElement($this);
        }

        return $this;
    }

    public function removeDate(ElementDate $elementDate): self
    {
        if ($this->dates->contains($elementDate)) {
            $this->dates->removeElement($elementDate);
            // set the owning side to null (unless already changed)
            if ($elementDate->getElement() === $this) {
                $elementDate->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementCompany[]
     */
    public function getCompanies(): Collection
    {
        return $this->companies;
    }

    public function addCompany(ElementCompany $elementCompany): self
    {
        if (!$this->companies->contains($elementCompany)) {
            $this->companies[] = $elementCompany;
            $elementCompany->setElement($this);
        }

        return $this;
    }

    public function removeCompany(ElementCompany $elementCompany): self
    {
        if ($this->companies->contains($elementCompany)) {
            $this->companies->removeElement($elementCompany);
            // set the owning side to null (unless already changed)
            if ($elementCompany->getElement() === $this) {
                $elementCompany->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementPerson[]
     */
    public function getPeople(): Collection
    {
        return $this->people;
    }

    public function addPerson(ElementPerson $elementPerson): self
    {
        if (!$this->people->contains($elementPerson)) {
            $this->people[] = $elementPerson;
            $elementPerson->setElement($this);
        }

        return $this;
    }

    public function removePerson(ElementPerson $elementPerson): self
    {
        if ($this->people->contains($elementPerson)) {
            $this->people->removeElement($elementPerson);
            // set the owning side to null (unless already changed)
            if ($elementPerson->getElement() === $this) {
                $elementPerson->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementPrice[]
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(ElementPrice $elementPrice): self
    {
        if (!$this->prices->contains($elementPrice)) {
            $this->prices[] = $elementPrice;
            $elementPrice->setElement($this);
        }

        return $this;
    }

    public function removePrice(ElementPrice $elementPrice): self
    {
        if ($this->prices->contains($elementPrice)) {
            $this->prices->removeElement($elementPrice);
            // set the owning side to null (unless already changed)
            if ($elementPrice->getElement() === $this) {
                $elementPrice->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementIllustration[]
     */
    public function getIllustrations(): Collection
    {
        return $this->illustrations;
    }

    public function addIllustration(ElementIllustration $illustration): self
    {
        if (!$this->illustrations->contains($illustration)) {
            $this->illustrations[] = $illustration;
            $illustration->setElement($this);
        }

        return $this;
    }

    public function removeIllustration(ElementIllustration $illustration): self
    {
        if ($this->illustrations->contains($illustration)) {
            $this->illustrations->removeElement($illustration);
            $illustration->getImage()->deleteFile();
            // set the owning side to null (unless already changed)
            if ($illustration->getElement() === $this) {
                $illustration->setElement(null);
            }
        }

        return $this;
    }

    /**
     * Init the slug.
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function initSlug()
    {
        $name = (string) $this;
        $slug = new Slug($name);

        $slug->minify('_', false);

        $this->slug = mb_substr($slug->getText(), 0, 128);
        $this->initUpdatedAt();
    }

    /**
     * Init the element name slug.
     *
     * @ORM\PreUpdate()
     */
    public function initNameSlugs()
    {
        foreach ($this->names as $elementName) {
            $elementName->initSlug();
        }
    }

    /**
     * Get the complete name of the element.
     *
     * @return string The name
     */
    public function __toString(): string
    {
        $name = '';

        if (null !== $this->elementSet && $this->elementSet->isSetDisplayInElementTitle()) {
            $name = $this->elementSet;

            if (null !== $this->numberInSet) {
                if (null !== $this->elementSet->getSetElementName()) {
                    $name .= ' : '.$this->elementSet->getSetElementName();
                }
            }

            if ($this->setSpecialIssue) {
                $name .= ' ✨';
            }

            if (null !== $this->numberInSet) {
                $name .= ' '.$this->numberInSet;
            }

            if (\count($this->names) > 0) {
                $name .= ' : ';
            }
        }

        if (\count($this->names) > 0) {
            $name .= $this->names->first();
        }

        return $name;
    }

    /**
     * Return if the element is the same.
     *
     * @param \App\Entity\Element\Element $element The element
     *
     * @return bool If same
     */
    public function equals(self $element): bool
    {
        return $this->id === $element->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getRouteProperties()
    {
        if ($this->isSet()) {
            return [
                'element_set_show',
                [
                    'set' => $this->id,
                    'slug' => $this->slug,
                ],
            ];
        }

        return [
            'element_element_show',
            [
                'element' => $this->id,
                'slug' => $this->slug,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbParent()
    {
        if (null !== $this->elementSet) {
            return $this->elementSet;
        }

        if (1 === $this->parents->count()) {
            return $this->parents->first();
        }

        return $this->type;
    }

    public function getFirstSubname(): ?string
    {
        if (\count($this->names) > 0) {
            return $this->names->first()->getSubname();
        }

        return null;
    }

    /**
     * Determine if the element is an audio.
     *
     * @return bool If audio
     */
    public function isAudio(): bool
    {
        return $this instanceof Audio;
    }

    /**
     * Determine if the element is a video.
     *
     * @return bool If video
     */
    public function isVideo(): bool
    {
        return $this instanceof Video;
    }

    /**
     * Determine if the element is a software.
     *
     * @return bool If software
     */
    public function isSoftWare(): bool
    {
        return $this instanceof Software;
    }

    /**
     * Determine if the element is a video game.
     *
     * @return bool If video game
     */
    public function isVideoGame(): bool
    {
        return $this instanceof VideoGame;
    }

    /**
     * Determine if the element is a book.
     *
     * @return bool If book
     */
    public function isBook(): bool
    {
        return $this instanceof Book;
    }

    /**
     * Determine if the element is a periodical.
     *
     * @return bool If periodical
     */
    public function isPeriodical(): bool
    {
        return $this instanceof Periodical;
    }

    /**
     * Determine if the element is a cooking recipe.
     *
     * @return bool If cooking recipe
     */
    public function isCookingRecipe(): bool
    {
        return $this instanceof CookingRecipe;
    }

    /**
     * Get the main illustration.
     *
     * @return \App\Entity\File\Image|null The illustration
     */
    public function getMainIllustration(): ?Image
    {
        if (null !== $this->illustrationRecto) {
            return $this->illustrationRecto;
        }

        if (\count($this->illustrations) > 0) {
            return $this->illustrations->first()->getImage();
        }

        return null;
    }

    /**
     * Get the ordered children.
     *
     * @return \App\Entity\Element\Element[] The elements
     */
    public function getOrderedChildren(): array
    {
        $children = $this->children->toArray();

        usort($children, function (self $element1, self $element2) {
            $elementDate1 = $element1->getOldestElementDate();
            $elementDate2 = $element2->getOldestElementDate();

            if (null === $elementDate1 && null === $elementDate2) {
                return 0;
            }

            if (null === $elementDate1) {
                return 1;
            }

            if (null === $elementDate2) {
                return -1;
            }

            return $this->compareElementDates($elementDate1, $elementDate2);
        });

        return $children;
    }

    /**
     * Get the first level of submenus.
     *
     * @return \Doctrine\Common\Collections\Collection The sub-elements
     */
    public function getFirstSubElements(): Collection
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->isNull('parent'));

        return $this->subElements->matching($criteria);
    }

    public function addFirstSubElement(SubElement $subElement): self
    {
        return $this->addSubElement($subElement);
    }

    public function removeFirstSubElement(SubElement $subElement): self
    {
        return $this->removeSubElement($subElement);
    }

    /**
     * Determines if the element has a genre.
     *
     * @param int $genreId The genre ID
     *
     * @return bool If It has the genre ID
     */
    public function hasGenreId(int $genreId): bool
    {
        foreach ($this->genres as $genre) {
            if ($genreId === $genre->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the ordered element dates.
     *
     * @return \App\Entity\Element\ElementDate[] The element dates
     */
    public function getOrderedElementDates(): array
    {
        $elementDates = $this->dates->toArray();

        usort($elementDates, function (ElementDate $elementDate1, ElementDate $elementDate2) {
            return $this->compareElementDates($elementDate1, $elementDate2);
        });

        return $elementDates;
    }

    /**
     * Get the element date by day and month.
     *
     * @param int $day   The day
     * @param int $month The month
     *
     * @return \App\Entity\Element\ElementDate|null The element date
     */
    public function getElementDateByDayAndMonth(int $day, int $month): ?ElementDate
    {
        foreach ($this->dates as $elementDate) {
            if ($elementDate->getDay() === $day && $elementDate->getMonth() === $month) {
                return $elementDate;
            }
        }

        return null;
    }

    /**
     * The comparison function to compare element dates.
     *
     * @param \App\Entity\Element\ElementDate $elementDate1 The first element date
     * @param \App\Entity\Element\ElementDate $elementDate2 The second element date
     *
     * @return int The comparison result
     */
    private function compareElementDates(ElementDate $elementDate1, ElementDate $elementDate2): int
    {
        $year1 = $elementDate1->getYear();
        $year2 = $elementDate2->getYear();

        if ($year1 !== $year2) {
            return $year1 <=> $year2;
        }

        if ($elementDate1->getDate() === $elementDate2->getDate()) {
            return 0;
        }

        if (null === $elementDate1->getDate()) {
            return 1;
        }

        if (null === $elementDate2->getDate()) {
            return -1;
        }

        return $elementDate1->getDate() <=> $elementDate2->getDate();
    }

    /**
     * Get the oldest element date.
     *
     * @return \DateTimeInterface|null The oldest date
     */
    public function getOldestElementDate(): ?ElementDate
    {
        $elementDate = $date = null;

        foreach ($this->dates as $currentElementDate) {
            if ($currentElementDate->getYear() < 0) {
                return $currentElementDate;
            }

            $currentDate = $currentElementDate->getDate() ?? new DateTime($currentElementDate->getYear().'-12-31');

            if (null === $date || $currentDate < $date) {
                $date = $currentDate;
                $elementDate = $currentElementDate;
            }
        }

        return $elementDate;
    }

    /**
     * Get the element year.
     *
     * @return int|null The year
     */
    public function getYear(): ?int
    {
        $oldestElementDate = $this->getOldestElementDate();

        if (null === $oldestElementDate) {
            return null;
        }

        return $oldestElementDate->getYear();
    }

    public function getHighlightedCompanies(): Collection
    {
        return $this->companies->filter(function (ElementCompany $elementCompany) {
            return $elementCompany->isHighlighted();
        });
    }

    public function getHighlightedPeople(): Collection
    {
        return $this->people->filter(function (ElementPerson $elementPerson) {
            return $elementPerson->isHighlighted();
        });
    }

    /**
     * Get the ElementCompany entities grouped by CompanyRole.
     *
     * @return array The element companies
     */
    public function getCompaniesByRole(): array
    {
        $companiesByRole = [];
        $companies = !$this->companies->isEmpty() || null === $this->elementSet ? $this->companies : $this->elementSet->getCompanies();

        foreach ($companies as $elementCompany) {
            $roleId = $elementCompany->getRole()->getId();

            if (!\array_key_exists($roleId, $companiesByRole)) {
                $companiesByRole[$roleId] = [
                    'role' => $elementCompany->getRole(),
                    'element_companies' => [],
                ];
            }

            $companiesByRole[$roleId]['element_companies'][] = $elementCompany;
        }

        return $companiesByRole;
    }

    /**
     * Get the ElementPerson entities grouped by PersonRole.
     *
     * @return array The element people
     */
    public function getPeopleByRole(): array
    {
        $peopleByRole = [];
        $people = !$this->people->isEmpty() || null === $this->elementSet ? $this->people : $this->elementSet->getPeople();

        foreach ($people as $elementPerson) {
            $roleId = $elementPerson->getRole()->getId();

            if (!\array_key_exists($roleId, $peopleByRole)) {
                $peopleByRole[$roleId] = [
                    'role' => $elementPerson->getRole(),
                    'element_people' => [],
                ];
            }

            $peopleByRole[$roleId]['element_people'][] = $elementPerson;
        }

        return $peopleByRole;
    }

    /**
     * Determine if the element has at least one platform.
     *
     * @return bool If a platform is found
     */
    public function hasPlatform(): bool
    {
        return ($this->isSoftWare() || $this->isVideoGame()) && \count($this->getPlatforms()) > 0;
    }

    /**
     * Get the element class name for a type.
     *
     * @param \App\Entity\Type $type The type
     *
     * @return string The class
     */
    public static function getClassByType(Type $type): string
    {
        switch ($type->getId()) {
            case Type::AUDIO:
                return Audio::class;
            case Type::VIDEO:
                return Video::class;
            case Type::SOFTWARE:
                return Software::class;
            case Type::VIDEO_GAME:
                return VideoGame::class;
            case Type::BOOK:
                return Book::class;
            case Type::PERIODICAL:
                return Periodical::class;
            case Type::COOKING_RECIPE:
                return CookingRecipe::class;
        }

        return self::class;
    }
}
