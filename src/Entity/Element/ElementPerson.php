<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element;

use App\Entity\Person\Person;
use App\Entity\Person\PersonRole;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PositionTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The relation entity between an element and a person.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 *
 * @UniqueEntity(fields={"element", "person", "role"})
 */
class ElementPerson
{
    use IdTrait;

    use PositionTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Element\Element", inversedBy="people")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    /**
     * @var \App\Entity\Person\Person
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Person\Person", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotNull()
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person\PersonRole")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotNull()
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $details;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    private $highlighted = false;

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getRole(): ?PersonRole
    {
        return $this->role;
    }

    public function setRole(?PersonRole $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(?string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function isHighlighted(): bool
    {
        return $this->highlighted;
    }

    public function setHighlighted(bool $highlighted): self
    {
        $this->highlighted = $highlighted;

        return $this;
    }

    /**
     * Get the first name.
     *
     * @return string|null The first name
     */
    public function getFirstName(): ?string
    {
        return null !== $this->person ? $this->person->getFirstName() : null;
    }

    /**
     * Get the last name.
     *
     * @return string|null The last name
     */
    public function getLastName(): ?string
    {
        return null !== $this->person ? $this->person->getLastName() : null;
    }
}
