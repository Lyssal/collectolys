<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element;

use App\Entity\Language;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PositionTrait;
use Doctrine\ORM\Mapping as ORM;
use Lyssal\Text\Slug;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An element name.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 * @ORM\Table(indexes={@ORM\Index(columns={"position"})})
 * @ORM\HasLifecycleCallbacks()
 */
class ElementName
{
    use IdTrait;
    use PositionTrait;

    /**
     * @var \App\Entity\Element\Element
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Element\Element", inversedBy="names")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    /**
     * @ORM\Column(type="string", length=128)
     *
     * @Assert\NotNull()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $subname;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     * @ORM\JoinColumn(nullable=true)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $precisions;

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        // To update also the element if only names have changed
        if (null !== $this->element) {
            $this->initSlug();
        }

        return $this;
    }

    public function getSubname(): ?string
    {
        return $this->subname;
    }

    public function setSubname(string $subname): self
    {
        $this->subname = $subname;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getPrecisions(): ?string
    {
        return $this->precisions;
    }

    public function setPrecisions(?string $precisions): self
    {
        $this->precisions = $precisions;

        return $this;
    }

    /**
     * Init the slug.
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function initSlug()
    {
        $name = '';

        if (null !== $this->element->getElementSet() && $this->element->getElementSet()->isSetDisplayInElementTitle()) {
            $name = $this->element->getElementSet();

            if (null !== $this->element->getNumberInSet()) {
                if (null !== $this->element->getElementSet()->getSetElementName()) {
                    $name .= ' : '.$this->element->getElementSet()->getSetElementName();
                }

                $name .= ' '.$this->element->getNumberInSet();
            }

            $name .= ' : ';
        }

        $name .= $this->name;
        $slug = new Slug($name);

        $slug->minify('_', false);

        $this->slug = mb_substr($slug->getText(), 0, 128);

        $this->element->initSlug();
    }

    /**
     * Get the element name.
     *
     * @return string The name
     */
    public function __toString(): string
    {
        return $this->name;
    }
}
