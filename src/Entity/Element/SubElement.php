<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Entity\Element;

use App\Entity\Traits\IdTrait;
use App\Entity\Traits\PositionTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A sub-element.
 *
 * @category Entity
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @ORM\Entity()
 */
class SubElement
{
    use IdTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Element\Element", inversedBy="subElements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull()
     * @Assert\Length(max=255)
     */
    private $name;

    /**
     * @var \App\Entity\Element\SubElement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Element\SubElement", inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Element\SubElement", mappedBy="parent", cascade={"persist"})
     * @ORM\OrderBy({"position":"ASC"})
     *
     * @Assert\Valid()
     */
    private $children;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $orderedChildren = true;

    use PositionTrait;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        if (null === $this->parent && -2 === $this->getLevel()) {
            $this->element = null;
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
            $child->setElement($this->element);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getOrderedChildren(): ?bool
    {
        return $this->orderedChildren;
    }

    public function setOrderedChildren(bool $orderedChildren): self
    {
        $this->orderedChildren = $orderedChildren;

        return $this;
    }

    /**
     * Get the name.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    /**
     * Get the element level.
     *
     * @return int The level
     */
    public function getLevel(): int
    {
        if (null === $this->parent) {
            return -1;
        }

        return $this->parent->getLevel() - 1;
    }
}
