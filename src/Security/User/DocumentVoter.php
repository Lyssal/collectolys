<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Security\User;

use App\Entity\User\Document;
use App\Entity\User\User;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * The DocumentVoter voter.
 *
 * @category Security
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class DocumentVoter extends Voter
{
    /**
     * The showing access.
     *
     * @var string
     */
    const SHOW = 'document_show';

    /**
     * The adding access.
     *
     * @var string
     */
    const ADD = 'document_add';

    /**
     * The edition access.
     *
     * @var string
     */
    const EDIT = 'document_edit';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        return
            (
                \in_array(
                    $attribute,
                    [
                        self::SHOW,
                        self::EDIT,
                    ],
                    true
                )
                && $subject instanceof Document
            )
            || self::ADD === $attribute
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (self::ADD === $attribute) {
            return true;
        }

        switch ($attribute) {
            case self::SHOW:
                return $this->canShow($user, $subject);
            case self::EDIT:
                return $this->canEdit($user, $subject);
        }

        throw new LogicException('This code should not be reached!');
    }

    /**
     * Determine if the user can add the document.
     *
     * @param \App\Entity\User\User $user The user
     *
     * @return bool If the user can add the document
     */
    private function canShow(User $user, Document $document): bool
    {
        return $this->canEdit($user, $document);
    }

    /**
     * Determine if the user can edit the document.
     *
     * @param \App\Entity\User\User $user The user
     *
     * @return bool If the user can edit the document
     */
    private function canEdit(User $user, Document $document): bool
    {
        return $user->equals($document->getUser());
    }
}
