<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Security\User;

use App\Entity\User\User;
use App\Entity\User\UserElement;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * The UserElement voter.
 *
 * @category Security
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class UserElementVoter extends Voter
{
    /**
     * The adding access.
     *
     * @var string
     */
    const ADD = 'user_element_add';

    /**
     * The edition access.
     *
     * @var string
     */
    const EDIT = 'user_element_edit';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        return
            self::ADD === $attribute
            || (self::EDIT === $attribute && $subject instanceof UserElement)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ADD:
                return $this->canAdd($user);
            case self::EDIT:
                return $this->canEdit($user);
        }

        throw new LogicException('This code should not be reached!');
    }

    /**
     * Determine if the user can add the element.
     *
     * @param \App\Entity\User\User $user The user
     *
     * @return bool If the user can add the element
     */
    private function canAdd(User $user): bool
    {
        return $this->canEdit($user);
    }

    /**
     * Determine if the user can edit the element.
     *
     * @param \App\Entity\User\User $user The user
     *
     * @return bool If the user can edit the element
     */
    private function canEdit(User $user): bool
    {
        return $user->isAdmin();
    }
}
