<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Security;

use App\Entity\Currency;
use App\Entity\Location;
use App\Entity\User\User;
use Symfony\Component\Security\Core\Security;

class UserInformations
{
    /**
     * The security service.
     *
     * @var \Symfony\Component\Security\Core\Security
     */
    private $security;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Security\Core\Security $security The security service
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getCurrency(): ?Currency
    {
        $location = $this->getLocation();

        return null !== $location ? $location->getCurrency() : null;
    }

    public function getLocation(): ?Location
    {
        $user = $this->getUser();

        return null !== $user ? $user->getLocation() : null;
    }

    public function getUser(): ?User
    {
        return $this->security->getUser();
    }
}
