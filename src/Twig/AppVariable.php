<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Twig;

use App\Doctrine\Manager\TypeManager;
use Symfony\Bridge\Twig\AppVariable as TwigAppVariable;

/**
 * {@inheritdoc}
 *
 * @category Twig
 */
class AppVariable extends TwigAppVariable
{
    /**
     * @var \App\Doctrine\Manager\TypeManager
     */
    private $typeManager;

    /**
     * Set the Type manager.
     *
     * @param \App\Doctrine\Manager\TypeManager $typeManager The type manager
     */
    public function setTypeManager(TypeManager $typeManager): void
    {
        $this->typeManager = $typeManager;
    }

    /**
     * Get the types.
     *
     * @return \App\Entity\Type[] The type entities
     */
    public function getTypes(): array
    {
        return $this->typeManager->findAll();
    }
}
