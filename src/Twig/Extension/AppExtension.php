<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Twig\Extension;

use App\Doctrine\Manager\PlatformManager;
use App\Entity\Type;
use Lyssal\Text\Html;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * The general Twig extension for the application.
 *
 * @category Twig
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class AppExtension extends AbstractExtension
{
    /**
     * The platform manager.
     *
     * @var \App\Doctrine\Manager\PlatformManager
     */
    private $platformManager;

    /**
     * Constructor.
     *
     * @param \App\Doctrine\Manager\PlatformManager $platformManager The platform manager
     */
    public function __construct(PlatformManager $platformManager)
    {
        $this->platformManager = $platformManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('format_html', [$this, 'formatHtml'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('platforms_by_type', [$this, 'getPlatformsByType']),
        ];
    }

    public function formatHtml(string $html): string
    {
        return (new Html($html))->addTargetBlankToLinks()->getText();
    }

    /**
     * @see \App\Doctrine\Manager\PlatformManager::findByType()
     */
    public function getPlatformsByType(Type $type): array
    {
        return $this->platformManager->findByType($type);
    }
}
