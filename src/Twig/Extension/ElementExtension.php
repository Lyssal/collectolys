<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Twig\Extension;

use App\Doctrine\Decorator\Element\ElementDecorator;
use App\Element\ElementSearcher;
use App\Entity\Genre;
use App\Entity\User\ElementUse;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Symfony\Component\Security\Core\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * The Twig extension for elements.
 *
 * @category Twig
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ElementExtension extends AbstractExtension
{
    /**
     * The security service.
     *
     * @var \Symfony\Component\Security\Core\Security
     */
    private $security;

    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    protected $entityAdministratorManager;

    /**
     * @var \App\Element\ElementSearcher
     */
    protected $elementSearcher;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Security\Core\Security                     $security                   The security service
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     */
    public function __construct(Security $security, EntityAdministratorManager $entityAdministratorManager, ElementSearcher $elementSearcher)
    {
        $this->security = $security;
        $this->entityAdministratorManager = $entityAdministratorManager;
        $this->elementSearcher = $elementSearcher;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('last_use', [$this, 'getLastUse']),
            new TwigFilter('elements_by_genre', [$this, 'getElementsByGenre']),
        ];
    }

    /**
     * @see \App\Doctrine\Repository\User\ElementUseRepository::getLastByUserAndElement()
     *
     * @param \App\Doctrine\Decorator\Element\ElementDecorator $element The element
     */
    public function getLastUse(ElementDecorator $element): ?ElementUse
    {
        $elementUseManager = $this->entityAdministratorManager->get(ElementUse::class);

        return $elementUseManager->getRepository()->getLastByUserAndElement(
            $this->security->getUser(),
            $element->getEntity()
        );
    }

    /**
     * Get the elements by genre.
     */
    public function getElementsByGenre(Genre $genre): array
    {
        return $this->elementSearcher
            ->getSearchQueryBuilder(
                $this->security->getUser(),
                ['genre' => $genre],
                ['title' => 'ASC'],
                false
            )
            ->getQuery()
            ->getResult()
        ;
    }
}
