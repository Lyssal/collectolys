<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Twig\Extension;

use App\Doctrine\Decorator\Element\ElementDecorator;
use App\Doctrine\Manager\Element\ElementManager;
use App\Entity\Element\Element;
use App\Entity\Element\ElementIllustration;
use App\Entity\File\Icon;
use App\Entity\File\IconableInterface;
use App\Entity\File\Image;
use App\Entity\File\ImageableInterface;
use App\Entity\Language;
use App\Entity\Location;
use DateTimeInterface;
use Lyssal\Entity\Decorator\DecoratorInterface;
use Lyssal\EntityBundle\Entity\RoutableInterface;
use Lyssal\EntityBundle\Router\EntityRouterManager;
use Lyssal\Exception\InvalidArgumentException;
use Symfony\Component\Asset\Packages;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * The Twig extension for images.
 *
 * @category Twig
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ImageExtension extends AbstractExtension
{
    /**
     * The asset packages.
     *
     * @var \Symfony\Component\Asset\Packages
     */
    private $packages;

    /**
     * The entity router.
     *
     * @var \Lyssal\EntityBundle\Router\EntityRouterManager
     */
    private $entityRouter;

    /**
     * @var \App\Doctrine\Manager\Element\ElementManager The Element manager
     */
    private $elementManager;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Asset\Packages               $packages       The asset packages
     * @param \Lyssal\EntityBundle\Router\EntityRouterManager $entityRouter   The entity router
     * @param \App\Doctrine\Manager\Element\ElementManager    $elementManager The Element manager
     */
    public function __construct(Packages $packages, EntityRouterManager $entityRouter, ElementManager $elementManager)
    {
        $this->packages = $packages;
        $this->entityRouter = $entityRouter;
        $this->elementManager = $elementManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('icon_link', [$this, 'getIconLink'], ['is_safe' => ['html']]),
            new TwigFilter('icon', [$this, 'getIcon'], ['is_safe' => ['html']]),
            new TwigFilter('icon_by_element', [$this, 'getIconByElement'], ['is_safe' => ['html']]),
            new TwigFilter('image', [$this, 'getImage'], ['is_safe' => ['html']]),
            new TwigFilter('thumbnail', [$this, 'getThumbnail'], ['is_safe' => ['html']]),
            new TwigFilter('thumbnail_src', [$this, 'getThumbnailSrc']),
        ];
    }

    /**
     * Get the entity icon with link.
     *
     * @param \Lyssal\EntityBundle\Entity\RoutableInterface $routable The entity
     *
     * @return string The HTML
     */
    public function getIconLink(RoutableInterface $routable): string
    {
        return '<a href="'.$this->entityRouter->generate($routable).'" title="'.$routable.'">'.$this->getIcon($routable).'</a>';
    }

    /**
     * Get the entity icon.
     *
     * @param \App\Entity\File\IconableInterface|\App\Entity\Location                      $iconable The entity
     * @param \App\Entity\Element\Element|\App\Doctrine\Decorator\Element\ElementDecorator $element  The element
     *
     * @return string The HTML icon
     */
    public function getIconByElement($iconable, $element): string
    {
        if (!$element instanceof Element && !$element instanceof ElementDecorator) {
            throw new InvalidArgumentException('The element has to be an Element or ElementDecorator.');
        }

        $elementDate = $element->getOldestElementDate();

        if (null === $elementDate) {
            return $this->getIcon($iconable);
        }

        return $this->getIcon($iconable, $elementDate->getDateTime());
    }

    /**
     * Get the entity icon.
     *
     * @param \App\Entity\File\IconableInterface|\App\Entity\Location $iconable The entity
     * @param \DateTimeInterface|null                                 $date     The date
     *
     * @return string The HTML icon
     */
    public function getIcon($iconable, ?DateTimeInterface $date = null): string
    {
        $class = null;
        $name = $iconable;

        if ($iconable instanceof IconableInterface) {
            $src = $iconable->getIcon()->getUrl();
        } elseif ($iconable instanceof Location) {
            $emojiOrIcon = $iconable->getVersionEmojiOrIcon($date);

            if (null === $emojiOrIcon) {
                return '';
            }

            $name = $iconable->getVersionName($date);

            if (!$emojiOrIcon instanceof Icon) {
                return '<span class="flag" title="'.$name.'">'.$emojiOrIcon.'</span>';
            }

            $class = 'flag';
            $src = $emojiOrIcon->getUrl();
        } elseif ($iconable instanceof Language) {
            return $this->getIcon($iconable->getLocation());
        } else {
            throw new InvalidArgumentException('Invalid entity for the icon filter.');
        }

        return '<img src="'.$this->packages->getUrl($src).'" alt="'.$name.'" title="'.$name.'"'.(null !== $class ? ' class="'.$class.'"' : '').'>';
    }

    /**
     * Get the entity image.
     *
     * @param mixed $imageable  The entity
     * @param array $attributes The img attributes
     *
     * @return string The HTML image
     */
    public function getImage($imageable, array $attributes = []): string
    {
        return $this->getThumbnail($imageable, $attributes);
    }

    /**
     * Get the entity image thumbnail.
     *
     * @param mixed $imageable  The entity
     * @param array $attributes The img attributes
     *
     * @return string The HTML image
     */
    public function getThumbnail($imageable, array $attributes = []): string
    {
        $attributesString = implode(' ', array_map(
            function ($name, $value) { return $name.'="'.htmlspecialchars($value).'"'; },
            array_keys($attributes), $attributes
        ));

        return '<img src="'.$this->getThumbnailSrc($imageable).'" alt="'.$imageable.'" '.$attributesString.'>';
    }

    /**
     * Get the entity image thumbnail stc for the img tag.
     *
     * @param mixed $imageable The entity
     *
     * @return string The img src parameter
     */
    public function getThumbnailSrc($imageable): string
    {
        switch (true) {
            case $imageable instanceof Element:
            case $imageable instanceof ElementDecorator:
                $image = $imageable->getMainIllustration();

                if (null === $image && $imageable->isSet()) {
                    $element = $this->elementManager->getFirstSetElementWithIllustrationRecto(
                        $imageable instanceof DecoratorInterface ? $imageable->getEntity() : $imageable
                    );

                    if (null !== $element) {
                        $image = $element->getMainIllustration();
                    }
                }

                break;

            case $imageable instanceof ElementIllustration:
                $image = $imageable->getImage();
                break;

            case $imageable instanceof ImageableInterface:
                $image = $imageable->getImage();
                break;

            case $imageable instanceof Image:
            case $imageable instanceof Icon:
                $image = $imageable;
                break;

            default:
                throw new InvalidArgumentException('Invalid entity for the thumbnail filter.');
        }

        if (null !== $image) {
            $src = $this->packages->getUrl($image->getUrl());
        } else {
            $src = $this->getNullImageSrc();
        }

        return $src;
    }

    /**
     * Get the image to display by default.
     *
     * @return string The image URL
     */
    private function getNullImageSrc(): string
    {
        // Semi-transparent
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8LwkAAh0BGumlBj4AAAAASUVORK5CYII=';
    }
}
