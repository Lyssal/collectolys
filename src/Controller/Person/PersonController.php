<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Person;

use App\Doctrine\Manager\Person\PersonRoleManager;
use App\Doctrine\Repository\Person\PersonRoleRepository;
use App\Entity\Person\Person;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Person controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/Personne", name="person_person_")
 * @IsGranted("ROLE_USER")
 */
final class PersonController extends AbstractController
{
    /**
     * Show the company.
     *
     * @Route("/{person}/{page}", name="show", methods={"GET"})
     * @ParamConverter("person", options={"mapping": {"person": "slug"}})
     */
    public function show(ViewParameter $viewParameter, Person $person, int $page = 1): Response
    {
        return $this->render('person/show.html.twig', $viewParameter->get([
            'person' => $person,
        ], [], $page, false));
    }

    /**
     * The tooltip HTML.
     *
     * @Route("/{person}/tooltip", name="tooltip", methods={"GET"}, priority=1)
     */
    public function tooltip(PersonRoleManager $personRoleManager, Person $person): Response
    {
        return $this->render('person/tooltip.html.twig', [
            'person' => $person,
            'person_roles' => $personRoleManager->getRepository()->getPersonRolesByPerson($person),
        ]);
    }
}
