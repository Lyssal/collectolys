<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller;

use App\Doctrine\Manager\Company\CompanyManager;
use App\Doctrine\Manager\Element\ElementManager;
use App\Doctrine\Manager\Person\PersonManager;
use App\Doctrine\Manager\TypeManager;
use App\Doctrine\Manager\UniverseManager;
use App\Entity\Type;
use App\Statistic\GenreStatistic;
use App\Statistic\GlobalStatistic;
use App\Statistic\OriginStatistic;
use App\Statistic\VideoGamePlatformStatistic;
use App\Statistic\WebSourceStatistic;
use App\Statistic\YearStatistic;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The statistic controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/Statistiques", name="statistic_")
 * @IsGranted("ROLE_USER")
 */
final class StatisticController extends AbstractController
{
    /**
     * The global statistics.
     *
     * @Route("/Global", name="global")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function global(ElementManager $elementManager, GlobalStatistic $globalStatistic): Response
    {
        return $this->render('statistic/global.html.twig', [
            'chart_config' => $globalStatistic->getConfig(),
            'global_total' => $elementManager->count(['set' => false]),
        ]);
    }

    /**
     * The universes.
     *
     * @Route("/Univers", name="universes")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function universes(UniverseManager $universeManager): Response
    {
        return $this->render('statistic/universes.html.twig', [
            'best_universes' => $universeManager->getRepository()->getUniverseElementCounts(48),
        ]);
    }

    /**
     * The sets.
     *
     * @Route("/Series", name="sets")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function sets(ElementManager $elementManager): Response
    {
        return $this->render('statistic/sets.html.twig', [
            'best_sets' => $elementManager->getRepository()->getSetElementCounts(96),
        ]);
    }

    /**
     * The video game platforms.
     *
     * @Route("/JeuxVideos/Plateformes", name="videogameplatforms")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function videoGamePlatforms(TypeManager $typeManager, VideoGamePlatformStatistic $videoGamePlatformStatistic): Response
    {
        return $this->render('statistic/video_game_platforms.html.twig', [
            'type' => $typeManager->findOneById(Type::VIDEO_GAME),
            'chart_config' => $videoGamePlatformStatistic->getConfig(),
            'best_platforms' => $videoGamePlatformStatistic->getBestPlatforms(),
        ]);
    }

    /**
     * The element origins.
     *
     * @Route("/Nationalites/{type}", name="origins")
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function origins(OriginStatistic $originStatistic, ?Type $type = null): Response
    {
        $viewParameters = [
            'best_locations' => $originStatistic->getBestLocations($type, 16),
            'mappemonde_series' => $originStatistic->getMappemondeSeries($type),
        ];

        if (null !== $type) {
            $viewParameters['type'] = $type;
        }

        return $this->render('statistic/origins.html.twig', $viewParameters);
    }

    /**
     * The genres.
     *
     * @Route("/Genres", name="genres")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function genres(GenreStatistic $genreStatistic): Response
    {
        return $this->render('statistic/genres.html.twig', [
            'chart_configs' => $genreStatistic->getConfigs(),
            'best_genres_by_type' => $genreStatistic->getBestGenres(10),
        ]);
    }

    /**
     * The years.
     *
     * @Route("/Annees", name="years")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function years(YearStatistic $yearStatistic): Response
    {
        return $this->render('statistic/years.html.twig', [
            'chart_config' => $yearStatistic->getConfig(),
            'best_years_by_type' => $yearStatistic->getBestYears(12),
        ]);
    }

    /**
     * The companies.
     *
     * @Route("/Societes", name="companies")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function companies(CompanyManager $companyManager): Response
    {
        return $this->render('statistic/companies.html.twig', [
            'best_companies' => $companyManager->getRepository()->getCompanyElementCounts(48),
        ]);
    }

    /**
     * The people.
     *
     * @Route("/Personnes", name="people")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function people(PersonManager $personManager): Response
    {
        return $this->render('statistic/people.html.twig', [
            'best_people' => $personManager->getRepository()->getPersonElementCounts(48),
        ]);
    }

    /**
     * The web sources.
     *
     * @Route("/Sources_web", name="websources")
     *
     * @return \Symfony\Component\HttpFoundation\Response The view
     */
    public function webSources(WebSourceStatistic $webSourceStatistic): Response
    {
        return $this->render('statistic/web_sources.html.twig', [
            'chart_config' => $webSourceStatistic->getConfig(),
            'best_domains' => $webSourceStatistic->getBestDomains(),
        ]);
    }
}
