<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Company;

use App\Doctrine\Manager\Company\CompanyRoleManager;
use App\Entity\Company\Company;
use App\Search\AutocompleteResponse;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Company controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="company_company_")
 * @IsGranted("ROLE_USER")
 */
final class CompanyController extends AbstractController
{
    /**
     * Show the company.
     *
     * @Route("/Societe/{company}/{page}", name="show", methods={"GET"}, requirements={"company": "[a-zA-Z0-9_]+"})
     * @ParamConverter("company", options={"mapping": {"company": "slug"}})
     */
    public function show(ViewParameter $viewParameter, Company $company, int $page = 1): Response
    {
        return $this->render('company/show.html.twig', $viewParameter->get([
            'company' => $company,
        ], [], $page, false));
    }

    /**
     * The autocomplete response.
     *
     * @Route("/companies/autocomplete", name="autocomplete", methods={"GET"})
     */
    public function autocomplete(AutocompleteResponse $autocompleteResponse, Request $request): JsonResponse
    {
        return new JsonResponse($autocompleteResponse->getCompanyJson($request));
    }

    /**
     * The tooltip HTML.
     *
     * @Route("/companies/{company}/tooltip", name="tooltip", methods={"GET"})
     */
    public function tooltip(CompanyRoleManager $companyRoleManager, Company $company): Response
    {
        return $this->render('company/tooltip.html.twig', [
            'company' => $company,
            'company_roles' => $companyRoleManager->getRepository()->getCompanyRolesByCompany($company),
        ]);
    }
}
