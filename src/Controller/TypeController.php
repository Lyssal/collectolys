<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller;

use App\Element\ElementSearcher;
use App\Entity\Type;
use App\Search\ViewParameter;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Type controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="type_")
 * @IsGranted("ROLE_USER")
 */
final class TypeController extends AbstractController
{
    /**
     * The list of the types.
     *
     * @Route(name="index", methods={"GET"})
     */
    public function index(ElementSearcher $elementSearcher): Response
    {
        $today = new DateTime();
        $birthdayElements = array_map(function (array $elementResult) { return $elementResult[0]; }, $elementSearcher
            ->getSearchQueryBuilder($this->getUser(), ['birthday' => $today])
            ->getQuery()
            ->getResult())
        ;

        return $this->render(
            'type/index.html.twig',
            [
                'birth_day' => (int) $today->format('d'),
                'birth_month' => (int) $today->format('m'),
                'birthday_elements' => $birthdayElements,
            ]
        );
    }

    /**
     * Show the type elements.
     *
     * @Route("/Collection/{type}/{page}", name="show", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function show(ViewParameter $viewParameter, Type $type, int $page = 1): Response
    {
        return $this->render('type/show.html.twig', $viewParameter->get([
            'type' => $type,
        ], [], $page, true));
    }
}
