<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The controller for parameters.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/config", name="config_")
 */
final class ConfigController extends AbstractController
{
    /**
     * The dynamic CSS.
     *
     * @Route("/stylesheet.css", name="stylesheet", methods={"GET"})
     */
    public function stylesheet()
    {
        $response = new Response(null, Response::HTTP_OK, [
            'Content-Type' => 'text/css',
        ]);

        return $this->render('config/stylesheet.css.twig', [], $response);
    }
}
