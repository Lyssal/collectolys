<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Doctrine\Manager\GenreManager;
use App\Entity\Genre;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Lyssal\Doctrine\Orm\QueryBuilder;
use Symfony\Component\Form\FormBuilderInterface;

class GenreCrudController extends AbstractCrudController
{
    /**
     * @var \App\Doctrine\Manager\GenreManager
     */
    private $genreManager;

    public function __construct(GenreManager $genreManager)
    {
        $this->genreManager = $genreManager;
    }

    public static function getEntityFqcn(): string
    {
        return Genre::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'genres')
            ->setSearchFields(['name', 'slug', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $type = AssociationField::new('type', 'collection');
        $parent = AssociationField::new('parent', 'parent');
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');
        $children = AssociationField::new('children', 'children');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $type, $parent, $children];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $slug, $id, $type, $parent, $children];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $type, $parent];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $type, $parent];
        }
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        return parent::createNewFormBuilder($entityDto, $formOptions, $context)
            ->add('type', null, [
                'label' => 'collection',
            ])
            ->remove('parent')
        ;
    }

    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $parents = $this->genreManager->findBy([
            'type' => $entityDto->getInstance()->getType(),
            QueryBuilder::WHERE_NOT_EQUAL => [QueryBuilder::ALIAS => $entityDto->getInstance()],
        ]);

        return parent::createEditFormBuilder($entityDto, $formOptions, $context)
            ->add('type', null, [
                'label' => 'collection',
                'disabled' => true,
            ])
            ->add('parent', null, [
                'choices' => $parents,
            ])
        ;
    }
}
