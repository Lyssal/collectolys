<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Support\Support;
use App\Form\Type\File\IconType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SupportCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Support::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'supports')
            ->setSearchFields(['name', 'slug', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $types = AssociationField::new('types', 'collections');
        $icon = TextField::new('icon')->setFormType(IconType::class)->setFormTypeOption('path', Support::ICON_PATH);
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');
        $iconUrl = ImageField::new('icon.url', 'icon');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $iconUrl, $name, $types];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $slug, $id, $types, $icon];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $types, $icon];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $types, $icon];
        }
    }
}
