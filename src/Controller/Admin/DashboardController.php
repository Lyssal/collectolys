<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Company\Company;
use App\Entity\Company\CompanyRole;
use App\Entity\Currency;
use App\Entity\Element\Element;
use App\Entity\Element\SpecialSet;
use App\Entity\Genre;
use App\Entity\Keyword;
use App\Entity\Language;
use App\Entity\Location;
use App\Entity\LocationVersion;
use App\Entity\Person\Person;
use App\Entity\Person\PersonRole;
use App\Entity\Platform;
use App\Entity\Support\Support;
use App\Entity\Type;
use App\Entity\Universe;
use App\Entity\User\ElementStorage;
use App\Entity\User\UserSet;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Lyssal\SimpleLocationBundle\Entity\LocationType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/console")
     */
    public function index(): Response
    {
        // redirect to some CRUD controller
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(CompanyCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('CollectoLys');
    }

    public function configureCrud(): Crud
    {
        return Crud::new();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('collections', 'fas fa-folder-open', Type::class);
        yield MenuItem::linkToCrud('special_sets', 'fas fa-folder-open', SpecialSet::class);
        yield MenuItem::linkToCrud('my_sets', 'fas fa-folder-open', UserSet::class);
        yield MenuItem::linkToCrud('universes', 'fas fa-folder-open', Universe::class);
        yield MenuItem::linkToCrud('genres', 'fas fa-folder-open', Genre::class);
        yield MenuItem::linkToCrud('keywords', 'fas fa-tags', Keyword::class);
        yield MenuItem::linkToCrud('platforms', 'fas fa-folder-open', Platform::class);
        yield MenuItem::linkToCrud('supports', 'far fa-save', Support::class);

        yield MenuItem::section('elements', 'fas fa-folder-open');
        yield MenuItem::linkToCrud('Element', 'fas fa-folder-open', Element::class);

        yield MenuItem::section('company', 'fas fa-folder-open');
        yield MenuItem::linkToCrud('companies', 'fas fa-building', Company::class);
        yield MenuItem::linkToCrud('company_roles', 'fas fa-building', CompanyRole::class);

        yield MenuItem::section('person', 'fas fa-folder-open');
        yield MenuItem::linkToCrud('people', 'fas fa-user', Person::class);
        yield MenuItem::linkToCrud('person_roles', 'fas fa-users', PersonRole::class);

        yield MenuItem::section('user', 'fas fa-folder-open');
        yield MenuItem::linkToCrud('storages', 'fas fa-archive', ElementStorage::class);

        yield MenuItem::section('administration', 'fas fa-folder-open');
        yield MenuItem::linkToCrud('locations', 'fas fa-globe', Location::class);
        yield MenuItem::linkToCrud('location_types', 'fas fa-globe', LocationType::class);
        yield MenuItem::linkToCrud('location_versions', 'fas fa-globe', LocationVersion::class);
        yield MenuItem::linkToCrud('languages', 'fas fa-flag', Language::class);
        yield MenuItem::linkToCrud('currencies', 'fas fa-euro-sign', Currency::class);
        yield MenuItem::linkToUrl('images', 'far fa-image', '/elfinder');
    }
}
