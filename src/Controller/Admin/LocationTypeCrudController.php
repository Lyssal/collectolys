<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Lyssal\SimpleLocationBundle\Entity\LocationType;

class LocationTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return LocationType::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'location_types')
            ->setSearchFields(['id', 'name']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $parents = AssociationField::new('parents');
        $children = AssociationField::new('children');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $parents, $children];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $parents, $children];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $parents, $children];
        }
    }
}
