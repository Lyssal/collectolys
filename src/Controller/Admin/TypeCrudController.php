<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Type;
use App\Form\Type\File\IconType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Type::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'types')
            ->setSearchFields(['name', 'slug', 'emoji', 'elementName', 'color', 'defaultLanguageSources', 'id', 'position']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $emoji = TextField::new('emoji', 'emoji');
        $elementName = TextField::new('elementName');
        $color = TextField::new('color');
        $defaultLanguageSources = ArrayField::new('defaultLanguageSources');
        $icon = TextField::new('icon')->setFormType(IconType::class)->setFormTypeOption('path', Type::ICON_PATH);
        $position = IntegerField::new('position');
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');
        $genres = AssociationField::new('genres');
        $specialSets = AssociationField::new('specialSets');
        $supports = AssociationField::new('supports');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $emoji, $name];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $slug, $emoji, $elementName, $color, $defaultLanguageSources, $id, $position, $genres, $specialSets, $supports, $icon];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $emoji, $elementName, $color, $defaultLanguageSources, $icon, $position];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $emoji, $elementName, $color, $defaultLanguageSources, $icon, $position];
        }
    }
}
