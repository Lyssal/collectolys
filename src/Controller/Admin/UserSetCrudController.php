<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\User\UserSet;
use App\Form\Type\SimpleArrayType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

final class UserSetCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UserSet::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'my_sets')
            ->setSearchFields(['name']);
    }

    public function configureFields(string $pageName): iterable
    {
        $id = IntegerField::new('id', 'ID');
        $name = TextField::new('name', 'name');
        $type = AssociationField::new('type', 'collection');
        $elementIds = ArrayField::new('elementIds')->setFormType(SimpleArrayType::class);

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $type, $name];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$type, $name, $elementIds];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$type, $name, $elementIds];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$type, $name, $elementIds];
        }
    }
}
