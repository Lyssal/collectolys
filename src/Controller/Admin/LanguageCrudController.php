<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Language;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class LanguageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Language::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'languages')
            ->setSearchFields(['name', 'code', 'culture', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $location = AssociationField::new('location', 'location');
        $code = TextField::new('code', 'code');
        $culture = TextField::new('culture', 'culture');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $location, $code, $culture];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $code, $culture, $id, $location];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $location, $code, $culture];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $location, $code, $culture];
        }
    }
}
