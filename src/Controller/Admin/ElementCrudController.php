<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Doctrine\Manager\Element\ElementManager;
use App\Entity\Element\Element;
use App\Form\Type\Element\ElementDateType;
use App\Form\Type\Element\ElementIllustrationType;
use App\Form\Type\Element\ElementNameType;
use App\Form\Type\Element\ElementPriceType;
use App\Form\Type\Element\SubElementType;
use App\Form\Type\File\ImageType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ElementCrudController extends AbstractCrudController
{
    private $elementManager;

    public function __construct(ElementManager $elementManager)
    {
        $this->elementManager = $elementManager;
    }

    public static function getEntityFqcn(): string
    {
        return Element::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Element')
            ->setEntityLabelInPlural('Element')
            ->setSearchFields(['slug', 'numberInSet', 'positionInSet', 'description', 'content', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $type = AssociationField::new('type');
        $names = CollectionField::new('names')->setFormTypeOptions(['entry_type' => ElementNameType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'error_bubbling' => false]);
        $description = TextField::new('description');
        $subElements = CollectionField::new('subElements')->setFormTypeOptions(['entry_type' => SubElementType::class, 'allow_add' => true, 'allow_delete' => true, 'error_bubbling' => false]);
        $content = TextareaField::new('content');
        $illustrationRecto = TextField::new('illustrationRecto')->setFormType(ImageType::class)->setFormTypeOption('path', Element::IMAGES_PATH);
        $illustrationVerso = TextField::new('illustrationVerso')->setFormType(ImageType::class)->setFormTypeOption('path', Element::IMAGES_PATH);
        $origins = AssociationField::new('origins');
        $set = AssociationField::new('set')->autocomplete();
        $numberInSet = TextField::new('numberInSet');
        $parents = AssociationField::new('parents')->autocomplete();
        $children = AssociationField::new('children')->autocomplete();
        $universes = AssociationField::new('universes');
        $genres = AssociationField::new('genres');
        $dates = CollectionField::new('dates')->setFormTypeOptions(['entry_type' => ElementDateType::class, 'allow_add' => true, 'allow_delete' => true, 'error_bubbling' => false]);
        $prices = CollectionField::new('prices')->setFormTypeOptions(['entry_type' => ElementPriceType::class, 'allow_add' => true, 'allow_delete' => true, 'error_bubbling' => false]);
        $illustrations = CollectionField::new('illustrations')->setFormTypeOptions(['entry_type' => ElementIllustrationType::class, 'allow_add' => true, 'allow_delete' => true, 'error_bubbling' => false]);
        $slug = TextField::new('slug');
        $positionInSet = IntegerField::new('positionInSet');
        $orderedSubElements = Field::new('orderedSubElements');
        $createdAt = DateTimeField::new('createdAt');
        $id = IntegerField::new('id', 'ID');
        $updatedAt = DateTimeField::new('updatedAt');
        $references = AssociationField::new('references');
        $referencedElements = AssociationField::new('referencedElements');
        $previousElement = AssociationField::new('previousElement');
        $nextElements = AssociationField::new('nextElements');
        $similarElements = AssociationField::new('similarElements');
        $otherSimilarElements = AssociationField::new('otherSimilarElements');
        $companies = AssociationField::new('companies');
        $people = AssociationField::new('people');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$numberInSet, $positionInSet, $description, $orderedSubElements, $createdAt, $id, $type];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$slug, $numberInSet, $positionInSet, $description, $content, $orderedSubElements, $createdAt, $id, $updatedAt, $type, $names, $set, $illustrationRecto, $illustrationVerso, $parents, $children, $references, $referencedElements, $previousElement, $nextElements, $similarElements, $otherSimilarElements, $subElements, $universes, $genres, $origins, $dates, $companies, $people, $prices, $illustrations];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$type, $names, $description, $subElements, $content, $illustrationRecto, $illustrationVerso, $origins, $set, $numberInSet, $parents, $children, $universes, $genres, $dates, $prices, $illustrations];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$type, $names, $description, $subElements, $content, $illustrationRecto, $illustrationVerso, $origins, $set, $numberInSet, $parents, $children, $universes, $genres, $dates, $prices, $illustrations];
        }
    }

    /**
     * @param \App\Entity\Element\Element $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->elementManager->save($entityInstance);
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->updateEntity($entityManager, $entityInstance);
    }
}
