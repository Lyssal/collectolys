<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Location;
use App\Entity\LocationVersion;
use App\Form\Type\File\IconType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

final class LocationVersionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return LocationVersion::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'location_versions')
            ->setSearchFields(['name']);
    }

    public function configureFields(string $pageName): iterable
    {
        $location = AssociationField::new('location', 'location');
        $startDate = DateField::new('startDate', 'date.start');
        $endDate = DateField::new('endDate', 'date.end');
        $name = TextField::new('name', 'name');
        $emoji = TextField::new('emoji');
        $icon = TextField::new('icon')->setFormType(IconType::class)->setFormTypeOption('path', Location::ICON_PATH);
        $id = IntegerField::new('id', 'id');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $location, $name];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$location, $startDate, $endDate, $id, $name, $emoji, $icon];
        } elseif (\in_array($pageName, [Crud::PAGE_NEW, Crud::PAGE_EDIT], true)) {
            return [$location, $startDate, $endDate, $name, $emoji, $icon];
        }
    }
}
