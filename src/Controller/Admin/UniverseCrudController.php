<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Universe;
use App\Form\Type\File\IconType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UniverseCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Universe::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'universes')
            ->setSearchFields(['name', 'slug', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $icon = TextField::new('icon')->setFormType(IconType::class)->setFormTypeOption('path', Universe::ICON_PATH);
        $parent = AssociationField::new('parent', 'parent');
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');
        $children = AssociationField::new('children');
        $iconUrl = ImageField::new('icon.url', 'icon');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $iconUrl, $name, $parent];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $slug, $id, $parent, $children, $icon];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $icon, $parent];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $icon, $parent];
        }
    }
}
