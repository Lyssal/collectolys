<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Doctrine\Manager\Person\PersonManager;
use App\Entity\Person\Person;
use App\Form\Type\File\ImageType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PersonCrudController extends AbstractCrudController
{
    /**
     * @var \App\Doctrine\Manager\Person\PersonManager
     */
    private $personManager;

    public function __construct(PersonManager $personManager)
    {
        $this->personManager = $personManager;
    }

    public static function getEntityFqcn(): string
    {
        return Person::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'people')
            ->setSearchFields(['firstName', 'lastName', 'slug', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $firstName = TextField::new('firstName');
        $lastName = TextField::new('lastName');
        $nationalities = AssociationField::new('nationalities');
        $image = TextField::new('image')->setFormType(ImageType::class)->setFormTypeOption('path', Person::IMAGE_PATH);
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');
        $imageUrl = ImageField::new('image.url', 'image');
        $name = TextareaField::new('name', 'name');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $imageUrl, $name];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$firstName, $lastName, $slug, $id, $image, $nationalities];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$firstName, $lastName, $nationalities, $image];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$firstName, $lastName, $nationalities, $image];
        }
    }

    /**
     * @param \App\Entity\Person\Person $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->personManager->save($entityInstance);
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->updateEntity($entityManager, $entityInstance);
    }
}
