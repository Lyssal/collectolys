<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Keyword;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

final class KeywordCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Keyword::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'keywords')
            ->setSearchFields(['name', 'slug', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $parents = AssociationField::new('parents', 'parents');
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');
        $children = AssociationField::new('children', 'children');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $parents, $children];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $slug, $id, $parents, $children];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $parents, $children];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $parents, $children];
        }
    }
}
