<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Element\SpecialSet;
use App\Form\Type\File\ImageType;
use App\Form\Type\SimpleArrayType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SpecialSetCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SpecialSet::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'special_sets')
            ->setSearchFields(['name', 'slug', 'ids', 'id', 'position']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $type = AssociationField::new('type', 'collection');
        $image = TextField::new('image')->setFormType(ImageType::class)->setFormTypeOption('path', SpecialSet::IMAGE_PATH);
        $ids = ArrayField::new('ids')->setFormType(SimpleArrayType::class);
        $position = IntegerField::new('position');
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $type];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $slug, $ids, $id, $position, $type, $image];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $type, $image, $ids, $position];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $type, $image, $ids, $position];
        }
    }
}
