<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Person\PersonRole;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PersonRoleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PersonRole::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'person_roles')
            ->setSearchFields(['name', 'id', 'position']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $type = AssociationField::new('type', 'collection');
        $position = IntegerField::new('position');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $type];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $id, $position, $type];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $type, $position];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $type, $position];
        }
    }
}
