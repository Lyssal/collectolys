<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Doctrine\Manager\Company\CompanyManager;
use App\Entity\Company\Company;
use App\Form\Type\File\ImageType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CompanyCrudController extends AbstractCrudController
{
    private $companyManager;

    public function __construct(CompanyManager $companyManager)
    {
        $this->companyManager = $companyManager;
    }

    public static function getEntityFqcn(): string
    {
        return Company::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'companies')
            ->setFormOptions(['validation_groups' => ['Default', 'creation']], ['validation_groups' => ['Default', 'creation']])

            ->setSearchFields(['name', 'slug', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $nationality = AssociationField::new('nationality');
        $foundedAt = DateField::new('foundedAt');
        $image = TextField::new('image')->setFormType(ImageType::class)->setFormTypeOption('path', Company::IMAGE_PATH);
        $headquarter = AssociationField::new('headquarter', 'headquarter')->autocomplete();
        $subsidiaries = AssociationField::new('subsidiaries', 'subsidiaries')->setFormTypeOption('by_reference', false)->autocomplete();
        $previousCompany = AssociationField::new('previousCompany')->autocomplete();
        $nextCompany = AssociationField::new('nextCompany')->autocomplete();
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');
        $imageUrl = ImageField::new('image.url', 'image');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $imageUrl, $name, $headquarter, $subsidiaries];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $slug, $foundedAt, $id, $nationality, $image, $headquarter, $subsidiaries, $previousCompany, $nextCompany];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $nationality, $foundedAt, $image, $headquarter, $subsidiaries, $previousCompany, $nextCompany];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $nationality, $foundedAt, $image, $headquarter, $subsidiaries, $previousCompany, $nextCompany];
        }
    }

    /**
     * @param \App\Entity\Company\Company $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->companyManager->save($entityInstance);
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->updateEntity($entityManager, $entityInstance);
    }
}
