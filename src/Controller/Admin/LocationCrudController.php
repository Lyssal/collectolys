<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Doctrine\Manager\LocationManager;
use App\Entity\Location;
use App\Form\Type\File\IconType;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class LocationCrudController extends AbstractCrudController
{
    private $locationManager;

    public function __construct(LocationManager $locationManager)
    {
        $this->locationManager = $locationManager;
    }

    public static function getEntityFqcn(): string
    {
        return Location::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'locations')
            ->setSearchFields(['slug', 'emoji', 'id', 'name']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'name');
        $code = TextField::new('code');
        $emoji = TextField::new('emoji');
        $icon = TextField::new('icon')->setFormType(IconType::class)->setFormTypeOption('path', Location::ICON_PATH);
        $type = AssociationField::new('type', 'type');
        $versions = AssociationField::new('versions');
        $parents = AssociationField::new('parents');
        $children = AssociationField::new('children');
        $currency = AssociationField::new('currency');
        $slug = TextField::new('slug');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $code, $type];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$slug, $code, $emoji, $id, $name, $icon, $currency, $versions, $type, $parents, $children];
        } elseif (\in_array($pageName, [Crud::PAGE_NEW, Crud::PAGE_EDIT], true)) {
            return [$name, $code, $emoji, $icon, $type, $versions, $parents, $children, $currency];
        }
    }

    /**
     * @param \App\Entity\Element\Element $entityInstance
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->locationManager->save($entityInstance);
    }

    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $this->updateEntity($entityManager, $entityInstance);
    }
}
