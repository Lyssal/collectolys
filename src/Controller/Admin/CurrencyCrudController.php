<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Admin;

use App\Entity\Currency;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CurrencyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Currency::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'currencies')
            ->setSearchFields(['code', 'symbol', 'id']);
    }

    public function configureFields(string $pageName): iterable
    {
        $code = TextField::new('code');
        $symbol = TextField::new('symbol', 'symbol');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $code, $symbol];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$code, $symbol, $id];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$code, $symbol];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$code, $symbol];
        }
    }
}
