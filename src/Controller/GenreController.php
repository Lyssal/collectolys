<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller;

use App\Entity\Genre;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Genre controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="genre_")
 * @IsGranted("ROLE_USER")
 */
final class GenreController extends AbstractController
{
    /**
     * Show the genre.
     *
     * @Route("/Genre/{genre}/{page}", name="show", methods={"GET"})
     * @ParamConverter("genre", options={"mapping": {"genre": "slug"}})
     */
    public function show(ViewParameter $viewParameter, Genre $genre, int $page = 1): Response
    {
        $parameters = $viewParameter->get([
            'genre' => $genre,
        ], [], $page, false);

        $parameters['type'] = $genre->getType();

        return $this->render('genre/show.html.twig', $parameters);
    }
}
