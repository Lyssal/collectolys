<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller;

use App\Entity\Universe;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Universe controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="universe_")
 * @IsGranted("ROLE_USER")
 */
final class UniverseController extends AbstractController
{
    /**
     * Show the universe.
     *
     * @Route("/Univers/{universe}/{page}", name="show", methods={"GET"})
     * @ParamConverter("universe", options={"mapping": {"universe": "slug"}})
     */
    public function show(ViewParameter $viewParameter, Universe $universe, int $page = 1): Response
    {
        return $this->render('universe/show.html.twig', $viewParameter->get([
            'universe' => $universe,
        ], [], $page, false));
    }
}
