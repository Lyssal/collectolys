<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller;

use App\Element\ElementSearcher;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Lyssal\System\Server;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/file-generation", name="filegeneration_")
 */
final class FileGenerationController extends AbstractController
{
    /**
     * @Route("/collectolys.html", name="html")
     */
    public function html(ElementSearcher $elementSearcher, ContainerInterface $container): Response
    {
        if ($container->has('profiler')) {
            $container->get('profiler')->disable();
        }

        Server::setUnlimitedTimeLimit();

        $elements = $elementSearcher
            ->getSearchQueryBuilder(
                null,
                [],
                ['title' => 'ASC'],
                false
            )
            ->getQuery()
            ->getResult()
        ;

        return $this->render(
            'file_generation/index.html.twig',
            [
                'elements' => $elements,
                'file_type' => 'html',
            ]
        );
    }

    /**
     * @Route("/collectolys.pdf", name="pdf")
     */
    public function pdf(ElementSearcher $elementSearcher, Pdf $pdf): PdfResponse
    {
        $elements = $elementSearcher
            ->getSearchQueryBuilder(
                null,
                [],
                ['title' => 'ASC'],
                false
            )
            ->getQuery()
            ->getResult()
        ;

        $html = $this->renderView(
            'file_generation/index.html.twig',
            [
                'elements' => $elements,
                'file_type' => 'pdf',
            ]
        );

        $pdf->setTimeout(600);

        return new PdfResponse(
            $pdf->getOutputFromHtml(
                $html,
                [
                    'enable-internal-links' => true,
                ]
            ),
            'collectolys.pdf'
        );
    }
}
