<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller;

use App\Entity\Keyword;
use App\Search\AutocompleteResponse;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Keyword controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/MotClef", name="keyword_")
 * @IsGranted("ROLE_USER")
 */
final class KeywordController extends AbstractController
{
    /**
     * Show the keyword.
     *
     * @Route("/{keyword}/{page}", name="show", methods={"GET"})
     * @ParamConverter("keyword", options={"mapping": {"keyword": "slug"}})
     */
    public function show(ViewParameter $viewParameter, Keyword $keyword, int $page = 1): Response
    {
        $parameters = $viewParameter->get([
            'keyword' => $keyword,
        ], [], $page, false);

        return $this->render('keyword/show.html.twig', $parameters);
    }

    /**
     * The autocomplete response.
     *
     * @Route("/Autocomplete", name="autocomplete", methods={"GET"}, priority=1)
     */
    public function autocomplete(AutocompleteResponse $autocompleteResponse, Request $request): JsonResponse
    {
        return new JsonResponse($autocompleteResponse->getKeywordJson($request));
    }
}
