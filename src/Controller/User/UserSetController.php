<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\User;

use App\Doctrine\Manager\Element\ElementManager;
use App\Element\UserSetGetter;
use App\Entity\Type;
use Lyssal\Entity\Decorator\DecoratorManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2024 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/Utilisateur", name="user_userset_")
 * @IsGranted("ROLE_ADMIN")
 */
final class UserSetController extends AbstractController
{
    /**
     * The user set list.
     *
     * @Route("/MesSeries/{type}", name="list", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function list(Request $request, UserSetGetter $userSetGetter, DecoratorManager $decoratorManager, ElementManager $elementManager, ?Type $type = null): Response
    {
        $userSets = $userSetGetter->get($type);
        $parametres = [
            'aggregate_by_set' => false,
            'user_sets' => $userSets,
            'elements_by_id' => $decoratorManager->get($elementManager->getByUserSets($userSets)),
        ];

        if (null !== $type) {
            $parametres['type'] = $type;
        }

        return $this->render(
            'user/user_set/list.html.twig',
            $parametres,
        );
    }
}
