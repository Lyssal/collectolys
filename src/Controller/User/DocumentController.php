<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\User;

use App\Entity\User\Document;
use App\Form\Type\User\DocumentType;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Document controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/Document", name="user_document_")
 * @IsGranted("ROLE_USER")
 */
final class DocumentController extends AbstractController
{
    /**
     * @Route("s", name="list", methods={"GET"})
     */
    public function list(): Response
    {
        /**
         * @var \App\Entity\User\User $user
         */
        $user = $this->getUser();

        return $this->render('document/list.html.twig', ['documents' => $user->getDocuments()]);
    }

    /**
     * @Route("/{document}", name="show", methods={"GET"}, requirements={"document": "\d+"})
     *
     * @IsGranted("document_show", subject="document")
     */
    public function show(Document $document): Response
    {
        return $this->render('document/show.html.twig', ['document' => $document]);
    }

    /**
     * @Route("/Add", name="add", methods={"GET", "POST"})
     *
     * @IsGranted("document_add")
     */
    public function add(Request $request, EntityAdministratorManager $entityAdministratorManager): Response
    {
        $document = (new Document())->setUser($this->getUser());

        return $this->edit($request, $entityAdministratorManager, $document);
    }

    /**
     * @Route("/Edit/{document}", name="edit", methods={"GET", "POST"}, requirements={"document": "\d+"})
     *
     * @IsGranted("document_edit", subject="document")
     */
    public function edit(Request $request, EntityAdministratorManager $entityAdministratorManager, Document $document): Response
    {
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityAdministratorManager->get(Document::class)->save($document);
            $this->addFlash('success', 'message.save.success');

            return $this->redirectToRoute('user_document_show', ['document' => $document->getId()]);
        }

        return $this->render(
            'document/edit.html.twig',
            [
                'form' => $form->createView(),
                'document' => $document,
            ]
        );
    }
}
