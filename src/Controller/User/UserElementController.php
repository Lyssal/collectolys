<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\User;

use App\Doctrine\Manager\User\UserElementManager;
use App\Element\LastUsedElementsGetter;
use App\Element\WishListElementsGetter;
use App\Entity\Element\Element;
use App\Entity\Type;
use App\Form\Type\User\UserElementType;
use App\Security\User\UserElementVoter;
use App\Statistic\OriginStatistic;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The UserElement controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/Utilisateur/Element", name="user_userelement_")
 * @IsGranted("ROLE_ADMIN")
 */
final class UserElementController extends AbstractController
{
    /**
     * Show an user element.
     *
     * @Route("/{element}", name="show", methods={"GET"}, requirements={"element": "\d+"})
     */
    public function show(Element $element, UserElementManager $userElementManager): Response
    {
        $userElement = $userElementManager->getOrCreateByUserAndElement($this->getUser(), $element);

        return $this->render('user/user_element/show.html.twig', [
            'type' => $element->getType(),
            'element' => $element,
            'user_element' => $userElement,
        ]);
    }

    /**
     * Edit an user element.
     *
     * @Route("/{element}/Edition", name="edit", methods={"GET", "POST"}, requirements={"element": "\d+"})
     */
    public function edit(Element $element, UserElementManager $userElementManager, Request $request): Response
    {
        $userElement = $userElementManager->getOrCreateByUserAndElement($this->getUser(), $element);

        $this->denyAccessUnlessGranted(UserElementVoter::EDIT, $userElement);

        $form = $this->createForm(UserElementType::class, $userElement);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $userElementManager->save($userElement);
                $this->addFlash('success', 'message.save.success');

                return $this->redirectToRoute('user_userelement_show', [
                    'element' => $element->getId(),
                ]);
            }

            $this->addFlash('error', 'message.save.error');
        }

        return $this->render('user/user_element/edit.html.twig', [
            'type' => $userElement->getElement()->getType(),
            'form' => $form->createView(),
            'user_element' => $userElement,
        ]);
    }

    /**
     * The user wish list.
     *
     * @Route("/MesEnvies/{type}", name="wishlist", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function wishList(Request $request, WishListElementsGetter $wishListElementsGetter, ?Type $type = null): Response
    {
        $parameters = [
            'elements' => $wishListElementsGetter->get($type),
            'aggregate_by_set' => false,
        ];

        if (null !== $type) {
            $parameters['type'] = $type;
        }

        return $this->render('user/user_element/wish_list.html.twig', $parameters);
    }

    /**
     * Get the user last used elements.
     *
     * @Route("/DernieresUtilisations/{page}", name="lastuses", methods={"GET"})
     */
    public function lastUses(LastUsedElementsGetter $lastUsedElementsGetter, int $page = 1): Response
    {
        return $this->render('user/user_element/last_uses.html.twig', $lastUsedElementsGetter->getViewParameters($page));
    }

    /**
     * Get the user last used elements for a type.
     *
     * @Route("/{type}/DernieresUtilisations/{page}", name="lastusesbytype", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function lastUsesByType(LastUsedElementsGetter $lastUsedElementsGetter, Type $type, int $page = 1): Response
    {
        return $this->render('user/user_element/last_uses_by_type.html.twig', $lastUsedElementsGetter->getViewParameters($page, $type));
    }

    /**
     * @Route("/Nationalites/{type}", name="origins")
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function origins(OriginStatistic $originStatistic, ?Type $type = null): Response
    {
        $viewParameters = [
            'mappemonde_series' => $originStatistic->getLastUsesMappemondeSeries($this->getUser(), $type),
        ];

        if (null !== $type) {
            $viewParameters['type'] = $type;
        }

        return $this->render('user/user_element/origins.html.twig', $viewParameters);
    }
}
