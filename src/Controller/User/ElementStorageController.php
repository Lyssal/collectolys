<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\User;

use App\Entity\User\ElementStorage;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The ElementStorage controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route("/EspaceRangement", name="user_elementstorage_")
 * @IsGranted("ROLE_ADMIN")
 */
final class ElementStorageController extends AbstractController
{
    /**
     * Show a storage.
     *
     * @Route("/{elementStorage}/{page}", name="show", methods={"GET"})
     */
    public function show(ViewParameter $viewParameter, ElementStorage $elementStorage, int $page = 1): Response
    {
        $parameters = $viewParameter->get([
            'element_storage' => $elementStorage,
        ], [], $page, false);

        return $this->render('user/element_storage/show.html.twig', $parameters);
    }
}
