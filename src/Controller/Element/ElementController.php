<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Element;

use App\Doctrine\Manager\Element\ElementManager;
use App\Doctrine\Manager\User\UserElementManager;
use App\Entity\Element\Element;
use App\Entity\Location;
use App\Entity\Platform;
use App\Entity\Support\Support;
use App\Entity\Type;
use App\Entity\User\ElementStorage;
use App\Exception\ImportException;
use App\Form\Type\Element\ImportType;
use App\Form\Type\ElementTypeFactory;
use App\Import\ElementImporter;
use App\Search\AutocompleteResponse;
use App\Search\Searcher;
use App\Search\ViewParameter;
use App\Security\Element\ElementVoter;
use Lyssal\Entity\Decorator\DecoratorManager;
use Lyssal\EntityBundle\Router\EntityRouter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * The Element controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="element_element_")
 * @IsGranted("ROLE_USER")
 */
final class ElementController extends AbstractController
{
    /**
     * Show an element.
     *
     * @Route("/Element/{slug}-{element}", name="show", methods={"GET", "POST"}, requirements={"slug": ".*"})
     */
    public function show(Element $element, DecoratorManager $decorator, UserElementManager $userElementManager): Response
    {
        $userElement = $userElementManager->findOneBy([
            'element' => $element,
            'user' => $this->getUser(),
        ]);

        return $this->render('element/element/show.html.twig', [
            'type' => $element->getType(),
            'element' => $decorator->get($element),
            'user_element' => $userElement,
        ]);
    }

    /**
     * Show an element menu in the element list.
     *
     * @Route("/Element/{element}/Menu", name="showmenu", methods={"GET"})
     */
    public function showMenu(Element $element, DecoratorManager $decorator, UserElementManager $userElementManager): Response
    {
        $userElement = $userElementManager->findOneBy([
            'element' => $element,
            'user' => $this->getUser(),
        ]);

        return $this->render('_sections/element/menu_content.html.twig', [
            'element' => $decorator->get($element),
            'user_element' => $userElement,
        ]);
    }

    /**
     * Add an element.
     *
     * @Route("/Element/{type}/Add", name="add", methods={"GET", "POST"})
     */
    public function add(Type $type, DecoratorManager $decorator, ElementManager $elementManager, Request $request): Response
    {
        $this->denyAccessUnlessGranted(ElementVoter::ADD);

        /** @var \App\Entity\Element\Element $element */
        $element = $elementManager->create([
            'type' => $type,
        ]);

        return $this->edit($element, $decorator, $elementManager, $request);
    }

    /**
     * Edit an element.
     *
     * @Route("/Element/{element}/Edition", name="edit", methods={"GET", "POST"})
     */
    public function edit(Element $element, DecoratorManager $decorator, ElementManager $elementManager, Request $request): Response
    {
        $this->denyAccessUnlessGranted(ElementVoter::EDIT, $element);

        $form = $this->createForm(ElementTypeFactory::getTypeClass($element), $element);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $elementManager->save($element);
                $this->addFlash('success', 'message.save.success');

                return $this->redirectToRoute('element_element_show', [
                    'element' => $element->getId(),
                    'slug' => $element->getSlug(),
                ]);
            }
            $this->addFlash('error', 'message.save.error');
        }

        return $this->render('element/element/edit.html.twig', [
            'type' => $element->getType(),
            'element' => $decorator->get($element),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Import elements.
     *
     * @Route("/Element/Import", name="import", methods={"GET", "POST"})
     */
    public function import(Request $request, TranslatorInterface $translator, ElementImporter $importer): Response
    {
        $this->denyAccessUnlessGranted(ElementVoter::ADD);

        $form = $this->createForm(ImportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $file = $form->get('file')->getData();
                try {
                    $lineCount = $importer->importe($file);
                    $this->addFlash('success', $translator->trans('message.import.save.success', [
                        '%line_count%' => $lineCount,
                    ]));

                    return $this->redirectToRoute('element_element_lastaddings');
                } catch (ImportException $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }

            $this->addFlash('error', 'message.save.error');
        }

        return $this->render('element/element/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Remove an element.
     *
     * @Route("/Element/{element}/Suppression", name="remove", methods={"POST"})
     */
    public function remove(Element $element, ElementManager $elementManager, EntityRouter $entityRouter): RedirectResponse
    {
        $this->denyAccessUnlessGranted(ElementVoter::DELETE, $element);

        $elementManager->delete($element);

        $this->addFlash('success', 'message.delete.success');

        return new RedirectResponse($entityRouter->generate($element->getType()));
    }

    /**
     * Search elements.
     *
     * @Route("/Recherche/{page}", name="search", methods={"GET"})
     */
    public function search(Request $request, Searcher $searcher, int $page = 1): Response
    {
        return $this->render('element/element/search.html.twig', $searcher->getViewParameters($request, $page));
    }

    /**
     * List elements for a type and a location.
     *
     * @Route("/{type}/Origine/{location}/{page}", name="listbytypeandorigin", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     * @ParamConverter("location", options={"mapping": {"location": "slug"}})
     */
    public function listByTypeAndOrigin(Type $type, Location $location, ViewParameter $viewParameter, int $page = 1)
    {
        return $this->render('element/element/listbytypeandlocation.html.twig', $viewParameter->get([
            'type' => $type,
            'origin' => $location,
        ], [], $page, false));
    }

    /**
     * List elements for a type and an year.
     *
     * @Route("/{type}/Annee/{year}/{page}", name="listbytypeandyear", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function listByTypeAndYear(Type $type, int $year, ViewParameter $viewParameter, int $page = 1)
    {
        return $this->render('element/element/listbytypeandyear.html.twig', $viewParameter->get([
            'type' => $type,
            'year' => $year,
        ], [], $page, false));
    }

    /**
     * List elements for a type and a platform.
     *
     * @Route("/{type}/Plateforme/{platform}/{page}", name="listbytypeandplatform", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     * @ParamConverter("platform", options={"mapping": {"platform": "slug"}})
     */
    public function listByTypeAndPlatform(Type $type, Platform $platform, ViewParameter $viewParameter, int $page = 1)
    {
        return $this->render('element/element/listbytypeandplatform.html.twig', $viewParameter->get([
            'type' => $type,
            'platform' => $platform,
        ], [], $page, false));
    }

    /**
     * List elements for a type and a support.
     *
     * @Route("/{type}/Support/{support}/{page}", name="listbytypeandsupport", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     * @ParamConverter("support", options={"mapping": {"support": "slug"}})
     */
    public function listByTypeAndSupport(Type $type, Support $support, ViewParameter $viewParameter, int $page = 1)
    {
        return $this->render('element/element/listbytypeandsupport.html.twig', $viewParameter->get([
            'type' => $type,
            'support' => $support,
        ], [], $page, false));
    }

    /**
     * List elements for a type and an element storage.
     *
     * @Route("/{type}/EspaceRangement/{elementStorage}/{page}", name="listbytypeandelementstorage", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function listByTypeAndElementStorage(Type $type, ElementStorage $elementStorage, ViewParameter $viewParameter, int $page = 1)
    {
        return $this->render('element/element/listbytypeandelementstorage.html.twig', $viewParameter->get([
            'type' => $type,
            'element_storage' => $elementStorage,
        ], [], $page, false));
    }

    /**
     * List by last IDs.
     *
     * @Route("/DerniersAjouts/{page}", name="lastaddings", methods={"GET"}, defaults={"type": null})
     * @Route("/{type}/DerniersAjouts/{page}", name="lastaddingsbytype", methods={"GET"})
     * @ParamConverter("type", options={"mapping": {"type": "slug"}})
     */
    public function lastAddings(?Type $type, ViewParameter $viewParameter, int $page = 1)
    {
        $viewParameters = [
            'set' => false,
        ];

        if (null !== $type) {
            $viewParameters['type'] = $type;
        }

        return $this->render('element/element/last_addings.html.twig', $viewParameter->get($viewParameters, ['element.id' => 'DESC'], $page, false));
    }

    /**
     * The autocomplete response.
     *
     * @Route("/Element/Autocomplete", name="autocomplete", methods={"GET"})
     */
    public function autocomplete(AutocompleteResponse $autocompleteResponse, Request $request): JsonResponse
    {
        return new JsonResponse($autocompleteResponse->getElementJson($request));
    }
}
