<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Element;

use App\Custom\FourmisRouges;
use App\Doctrine\Manager\TypeManager;
use App\Entity\Type;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The controller for custom lists.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="element_custom_")
 * @IsGranted("ROLE_USER")
 */
final class CustomController extends AbstractController
{
    /**
     * The movies in black & white.
     *
     * @Route("/Videotheque/FilmsNoirEtBlanc/{page}", name="blackandwhitevideos", methods={"GET"})
     */
    public function blackAndWhiteVideos(TypeManager $typeManager, ViewParameter $viewParameter, int $page = 1)
    {
        $parameters = $viewParameter->get([
            'type' => $typeManager->findOneById(Type::VIDEO),
            'black_and_white' => true,
        ], [], $page, false);

        return $this->render('element/custom/blackandwhitevideos.html.twig', $parameters);
    }

    /**
     * Les fourmis rouges.
     *
     * @Route("/FourmisRouges/{year}", name="fourmisrouges", defaults={"year": null}, methods={"GET"}, requirements={"year": "\d{4}"})
     */
    public function fourmisRouges(TypeManager $typeManager, ?int $year, FourmisRouges $fourmisRouges)
    {
        $parameters = [
            'type' => $typeManager->findOneById(Type::VIDEO_GAME),
            'years' => $fourmisRouges->getYears(),
            'aggregate_by_set' => false,
        ];

        if (null !== $year) {
            $parameters['year'] = $year;
            $parameters['elements'] = $fourmisRouges->getByYear($year);
        }

        return $this->render('element/custom/fourmisrouges.html.twig', $parameters);
    }

    /**
     * Les oubliés des fourmis rouges.
     *
     * @Route("/FourmisRouges/Oublies", name="fourmisrougesoublies", methods={"GET"})
     */
    public function fourmisRougesOublies(TypeManager $typeManager, FourmisRouges $fourmisRouges)
    {
        return $this->render('element/custom/fourmisrougesoublies.html.twig', [
            'type' => $typeManager->findOneById(Type::VIDEO_GAME),
            'elements' => $fourmisRouges->getOublies(),
            'aggregate_by_set' => false,
        ]);
    }
}
