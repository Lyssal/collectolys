<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Element;

use App\Entity\Element\Element;
use App\Search\AutocompleteResponse;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Set controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="element_set_")
 * @IsGranted("ROLE_USER")
 */
final class SetController extends AbstractController
{
    /**
     * Show the set.
     *
     * @Route("/Serie/{slug}-{set}/{page}", name="show", methods={"GET"})
     */
    public function show(ViewParameter $viewParameter, Element $set, int $page = 1): Response
    {
        if (!$set->isSet()) {
            throw $this->createNotFoundException('The element "'.$set.'" is not a set.');
        }

        $parameters = $viewParameter->get([
            'element_set' => $set,
        ], [
            'element.setSpecialIssue' => 'ASC',
            'element.positionInSet' => 'ASC',
            'element.numberInSet' => 'ASC',
            'date' => 'ASC',
        ], $page, false);

        $parameters['type'] = $set->getType();

        return $this->render('element/set/show.html.twig', $parameters);
    }

    /**
     * The autocomplete response.
     *
     * @Route("/set/autocomplete", name="autocomplete", methods={"GET"})
     */
    public function autocomplete(AutocompleteResponse $autocompleteResponse, Request $request): JsonResponse
    {
        return new JsonResponse($autocompleteResponse->getSetJson($request));
    }

    /**
     * The tooltip HTML.
     *
     * @Route("/set/{set}/tooltip", name="tooltip", methods={"GET"})
     */
    public function tooltip(Element $set): Response
    {
        return $this->render('element/set/tooltip.html.twig', [
            'set' => $set,
        ]);
    }
}
