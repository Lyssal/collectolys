<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Element\Type;

use App\Doctrine\Manager\TypeManager;
use App\Entity\Element\Element;
use App\Entity\Type;
use App\Enum\VideoTypeEnum;
use App\Search\ViewParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The Video controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="element_element_type_video_")
 *
 * @IsGranted("ROLE_USER")
 */
final class VideoController extends AbstractController
{
    /**
     * List elements for a video type.
     *
     * @Route("/VideoType/{videoType}/{page}", name="videotype", methods={"GET"})
     */
    public function videoType(TypeManager $typeManager, ViewParameter $viewParameter, string $videoType, int $page = 1)
    {
        $parameters = array_merge(
            [
                'video_type_label' => VideoTypeEnum::getLabel($videoType),
            ],
            $viewParameter->get(
                [
                    'type' => $typeManager->findOneById(Type::VIDEO),
                    'videoType' => $videoType,
                ],
                [],
                $page,
                false
            )
        );

        return $this->render('element/element/type/video/videotype.html.twig', $parameters);
    }
}
