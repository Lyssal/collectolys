<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Controller\Element;

use App\Element\SpecialSetElementsGetter;
use App\Entity\Element\SpecialSet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * The SpecialSet controller.
 *
 * @category Controller
 *
 * @author    Rémi Leclerc
 * @copyright 2020 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @Route(name="element_specialset_")
 * @IsGranted("ROLE_USER")
 */
final class SpecialSetController extends AbstractController
{
    /**
     * Show the special set.
     *
     * @Route("/SerieSpeciale/{specialSet}", name="show", methods={"GET"})
     * @ParamConverter("specialSet", options={"mapping": {"specialSet": "slug"}})
     */
    public function show(SpecialSetElementsGetter $specialSetElementsGetter, SpecialSet $specialSet): Response
    {
        return $this->render('element/special_set/show.html.twig', [
            'type' => $specialSet->getType(),
            'special_set' => $specialSet,
            'elements' => $specialSetElementsGetter->get($specialSet),
            'aggregate_by_set' => false,
        ]);
    }
}
