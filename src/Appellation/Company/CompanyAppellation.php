<?php

namespace App\Appellation\Company;

use App\Entity\Company\Company;
use Lyssal\EntityBundle\Appellation\AbstractDefaultAppellation;
use Lyssal\EntityBundle\Router\EntityRouterManager;
use Symfony\Component\Routing\RouterInterface;

final class CompanyAppellation extends AbstractDefaultAppellation
{
    public function __construct(EntityRouterManager $entityRouterManager, private RouterInterface $router)
    {
        parent::__construct($entityRouterManager);
    }

    public function supports($object): bool
    {
        return $object instanceof Company;
    }

    /**
     * @param Company $object
     */
    public function appellationHtml($object)
    {
        $url =  $this->entityRouterManager->generate($object);

        if (null !== $url) {
            return '<span data-tooltip-url="'.$this->router->generate('company_company_tooltip', ['company' => $object->getId()]).'" data-tooltip data-allow-html="true" title="'.htmlspecialchars('<div class="spinner"></div>').'"><a href="'.$url.'">'.$this->appellation($object).'</a></span>';
        }

        return parent::appellationHtml($object);
    }
}
