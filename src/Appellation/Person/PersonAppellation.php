<?php

namespace App\Appellation\Person;

use App\Entity\Person\Person;
use Lyssal\EntityBundle\Appellation\AbstractDefaultAppellation;
use Lyssal\EntityBundle\Router\EntityRouterManager;
use Symfony\Component\Routing\RouterInterface;

final class PersonAppellation extends AbstractDefaultAppellation
{
    public function __construct(EntityRouterManager $entityRouterManager, private RouterInterface $router)
    {
        parent::__construct($entityRouterManager);
    }

    public function supports($object): bool
    {
        return $object instanceof Person;
    }

    /**
     * @param Person $object
     */
    public function appellationHtml($object)
    {
        $url =  $this->entityRouterManager->generate($object);

        if (null !== $url) {
            return '<span data-tooltip-url="'.$this->router->generate('person_person_tooltip', ['person' => $object->getId()]).'" data-tooltip data-allow-html="true" title="'.htmlspecialchars('<div class="spinner"></div>').'"><a href="'.$url.'">'.$this->appellation($object).'</a></span>';
        }

        return parent::appellationHtml($object);
    }
}
