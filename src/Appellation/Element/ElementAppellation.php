<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Appellation\Element;

use App\Doctrine\Decorator\Element\ElementDecorator;
use App\Entity\Element\Element;
use Lyssal\EntityBundle\Appellation\AbstractDefaultAppellation;
use Lyssal\EntityBundle\Router\EntityRouterManager;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class ElementAppellation extends AbstractDefaultAppellation
{
    public function __construct(private TranslatorInterface $translator, private RouterInterface $router, EntityRouterManager $entityRouterManager)
    {
        parent::__construct($entityRouterManager);
    }

    public function supports($object)
    {
        return $object instanceof Element || $object instanceof ElementDecorator;
    }

    /**
     * @param Element|ElementDecorator $object
     */
    public function appellation($object)
    {
        // To avoid to call the ElementDecorator __call method.
        $element = $object instanceof ElementDecorator ? $object->getEntity() : $object;
        $name = '';
        $elementSet = $element->getElementSet();
        $names = $element->getNames();

        if (null !== $elementSet && $elementSet->isSetDisplayInElementTitle()) {
            $name = $elementSet;

            if (null !== $element->getNumberInSet()) {
                if (null !== $elementSet->getSetElementName()) {
                    $name .= ' : '.$elementSet->getSetElementName();
                }
            }

            if ($element->isSetSpecialIssue()) {
                $name .= ' '.$this->translator->trans('special_issue');
            }

            if (null !== $element->getNumberInSet()) {
                $name .= ' '.$element->getNumberInSet();
            }

            if (\count($names) > 0) {
                $name .= ' : ';
            }
        }

        if (\count($names) > 0) {
            /**
             * @var \App\Entity\Element\ElementName $elementName
             */
            $elementName = $names->first();

            $name .= $elementName.(null !== $elementName->getSubname() ? ' - '.$elementName->getSubname() : '');
        }

        return $name;
    }

    /**
     * @param Element|ElementDecorator $object
     */
    public function appellationHtml($object)
    {
        if ($object->isSet()) {
            return '<span data-tooltip-url="'.$this->router->generate('element_set_tooltip', ['set' => $object->getId()]).'" data-tooltip data-allow-html="true" title="'.htmlspecialchars('<div class="spinner"></div>').'">'.parent::appellationHtml($object).'</span>';
        }

        return $this->appellation($object);
    }
}
