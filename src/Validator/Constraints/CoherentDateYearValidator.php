<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validate that the element year is same than date.
 *
 * @category Validator
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class CoherentDateYearValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @param \App\Entity\Element\ElementDate $value
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CoherentDateYear) {
            throw new UnexpectedTypeException($constraint, CoherentDateYear::class);
        }

        if (null !== $value->getDate()) {
            $dateYear = (int) $value->getDate()->format('Y');

            if ($dateYear !== $value->getYear()) {
                $this->context->buildViolation($constraint->message)->atPath('year')->addViolation();
            }
        }
    }
}
