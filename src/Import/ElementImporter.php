<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Import;

use App\Element\ElementSearcher;
use App\Entity\Company\Company;
use App\Entity\Company\CompanyRole;
use App\Entity\Currency;
use App\Entity\Element\Element;
use App\Entity\Element\ElementCompany;
use App\Entity\Element\ElementDate;
use App\Entity\Element\ElementName;
use App\Entity\Element\ElementPerson;
use App\Entity\Element\ElementPrice;
use App\Entity\Genre;
use App\Entity\Language;
use App\Entity\Location;
use App\Entity\Person\Person;
use App\Entity\Person\PersonRole;
use App\Entity\Support\Support;
use App\Entity\Type;
use App\Entity\Universe;
use App\Entity\User\ElementStorage;
use App\Entity\User\ElementSupport;
use App\Entity\User\UserElement;
use App\Exception\ImportException;
use DateTime;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Lyssal\Dsv\Csv;
use Lyssal\Text\Slug;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Security;

/**
 * The element importer.
 *
 * @category Import
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementImporter
{
    /**
     * The column count.
     *
     * @var int
     */
    const COL_COUNT = 33;

    /**
     * The security service.
     *
     * @var \Symfony\Component\Security\Core\Security
     */
    private $security;

    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    private $entityAdministratorManager;

    /**
     * The element searcher.
     *
     * @var \App\Element\ElementSearcher
     */
    private $elementSearcher;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Security\Core\Security                     $security                   The security service
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     */
    public function __construct(Security $security, EntityAdministratorManager $entityAdministratorManager, ElementSearcher $elementSearcher)
    {
        $this->security = $security;
        $this->entityAdministratorManager = $entityAdministratorManager;
        $this->elementSearcher = $elementSearcher;
    }

    /**
     * Import the CSV.
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file The uploaded file
     *
     * @throws \App\Exception\ImportException If the CSV file is invalid
     * @throws \Lyssal\Exception\IoException  If an error occured during the file reading
     *
     * @return int The saved element count
     */
    public function importe(UploadedFile $file): int
    {
        /** @var \App\Doctrine\Manager\LocationManager $locationManager */
        $locationManager = $this->entityAdministratorManager->get(Location::class);
        /** @var \App\Doctrine\Manager\LanguageManager $languageManager */
        $languageManager = $this->entityAdministratorManager->get(Language::class);
        /** @var \App\Doctrine\Manager\CurrencyManager $currencyManager */
        $currencyManager = $this->entityAdministratorManager->get(Currency::class);
        /** @var \App\Doctrine\Manager\Element\ElementManager $elementManager */
        $elementManager = $this->entityAdministratorManager->get(Element::class);
        /** @var \App\Doctrine\Manager\GenreManager $genreManager */
        $genreManager = $this->entityAdministratorManager->get(Genre::class);
        /** @var \App\Doctrine\Manager\Company\CompanyManager $companyManager */
        $companyManager = $this->entityAdministratorManager->get(Company::class);
        /** @var \App\Doctrine\Manager\Company\CompanyRoleManager $companyRoleManager */
        $companyRoleManager = $this->entityAdministratorManager->get(CompanyRole::class);
        /** @var \App\Doctrine\Manager\Person\PersonManager $personManager */
        $personManager = $this->entityAdministratorManager->get(Person::class);
        /** @var \App\Doctrine\Manager\Person\PersonRoleManager $personRoleManager */
        $personRoleManager = $this->entityAdministratorManager->get(PersonRole::class);
        /** @var \App\Doctrine\Manager\User\ElementStorageManager $userElementStorageManager */
        $userElementStorageManager = $this->entityAdministratorManager->get(ElementStorage::class);
        /** @var \App\Doctrine\Manager\Support\SupportManager $supportManager */
        $supportManager = $this->entityAdministratorManager->get(Support::class);

        /** @var \App\Entity\User\User $user */
        $user = $this->security->getUser();
        $francais = $languageManager->findOneById(1);
        $row = 2;

        $csv = new Csv($file->getPathname());
        $csv->import(true);

        foreach ($csv->getLines() as $line) {
            if (self::COL_COUNT !== \count($line)) {
                throw new ImportException('Line '.$row.': Column count does not match ('.self::COL_COUNT.' expected).');
            }

            $col = 0;
            $typeName = $line[$col];
            $elementNameFr = $line[++$col];
            $descriptionFr = ('' !== $line[++$col] ? $line[$col] : null);
            $contentFr = ('' !== $line[++$col] ? $line[$col] : null);
            $setName = $line[++$col];
            $numberInSet = ('' !== $line[++$col] ? $line[$col] : null);
            $positionInSet = ('' !== $line[++$col] ? (int) $line[$col] : null);
            $universeName1 = $line[++$col];
            $genreName1 = $line[++$col];
            $genreName2 = $line[++$col];
            $originName1 = $line[++$col];
            $date1LocationName = $line[++$col];
            $date1DateCompleteMysql = $line[++$col];
            $date1Year = $line[++$col];
            $price1LocationName = $line[++$col];
            $price1CurrencyName = $line[++$col];
            $price1Value = (float) str_replace(',', '.', $line[++$col]);
            $company1RoleName = $line[++$col];
            $company1Name = $line[++$col];
            $company2RoleName = $line[++$col];
            $company2Name = $line[++$col];
            $person1RoleName = $line[++$col];
            $person1FirstName = $line[++$col];
            $person1LastName = $line[++$col];
            $person1Details = $line[++$col];
            $userElementSupport1StorageName = $line[++$col];
            $userElementSupport1SupportName = $line[++$col];
            $userElementSupport1Version = $line[++$col];
            $userElementSupport1Comment = $line[++$col];
            $userElementSupport2StorageName = $line[++$col];
            $userElementSupport2SupportName = $line[++$col];
            $userElementSupport2Version = $line[++$col];
            $userElementSupport2Comment = $line[++$col];

            // Type
            $type = $this->entityAdministratorManager->get(Type::class)->findOneBy(['name' => $typeName]);
            if (null === $type) {
                throw new ImportException('Line '.$row.': Type "'.$typeName.'" unfound.');
            }

            // Set
            $elementSet = null;
            if ('' !== $setName) {
                $setQueryBuilder = $this
                    ->elementSearcher
                    ->getSearchQueryBuilder(
                        null,
                        [
                            'set' => true,
                            'main_name' => $setName,
                        ]
                    )
                ;
                $elementSet = $setQueryBuilder
                    ->select($setQueryBuilder->getRootAliases()[0])
                    ->resetDQLPart('groupBy')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult()
                ;

                if (null === $elementSet) {
                    throw new ImportException('Line '.$row.': Set "'.$setName.'" unfound.');
                }
            }

            // Universe
            $universe1 = null;
            if ('' !== $universeName1) {
                $universe1 = $this->entityAdministratorManager->get(Universe::class)->findOneBy(['name' => $universeName1]);
                if (null === $universe1) {
                    throw new ImportException('Line '.$row.': Universe "'.$universeName1.'" unfound.');
                }
            }

            // Genres
            $genre1 = null;
            if ('' !== $genreName1) {
                $genre1 = $genreManager->findOneBy(['name' => $genreName1, 'type' => $type]);
                if (null === $genre1) {
                    throw new ImportException('Line '.$row.': Genre "'.$genreName1.'" unfound.');
                }
            }

            $genre2 = null;
            if ('' !== $genreName2) {
                $genre2 = $genreManager->findOneBy(['name' => $genreName2, 'type' => $type]);
                if (null === $genre2) {
                    throw new ImportException('Line '.$row.': Genre "'.$genreName2.'" unfound.');
                }
            }

            // Origin
            $origin1 = null;
            if ('' !== $originName1) {
                $origin1 = $locationManager->findOneBy(['name' => $originName1]);
                if (null === $origin1) {
                    throw new ImportException('Line '.$row.': Origin "'.$originName1.'" unfound.');
                }
            }

            // Date
            $elementDate1 = null;
            if ('' !== $date1LocationName) {
                $date1Location = $locationManager->findOneBy(['name' => $date1LocationName]);
                if (null === $date1Location) {
                    throw new ImportException('Line '.$row.': Year of date "'.$date1LocationName.'" unfound.');
                }

                $date1DateComplete = null;
                if ('' !== $date1DateCompleteMysql) {
                    $date1DateComplete = new DateTime($date1DateCompleteMysql);
                    if ('' === $date1Year) {
                        $date1Year = $date1DateComplete->format('Y');
                    }
                }

                if ('' === $date1Year) {
                    throw new ImportException('Line '.$row.': Year for date 1 required.');
                }

                $elementDate1 = new ElementDate();
                $elementDate1->setLocation($date1Location);
                $elementDate1->setYear((int) $date1Year);
                $elementDate1->setDate($date1DateComplete);
            }

            // Price
            $elementPrice1 = null;
            if ('' !== $price1LocationName) {
                $price1Location = $locationManager->findOneBy(['name' => $price1LocationName]);

                if (null === $price1Location) {
                    throw new ImportException('Line '.$row.': Year of price 1 "'.$price1LocationName.'" unfound.');
                }

                $price1Currency = $currencyManager->findOneBy(['code' => $price1CurrencyName]);
                if (null === $price1Currency) {
                    throw new ImportException('Line '.$row.': Price currency 1 "'.$price1CurrencyName.'" unfound.');
                }

                if ('' === $price1Value) {
                    throw new ImportException('Line '.$row.': Price value 1 required.');
                }

                $elementPrice1 = new ElementPrice();
                $elementPrice1->setLocation($price1Location);
                $elementPrice1->setCurrency($price1Currency);
                $elementPrice1->setPrice((float) str_replace(',', '.', $price1Value));
            }

            // Companies
            $elementCompany1 = null;
            if ('' !== $company1RoleName && '' !== $company1Name) {
                $company1Role = $companyRoleManager->findOneBy(['name' => $company1RoleName]);
                if (null === $company1Role) {
                    throw new ImportException('Line '.$row.': Company role 1 "'.$company1RoleName.'" unfound.');
                }

                $company1 = $companyManager->findOneBy(['name' => $company1Name]);
                if (null === $company1) {
                    throw new ImportException('Line '.$row.': Company 1 "'.$company1Name.'" unfound.');
                }

                $elementCompany1 = new ElementCompany();
                $elementCompany1->setCompany($company1);
                $elementCompany1->setRole($company1Role);
            }

            $elementCompany2 = null;
            if ('' !== $company2RoleName && '' !== $company2Name) {
                $company2Role = $companyRoleManager->findOneBy(['name' => $company2RoleName]);
                if (null === $company2Role) {
                    throw new ImportException('Line '.$row.': Company role 2 "'.$company2RoleName.'" unfound.');
                }

                $company2 = $companyManager->findOneBy(['name' => $company2Name]);
                if (null === $company2) {
                    throw new ImportException('Line '.$row.': Company 2 "'.$company2Name.'" unfound.');
                }

                $elementCompany2 = new ElementCompany();
                $elementCompany2->setCompany($company2);
                $elementCompany2->setRole($company2Role);
            }

            // Person
            $elementPerson1 = null;
            if ('' !== $person1RoleName && '' !== $person1FirstName) {
                $person1Role = $personRoleManager->findOneBy(['name' => $person1RoleName, 'type' => $type]);
                if (null === $person1Role) {
                    throw new ImportException('Line '.$row.': Person role 1 "'.$person1RoleName.'" unfound.');
                }

                $person1Slug = new Slug($person1FirstName.' '.$person1LastName);
                $person1 = $personManager->findOneBy(['slug' => $person1Slug->minify('_', false)->getText()]);
                if (null === $person1) {
                    $person1 = $personManager->create([
                        'firstName' => $person1FirstName,
                        'lastName' => $person1LastName,
                    ]);
                }

                $elementPerson1 = new ElementPerson();
                $elementPerson1->setRole($person1Role);
                $elementPerson1->setPerson($person1);
                $elementPerson1->setDetails('' !== $person1Details ? $person1Details : null);
                $elementPerson1->setPosition(0);
            }

            // User elements
            $elementSupport1 = null;
            $elementSupportClass = ElementSupport::getClassByType($type);
            if ('' !== $userElementSupport1StorageName || '' !== $userElementSupport1SupportName) {
                $userElementSupport1Storage = null;
                if ('' !== $userElementSupport1StorageName) {
                    $userElementSupport1Storage = $userElementStorageManager->findOneBy(['name' => $userElementSupport1StorageName]);
                    if (null === $userElementSupport1Storage) {
                        throw new ImportException('Line '.$row.': Support 1 - User storage "'.$userElementSupport1StorageName.'" unfound.');
                    }
                }

                $userElementSupport1Support = null;
                if ('' !== $userElementSupport1SupportName) {
                    $userElementSupport1Support = $supportManager->findOneBy(['name' => $userElementSupport1SupportName]);
                    if (null === $userElementSupport1Support) {
                        throw new ImportException('Line '.$row.': Support 1 - Support "'.$userElementSupport1SupportName.'" unfound.');
                    }
                }

                $elementSupport1 = new $elementSupportClass();
                $elementSupport1->setStorage($userElementSupport1Storage);
                $elementSupport1->setSupport($userElementSupport1Support);
                $elementSupport1->setVersion('' !== $userElementSupport1Version ? $userElementSupport1Version : null);
                $elementSupport1->setComment('' !== $userElementSupport1Comment ? $userElementSupport1Comment : null);
            }

            $elementSupport2 = null;
            if ('' !== $userElementSupport2StorageName || '' !== $userElementSupport2SupportName) {
                $userElementSupport2Storage = null;
                if ('' !== $userElementSupport2StorageName) {
                    $userElementSupport2Storage = $userElementStorageManager->findOneBy(['name' => $userElementSupport2StorageName]);
                    if (null === $userElementSupport2Storage) {
                        throw new ImportException('Line '.$row.': Support 2 - User storage "'.$userElementSupport2StorageName.'" unfound.');
                    }
                }

                $userElementSupport2Support = null;
                if ('' !== $userElementSupport2SupportName) {
                    $userElementSupport2Support = $supportManager->findOneBy(['name' => $userElementSupport2SupportName]);
                    if (null === $userElementSupport2Support) {
                        throw new ImportException('Line '.$row.': Support 2 - Support "'.$userElementSupport2SupportName.'" unfound.');
                    }
                }

                $elementSupport2 = new $elementSupportClass();
                $elementSupport2->setStorage($userElementSupport2Storage);
                $elementSupport2->setSupport($userElementSupport2Support);
                $elementSupport2->setVersion('' !== $userElementSupport2Version ? $userElementSupport2Version : null);
                $elementSupport2->setComment('' !== $userElementSupport2Comment ? $userElementSupport2Comment : null);
            }

            $elementClass = Element::getClassByType($type);
            /** @var \App\Entity\Element\Element $element */
            $element = new $elementClass();
            $element->setType($type);
            if (!empty($elementNameFr)) {
                $elementName = new ElementName();
                $elementName->setName($elementNameFr);
                $elementName->setLanguage($francais);
                $element->addName($elementName);
            }
            $element->setDescription($descriptionFr);
            $element->setContent($contentFr);
            $element->setElementSet($elementSet);
            $element->setNumberInSet($numberInSet);
            $element->setPositionInSet($positionInSet);
            if (null !== $universe1) {
                $element->addUniverse($universe1);
            }
            if (null !== $genre1) {
                $element->addGenre($genre1);
            }
            if (null !== $genre2) {
                $element->addGenre($genre2);
            }
            if (null !== $origin1) {
                $element->addOrigin($origin1);
            }
            if (null !== $elementDate1) {
                $element->addDate($elementDate1);
            }
            if (null !== $elementPrice1) {
                $element->addPrice($elementPrice1);
            }
            if (null !== $elementCompany1) {
                $element->addCompany($elementCompany1);
            }
            if (null !== $elementCompany2) {
                $element->addCompany($elementCompany2);
            }
            if (null !== $elementPerson1) {
                $element->addPerson($elementPerson1);
            }

            $elementManager->save($element);

            if (null !== $elementSupport1) {
                $userElement = new UserElement();
                $userElement->setUser($user);
                $userElement->setElement($element);
                $userElement->addSupport($elementSupport1);
                if (null !== $elementSupport2) {
                    $userElement->addSupport($elementSupport2);
                }

                $elementManager->save($userElement);
            }

            ++$row;
        }

        return $row;
    }
}
