<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Statistic;

use App\Entity\User\ElementSupport;

/**
 * The web source domains.
 *
 * @category Statistic
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class WebSourceStatistic extends AbstractStatistic
{
    /**
     * The domain count minimum.
     *
     * @var int
     */
    const DOMAIN_COUNT_MIN = 10;

    /**
     * Get the config chart.
     *
     * @return array The config
     */
    public function getConfig(): array
    {
        $results = $this->getBestDomains();
        $this->removeCountLessThanMinPercentage($results);

        return [
            'type' => 'doughnut',
            'data' => [
                'datasets' => [[
                    'data' => $this->getValuesByKey('count', $results),
                    'backgroundColor' => $this->getDefaultColors(\count($results)),
                    'borderWidth' => 0,
                ]],
                'labels' => $this->getValuesByKey('name', $results),
            ],
            'options' => $this->getConfigOptions(),
        ];
    }

    /**
     * Get the domains with most of count.
     *
     * @return array The results
     */
    public function getBestDomains(): array
    {
        /**
         * @var \App\Doctrine\Repository\User\ElementSupportRepository $elementSupportRepository
         */
        $elementSupportRepository = $this->entityAdministratorManager->get(ElementSupport::class)->getRepository();

        return $elementSupportRepository->getWebDomainSourceCounts(self::DOMAIN_COUNT_MIN);
    }
}
