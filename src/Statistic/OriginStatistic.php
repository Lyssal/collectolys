<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Statistic;

use App\Entity\Location;
use App\Entity\Type;
use App\Entity\User\User;
use Lyssal\Doctrine\Orm\QueryBuilder;

/**
 * The element origins.
 *
 * @category Statistic
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class OriginStatistic extends AbstractStatistic
{
    /**
     * Get the origins by type with most of count.
     *
     * @param int|null $maxCount The max element count
     *
     * @return array The results
     */
    public function getBestLocations(?Type $type = null, ?int $maxCount = null): array
    {
        $locationAdministrator = $this->entityAdministratorManager->get(Location::class);
        $typeResults = $locationAdministrator->getRepository()->getOriginElementCounts($type);

        if (null !== $maxCount) {
            return \array_slice($typeResults, 0, $maxCount);
        }

        return $typeResults;
    }

    public function getMappemondeSeries(?Type $type = null): array
    {
        $locationAdministrator = $this->entityAdministratorManager->get(Location::class);

        return $this->formatOriginElementCountsForVectormap($locationAdministrator->getRepository()->getOriginElementCounts($type));
    }

    public function getLastUsesMappemondeSeries(User $user, ?Type $type = null): array
    {
        /**
         * @var \App\Doctrine\Repository\LocationRepository $locationRepository
         */
        $locationRepository = $this->entityAdministratorManager->get(Location::class)->getRepository();

        return $this->formatOriginElementCountsForVectormap($locationRepository->getOriginLastUsedElementCounts($user, $type));
    }

    private function formatOriginElementCountsForVectormap(array $counts): array
    {
        $locationAdministrator = $this->entityAdministratorManager->get(Location::class);
        $locations = $locationAdministrator->findBy([
            QueryBuilder::WHERE_NOT_NULL => 'code',
        ]);
        $series = [];

        foreach ($locations as $location) {
            $series[$location->getCode()] = [
                'name' => (string) $location,
                'count' => 0,
            ];
        }

        foreach ($counts as $count) {
            $code = $count['code'];

            if (null === $code) {
                continue;
            }

            if (isset($series[$code])) {
                $series[$code]['count'] += $count['count'];
            }
        }

        return $series;
    }
}
