<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Statistic;

use App\Entity\Platform;

/**
 * The video game platforms.
 *
 * @category Statistic
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class VideoGamePlatformStatistic extends AbstractStatistic
{
    /**
     * Get the config chart.
     *
     * @return array The config
     */
    public function getConfig(): array
    {
        $results = $this->getBestPlatforms();
        $this->removeCountLessThanMinPercentage($results);

        return [
            'type' => 'doughnut',
            'data' => [
                'datasets' => [[
                    'data' => $this->getValuesByKey('count', $results),
                    'backgroundColor' => $this->getDefaultColors(\count($results)),
                    'borderWidth' => 0,
                ]],
                'labels' => $this->getValuesByKey('name', $results),
            ],
            'options' => $this->getConfigOptions(),
        ];
    }

    /**
     * Get the platforms with most of count.
     *
     * @return array The results
     */
    public function getBestPlatforms(): array
    {
        /** @var \App\Doctrine\Manager\PlatformManager $platformManager */
        $platformManager = $this->entityAdministratorManager->get(Platform::class);

        return $platformManager->getRepository()->getVideoGamePlatformElementCounts();
    }
}
