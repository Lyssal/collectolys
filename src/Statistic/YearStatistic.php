<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Statistic;

use App\Entity\Element\ElementDate;
use App\Entity\Type;

/**
 * The years.
 *
 * @category Statistic
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class YearStatistic extends AbstractStatistic
{
    /**
     * The min year to display.
     *
     * @var int
     */
    const MIN_YEAR = 1900;

    /**
     * Get the config chart.
     *
     * @return array The config
     */
    public function getConfig(): array
    {
        /** @var \App\Doctrine\Manager\TypeManager $typeManager */
        $typeManager = $this->entityAdministratorManager->get(Type::class);
        $elementDateManager = $this->entityAdministratorManager->get(ElementDate::class);

        $types = $typeManager->findAll();
        $results = $elementDateManager->getRepository()->getYearElementCounts();
        $minYear = max(self::MIN_YEAR, $this->getMinValue('year', $results));

        $currentYear = (int) date('Y');
        $labels = range($minYear, $currentYear);
        $datasets = [];

        foreach ($types as $type) {
            $typeResults = $this->filterByValue('type_id', $type->getId(), $results);

            if (empty($typeResults)) {
                break;
            }

            $this->fillEmptyYearsInResults($typeResults, $type, $minYear);

            $datasets[] = [
                'label' => (string) $type,
                'backgroundColor' => $type->getColor(),
                'borderColor' => $type->getColor(),
                'data' => $this->getValuesByKey('count', $typeResults),
                'fill' => false,
            ];
        }

        return [
            'type' => 'line',
            'data' => [
                'labels' => $labels,
                'datasets' => $datasets,
            ],
            'options' => $this->getConfigOptions(),
        ];
    }

    /**
     * Add in the array the year without element.
     *
     * @param array            $results The result array
     * @param \App\Entity\Type $type    The type
     * @param int              $minYear The min year
     */
    private function fillEmptyYearsInResults(array &$results, Type $type, int $minYear): void
    {
        $years = $this->getValuesByKey('year', $results);
        $currentYear = (int) date('Y');

        // We fill the empty years
        for ($i = $minYear; $i <= $currentYear; ++$i) {
            if (!\in_array($i, $years, true)) {
                $results[] = [
                    'type_id' => $type->getId(),
                    'year' => $i,
                    'count' => 0,
                ];
            }
        }

        // Sort by year ASC
        usort($results, function (array $valueArray1, array $valueArray2) {
            return $valueArray1['year'] <=> $valueArray2['year'];
        });
    }

    /**
     * Get the years by type with most of count.
     *
     * @param int $maxCount The max element count
     *
     * @return array The results
     */
    public function getBestYears(int $maxCount): array
    {
        /** @var \App\Doctrine\Manager\TypeManager $typeManager */
        $typeManager = $this->entityAdministratorManager->get(Type::class);
        $elementDateManager = $this->entityAdministratorManager->get(ElementDate::class);

        $types = $typeManager->findAll();
        $results = $elementDateManager->getRepository()->getYearElementCounts();
        $bestYears = [];

        foreach ($types as $type) {
            $typeResults = $this->filterByValue('type_id', $type->getId(), $results);

            if (0 === \count($typeResults)) {
                break;
            }

            usort($typeResults, function (array $valueArray1, array $valueArray2) {
                return $valueArray2['count'] <=> $valueArray1['count'];
            });

            $bestYears[] = [
                'type' => $type,
                'years' => \array_slice($typeResults, 0, $maxCount),
            ];
        }

        return $bestYears;
    }
}
