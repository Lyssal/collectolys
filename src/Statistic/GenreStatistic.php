<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Statistic;

use App\Entity\Genre;
use App\Entity\Type;

/**
 * The genres.
 *
 * @category Statistic
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class GenreStatistic extends AbstractStatistic
{
    /**
     * Get the config chart.
     *
     * @return array The config
     */
    public function getConfigs(): array
    {
        $types = $this->entityAdministratorManager->get(Type::class)->findAll();
        /** @var \App\Doctrine\Manager\GenreManager $genreManager */
        $genreManager = $this->entityAdministratorManager->get(Genre::class);
        $results = $genreManager->getRepository()->getGenreElementCounts();
        $configs = [];

        foreach ($types as $type) {
            $typeResults = $this->filterByValue('type_id', $type->getId(), $results);
            $this->removeCountLessThanMinPercentage($typeResults);

            $options = $this->getConfigOptions();
            $options['title'] = array_merge($options['title'], [
                'text' => (string) $type,
            ]);

            $configs[$type->getId()] = [
                'type' => 'doughnut',
                'data' => [
                    'datasets' => [[
                        'data' => $this->getValuesByKey('count', $typeResults),
                        'backgroundColor' => $this->getDefaultColors(\count($typeResults)),
                        'borderWidth' => 0,
                    ]],
                    'labels' => $this->getValuesByKey('name', $typeResults),
                ],
                'options' => $options,
            ];
        }

        return $configs;
    }

    /**
     * Get the genres by type with most of count.
     *
     * @param int $maxCount The max element count
     *
     * @return array The results
     */
    public function getBestGenres(int $maxCount): array
    {
        /** @var \App\Doctrine\Manager\TypeManager $typeManager */
        $typeManager = $this->entityAdministratorManager->get(Type::class);
        $genreManager = $this->entityAdministratorManager->get(Genre::class);

        $types = $typeManager->findAll();
        $results = $genreManager->getRepository()->getGenreElementCounts();
        $bestGenres = [];

        foreach ($types as $type) {
            $typeResults = $this->filterByValue('type_id', $type->getId(), $results);

            if (0 === \count($typeResults)) {
                break;
            }

            usort($typeResults, function (array $valueArray1, array $valueArray2) {
                return $valueArray2['count'] <=> $valueArray1['count'];
            });

            $bestGenres[] = [
                'type' => $type,
                'genres' => \array_slice($typeResults, 0, $maxCount),
            ];
        }

        return $bestGenres;
    }
}
