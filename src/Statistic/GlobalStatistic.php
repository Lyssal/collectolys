<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Statistic;

use App\Entity\Type;

/**
 * The type statistics.
 *
 * @category Statistic
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class GlobalStatistic extends AbstractStatistic
{
    /**
     * Get the config chart.
     *
     * @return array The config
     */
    public function getConfig(): array
    {
        /** @var \App\Doctrine\Manager\TypeManager $typeManager */
        $typeManager = $this->entityAdministratorManager->get(Type::class);
        $results = $typeManager->getRepository()->getTypeElementCounts();

        return [
            'type' => 'polarArea',
            'data' => [
                'datasets' => [
                    [
                        'data' => $this->getValuesByKey('count', $results),
                        'backgroundColor' => $this->getValuesByKey('color', $results),
                        'borderWidth' => 0,
                    ],
                ],
                'labels' => $this->getValuesByKey('name', $results),
            ],
            'options' => $this->getConfigOptions(),
        ];
    }
}
