<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Statistic;

use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * The statistics.
 *
 * @category Statistic
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
abstract class AbstractStatistic
{
    /**
     * The color for text.
     */
    const TEXT_COLOR = '#ffffff';

    /**
     * The min percentage to display the value.
     *
     * @var int
     */
    const PERCENTAGE_MIN = 1;

    /**
     * The default colors to use in chart.
     *
     * @var string[]
     */
    const DEFAULT_COLORS = [
        '#e6194b',
        '#3cb44b',
        '#ffe119',
        '#4363d8',
        '#f58231',
        '#911eb4',
        '#46f0f0',
        '#f032e6',
        '#bcf60c',
        '#fabebe',
        '#008080',
        '#e6beff',
        '#9a6324',
        '#fffac8',
        '#800000',
        '#aaffc3',
        '#808000',
        '#ffd8b1',
        '#000075',
        '#808080',
        '#ffffff',
        '#000000',
    ];

    /**
     * The translator.
     *
     * @var \Symfony\Contracts\Translation\TranslatorInterface
     */
    protected $translator;

    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    protected $entityAdministratorManager;

    /**
     * Constructor.
     *
     * @param \Symfony\Contracts\Translation\TranslatorInterface            $translator                 The translator
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     */
    public function __construct(TranslatorInterface $translator, EntityAdministratorManager $entityAdministratorManager)
    {
        $this->translator = $translator;
        $this->entityAdministratorManager = $entityAdministratorManager;
    }

    /**
     * Get the values indexed by key in the array.
     *
     * @param string $key         The key
     * @param array  $valuesArray The array
     *
     * @return array The values
     */
    protected function getValuesByKey(string $key, array $valuesArray): array
    {
        $values = [];

        foreach ($valuesArray as $valueArray) {
            $values[] = $valueArray[$key];
        }

        return $values;
    }

    /**
     * Get the min value in the array.
     *
     * @param string $key         The key
     * @param array  $valuesArray The array
     *
     * @return mixed The min value
     */
    protected function getMinValue(string $key, array $valuesArray)
    {
        $minValue = null;

        foreach ($valuesArray as $valueArray) {
            if (null === $minValue || $valueArray[$key] < $minValue) {
                $minValue = $valueArray[$key];
            }
        }

        if (null === $minValue) {
            return 0;
        }

        return $minValue;
    }

    /**
     * Get lines in the array according a key value correspondance.
     *
     * @param string $key         The key
     * @param string $value       The value
     * @param array  $valuesArray The array
     *
     * @return array The filtered array
     */
    protected function filterByValue(string $key, string $value, array $valuesArray): array
    {
        $filteredArray = [];

        foreach ($valuesArray as $valueArray) {
            if ($valueArray[$key] === $value) {
                $filteredArray[] = $valueArray;
            }
        }

        return $filteredArray;
    }

    /**
     * Remove in the array the counts where the percentage is too low and create an "other" value.
     *
     * @param array  $valuesArray The array
     * @param string $labelKey    The key for the label
     */
    protected function removeCountLessThanMinPercentage(array &$valuesArray, string $labelKey = 'name'): void
    {
        $tooLowCounts = [];
        $countTotal = $this->getTotal($this->getValuesByKey('count', $valuesArray));
        $countMin = $countTotal * self::PERCENTAGE_MIN / 100;

        // Get the too low counts
        foreach ($valuesArray as $arraykey => $valueArray) {
            $count = (int) $valueArray['count'];

            if ($count < $countMin) {
                $tooLowCounts[$arraykey] = $count;
            }
        }

        if (\count($tooLowCounts) > 1) {
            // Remove the too low counts
            foreach ($tooLowCounts as $arrayKey => $count) {
                unset($valuesArray[$arrayKey]);
            }

            // Create the other value
            $valuesArray[] = [
                $labelKey => $this->translator->trans('others'),
                'count' => array_sum($tooLowCounts),
            ];
        }
    }

    /**
     * Get the default colors.
     *
     * @param int $count The color count
     *
     * @return array The colors
     */
    public function getDefaultColors(int $count): array
    {
        $defaultColorCount = \count(self::DEFAULT_COLORS);
        $colors = [];

        for ($i = 0; $i < $count; ++$i) {
            $colors[] = self::DEFAULT_COLORS[$i % $defaultColorCount];
        }

        return $colors;
    }

    /**
     * Get the count total.
     *
     * @param array $counts The counts
     *
     * @return int The total
     */
    protected function getTotal(array $counts): int
    {
        return array_sum($counts);
    }

    /**
     * Get the default chart options.
     *
     * @return array The config options
     */
    protected function getConfigOptions(): array
    {
        return [
            'responsive' => true,
            'maintainAspectRatio' => false,
            'legend' => [
                'position' => 'right',
                'labels' => [
                    'fontSize' => 16,
                    'fontColor' => self::TEXT_COLOR,
                ],
            ],
            'title' => [
                'fontColor' => self::TEXT_COLOR,
                'fontSize' => 24,
            ],
        ];
    }
}
