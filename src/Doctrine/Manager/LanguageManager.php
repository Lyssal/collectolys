<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager;

use App\Entity\Language;

/**
 * The Language manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class LanguageManager extends AbstractEntityManager
{
    /**
     * {@inheritdoc}
     */
    public static $DEFAULT_ORDER_BY = [
        'name' => 'ASC',
    ];

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return Language::class;
    }
}
