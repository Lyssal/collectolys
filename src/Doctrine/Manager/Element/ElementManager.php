<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager\Element;

use App\Doctrine\Manager\AbstractEntityManager;
use App\Entity\Element\Element;
use App\Entity\Type;
use App\Entity\User\UserElement;
use App\Entity\User\UserSet;
use Lyssal\Doctrine\Orm\QueryBuilder;
use Lyssal\Entity\Setter\PropertySetter;
use Lyssal\Exception\InvalidArgumentException;

/**
 * The Element manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @method \App\Doctrine\Repository\Element\ElementRepository getRepository()
 * @method \App\Entity\Element\Element                        findOneBy(array $conditions, array $orderBy = null, array $extras = array())
 * @method \App\Entity\Element\Element[]                      findAll()
 */
final class ElementManager extends AbstractEntityManager
{
    /**
     * {@inheritdoc}
     *
     * @return \App\Entity\Element\Element The element
     */
    public function create($propertyValues = [])
    {
        if (!\array_key_exists('type', $propertyValues)) {
            return parent::create($propertyValues);
        }

        $type = $propertyValues['type'];
        if (!$type instanceof Type) {
            throw new InvalidArgumentException('The type property has to be a Type entity.');
        }

        $elementClass = Element::getClassByType($type);
        $element = new $elementClass();

        $entityGetter = new PropertySetter($element);

        return $entityGetter->set($propertyValues);
    }

    /**
     * {@inheritdoc}
     */
    public function persist($oneOrManyEntities): void
    {
        if ($oneOrManyEntities instanceof Element) {
            if (null !== $oneOrManyEntities->getIllustrationRecto()
                && null === $oneOrManyEntities->getIllustrationRecto()->getFilename()
                && null === $oneOrManyEntities->getIllustrationRecto()->getUploadedFile()
            ) {
                $oneOrManyEntities->setIllustrationRecto(null);
            }

            if (null !== $oneOrManyEntities->getIllustrationVerso()
                && null === $oneOrManyEntities->getIllustrationVerso()->getFilename()
                && null === $oneOrManyEntities->getIllustrationVerso()->getUploadedFile()
            ) {
                $oneOrManyEntities->setIllustrationVerso(null);
            }

            foreach ($oneOrManyEntities->getIllustrations() as $illustration) {
                if (
                    null !== $illustration->getImage()
                    && null === $illustration->getImage()->getFilename()
                    && null === $illustration->getImage()->getUploadedFile()
                ) {
                    $oneOrManyEntities->removeIllustration($illustration);
                    $this->delete($illustration);
                }
            }
        }

        parent::persist($oneOrManyEntities);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($oneOrManyEntities): void
    {
        if ($oneOrManyEntities instanceof Element) {
            $userElements = $this->entityManager->getRepository(UserElement::class)->findBy([
                'element' => $oneOrManyEntities,
            ]);

            parent::remove($userElements);
        }

        parent::remove($oneOrManyEntities);
    }

    /**
     * Get the first element with a recto illustration by a set.
     *
     * @return \App\Entity\Element\Element|null The element
     */
    public function getFirstSetElementWithIllustrationRecto(Element $elementSet): ?Element
    {
        return $this->findOneBy(
            [
                'elementSet' => $elementSet->getId(),
                QueryBuilder::WHERE_NOT_NULL => 'illustrationRecto',
            ],
            [
                'positionInSet',
                'numberInSet',
            ]
        );
    }

    /**
     * @param \App\Entity\User\UserSet[] $userSets
     *
     * @return \App\Entity\Element\Element[]
     */
    public function getByUserSets(array $userSets): array
    {
        $elementIds = [];
        array_walk(
            $userSets,
            function (UserSet $userSet) use (&$elementIds) {
                $elementIds = [...$elementIds, ...$userSet->getElementIds()];
            },
        );
        $elementIds = array_unique($elementIds);

        return $this->findByKeyedById(['id' => $elementIds]);
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return Element::class;
    }
}
