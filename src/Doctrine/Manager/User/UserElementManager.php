<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager\User;

use App\Doctrine\Manager\AbstractEntityManager;
use App\Entity\Element\Element;
use App\Entity\Type;
use App\Entity\User\User;
use App\Entity\User\UserElement;
use Lyssal\Doctrine\Orm\QueryBuilder;

/**
 * The UserElement manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @method \App\Entity\User\UserElement create()
 */
final class UserElementManager extends AbstractEntityManager
{
    /**
     * Get or create an user element.
     *
     * @param \App\Entity\User\User       $user    The user
     * @param \App\Entity\Element\Element $element The element
     *
     * @throws \Lyssal\Entity\Exception\EntityException
     *
     * @return UserElement The user element
     */
    public function getOrCreateByUserAndElement(User $user, Element $element): UserElement
    {
        $userElement = $this->findOneBy(['user' => $user, 'element' => $element]);

        if (null === $userElement) {
            $userElement = $this->create([
                'user' => $user,
                'element' => $element,
            ]);
        }

        return $userElement;
    }

    /**
     * @return \App\Entity\User\UserElement[]
     */
    public function getWishList(User $user, ?Type $type = null): array
    {
        $conditions = [
            'user' => $user,
            'wish' => true,
        ];

        if (null !== $type) {
            $conditions['element.type'] = $type;
        }

        return $this->findBy(
            $conditions,
            ['wishDate' => 'DESC'],
            extras: [
                QueryBuilder::INNER_JOINS => ['element' => 'element'],
                QueryBuilder::SELECTS => [QueryBuilder::ALIAS, 'element'],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return UserElement::class;
    }
}
