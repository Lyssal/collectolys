<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager\User;

use App\Doctrine\Manager\AbstractEntityManager;
use App\Entity\Type;
use App\Entity\User\UserSet;

/**
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class UserSetManager extends AbstractEntityManager
{
    /**
     * @return \App\Entity\User\UserSet[]
     */
    public function get(User $user, ?Type $type = null): array
    {
        if (null !== $type) {
            $conditions['type'] = $type;
        }

        return $this->findBy(
            $conditions,
            ['name' => 'ASC'],
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return UserSet::class;
    }
}
