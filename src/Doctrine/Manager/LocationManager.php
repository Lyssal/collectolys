<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager;

use App\Entity\Location;

/**
 * The Location manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @method \App\Doctrine\Repository\LocationRepository getRepository()
 * @method \App\Entity\Location                        findOneBy()
 */
final class LocationManager extends AbstractEntityManager
{
    /**
     * {@inheritdoc}
     */
    public static $DEFAULT_ORDER_BY = [
        'name' => 'ASC',
    ];

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return Location::class;
    }

    public function persist($oneOrManyEntities): void
    {
        if ($oneOrManyEntities instanceof Location) {
            if (
                null !== $oneOrManyEntities->getIcon()
                && null === $oneOrManyEntities->getIcon()->getFilename()
                && null === $oneOrManyEntities->getIcon()->getUploadedFile()
            ) {
                $oneOrManyEntities->setIcon(null);
            }
        }

        parent::persist($oneOrManyEntities);
    }
}
