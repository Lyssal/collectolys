<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager\Company;

use App\Doctrine\Manager\AbstractEntityManager;
use App\Entity\Company\CompanyRole;

/**
 * The CompanyRole manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @method \App\Entity\Company\CompanyRole findOneBy()
 */
final class CompanyRoleManager extends AbstractEntityManager
{
    /**
     * {@inheritdoc}
     */
    public static $DEFAULT_ORDER_BY = [
        'name' => 'ASC',
    ];

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return CompanyRole::class;
    }
}
