<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager\Company;

use App\Doctrine\Manager\AbstractEntityManager;
use App\Entity\Company\Company;

/**
 * The Company manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @method \App\Entity\Company\Company findOneBy()
 */
final class CompanyManager extends AbstractEntityManager
{
    /**
     * {@inheritdoc}
     */
    public static $DEFAULT_ORDER_BY = [
        'name' => 'ASC',
    ];

    /**
     * {@inheritdoc}
     */
    public function persist($oneOrManyEntities): void
    {
        if ($oneOrManyEntities instanceof Company) {
            if (null !== $oneOrManyEntities->getImage()) {
                $oneOrManyEntities->getImage()->uploadFile();
            }

            if (null !== $oneOrManyEntities->getImage()
                && null === $oneOrManyEntities->getImage()->getFilename()
                && null === $oneOrManyEntities->getImage()->getUploadedFile()
            ) {
                $oneOrManyEntities->setImage(null);
            }
        }

        parent::persist($oneOrManyEntities);
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return Company::class;
    }
}
