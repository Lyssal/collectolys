<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager;

use App\Entity\File\ImageableInterface;
use Lyssal\Doctrine\Orm\Administrator\EntityAdministrator;

/**
 * The parent entity manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
abstract class AbstractEntityManager extends EntityAdministrator
{
    /**
     * {@inheritdoc}
     *
     * @param \App\Entity\Company\Company|\App\Entity\Company\Company[] $oneOrManyEntities
     */
    public function persist($oneOrManyEntities): void
    {
        if ($oneOrManyEntities instanceof ImageableInterface) {
            $this->verifyAndCleanImage($oneOrManyEntities);
        }

        parent::persist($oneOrManyEntities);
    }

    /**
     * Verify the image and clean if it does not exist.
     *
     * @param \App\Entity\File\ImageableInterface $imageable The imageable
     */
    private function verifyAndCleanImage(ImageableInterface $imageable): void
    {
        if (null !== $imageable->getImage() && null === $imageable->getImage()->getFilename()) {
            $imageable->setImage(null);
        }
    }
}
