<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager\Person;

use App\Doctrine\Manager\AbstractEntityManager;
use App\Entity\Person\Person;
use Lyssal\Text\Slug;

/**
 * The Person manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @method \App\Entity\Person\Person create()
 */
final class PersonManager extends AbstractEntityManager
{
    /**
     * {@inheritdoc}
     */
    public static $DEFAULT_ORDER_BY = [
        'firstName' => 'ASC',
        'lastName' => 'ASC',
    ];

    /**
     * {@inheritdoc}
     */
    public function persist($oneOrManyEntities): void
    {
        if ($oneOrManyEntities instanceof Person) {
            if (null !== $oneOrManyEntities->getImage()) {
                $oneOrManyEntities->getImage()->uploadFile();
            }

            if (null !== $oneOrManyEntities->getImage()
                && null === $oneOrManyEntities->getImage()->getFilename()
                && null === $oneOrManyEntities->getImage()->getUploadedFile()
            ) {
                $oneOrManyEntities->setImage(null);
            }
        }

        parent::persist($oneOrManyEntities);
    }

    /**
     * Get or create a Person.
     *
     * @param string $firstName The first naùe
     * @param string $lastName  The last name
     *
     * @throws \Lyssal\Entity\Exception\EntityException
     *
     * @return \App\Entity\Person\Person The person
     */
    public function getOrCreateByFirstNameAndLastName(string $firstName, ?string $lastName): Person
    {
        $personSlug = new Slug($firstName.' '.$lastName);
        $personSlug->minify('_', false);

        $person = $this->findOneBy(['slug' => $personSlug->getText()]);

        if (null === $person) {
            $person = $this->create([
                'firstName' => $firstName,
                'lastName' => $lastName,
            ]);
        }

        return $person;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return Person::class;
    }
}
