<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Manager;

use App\Entity\Platform;
use App\Entity\Type;

/**
 * The Platform manager.
 *
 * @category Manager
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @method \App\Doctrine\Repository\PlatformRepository getRepository()
 */
final class PlatformManager extends AbstractEntityManager
{
    /**
     * {@inheritdoc}
     */
    public static $DEFAULT_ORDER_BY = [
        'name' => 'ASC',
    ];

    /**
     * @see \App\Doctrine\Repository\PlatformRepository::findByType()
     */
    public function findByType(Type $type): array
    {
        return $this->getRepository()->findByType($type);
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return Platform::class;
    }
}
