<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Decorator;

use App\Entity\Element\Element;
use Lyssal\Entity\Decorator\DecoratorManager as LyssalDecoratorManager;
use Pagerfanta\Pagerfanta;
use Traversable;

/**
 * The decorator manager.
 *
 * @category Decorator
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class DecoratorManager extends LyssalDecoratorManager
{
    /**
     * {@inheritdoc}
     */
    public function get($oneOrManyEntities)
    {
        // Element Pagerfanta
        if ($oneOrManyEntities instanceof Pagerfanta) {
            $results = $oneOrManyEntities->getCurrentPageResults();

            if ($results instanceof Traversable) {
                foreach ($results as $i => $result) {
                    if (\is_array($result) && \array_key_exists(0, $result) && $result[0] instanceof Element) {
                        $results[$i][0] = $this->get($result[0]);
                    }
                }

                return $results;
            }
        }

        return parent::get($oneOrManyEntities);
    }
}
