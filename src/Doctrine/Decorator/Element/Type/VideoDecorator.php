<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Decorator\Element\Type;

use App\Doctrine\Decorator\Element\ElementDecorator;
use App\Doctrine\Manager\Element\ElementManager;
use App\Entity\Element\Type\Video;
use App\Enum\VideoTypeEnum;
use Lyssal\Entity\Decorator\DecoratorManager;

/**
 * The Video decorator.
 *
 * @category Decorator
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @property \App\Entity\Element\Type\Video $entity
 */
class VideoDecorator extends ElementDecorator
{
    /**
     * @var int
     */
    private $elementVideoGenreAnimationId;

    /**
     * {@inheritdoc}
     *
     * @param \App\Doctrine\Manager\Element\ElementManager $elementManager The Element manager
     */
    public function __construct(DecoratorManager $decoratorManager, ElementManager $elementManager, int $elementVideoGenreAnimationId)
    {
        parent::__construct($decoratorManager, $elementManager);

        $this->elementVideoGenreAnimationId = $elementVideoGenreAnimationId;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($entity)
    {
        return $entity instanceof Video;
    }

    /**
     * Get the real video type.
     *
     * @return string The type
     */
    public function getRealVideoType(): string
    {
        if ($this->isAnimatedSeries()) {
            return VideoTypeEnum::ANIMATED_SERIES;
        }

        if ($this->isNonAnimatedTelevisionSeries()) {
            return VideoTypeEnum::NON_ANIMATED_TELEVISION_SERIES;
        }

        if (
            null !== $this->entity->getDuration()
            && $this->entity->getDurationInSeconds() < VideoTypeEnum::SHORT_FILM_DURATION_MAX
            && \in_array($this->entity->getVideoType(), [VideoTypeEnum::FILM, VideoTypeEnum::TELEVISION_FILM], true)
        ) {
            return VideoTypeEnum::SHORT_FILM;
        }

        return $this->entity->getVideoType();
    }

    /**
     * Get the real video type label.
     *
     * @return string The label
     */
    public function getRealVideoTypeLabel(): string
    {
        return VideoTypeEnum::getLabel($this->getRealVideoType());
    }

    /**
     * Determines if the video is a non animated TV series.
     *
     * @return bool If It is a non animated TV series
     */
    private function isNonAnimatedTelevisionSeries(): bool
    {
        return $this->entity->isTelevisionSeries() && !$this->isAnimatedSeries();
    }

    /**
     * Determines if the video is an animated series.
     *
     * @return bool If It is an animated series
     */
    private function isAnimatedSeries(): bool
    {
        return $this->entity->isTelevisionSeries() && $this->entity->hasGenreId($this->elementVideoGenreAnimationId);
    }
}
