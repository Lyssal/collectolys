<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Decorator\Element;

use App\Doctrine\Manager\Element\ElementManager;
use App\Entity\Element\Element;
use App\Entity\Element\Type\Video;
use Countable;
use Doctrine\Common\Collections\Collection;
use Lyssal\Doctrine\Orm\QueryBuilder;
use Lyssal\Entity\Decorator\AbstractDecorator;
use Lyssal\Entity\Decorator\DecoratorManager;
use Lyssal\Entity\Getter\PropertyGetter;

/**
 * The Element decorator.
 *
 * @category Decorator
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 *
 * @property \App\Entity\Element\Element $entity
 */
class ElementDecorator extends AbstractDecorator
{
    /**
     * The first set max count.
     *
     * @var int
     */
    const FIRST_ELEMENT_COUNT = 6;

    /**
     * @var \App\Doctrine\Manager\Element\ElementManager
     */
    private $elementManager;

    /**
     * @var ?\DateTimeInterface The use date
     */
    private $useDate;

    /**
     * {@inheritdoc}
     *
     * @param \App\Doctrine\Manager\Element\ElementManager $elementManager The Element manager
     */
    public function __construct(DecoratorManager $decoratorManager, ElementManager $elementManager)
    {
        parent::__construct($decoratorManager);

        $this->elementManager = $elementManager;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($entity)
    {
        return $entity instanceof Element && !$entity instanceof Video;
    }

    public function __call($name, $args)
    {
        $value = parent::__call($name, $args);

        if (null !== $value && (!$value instanceof Countable || 0 !== \count($value))) {
            return $value;
        }

        $elementSet = $this->entity->getElementSet();

        if (null !== $elementSet) {
            $propertyGetter = new PropertyGetter($elementSet);

            return $propertyGetter->get($name, $args);
        }

        return parent::__call($name, $args);
    }

    /**
     * Get the previous element.
     *
     * @return \App\Entity\Element\Element|null The element
     */
    public function getPrevious(): ?Element
    {
        if (null !== $this->entity->getPreviousElement()) {
            return $this->entity->getPreviousElement();
        }

        if (null === $this->entity->getElementSet() || null === $this->entity->getPositionInSet()) {
            return null;
        }

        return $this->elementManager->findOneBy([
            'elementSet' => $this->entity->getElementSet(),
            'setSpecialIssue' => $this->entity->isSetSpecialIssue(),
            QueryBuilder::WHERE_LESS => ['positionInSet' => $this->entity->getPositionInSet()],
        ], [
            'positionInSet' => 'DESC',
        ]);
    }

    /**
     * Get the next element.
     *
     * @return \App\Entity\Element\Element|null The element
     */
    public function getNext(): ?Element
    {
        if (1 === \count($this->entity->getNextElements())) {
            return $this->entity->getNextElements()->first();
        }

        if (null === $this->entity->getElementSet() || null === $this->entity->getPositionInSet()) {
            return null;
        }

        return $this->elementManager->findOneBy([
            'elementSet' => $this->entity->getElementSet(),
            'setSpecialIssue' => $this->entity->isSetSpecialIssue(),
            QueryBuilder::WHERE_GREATER => ['positionInSet' => $this->entity->getPositionInSet()],
        ], [
            'positionInSet' => 'ASC',
        ]);
    }

    /**
     * Get the similar elements.
     *
     * @return \App\Entity\Element\Element[] The elements
     */
    public function getSimilars(): array
    {
        return array_merge(
            $this->entity->getSimilarElements()->toArray(),
            $this->entity->getOtherSimilarElements()->toArray()
        );
    }

    /**
     * @return \App\Entity\Element\Element[]
     */
    public function getSeeAlso(): array
    {
        $seeAlso = [];
        $previous = $this->getPrevious();
        $next = $this->getNext();
        $addElements = function (array | Collection $elements) use (&$seeAlso) {
            foreach ($elements as $element) {
                $isPresent = false;

                foreach ($seeAlso as $seeAlsoElement) {
                    if ($seeAlsoElement->equals($element instanceof self ? $element->getEntity() : $element)) {
                        $isPresent = true;
                        break;
                    }
                }

                if (!$isPresent) {
                    $seeAlso[] = $element;
                }
            }
        };

        $addElements($this->getReferences());

        if (null !== $previous) {
            $addElements([$previous]);
        }

        if (null !== $next) {
            $addElements([$next]);
        }

        $addElements($this->entity->getNextElements());
        $addElements($this->getSimilars());
        $addElements($this->getReferencedElements());

        return $seeAlso;
    }

    public function getSeeAlsoByType(): array
    {
        $seeAlso = $this->getSeeAlso();
        $seeAlsoByType = [];

        foreach ($seeAlso as $element) {
            $type = $element->getType();

            if (!isset($seeAlsoByType[$type->getId()])) {
                $seeAlsoByType[$type->getId()] = [
                    'type' => $type,
                    'elements' => [],
                ];
            }

            $seeAlsoByType[$type->getId()]['elements'][] = $element;
        }

        return $seeAlsoByType;
    }

    public function getUseDate(): ?\DateTimeInterface
    {
        return $this->useDate;
    }

    public function setUseDate(\DateTimeInterface $date): self
    {
        $this->useDate = $date;

        return $this;
    }

    /**
     * Get the set element count.
     *
     * @return int The count
     */
    public function countSetElements(): int
    {
        return $this->elementManager->count(['elementSet' => $this->entity->getId()]);
    }

    /**
     * Get the first set elements.
     *
     * @return \App\Entity\Element\Element[] The set elements
     */
    public function getSetFirstElements(): array
    {
        return $this->elementManager->findBy([
            'elementSet' => $this->entity,
        ], [
            'setSpecialIssue',
            'positionInSet',
            'numberInSet',
        ], self::FIRST_ELEMENT_COUNT);
    }
}
