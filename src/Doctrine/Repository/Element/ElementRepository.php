<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\Element;

use App\Entity\Element\Element;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The Element repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2022 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementRepository extends EntityRepository
{
    /**
     * Get all sets with element count.
     *
     * @param int $maxResults The max results
     *
     * @return array The sets and counts
     */
    public function getSetElementCounts(int $maxResults): array
    {
        $qb = $this->createQueryBuilder('series');

        $qb
            ->select(
                'IDENTITY(series.type) AS type_id',
                'series.id',
                'mainElementName.name',
                'mainElementName.slug',
                $qb->expr()->countDistinct('element').' AS count'
            )
            ->innerJoin(
                'series.names',
                'mainElementName',
                Join::WITH,
                $qb->expr()->eq('mainElementName.position', ':namePosition')
            )
            ->setParameter('namePosition', 0)
            ->innerJoin(
                Element::class,
                'element',
                Join::WITH,
                $qb->expr()->eq('element.elementSet', 'series')
            )
            ->where($qb->expr()->eq('series.set', ':set'))
            ->orderBy('count', 'DESC')
            ->groupBy('type_id', 'series.id', 'mainElementName.name', 'mainElementName.slug')
            ->setMaxResults($maxResults)
            ->setParameter('set', true)
        ;

        return $qb->getQuery()->getResult();
    }
}
