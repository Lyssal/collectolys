<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\Element;

use App\Entity\Element\ElementDate;
use App\Statistic\YearStatistic;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The ElementDate repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementDateRepository extends EntityRepository
{
    /**
     * Get years with element count.
     *
     * @return array The years and counts
     */
    public function getYearElementCounts(): array
    {
        $qb = $this->createQueryBuilder('elementDate');

        $qb
            ->select(
                'IDENTITY(element.type) AS type_id',
                'elementDate.year',
                $qb->expr()->countDistinct('element').' AS count'
            )
            ->innerJoin(
                'elementDate.element',
                'element',
                Join::WITH,
                $qb->expr()->eq('elementDate.year', '(SELECT MIN(_elementDate.year) FROM '.ElementDate::class.' _elementDate WHERE _elementDate.element = element)')
            )
            ->where($qb->expr()->gte('elementDate.year', ':minYear'))
            ->setParameter('minYear', YearStatistic::MIN_YEAR)
            ->orderBy('elementDate.year')
            ->groupBy('type_id', 'elementDate.year')
        ;

        return $qb->getQuery()->getResult();
    }
}
