<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository;

use App\Entity\Element\Element;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The Type repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class TypeRepository extends EntityRepository
{
    /**
     * Get all types with element count.
     *
     * @return array The types and counts
     */
    public function getTypeElementCounts(): array
    {
        $qb = $this->createQueryBuilder('type');

        $qb
            ->select(
                'type.name',
                'type.color',
                $qb->expr()->count('element').' AS count'
            )
            ->innerJoin(
                Element::class,
                'element',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('element.type', 'type'),
                    $qb->expr()->eq('element.set', ':set')
                )
            )
            ->orderBy('type.position')
            ->groupBy('type.name', 'type.color')
            ->setParameter('set', false)
        ;

        return $qb->getQuery()->getResult();
    }
}
