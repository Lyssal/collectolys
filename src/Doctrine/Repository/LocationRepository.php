<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository;

use App\Entity\Element\Element;
use App\Entity\Type;
use App\Entity\User\User;
use App\Entity\User\UserElement;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Lyssal\SimpleLocationBundle\Doctrine\Repository\LocationRepository as LyssalLocationRepository;

/**
 * The Location repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class LocationRepository extends LyssalLocationRepository
{
    /**
     * Get all locations with element count.
     *
     * @return array The locations and counts
     */
    public function getOriginElementCounts(?Type $type): array
    {
        return $this->getOriginElementCountsQueryBuilder($type)->getQuery()->getResult();
    }

    private function getOriginElementCountsQueryBuilder(?Type $type): QueryBuilder
    {
        $qb = $this->createQueryBuilder('location');

        $qb
            ->select(
                'IDENTITY(element.type) AS type_id',
                'location.name',
                'location.code',
                'location.slug',
                'location.emoji',
                $qb->expr()->countDistinct('element').' AS count'
            )
            ->innerJoin(
                Element::class,
                'element'
            )
            ->innerJoin(
                'element.origins',
                'origin'
            )
            ->where($qb->expr()->eq('origin', 'location'))
            ->orderBy('count', 'DESC')
            ->groupBy('location.name', 'location.slug', 'location.emoji')
        ;

        if (null !== $type) {
            $qb
                ->andWhere($qb->expr()->eq('element.type', ':type'))
                ->setParameter('type', $type)
            ;
        }

        return $qb;
    }

    public function getOriginLastUsedElementCounts(User $user, ?Type $type): array
    {
        $qb = $this->getOriginElementCountsQueryBuilder($type);

        $qb
            ->innerJoin(
                UserElement::class,
                'userElement',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('element.id', 'userElement.element'),
                    $qb->expr()->eq('userElement.user', ':user')
                )
            )
            ->setParameter('user', $user)
            ->innerJoin('userElement.uses', 'elementUse')
        ;

        return $qb->getQuery()->getResult();
    }
}
