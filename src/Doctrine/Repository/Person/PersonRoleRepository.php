<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\Person;

use App\Entity\Element\ElementPerson;
use App\Entity\Person\Person;
use App\Entity\Person\PersonRole;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The PersonRole repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class PersonRoleRepository extends EntityRepository
{
    /**
     * Get the person roles with count by role.
     */
    public function getPersonRolesByPerson(Person $person): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select(
                'personRole',
                $qb->expr()->countDistinct('elementPerson.id').' AS count'
            )
            ->from(PersonRole::class, 'personRole')
            ->innerJoin(
                ElementPerson::class,
                'elementPerson',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('elementPerson.role', 'personRole'),
                    $qb->expr()->eq('elementPerson.person', ':person')
                )
            )
            ->setParameter('person', $person)
            ->orderBy('count', 'DESC')
            ->groupBy('personRole')
        ;

        return $qb->getQuery()->getResult();
    }
}
