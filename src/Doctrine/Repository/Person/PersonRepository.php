<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\Person;

use App\Entity\Element\ElementPerson;
use App\Entity\Person\Person;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The Person repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class PersonRepository extends EntityRepository
{
    /**
     * Get all people with element count.
     *
     * @param int $maxResults The max results
     *
     * @return array The sets and counts
     */
    public function getPersonElementCounts(int $maxResults): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select(
                'person.firstName',
                'person.lastName',
                'person.slug',
                $qb->expr()->countDistinct('elementPerson.element').' AS count'
            )
            ->from(ElementPerson::class, 'elementPerson')
            ->innerJoin(
                Person::class,
                'person',
                Join::WITH,
                $qb->expr()->eq('elementPerson.person', 'person')
            )
            ->orderBy('count', 'DESC')
            ->groupBy('person.firstName', 'person.lastName', 'person.slug')
            ->setMaxResults($maxResults)
        ;

        return $qb->getQuery()->getResult();
    }
}
