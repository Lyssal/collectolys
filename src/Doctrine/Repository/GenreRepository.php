<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository;

use App\Entity\Element\Element;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The Genre repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class GenreRepository extends EntityRepository
{
    /**
     * Get all genres with element count.
     *
     * @return array The genres and counts
     */
    public function getGenreElementCounts(): array
    {
        $qb = $this->createQueryBuilder('genre');

        $qb
            ->select(
                'IDENTITY(genre.type) AS type_id',
                'genre.name',
                'genre.slug',
                $qb->expr()->countDistinct('element').' AS count'
            )
            ->innerJoin(
                Element::class,
                'element'
            )
            ->innerJoin(
                'element.genres',
                'elementGenre'
            )
            ->leftJoin('elementGenre.parent', 'elementGenreParent')
            ->where($qb->expr()->andX(
                $qb->expr()->isNull('genre.parent'),
                $qb->expr()->orX(
                    $qb->expr()->eq('elementGenre', 'genre'),
                    $qb->expr()->eq('elementGenreParent', 'genre')
                )
            ))
            ->orderBy('count', 'DESC')
            ->groupBy('type_id', 'genre.name', 'genre.slug')
        ;

        return $qb->getQuery()->getResult();
    }
}
