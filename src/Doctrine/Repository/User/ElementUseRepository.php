<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\User;

use App\Entity\Element\Element;
use App\Entity\Type;
use App\Entity\User\ElementUse;
use App\Entity\User\User;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The ElementUse repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementUseRepository extends EntityRepository
{
    /**
     * Get the last use of the element.
     *
     * @param \App\Entity\User\User       $user    The use
     * @param \App\Entity\Element\Element $element The element
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @return \App\Entity\User\ElementUse|null The last element use
     */
    public function getLastByUserAndElement(User $user, Element $element): ?ElementUse
    {
        $qb = $this->createQueryBuilder('elementUse');

        $qb
            ->innerJoin('elementUse.element', 'userElement', Join::WITH, $qb->expr()->andX(
                $qb->expr()->eq('userElement.user', ':user'),
                $qb->expr()->eq('userElement.element', ':element')
            ))
            ->setParameter('user', $user)
            ->setParameter('element', $element)
            ->orderBy('elementUse.date', 'DESC')
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Get the last uses.
     *
     * @param \App\Entity\User\User $user The user
     * @param \App\Entity\Type|null $type The Element type
     *
     * @return \Doctrine\ORM\QueryBuilder The QueryBuilder
     */
    public function getLastsQueryBuilder(User $user, ?Type $type = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('elementUse');

        $qb
            ->addSelect('userElement')
            ->innerJoin('elementUse.element', 'userElement', Join::WITH, $qb->expr()->eq('userElement.user', ':user'))
            ->setParameter('user', $user)
        ;

        if (null !== $type) {
            $qb
                ->addSelect('element')
                ->innerJoin('userElement.element', 'element', Join::WITH, $qb->expr()->eq('IDENTITY(element.type)', ':type'))
                ->setParameter('type', $type)
            ;
        }

        $qb
            ->orderBy('elementUse.date', 'DESC')
            ->addOrderBy('elementUse.id', 'DESC')
        ;

        return $qb;
    }
}
