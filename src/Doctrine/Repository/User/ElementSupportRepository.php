<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\User;

use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The ElementSupport repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementSupportRepository extends EntityRepository
{
    /**
     * Get the web domain source counts.
     *
     * @param int $countMin The count minimum to have
     *
     * @return array The domain with counts
     */
    public function getWebDomainSourceCounts(int $countMin): array
    {
        $qb = $this->createQueryBuilder('elementSupport');

        $qb
            ->select(
            "SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(elementSupport.webSource, '/', 3), '://', -1), '/', 1), '?', 1) AS name",
                $qb->expr()->countDistinct('elementSupport').' AS count'
            )
            ->where($qb->expr()->isNotNull('elementSupport.webSource'))
            ->groupBy('name')
            ->having($qb->expr()->gte($qb->expr()->countDistinct('elementSupport'), $countMin))
            ->orderBy('count', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }
}
