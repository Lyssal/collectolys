<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository;

use App\Entity\Element\Element;
use App\Entity\Element\Type\VideoGame;
use App\Entity\Type;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The Platform repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class PlatformRepository extends EntityRepository
{
    /**
     * Get the platforms by type.
     *
     * @param \App\Entity\Type $type The type
     *
     * @return \App\Entity\Platform[] The platforms
     */
    public function findByType(Type $type): array
    {
        $qb = $this->createQueryBuilder('platform');
        $elementClass = Element::getClassByType($type);

        $qb
            ->innerJoin($elementClass, 'element')
            ->innerJoin('element.platforms', 'elementPlatform', Join::WITH, $qb->expr()->eq('platform', 'elementPlatform'))
            ->groupBy('platform')
            ->orderBy('platform.name')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all videogame platforms with element count.
     *
     * @return array The platforms and counts
     */
    public function getVideoGamePlatformElementCounts(): array
    {
        $qb = $this->createQueryBuilder('platform');

        $qb
            ->select(
                'platform.name',
                'platform.slug',
                'icon.filename AS icon_filename',
                'icon.path AS icon_path',
                $qb->expr()->countDistinct('element').' AS count'
            )
            ->innerJoin(
                VideoGame::class,
                'element'
            )
            ->innerJoin(
                'element.platforms',
                'elementPlatform',
                Join::WITH,
                $qb->expr()->eq('platform', 'elementPlatform')
            )
            ->leftJoin('platform.icon', 'icon')
            ->orderBy('count', 'DESC')
            ->groupBy('platform.name', 'platform.slug', 'icon_filename', 'icon_path')
        ;

        return $qb->getQuery()->getResult();
    }
}
