<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository;

use App\Entity\Element\Element;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The Universe repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class UniverseRepository extends EntityRepository
{
    /**
     * Get all universes with element count.
     *
     * @param int $maxResults The max results
     *
     * @return array The sets and counts
     */
    public function getUniverseElementCounts(int $maxResults): array
    {
        $qb = $this->createQueryBuilder('universe');

        $qb
            ->select(
                'universe.name',
                'universe.slug',
                'icon.filename AS icon_filename',
                'icon.path AS icon_path',
                $qb->expr()->countDistinct('element').' AS count'
            )
            ->innerJoin(Element::class, 'element')
            ->innerJoin(
                'element.universes',
                'elementUniverse',
                Join::WITH,
                $qb->expr()->eq('elementUniverse', 'universe')
            )
            ->leftJoin('universe.icon', 'icon')
            ->orderBy('count', 'DESC')
            ->groupBy('universe.name', 'universe.slug', 'icon_filename', 'icon_path')
            ->setMaxResults($maxResults)
        ;

        return $qb->getQuery()->getResult();
    }
}
