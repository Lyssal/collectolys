<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\Company;

use App\Entity\Company\Company;
use App\Entity\Element\ElementCompany;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The Company repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class CompanyRepository extends EntityRepository
{
    /**
     * Get all companies with element count.
     *
     * @param int $maxResults The max results
     *
     * @return array The sets and counts
     */
    public function getCompanyElementCounts(int $maxResults): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select(
                'company.name',
                'company.slug',
                'location.name AS locationName',
                'location.emoji AS locationEmoji',
                $qb->expr()->countDistinct('elementCompany.element').' AS count'
            )
            ->from(ElementCompany::class, 'elementCompany')
            ->innerJoin(
                Company::class,
                'company',
                Join::WITH,
                $qb->expr()->eq('elementCompany.company', 'company')
            )
            ->leftJoin('company.nationality', 'location')
            ->orderBy('count', 'DESC')
            ->groupBy('company.name', 'company.slug', 'locationName', 'locationEmoji')
            ->setMaxResults($maxResults)
        ;

        return $qb->getQuery()->getResult();
    }
}
