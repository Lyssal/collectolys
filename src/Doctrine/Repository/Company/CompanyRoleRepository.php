<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Doctrine\Repository\Company;

use App\Entity\Company\Company;
use App\Entity\Company\CompanyRole;
use App\Entity\Element\ElementCompany;
use Doctrine\ORM\Query\Expr\Join;
use Lyssal\Doctrine\Orm\Repository\EntityRepository;

/**
 * The CompanyRole repository.
 *
 * @category Repository
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class CompanyRoleRepository extends EntityRepository
{
    /**
     * Get the company roles with count by role.
     */
    public function getCompanyRolesByCompany(Company $company): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb
            ->select(
                'companyRole',
                $qb->expr()->countDistinct('elementCompany.id').' AS count'
            )
            ->from(CompanyRole::class, 'companyRole')
            ->innerJoin(
                ElementCompany::class,
                'elementCompany',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('elementCompany.role', 'companyRole'),
                    $qb->expr()->eq('elementCompany.company', ':company')
                )
            )
            ->setParameter('company', $company)
            ->orderBy('count', 'DESC')
            ->groupBy('companyRole')
        ;

        return $qb->getQuery()->getResult();
    }
}
