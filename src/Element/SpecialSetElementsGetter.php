<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Element;

use App\Doctrine\Manager\Element\ElementManager;
use App\Entity\Element\Element;
use App\Entity\Element\SpecialSet;
use Lyssal\Doctrine\Orm\QueryBuilder;
use Lyssal\Entity\Decorator\DecoratorManager;

class SpecialSetElementsGetter
{
    /**
     * @var \Lyssal\Entity\Decorator\DecoratorManager The Decorator manager
     */
    private $decoratorManager;

    /**
     * @var \App\Doctrine\Manager\Element\ElementManager The Element manager
     */
    private $elementManager;

    /**
     * Constructor.
     *
     * @param \Lyssal\Entity\Decorator\DecoratorManager    $decoratorManager The Decorator manager
     * @param \App\Doctrine\Manager\Element\ElementManager $elementManager   The Element manager
     */
    public function __construct(DecoratorManager $decoratorManager, ElementManager $elementManager)
    {
        $this->decoratorManager = $decoratorManager;
        $this->elementManager = $elementManager;
    }

    /**
     * Get the special set elements.
     *
     * @return \App\Entity\Element\Element[] The elements
     */
    public function get(SpecialSet $specialSet): array
    {
        $elementIds = $specialSet->getFormattedIds();
        $elements = $this->elementManager->findBy([
            QueryBuilder::WHERE_IN => ['id' => $elementIds],
        ]);

        // Sort elements according to the element list
        usort($elements, function (Element $element1, Element $element2) use ($elementIds) {
            return array_search($element1->getId(), $elementIds, true) <=> array_search($element2->getId(), $elementIds, true);
        });

        return $this->decoratorManager->get($elements);
    }
}
