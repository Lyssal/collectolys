<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Element;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * The sorter service to manage the element list sort.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class Sorter
{
    /**
     * Order by title.
     *
     * @var string
     */
    const ORDER_BY_TITLE = 'title';

    /**
     * Order by date.
     *
     * @var string
     */
    const ORDER_BY_DATE = 'date';

    /**
     * Order by ID.
     *
     * @var string
     */
    const ORDER_BY_ID = 'id';

    /**
     * Order ascendant.
     *
     * @var string
     */
    const ORDER_ASC = 'asc';

    /**
     * Order descendant.
     *
     * @var string
     */
    const ORDER_DESC = 'desc';

    /**
     * The session name.
     *
     * @var string
     */
    const SESSION_NAME = 'order_by';

    /**
     * @var \Symfony\Component\HttpFoundation\Session\SessionInterface The session service
     */
    private $session;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session The session service
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Set the chosen user sort.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request The request
     */
    public function setByRequest(Request $request): void
    {
        if ('app_admin_dashboard_index' === $request->get('_route')) {
            // Easyadmin also use sort parameters
            return;
        }

        $sort = $request->query->get('sort');

        if (null !== $sort) {
            if (!\is_array($sort)) {
                throw new InvalidArgumentException('The sort parameter must be an array.');
            }

            $this->session->set(self::SESSION_NAME, $sort);
        }
    }

    /**
     * Get the chosen sort labels.
     *
     * @return array The sort labels
     */
    public function getChosenSort(): array
    {
        return $this->session->get(self::SESSION_NAME, [self::ORDER_BY_TITLE => self::ORDER_ASC]);
    }

    /**
     * Get the order by for the query.
     *
     * @return array The query order by
     */
    public function getOrderBy(): array
    {
        $orderBy = [];
        $sorts = $this->getChosenSort();

        foreach ($sorts as $sort => $direction) {
            if (!\in_array($direction, [self::ORDER_ASC, self::ORDER_DESC], true)) {
                throw new InvalidArgumentException('The sort direction parameter must be "'.self::ORDER_ASC.'" or "'.self::ORDER_DESC.'" ("'.$direction.'" given).');
            }

            switch ($sort) {
                case self::ORDER_BY_TITLE:
                    $orderBy['title'] = $direction;
                    break;
                case self::ORDER_BY_DATE:
                    $orderBy['date'] = $direction;
                    break;
                case self::ORDER_BY_ID:
                    $orderBy['id'] = $direction;
                    break;
            }
        }

        return $orderBy;
    }
}
