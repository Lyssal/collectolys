<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Element;

use App\Entity\Element\Element;
use App\Entity\Element\ElementDate;
use App\Entity\User\User;
use App\Entity\User\UserElement;
use App\Enum\ColorEnum;
use App\Enum\VideoTypeEnum;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Lyssal\Text\Slug;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * The service to search elements.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementSearcher
{
    /**
     * The Entity manager.
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;

    /**
     * The element count per page.
     *
     * @var int
     */
    private $elementCountPerPage;

    /**
     * The Video animation genre ID.
     *
     * @var int
     */
    private $elementVideoGenreAnimationId;

    /**
     * Constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager                The Entity manager
     * @param int                                  $elementCountPerPage          The element count per page
     * @param int                                  $elementVideoGenreAnimationId The Video animation genre ID
     */
    public function __construct(EntityManagerInterface $entityManager, int $elementCountPerPage, int $elementVideoGenreAnimationId)
    {
        $this->entityManager = $entityManager;
        $this->elementCountPerPage = $elementCountPerPage;
        $this->elementVideoGenreAnimationId = $elementVideoGenreAnimationId;
    }

    /**
     * Get the elements page.
     *
     * @param \App\Entity\User\User $user           The user
     * @param array                 $conditions     The conditions
     * @param array                 $orderBy        The order
     * @param int                   $page           The page number
     * @param bool                  $aggregateBySet If the elements are grouped by set
     *
     * @return \Pagerfanta\Pagerfanta The element pagination
     */
    public function getPagerfantaFind(User $user, array $conditions = [], array $orderBy = [], int $page = 1, bool $aggregateBySet = true): Pagerfanta
    {
        $queryBuilder = $this->getSearchQueryBuilder($user, $conditions, $orderBy, $aggregateBySet);
        $pagerFanta = new Pagerfanta(new QueryAdapter($queryBuilder, false));

        $pagerFanta->setMaxPerPage($this->elementCountPerPage);
        $pagerFanta->setCurrentPage($page);

        return $pagerFanta;
    }

    /**
     * Get the element search query builder.
     *
     * @param \App\Entity\User\User $user       The user
     * @param array                 $conditions The conditions
     * @param array                 $orderBy    The order
     * @param bool                  $groupBySet If the elements are grouped by set
     *
     * @return \Doctrine\ORM\QueryBuilder The query builder
     */
    public function getSearchQueryBuilder(?User $user, array $conditions = [], array $orderBy = [], bool $groupBySet = true): QueryBuilder
    {
        $elementRepository = $this->entityManager->getRepository(Element::class);

        // Not the element sets by default
        if ($groupBySet && !\array_key_exists('set', $conditions)) {
            $conditions['set'] = false;
        }

        /** @var \App\Entity\Type $type */
        $type = $conditions['type'] ?? null;

        if (null === $type) {
            $qb = $elementRepository->createQueryBuilder('element');
        } else {
            $qb = $this->entityManager->createQueryBuilder()
                ->select('element')
                ->from(Element::getClassByType($type), 'element');
        }

        $qb
            ->addSelect('(CASE WHEN series.setDisplayInElementTitle = true OR series.setAggregated = true THEN series.slug ELSE element.slug END) AS namePart1')
            ->addSelect('element.positionInSet AS namePart2')
            ->addSelect('element.slug AS namePart3')
            ->leftJoin(
                'element.names',
                'mainElementName',
                Join::WITH,
                $qb->expr()->eq('mainElementName.position', ':namePosition')
            )
            ->setParameter('namePosition', 0)
            ->addSelect('mainElementName')
            ->leftJoin(
                'element.elementSet',
                'aggregatedSet',
                Join::WITH,
                $qb->expr()->eq('aggregatedSet.setAggregated', ':aggregated')
            )
            ->setParameter('aggregated', true)
            // we get the min year
            ->leftJoin(
                'element.dates',
                'elementYear',
                Join::WITH,
                $qb->expr()->eq('elementYear.year', '(SELECT MIN(_elementYear.year) FROM '.ElementDate::class.' _elementYear WHERE _elementYear.element = element)')
            )
            ->addSelect('elementYear')
            ->leftJoin('element.elementSet', 'series')
            ->addSelect('series')
            ->leftJoin('element.illustrationRecto', 'illustrationRecto')
            ->addSelect('illustrationRecto')
            ->groupBy('resultId')
        ;

        foreach ($conditions as $condition => $value) {
            switch ($condition) {
                case 'black_and_white':
                    $qb
                        ->andWhere($qb->expr()->like('element.color', ':blackAndWhite'))
                        ->setParameter('blackAndWhite', ColorEnum::BLACK_AND_WHITE)
                    ;
                    break;
                case 'companies':
                    $qb
                        ->innerJoin(
                            'element.companies',
                            'elementCompany',
                            Join::WITH,
                            $qb->expr()->in('elementCompany.company', ':companies')
                        )
                        ->setParameter('companies', $value)
                    ;
                    break;
                case 'company':
                    $qb
                        ->innerJoin(
                            'element.companies',
                            'elementCompany',
                            Join::WITH,
                            $qb->expr()->eq('elementCompany.company', ':company')
                        )
                        ->setParameter('company', $value)
                    ;
                    break;
                case 'birthday':
                    $qb
                        ->innerJoin(
                            'element.dates',
                            'elementBirthdayDate',
                            Join::WITH,
                            $qb->expr()->andX(
                                $qb->expr()->eq('DAY(elementYear.date)', ':day'),
                                $qb->expr()->eq('MONTH(elementYear.date)', ':month')
                            )
                        )
                        ->setParameter('day', (int) $value->format('d'))
                        ->setParameter('month', (int) $value->format('m'))
                    ;
                    break;
                case 'element_set':
                    $qb
                        ->andWhere($qb->expr()->eq('series.id', ':elementSet'))
                        ->setParameter('elementSet', $value)
                    ;
                    break;
                case 'element_storage':
                    $this->addJoinElementSupport($qb, $user);
                    $qb
                        ->andWhere(
                            $qb->expr()->orX(
                                $qb->expr()->eq('elementSupport.storage', ':storage'),
                                $qb->expr()->eq('elementSupport.storageBackup', ':storage')
                            )
                        )
                        ->setParameter('storage', $value)
                    ;
                    break;
                case 'genre':
                    $qb
                        ->innerJoin('element.genres', 'genre', Join::WITH, $qb->expr()->eq('genre', ':genre'))
                        // Do not take if the element set also has the genre
                        ->leftJoin('series.genres', 'seriesGenre', Join::WITH, $qb->expr()->eq('seriesGenre', ':genre'))
                        ->andWhere($qb->expr()->isNull('seriesGenre'))
                        ->setParameter('genre', $value)
                    ;
                    break;
                case 'keyword':
                    $qb
                        ->innerJoin('element.keywords', 'keyword', Join::WITH, $qb->expr()->eq('keyword', ':keyword'))
                        ->setParameter('keyword', $value)
                    ;
                    break;
                case 'main_name':
                    $qb
                        ->andWhere($qb->expr()->like('mainElementName.name', ':main_name'))
                        ->setParameter('main_name', $value)
                    ;
                    break;
                case 'origin':
                    $qb
                        ->innerJoin('element.origins', 'origin', Join::WITH, $qb->expr()->eq('origin', ':origin'))
                        ->setParameter('origin', $value)
                    ;
                    break;
                case 'people':
                    $qb
                        ->innerJoin(
                            'element.people',
                            'elementPerson',
                            Join::WITH,
                            $qb->expr()->in('elementPerson.person', ':persons')
                        )
                        ->setParameter('persons', $value)
                    ;
                    break;
                case 'person':
                    $qb
                        ->innerJoin(
                            'element.people',
                            'elementPerson',
                            Join::WITH,
                            $qb->expr()->eq('elementPerson.person', ':person')
                        )
                        ->setParameter('person', $value)
                    ;
                    break;
                case 'platform':
                    $qb
                        ->innerJoin(
                            'element.platforms',
                            'platform',
                            Join::WITH,
                            $qb->expr()->eq('platform', ':platform')
                        )
                        ->setParameter('platform', $value)
                    ;
                    break;
                case 'query_advanced':
                    $valueSlug = new Slug($value);
                    $valueSlug->minify('_', false);
                    $qb
                        ->leftJoin('element.names', 'queryElementName')
                        ->leftJoin('element.subElements', 'querySubElement')
                        ->andWhere($qb->expr()->orx(
                            $qb->expr()->like('REPLACE(element.slug, \'_\', \'\')', ':querySlug'),
                            $qb->expr()->like('REPLACE(queryElementName.slug, \'_\', \'\')', ':querySlug'),
                            $qb->expr()->like('querySubElement.name', ':query'),
                            $qb->expr()->like('REPLACE(aggregatedSet.slug, \'_\', \'\')', ':querySlug'),
                            $qb->expr()->like('element.description', ':query'),
                            $qb->expr()->like('element.content', ':query')
                        ))
                        ->setParameter('query', '%'.$value.'%')
                        ->setParameter('querySlug', '%'.str_replace('_', '', $valueSlug->getText()).'%')
                    ;
                    break;
                case 'query_simple':
                    $valueSlug = new Slug($value);
                    $valueSlug->minify('_', false);
                    $qb
                        ->leftJoin('element.names', 'queryElementName')
                        ->andWhere($qb->expr()->orx(
                            $qb->expr()->like('REPLACE(element.slug, \'_\', \'\')', ':querySlug'),
                            $qb->expr()->like('REPLACE(queryElementName.slug, \'_\', \'\')', ':querySlug'),
                            $qb->expr()->like('REPLACE(aggregatedSet.slug, \'_\', \'\')', ':querySlug')
                        ))
                        ->setParameter('querySlug', '%'.str_replace('_', '', $valueSlug->getText()).'%')
                    ;
                    break;
                case 'set':
                    if (null === $value) {
                        break;
                    }

                    $qb
                        ->andWhere($qb->expr()->eq('element.set', ':set'))
                        ->setParameter('set', $value)
                    ;
                    break;
                case 'support':
                    $this->addJoinElementSupport($qb, $user);
                    $qb
                        ->innerJoin('elementSupport.support', 'support', Join::WITH, $qb->expr()->eq('support', ':support'))
                        ->setParameter('support', $value)
                    ;
                    break;
                case 'type':
                    $qb
                        ->andWhere($qb->expr()->eq('element.type', ':type'))
                        ->setParameter('type', $value)
                    ;
                    break;
                case 'types':
                    $qb
                        ->andWhere($qb->expr()->in('element.type', ':types'))
                        ->setParameter('types', $value)
                    ;
                    break;
                case 'universe':
                    $qb
                        ->innerJoin('element.universes', 'universe', Join::WITH, $qb->expr()->eq('universe', ':universe'))
                        // Do not take if the element set also has the universe
                        ->leftJoin('series.universes', 'seriesUniverse', Join::WITH, $qb->expr()->eq('seriesUniverse', ':universe'))
                        ->andWhere($qb->expr()->isNull('seriesUniverse'))
                        ->setParameter('universe', $value)
                    ;
                    break;
                case 'videoType':
                    $this->addVideoTypeCondition($qb, $value);
                    break;
                case 'year':
                    $qb
                        ->andWhere($qb->expr()->eq('elementYear.year', ':year'))
                        ->setParameter('year', $value)
                    ;
                    break;
            }
        }

        $this->addOrderBy($qb, $orderBy);

        if ($groupBySet) {
            $qb
                ->addSelect('COALESCE(aggregatedSet.id, element.id) AS resultId')
                ->addGroupBy('aggregatedSet.id')
            ;
        } else {
            $qb->addSelect('element.id AS resultId');
        }

        return $qb;
    }

    /**
     * Add the elementSupport jointure in the query builder.
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder The query builder
     * @param \App\Entity\User\User      $user         The user
     */
    private function addJoinElementSupport(QueryBuilder $queryBuilder, User $user): void
    {
        if (\in_array('elementSupport', $queryBuilder->getAllAliases(), true)) {
            return;
        }

        $this->addJoinUserElement($queryBuilder, $user);

        $queryBuilder
            ->innerJoin('userElement.supports', 'elementSupport')
        ;
    }

    /**
     * Add the userElement jointure in the query builder.
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder The query builder
     * @param \App\Entity\User\User      $user         The user
     */
    private function addJoinUserElement(QueryBuilder $queryBuilder, User $user): void
    {
        if (\in_array('userElement', $queryBuilder->getAllAliases(), true)) {
            return;
        }

        $queryBuilder
            ->innerJoin(
                UserElement::class,
                'userElement',
                Join::WITH,
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('element', 'userElement.element'),
                    $queryBuilder->expr()->eq('userElement.user', ':user')
                )
            )
            ->setParameter('user', $user)
        ;
    }

    /**
     * Add the video type condition.
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder The query builder
     * @param string                     $videoType    The video type
     */
    private function addVideoTypeCondition(QueryBuilder $queryBuilder, string $videoType): void
    {
        switch ($videoType) {
            case VideoTypeEnum::NON_ANIMATED_TELEVISION_SERIES:
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->like('element.videoType', ':videoType'))
                    ->setParameter('videoType', VideoTypeEnum::TELEVISION_SERIES)
                    ->andWhere($queryBuilder->expr()->notIn('element.id', $this->getAnimationVideoIdsSubQueryDql($queryBuilder)))
                ;
                break;
            case VideoTypeEnum::ANIMATED_SERIES:
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->like('element.videoType', ':videoType'))
                    ->setParameter('videoType', VideoTypeEnum::TELEVISION_SERIES)
                    ->andWhere($queryBuilder->expr()->in('element.id', $this->getAnimationVideoIdsSubQueryDql($queryBuilder)))
                ;
                break;
            case VideoTypeEnum::SHORT_FILM:
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->lt('element.duration', ':shortFilmDurationMax'))
                    ->setParameter('shortFilmDurationMax', DateTime::createFromFormat('U', VideoTypeEnum::SHORT_FILM_DURATION_MAX))
                    ->andWhere($queryBuilder->expr()->in('element.videoType', ':shortFilmTypes'))
                    ->setParameter('shortFilmTypes', [VideoTypeEnum::FILM, VideoTypeEnum::TELEVISION_FILM])
                ;
                break;
            default:
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->like('element.videoType', ':videoType'))
                    ->setParameter('videoType', $videoType)
                ;
        }
    }

    private function getAnimationVideoIdsSubQueryDql(QueryBuilder $parentQueryBuilder): string
    {
        $qb = $this->entityManager->getRepository(Element::class)->createQueryBuilder('video');

        $qb
            ->select('video.id')
            ->innerJoin('video.genres', 'genre', Join::WITH, $qb->expr()->eq('genre.id', ':animationId'))
        ;

        $parentQueryBuilder->setParameter('animationId', $this->elementVideoGenreAnimationId);

        return $qb;
    }

    /**
     * Add the order by in the query builder.
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder The query builder
     * @param array                      $orderBy      The order bys
     */
    private function addOrderBy(QueryBuilder $queryBuilder, array $orderBy): void
    {
        foreach ($orderBy as $property => $order) {
            switch ($property) {
                case 'title':
                    $queryBuilder
                        ->addOrderBy('namePart1', $order)
                        ->addOrderBy('namePart2', $order)
                        ->addOrderBy('namePart3', $order)
                    ;
                    break;
                case 'date':
                    $queryBuilder
                        ->leftJoin('element.dates', 'elementDate')
                        ->addSelect('COALESCE(elementDate.year, '.('asc' === mb_strtolower($order) ? '9999' : '-9999').') AS yearForSort')
                        ->addOrderBy('yearForSort', $order)
                        ->addOrderBy('elementDate.date', $order)
                    ;
                    break;
                case 'id':
                    $queryBuilder
                        ->addOrderBy('element.id', $order)
                    ;
                    break;
                default:
                    $queryBuilder->addOrderBy($property, $order);
            }
        }

        $queryBuilder->addOrderBy('elementYear.year');
    }
}
