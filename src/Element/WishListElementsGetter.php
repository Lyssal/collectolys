<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Element;

use App\Doctrine\Manager\User\UserElementManager;
use App\Entity\Type;
use App\Entity\User\UserElement;
use Lyssal\Entity\Decorator\DecoratorManager;
use Symfony\Component\Security\Core\Security;

/**
 * Get the user wish elements list.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2023 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class WishListElementsGetter
{
    public function __construct(
        private Security $security,
        private DecoratorManager $decoratorManager,
        private UserElementManager $userElementManager,
    ) {
    }

    public function get(?Type $type): array
    {
        return
            array_map(
                function (UserElement $userElement) {
                    return $this->decoratorManager->get($userElement->getElement());
                },
                $this->userElementManager->getWishList($this->security->getUser(), $type),
            )
        ;
    }
}
