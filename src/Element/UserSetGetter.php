<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Element;

use App\Doctrine\Manager\User\UserSetManager;
use App\Entity\Type;

/**
 * Get the user wish elements list.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2024 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class UserSetGetter
{
    public function __construct(private readonly UserSetManager $userSetManager)
    {
    }

    public function get(?Type $type): array
    {
        return $this->userSetManager->findBy(
            null !== $type ? ['type' => $type] : [],
            ['name' => 'ASC'],
        );
    }
}
