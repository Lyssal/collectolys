<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Element;

use App\Entity\Type;
use App\Entity\User\ElementUse;
use Doctrine\ORM\EntityManagerInterface;
use Lyssal\Entity\Decorator\DecoratorManager;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Security\Core\Security;

/**
 * Get the last used elements.
 *
 * @category Service
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class LastUsedElementsGetter
{
    /**
     * The security service.
     *
     * @var \Symfony\Component\Security\Core\Security
     */
    private $security;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface The Entity manager
     */
    private $entityManager;

    /**
     * @var \Lyssal\Entity\Decorator\DecoratorManager The decorator manager
     */
    private $decoratorManager;

    /**
     * The element count per page.
     *
     * @var int
     */
    private $elementCountPerPage;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Security\Core\Security $security            The security service
     * @param \Doctrine\ORM\EntityManagerInterface      $entityManager       The Entity manager
     * @param \Lyssal\Entity\Decorator\DecoratorManager $decoratorManager    The decorator manager
     * @param int                                       $elementCountPerPage The ElementUse repository
     */
    public function __construct(
        Security $security,
        EntityManagerInterface $entityManager,
        DecoratorManager $decoratorManager,
        int $elementCountPerPage
    ) {
        $this->security = $security;
        $this->entityManager = $entityManager;
        $this->decoratorManager = $decoratorManager;
        $this->elementCountPerPage = $elementCountPerPage;
    }

    /**
     * Get the view paramaters.
     *
     * @param int                   $page The page
     * @param \App\Entity\Type|null $type The Element type
     *
     * @throws \Lyssal\Entity\Decorator\Exception\DecoratorException
     *
     * @return array The view parameters
     */
    public function getViewParameters(int $page, ?Type $type = null): array
    {
        $pagerFanta = $this->getPagerfanta($page, $type);
        $elements =
            array_map(
                function (ElementUse $elementUse) {
                    $userElement = $elementUse->getElement();
                    $element = $userElement->getElement();

                    return $this->decoratorManager->get($element)->setUseDate($elementUse->getDate());
                },
                iterator_to_array($pagerFanta->getCurrentPageResults())
            )
        ;

        $parameters = [
            'sort' => null,
            'aggregate_by_set' => false,
            'result_pagerfanta' => $pagerFanta,
            'results' => $elements,
            'last_uses' => true,
        ];

        if (null !== $type) {
            $parameters['type'] = $type;
        }

        return $parameters;
    }

    /**
     * Get the Pagerfanta.
     *
     * @param int                   $page The page
     * @param \App\Entity\Type|null $type The Element type
     *
     * @return \Pagerfanta\Pagerfanta The Pagerfanta
     */
    private function getPagerfanta(int $page, ?Type $type): Pagerfanta
    {
        $queryBuilder = $this->entityManager->getRepository(ElementUse::class)->getLastsQueryBuilder($this->security->getUser(), $type);
        $adapter = new QueryAdapter($queryBuilder);
        $pagerFanta = new Pagerfanta($adapter);

        $pagerFanta->setMaxPerPage($this->elementCountPerPage);
        $pagerFanta->setCurrentPage($page);

        return $pagerFanta;
    }
}
