<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Custom;

use App\Doctrine\Manager\Element\ElementManager;
use App\Entity\Element\Element;
use Lyssal\Doctrine\Orm\QueryBuilder;

/**
 * List of a cinematic universe.
 *
 * @category Custom
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
abstract class AbstractCinematicUniverse
{
    /**
     * The Element manager.
     *
     * @var \App\Doctrine\Manager\Element\ElementManager
     */
    protected $elementManager;

    /**
     * Constructor.
     *
     * @param \App\Doctrine\Manager\Element\ElementManager $elementManager The Element manager
     */
    public function __construct(ElementManager $elementManager)
    {
        $this->elementManager = $elementManager;
    }

    /**
     * Get the Disney elements.
     *
     * @return \App\Entity\Element\Element[] The elements
     */
    public function get(): array
    {
        $elementIds = $this->getElementIds();
        $elements = $this->elementManager->findBy([
            QueryBuilder::WHERE_IN => ['id' => $elementIds],
        ]);

        // Sort elements according to the element list
        usort($elements, function (Element $element1, Element $element2) use ($elementIds) {
            return array_search($element1->getId(), $elementIds, true) <=> array_search($element2->getId(), $elementIds, true);
        });

        return $elements;
    }

    /**
     * Return the element IDs.
     *
     * @return int[] The element IDs
     */
    abstract protected function getElementIds(): array;
}
