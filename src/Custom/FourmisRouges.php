<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Custom;

use App\Entity\Element\Element;
use App\Entity\Element\Type\VideoGame;
use Lyssal\Doctrine\Orm\QueryBuilder;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;

/**
 * Les fourmis rouges.
 *
 * @category Custom
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class FourmisRouges
{
    /**
     * The element IDs.
     *
     * @var array<int, int[]>
     */
    const ELEMENTS = [
        1980 => [
            11770, // Zork I : The great underground empire
        ],

        1981 => [
            11771, // Zork II : The wizard of Frobozz
            15963, // Castle Wolfenstein
        ],

        1982 => [
            11772, // Zork III : The dungeon master
            10616, // Ultima II : Revenge of the Enchantress
        ],

        1983 => [
            10621, // Ultima III : Exodus
            2587, // Frog
            2588, // Carnaval
            15343, // Dig Dug
            10678, // 3-Demon
            2617, // Galaxian
            2481, // Frogger
        ],

        1984 => [
            15964, // Beyond Castle Wolfenstein
            2590, // Doggy
            5527, // Fruity Frank
            2589, // Le Sceptre d'Anubis
            5528, // Boulder Dash
            519, // 1990 King's Quest 1 : Quest For The Crown (réadaptation)
        ],

        1985 => [
            2591, // Infernal Runner
            3401, // Gauntlet
            10614, // Ultima IV : The Quest of the Avatar
            17406, // The Pawn
            604, // 1985 Orphée
            5529, // Boulder Dash II : Rockford's Revenge
            520, // 1987 King's Quest 2 : Romancing the Throne
        ],

        1986 => [
            2470, // Bob Winner
            5684, // Tomahawk
            10680, // M. G. T.
            3555, // Zombi
            15568, // Karma
            2479, // Crash Garrett
            17401, // Nitroglycérine
            13935, // À la poursuite de Carmen Sandiego dans le monde
            2592, // Le Pacte
            15819, // Leather Goddesses of Phobos
            2463, // L'Aigle d'Or
            697, // 1987 SRAM
            15641, // The Black Cauldron
            15871, // Space Quest 0: Replicated
            690, // 1990 Space Quest 1 : The Sarien Encounter (réadaptation)
            521, // 1987 King's Quest 3 : To Heir Is Human
        ],

        1987 => [
            15814, // Beyond Zork: The Coconut of Quendor
            5697, // Renaud : Marche à l'ombre
            20018, // Super Ski
            20048, // The Three Stooges
            6304, // Wall Street
            3557, // Cobra
            20063, // Test Drive
            3753, // Le Maître des âmes
            2510, // Sapiens
            5694, // Prohibition
            5742, // Les Trois mousquetaires
            5696, // Bob Morane : Jungle
            5720, // Bob Morane - Chevalerie
            5735, // Bob Morane : Science-fiction
            17409, // Gauntlet: The Deeper Dungeons
            2504, // Might and Magic : Book I
            15240, // Star Wars
            10615, // Ultima I : The first age of darkness
            17410, // Iznogoud
            19983, // Le Nécromancien
            17407, // Barbarian : Le Guerrier Absolu
            3754, // Meurtres en série
            15348, // Drakkhen
            750, // 1987 Top Secret
            17408, // Robinson Crusoé
            698, // 1987 SRAM 2
            2508, // Macadam Bumper
            17412, // Blueberry : Le spectre aux balles d'or
            17411, // Pharaon
            17403, // Trivial Pursuit: The Computer Game Genus Edition
            17402, // Le passager du temps
            15231, // Objectif France
            15236, // Objectif Europe
            13040, // Astérix chez Rahazade
            535, // 1998 La Chose de Grotemburg
            15250, // Au nom de l'hermine
            1555, // 1992 Police Quest 1 - In pursuit of the Death Angel
            1558, // 1987 Space Quest 2 : Vohaul's Revenge
            548, // 1991 Leisure Suit Larry 1 : The Land of the Lounge Lizards (réadaptation)
            2480, // Des chiffres et des lettres
            3755, // Le labyrinthe d'Orthophus
            20014, // Tetris
        ],

        1988 => [
            15830, // Zork Zero: The Revenge of Megaboz
            20034, // Barbarian II: The Dungeon of Drax
            13962, // Profession détective
            2484, // 20 000 Lieues sous les Mers
            15392, // Voyage au centre de la Terre
            5716, // Les 8 conquêtes d'Armorik le viking
            5750, // L'Affaire
            5721, // L'affaire Vera Cruz
            5770, // L'Affaire Sidney
            422, // 1989 Dungeon Master
            2505, // Might and Magic II : Gates to Another World
            1838, // Wings of Fury
            5828, // Dossier Kha
            1646, // Fire and Forget
            452, // 1987 Explora : Time Run
            5689, // Hurlements
            17416, // Sid Meier's Pirates!
            17414, // L'affaire Santa Fe
            3830, // Space Racer
            2486, // L'Arche du Capitaine Blood
            10619, // Ultima V : Warriors of Destiny
            13509, // Troubadours
            15965, // Maniac Mansion
            15420, // Flight Simulator 3
            542, // 1987 Le Manoir de Mortevielle
            543, // 1988 Le Manoir du Comte Frozarda
            2485, // BoBo
            6349, // Qin
            15360, // Gaminours : Formes et couleurs
            15357, // La bosse des maths
            1556, // 1988 Police Quest 2 - The vengeance
            15410, // 89 : La Révolution Française
            15872, // Space Quest: The Lost Chapter
            549, // 1988 Leisure Suit Larry 2 : Looking for Love
            778, // 1989 Zak Mc Kraken & the Alien Mindbenders
            15966, // The New Adventures of Zak McKracken
            522, // 1988 King's Quest 4 : The Perils of Rosella
            2462, // Arkanoid
            10583, // Street Fighter
            2472, // Bubble Ghost
        ],

        1989 => [
            6296, // Le Maître absolu
            5743, // Bob Morane : Océan
            19993, // Crazy Shot
            15823, // MechWarrior
            15408, // Tintin sur la Lune
            1966, // Orion Prime
            20019, // Oliver & Compagnie
            1139, // Targhan
            19204, // Batman: The Movie
            3402, // Gauntlet II
            3558, // Genghis Khan
            5730, // Opération Jupiter
            5812, // West Phaser
            6370, // Powerdrome
            1782, // Chicago 90
            15957, // Warlords
            20009, // La Quête de l'oiseau du temps
            15992, // Arthur: The Quest for Excalibur
            15598, // Weird Dreams
            15358, // Apprends-moi à lire
            2507, // Legend of Djel
            266, // 1989 Les Passagers du Vent
            17405, // Les passagers du vent 2 : L'heure du serpent
            1553, // 1989 Explora II
            558, // 1989 Les Portes du Temps
            257, // 1989 Fire!
            6348, // 007 : Licence to Kill
            15421, // Flight Simulator 4
            13553, // Paris révolutionnaire
            19959, // KULT: The Temple of Flying Saucers
            15228, // Gaminours fait un puzzle
            2777, // Astérix : Opération Getafix
            1559, // 1988 Space Quest 3 : The Pirates of Pestulon
            559, // 1989 Les Voyageurs du Temps
            540, // 1989 Le Fétiche Maya
            628, // 1990 Prince of Persia
            1839, // Bumpy
            15389, // Mean Streets
            1554, // 1989 Laura Bow : The Colonel's Bequest
            1617, // Arkanoid II : Revenge of Doh
            550, // 1989 Leisure Suit Larry 3 : Patti la Passion à la Poursuite des Pectoraux Puissants
            685, // 1989 Skweek
            498, // 1990 Indiana Jones and The Last Crusade
            1663, // SimCity
        ],

        1990 => [
            20064, // Test Drive III: The Passion
            15345, // Panza Kick Boxing
            3756, // Ranx
            3144, // Hammerin' Harry
            15242, // Ports of Call
            6294, // Hexsider
            1576, // 1990 Galactic Empire
            585, // 1990 Meurtre dans l'espace
            19766, // DragonStrike
            17404, // Dragon's Lair II: Escape from Singe's Castle
            586, // 1990 Meurtres à Venise
            16026, // Apprends-moi à compter
            1647, // Lode Runner
            272, // 1990 Sim Earth
            370, // 1990 Colorado
            1587, // 1992 Explora III
            770, // 1990 Wing Commander
            363, // 1990 Castle Master
            6431, // Castle Master II : The Crypt
            430, // 1991 Elvira 1 : Mistress of the Dark
            10620, // Ultima VI : The False Prophet
            10612, // Worlds of Ultima : The savage empire
            15247, // Adi
            15815, // Sid Meier's Railroad Tycoon
            5693, // Dick Tracy
            1645, // Fire and Forget 2 : The Death Convoy
            15352, // Austerlitz
            1934, // Commander Keen
            1130, // Commander Keen 2
            1935,  // Commander Keen 3
            15207, // Dino Sorcier
            6094, // La bande à Picsou : La ruée vers l'or
            262, // 1990 Iron Lord
            578, // 1990 Maupiti Island
            2513, // Time Race
            601, // 1990 Operation Stealth
            523, // 1990 King's Quest 5 : Absence makes the heart go yonder
            563, // 1990 Loom
            737, // 1990 The Secret of Monkey Island
        ],

        1991 => [
            1118, // Lagaf' : Les Aventures de Moktar
            20052, // Battle Isle
            15237, // Champion of the Raj
            616, // 1991 Planet's Edge
            3741, // Cadaver
            2839, // Mad TV
            15379, // Conan the Cimmerian
            383, // 1991 Croisière pour un cadavre
            10613, // Worlds of Ultima 2 : Martian dreams
            2514, // Wing Commander II : Vengeance of the Kilrathi
            1938, // Commander Keen - Keen Dreams
            1131, // Commander Keen 4
            1936, // Commander Keen 5
            1937, // Commander Keen 6
            1583, // 1991 Elvira 2 : The Jaws of Cerberus
            3736, // Elvira : The Arcade Game
            15224, // Merchant Colony
            15359, // Riders of Rohan
            1129, // Duke Nukem
            2596, // Might and Magic III : Isles of Terra
            2248, // Bumpy's arcade fantasy
            1577, // 1990 Eye of the Beholder
            1578, // 1991 Eye of the Beholder II : The Legend of Darkmoon
            5813, // The Simpsons : Arcade Game
            332, // 1991 Another World
            2511, // SimAnt
            15351, // L'Empereur
            2471, // Booly
            339, // 1992 Bargon Attack
            16025, // Les 4 saisons de l'écrit : Version CE / CM
            545, // 1991 Le Petit Chaperon Rouge
            3650, // Once Upon a Time : Baba Yaga
            3621, // Once Upon a Time : Abra Cadabra
            13508, // La crypte des maudits
            1581, // 1991 Fascination
            2487, // À la conquête de l'orthographe
            1557, // 1990 Police Quest 3 - The Kindred
            691, // 1991 Space Quest 4 : Roger Wilco & les voyageurs du temps
            551, // 1991 Leisure Suit Larry 5 : Passionate Patti Does a Little
            1584, // 1991 Gobliiins
            1582, // 1991 BushBuck - La course au trésor
            556, // 1991 Les Aventures de Willy Beamish
            626, // 1991 Prehistorik
            17895, // HeroQuest
            15244, // Castle of Dr. Brain
            15388, // Martian Memorandum
            2783, // Mangemot
            426, // 1991 Ecoquest 1 : le secret de la cité engloutie
            2076, // Lemmings
            2080, // Oh No! More Lemmings
            2077, // Xmas Lemmings 1991
            20080, // Orbits : Voyage à travers le système solaire
            1950, // Super Skweek
            3738, // 7 Colors
            1579, // 1990 Quadrel
            301, // 1991 Catacomb 3D : The Descent
            2619, // Catacomb Abyss
            2620, // Catacomb Armageddon
            2621, // Catacomb Apocalypse
            738, // 1990 The Secret of Monkey Island 2 : LeChuck's Revenge
        ],

        1992 => [
            1780, // Cannonade
            15377, // Les génies de l'Académie
            768, // 1992 Waxworks
            15978, // Genghis Khan II: Clan of the Gray Wolf
            13966, // Utopia
            15400, // Mosaïque
            769, // 1992 Ween : The Prophecy
            13936, // Abandoned Places : A Time For Heroes
            2461, // L'Aigle d'Or : Le Retour
            494, // 1993 Inca
            10617, // Ultima Underworld : The Stygian Abyss
            733, // 1992 The Lost Files of Sherlock Holmes : The Case Of The Serrated Scalpel
            1589, // 1992 Dune
            418, // 1992 Dune 2 : La Bataille d'Arrakis
            18236, // Comanche: Maximum Overkill
            1742, // Super Cauldron
            489, // 1992 Hook
            1119, // Nicky Boom
            2597, // Might and Magic IV : Les Nuages de Xeen
            15402, // The Simpsons: Bart's House of Weirdness
            5827, // D-Day
            6347, // Cyber Empires
            3758, // SimLife
            2506, // Leather Goddesses of Phobos 2
            15226, // Cogito
            1783, // Drôle d'école : Moins de 5 ans
            10641, // Drôle d'école : 5 à 7 ans
            10642, // Drôle d'école : 7 à 11 ans
            615, // 1992 Plan 9 From Outer Space
            1590, // 1992 Daughter of the Serpents
            3757, // Rome A.d. 42
            3829, // Aquaphobia
            324, // 1992 Alone in the Dark
            13937, // Adibou : Accompagnement scolaire 4-7 ans
            2640, // S.C. Out
            539, // 1992 Laura Bow 2 : Dagger of Amon Râ
            594, // 1992 Nippon Safes Inc
            639, // 1992 Quest for Glory 3 : les Gages de la Guerre
            3598, // Mickey ABC : une journée à la fête
            15233, // Mickey : Puzzles animés
            15213, // Mickey : Jeu de mémoire
            3811, // Ishar : Legend of the Fortress
            1588, // 1992 Lure of the Temptress
            1585, // 1992 Gobliins II
            3720, // Trivial Pursuit Deluxe
            582, // 1991 Mega Lo Mania
            447, // 1992 Eternam
            729, // 1992 The legend Of Kyrandia - Book 1
            357, // 1993 Caesar
            1954, // Tiny Skweeks
            524, // 1992 King's Quest 6
            2512, // Swap
            497, // 1992 Indiana Jones and the Fate of Atlantis
            2078, // Xmas Lemmings 1992
            4190, // Ultima VII : La Porte Noire
            10549, // Ultima VII : Part two : Serpent Isle
            772, // 1992 Wolfenstein 3D (avec Windows XP)
            305, // 1992 Spear of destiny (Wolfenstein II) (avec No One Lives Forever II)
            2637, // Spear of Destiny : Mission 2 : Return to danger
            2638, // Spear of Destiny : Mission 3 : Ultimate Challenge
            773, // 2001 Wolfenstein : Spear Resurrection
            1779, // Wolfenstein 3D : Spear End of Destiny
            6412, // Unsung
        ],

        1993 => [
            311, // 1993 Aces over Europe
            2763, // Wing Commander : Privateer
            11739, // Wing Commander : Privateer : Righteous Fire
            20038, // Peter : Les drôles de rêves
            17549, // MegaRace
            1592, // 1993 Isle of the Dead
            5037, // Toy Story
            1836, // The legacy realm of terror
            10618, // Ultima Underworld II : Labyrinth of Worlds
            318, // 1993 Qwak
            1586, // 1992 X-Wing
            454, // 1992 Eye of the Beholder III : Assault on Myth Drannor
            3712, // Nord & Sud
            17859, // Microcosm
            15975, // McDonaldland
            18051, // Liberation: Captive 2
            13550, // Le jardin enchanté
            471, // 1994 Genesia
            2622, // Duke Nukem 2
            15958, // Warlords II
            13038, // Bio Menace
            699, // 1993 Star Trek : Judgment Rites
            3713, // Les Ani'malins numéro 1
            10584, // Balloonz
            420, // 1993 Dungeon Hack
            1664, // Flight Simulator
            4113, // The Journeyman Project
            13551, // Bram Stoker's Dracula
            1594, // 1993 Veil of Darkness
            17550, // Star Wars: Rebel Assault
            2784, // Tesserae
            3812, // Ishar 2 : Messengers of Doom
            2509, // Mario's Time Machine
            1228, // 1992 Flashback
            3854, // Carlos
            15245, // Ken's Labyrinth
            2598, // Might and Magic V : La Face Cachée de Xeen
            10333, // Nicky Boom 2
            505, // 1993 Jurassic Park
            568, // 1993 Lost In Time
            15210, // Wordtris
            1593, // 1993 Le Chateau Magique de Scooter
            362, // 1994 Cannon Fodder
            1595, // 1993 Black sect
            6368, // The Terminator : Rampage
            5784, // Expédition Amazone
            649, // 1993 Return to Zork
            1597, // 1993 Freddy Pharkas - le pharmacien cowboy
            317, // 1993 Alien Breed
            500, // 1993 Innocent Until Caught
            15885, // The 7th Guest
            15397, // Le lièvre et la tortue
            15383, // Time Runners : La Porte du temps
            623, // 1993 Police Quest 4 - Open season
            1601, // 1994 Lands of Lore : The Throne of Chaos
            730, // 1993 The legend Of Kyrandia - Book 2 - The Hand of Fate
            2082, // Holiday Lemmings 1993
            1128, // Fury of the Furries
            552, // 1993 Leisure Suit Larry 6 : tu t'accroches ou tu décroches
            473, // 1993 Goblins 3
            692, // 1993 Space Quest 5 : La Mutation Suivante
            575, // 1992 Mario a disparu
            427, // 1993 Ecoquest 2 : S.O.S. forêt vierge
            397, // 1993 Day of the tentacle
            10495, // Monopoly
            534, // 1993 La Belle et la Bête
            3745, // Star Wars Chess
            657, // 1993 Sam & Max Hit the Road
            348, // 1993 Blake Stone - Aliens of gold
            334, // 1995 Astérix - le défi de César
            20090, // Du Big Bang aux dinosaures
            683, // 1993 Simon the Sorcerer
            684, // 1996 Simon the Sorcerer 2
            1580, // 1991 Civilization
            1145, // 1993 Doom (1995 The Ultimate Doom, avec Quake IV)
        ],

        1994 => [
            15962, // Wing Commander: Armada
            15235, // Battle Isle 2
            16057, // UFO: Enemy Unknown
            6298, // Sabre Team
            15880, // The Clue!
            16053, // SimTower
            15977, // The Yukon Trail
            19966, // L'Anagrammeur
            19967, // Terrace
            19998, // Peter & le Père Noël
            20028, // Tricky Quiky Games
            20043, // Circus !
            13949, // Jeu de dames
            13944, // Dames 2020
            13964, // Ravenloft : La possession de Strahd
            13135, // Star Wars : Tie Fighter
            13136, // Star Wars : Tie Fighter : Defender of the Empire
            15382, // D-Day : 6 juin 1944
            13952, // Harry et la maison hantée
            10585, // Isis
            5811, // Sammy, la maison des sciences
            1824, // Gadget : Past as Future
            687, // 1994 Soccer Kid
            14808, // 1993 Syndicate
            3760, // Syndicate : American Revolt
            17896, // HeroQuest II: Legacy of Sorasil
            6350, // Astronomica
            1603, // Anvil of Dawn
            757, // 1994 Universe
            394, // 1992 Dark Seed
            2759, // Wing Commander III : Heart of the Tiger
            314, // 1994 Aladdin
            1598, // 1994 The Elder Scrolls : Arena
            755, // 1994 Under a Killing Moon
            495, // 1994 Inca 2 : Wiracocha
            502, // 1994 Jagged Alliance
            501, // 1993 Jack In The Dark
            356, // 1994 Bureau 13
            15373, // Peter & Julie au pays des couleurs
            5695, // Earthsiege
            428, // 1994 Ecstatica
            573, // 1994 Magic Carpet
            499, // 1994 Inferno
            1599, // 1994 Cannon Fodder 2
            606, // 1994 Outpost
            10496, // Skunny Kart
            2081, // Lemmings 2 : The Tribes
            1591, // 1992 Conspiracy OU KGB
            754, // 1994 Ultima VIII
            2079, // Holiday Lemmings 1994
            541, // 1998 Le Livre de la Jungle
            15209, // Peter Pan : La trousse magique
            15222, // Hell : A Cyberpunk Thriller
            2083, // Lemmings 3 : Chronicles
            15371, // Atelier de jeux : Aladdin
            15365, // CyClones
            385, // 1994 Cyberia
            325, // 1993 Alone In The Dark 2
            2635, // The Fortress of Dr Radiaki
            2632, // Operation Body Count
            618, // 1994 Playtoons 1 : Oncle Archibald
            619, // 1994 Playtoons 2 : Spirou - MicMac à Champignac
            714, // 1994 System Shock
            630, // 1993 Prince of Persia 2 : The Shadow & The Flame
            2627, // Depth Dwellers
            6437, // Lode Runner : The legend returns
            3205, // Le Monde sous-marin
            3145, // Robinson's Requiem
            3813, // Ishar 3 : Seven Gates of Infinity
            5732, // Le Corps humain en 3 dimensions
            561, // 1995 Little Big Adventure
            2625, // Corridor 7
            5749, // Les Insectes et autres petites bêtes
            614, // 1993 Pirates! Gold
            349, // 1994 Blake Stone 2 - Planet Strike
            596, // 1994 Noctropolis
            731, // 1994 The legend Of Kyrandia - Book 3 - Malcolm's Revenge
            416, // 1994 Dreamweb
            19970, // Le Pinceau magique II
            1600, // 1994 Raptor : Call of the Shadows
            263, // 1994 King's Quest 7
            343, // 1994 Beneath a steel sky
            1596, // 1993 Gabriel Knight
            1148, // 1994 Myst
            670, // 1993 Shadow of the comet
            1170, // Shanghaï : Grands Moments
            412, // 1994 Dragon Lore : la légende commence
            2044, // Lords of the Realm
            4, // 1994 Theme Park
            1143, // 1994 Doom II : Hell on Earth
            2626, // Master Levels for Doom II
            15183, // Doom II: TNT: Evilution
            15184, // Doom II: The Plutonia Experiment
            2650, // Doom II : Hell Revealed
            2651, // Doom II : Hell Revealed II
            2652, // Doom II : The Alien Vendetta
            6413, // Doom II : PsychoPhobia
        ],

        1995 => [
            15959, // Wing Commander IV : Le prix de la liberté
            17897, // Blood Bowl
            13948, // Funball
            19958, // Le Livre de Lulu
            19962, // Peter & Julie : Les Tables de Multiplication
            13388, // Frontier : First Encounters
            19973, // Les Contes Magiques : Le Petit Samouraï
            19989, // The Daedalus Encounter
            19990, // Une Aventure à Noël
            20070, // Mini-Loup à l'école
            13956, // Les aventures de Batman et Robin : Mon studio de cartoon
            16058, // X-COM: Terror from the Deep
            15931, // Pouce-Pouce sauve le zoo
            13554, // Lords of Midnight : The Citadel
            11372, // Le bus magique explore le corps humain
            13033, // Le bus magique explore le système solaire
            10679, // Karma : Curse of the 12 caves
            6302, // Star Trek : The Next Generation - A Final Unity
            10561, // Blue ice
            19961, // Alien Virus
            15403, // Les vacances de Caroline
            5800, // Lisa, la maison du temps et de l'espace
            15217, // Kiyeko et les voleurs de nuit
            6299, // Apache Longbow
            1147, // Pac PC
            15399, // Casper : Histoire Interactive
            3732, // Thexder
            3778, // Congo: The Movie - Descent into Zinj
            2221, // You Don't Know Jack
            1840, // Starball
            1826, // In the 1st degree
            1821, // Bad day on the midway
            399, // 1995 Defcon 5
            453, // 1995 Extractors : The Hanging Worlds of Zarg
            2494, // Fort Boyard : Le Défi
            1138, // Super Street Fighter II Turbo
            716, // 1995 Tekwar
            1745, // BC Racers
            1632, // 1995 Les Guignols de L'Info...Le Jeu
            1155, // 3D Pinball : Space Cadet
            774, // 1994 Woodruff and The Schnibble of Azimuth
            273, // 1995 Sim Isle : Missions dans la forêt tropicale
            620, // 1995 Playtoons 3 : Le Secret du Chateau
            15103, // Playtoons 4 : Le prince Mandarine
            621, // 1995 Playtoons 5 : La Pierre de Wakan
            461, // 1995 Frankenstein : A travers les Yeux du Monstre
            528, // 1995 L'Enigme de Maitre Lu
            372, // 1995 Comix Zone
            15387, // Atelier de jeux : Le Roi Lion
            15391, // L'île au trésor
            612, // 1995 Phantasmagoria
            717, // 1995 Terminal Velocity
            1952, // The Settlers
            3178, // Police Quest : SWAT
            3750, // Diggers
            464, // 1995 Full Throttle
            574, // 1996 Magic Carpet 2
            16030, // Connexions
            424, // 1995 Dust : A Tale of the Wired West
            387, // 1995 Cybermage : Darklight Awakening
            693, // 1995 Space Quest 6 : Roger Wilco in the Spinal Frontier
            727, // 1995 The Journeyman Project 2 : Buried in Time
            368, // 1995 CivNet
            752, // 1995 Torin's Passage
            423, // 1994 Dungeon Master 2 : The legend of Skullkeep
            467, // 1995 Gabriel Knight 2 : The Beast Within
            395, // 1995 Dark Seed 2
            345, // 1995 Bioforge
            3742, // Commander Blood
            603, // 1995 Orion Conspiracy
            322, // 1995 Aliens : A Comic Book Adventure
            723, // 1995 The dark eye
            581, // 1995 Mechwarrior 2
            386, // 1995 Cyberia 2 : Resurrection
            3380, // Dr. Brain a perdu la tête !
            1673, // 1996 Brain Dead 13
            270, // 1995 The 11th Hour
            479, // 1995 Guilty
            15882, // Virtua Cop
            6295, // Zone Raiders
            2084, // Lemmings 3D
            2085, // Lemmings 3D Winterland
            16022, // Baba Yaga et les oies magiques
            2360, // S.T.O.R.M.
            320, // 1995 Alien Odyssey
            2634, // The Terminator : Future Shock
            4962, // Pitfall : The Mayan Adventure
            3897, // Les Ani'malins numéro 2
            5759, // Les Ani'malins numéro 3
            1668, // 1995 I Have no Mouth and I Must Scream
            16045, // High Seas Trader
            724, // The Dig
            627, // 1995 Primal Rage
            369, // 1994 Colonization
            2838, // Capitalism
            2496, // Garfield : Caught in the Act
            2623, // 3D Ultra Pinball
            3746, // The Big Red Adventure
            3668, // Les Schtroumpfs : le téléportaschtroumpf
            16046, // Marine Malice : Le mystère des graines d'algues
            682, // SimCity 2000
            392, // 1995 Dark Forces
            403, // 1995 Discworld
            567, // 1995 Lost Eden
            632, // 1995 Prisoner of Ice
            358, // 1996 Caesar II
            326, // 1995 Alone in the dark 3
            15401, // Voyage sur le Nil
            2636, // Witchaven
            2609, // Stonekeep
            2429, // Wetlands
            2012, // Rise of the Triad : Dark War
            2633, // Extreme Rise of the triad
            13031, // Explorateurs du nouveau monde
            3426, // Descent
            373, // 1995 Command & Conquer
            2492, // Command & Conquer : Opérations Survie
            484, // 1994 Heretic (avec Red Faction)
            6414, // Heretic : Hordes of choas
            261, // 1995 Worms
            2102, // Rayman
            3560, // Rayman Forever
            3759, // SimTown
            486, // 1995 Hexen : Beyond Heretic
            1691, // 1996 Hexen : Deathkings of the Dark Citadel
            458, // 1995 L'Amazone Queen (La Reine Amazone)
            764, // 1995 WarCraft : Orcs & Humans
        ],

        1996 => [
            16040, // SimCopter
            19991, // EdenDaho : Le Jeu
            17552, // Silent Hunter
            16052, // SimPark
            19956, // Un bébé ? Quelle drôle d'idée
            19974, // Time Paradox
            20071, // Drôle de Noël pour Mini-Loup
            19977, // Grégoire et la Montgolfière
            19205, // Deus
            17548, // MegaRace 2
            15238, // Playtoons : Playtoons Jeu de Construction 1 : Les Monstres
            13959, // Olympic Games
            17556, // Sega Touring Car Championship
            15344, // Johnny Bazookatone
            5532, // Robert E. Lee : Civil War General
            11386, // Sea Legends
            13036, // Banzai Bug
            13390, // Reloaded
            16042, // Toutou Sauteur
            16041, // Ballon Rama
            3752, // Fade to Black
            16033, // Tim, Tom et Zoé
            2497, // Hariboy's Quest
            1834, // Ripper
            4898, // This means War
            15218, // Nihilist
            16034, // Imo et le roi
            5652, // Le miroir sacré du Kofun
            1829, // Les aventures du 5ème mousquetaire
            1644, // Death Rally
            1832, // Private Eye
            20069, // Amber: Journeys Beyond
            1823, // Evidence : The Last Report
            4500, // Wooden Ships & Iron Men
            13439, // Les incroyables machines du Prof. Tim
            2499, // Heroes of Might and Magic
            1633, // Le Cauchemar De PPD
            711, // 1996 SWIV Temperate Zone
            470, // 1996 Gene Wars
            15818, // Monster Truck Madness
            3831, // Earthsiege 2
            579, // 1996 M.A.X. : Mechanised Assault And Exploration
            3651, // Master of Dimensions
            18275, // Chronicles of the Sword
            2086, // Lemmings Paintball
            3737, // Chessmaster 5000
            15418, // Ms Pac PC
            15422, // Flight Simulator 95
            19925, // Chex Quest
            19926, // Chex Quest 2: Flemoids Take Chextropolis
            19927, // Chex Quest 3
            15960, // Witchaven II: Blood Vengeance
            15362, // MissionForce : Cyberstorm
            20089, // Close Combat
            3284, // Pyramide : le défi de Pharaon
            5731, // Age of Rifles : 1846-1905
            1817, // 3D Ultra Pinball : Creep Night
            2593, // Le Trésor des Toltèques
            1818, // Angel Devoid : Face of the Enemy
            1670, // 1996 The Gene Machine
            10588, // Pandemonium!
            3747, // Tintin au Tibet
            756, // 1996 Une poupée pleine aux as
            527, // 1995 L'affaire Morlov
            384, // 1995 Crusader : No Remorse
            760, // 1996 Urban Runner
            664, // 1996 Secret Mission
            16051, // Sam Pyjam : Héros de la nuit
            673, // 1996 Sherlock Holmes - L'Affaire de la Rose tatouée
            474, // 1996 Golden Gate Killer
            460, // 1996 Fragile Allegiance
            307, // 1996 9 : The Last Resort
            732, // Lighthouse : The Dark Being
            533, // 1996 La bataille de Waterloo
            11385, // Baku Baku Animal
            16047, // Marine Malice : Le mystère de l'école hantée
            414, // 1996 Dragor le dragon
            635, // 1996 Pyst
            269, // 1996 Phantasmagoria 2
            695, // 1996 Spirou
            1671, // 1996 Spycraft
            482, // 1996 Harvester
            315, // 1996 Albion
            597, // 1996 Normality
            351, // 1996 Blood & Magic
            410, // 1996 Down in the Dumps
            749, // 1996 Toonstruck
            672, // 1996 Shellshock
            713, // 1996 Syndicate Wars
            705, // 1996 Stargunner
            10586, // Cyber Gladiators
            3421, // Astérix & Obélix
            415, // 1996 Drascula
            745, // 1996 Time Gate : Le secret du templier
            338, // 1996 Bad Mojo
            355, // 1996 Bud Tucker in Double Trouble
            736, // 1996 The Pandora Directive
            602, // 1996 Orion Burger
            321, // 1996 Alien Trilogy
            677, // 1996 Shivers
            3978, // Holiday Island
            1165, // 1996 Assassin 2015
            3142, // Champions en s'amusant
            746, // 1996 Timelapse
            2616, // Warhammer : Dans l'ombre du Rat Cornu
            328, // 1996 A.M.O.K.
            13950, // Grégoire et la montgolfière
            13947, // Dracula : Le mystère du château
            379, // 1996 Conquest of the New World
            742, // 1996 The Settlers II
            13960, // Où est Charlie ? Au cirque
            15380, // Opération Teddy Bear
            781, // 1996 Zork Nemesis
            771, // 1996 Wing Commander Privateer 2 : The Darkening
            15407, // La reine des neiges
            344, // 1996 Bermuda Syndrome
            15249, // Virtua Cop 2
            402, // 1996 Die Hard Trilogy
            310, // 1996 Abuse
            1672, // 1996 Killing Time
            1167, // 1996 Strife
            319, // 1996 Alien Incident
            490, // 1996 Hunter Hunted
            15349, // La machine à remonter le temps
            1689, // 1996 Discworld 2 : Mortellement Vôtre
            3990, // Birthright : Le Pacte des Ténèbres
            553, // 1996 Leisure Suit Larry 7 : Drague en haute mer
            11370, // Pray for death
            2498, // Heroes of Might and Magic II : The Succession Wars
            15923, // Heroes of Might and Magic II: The Price of Loyalty
            15212, // Deadly Tide
            3379, // Descent II
            1690, // 1996 Warwind
            590, // 1996 Necrodome
            3141, // AH-64D Longbow
            1988, // Les Chevaliers de Baphomet
            1951, // The Elder Scrolls II : Daggerfall
            2259, // Archimedean Dynasty
            1694, // 1996 Fable
            5513, // Tomb Raider
            15994, // Tomb Raider: Unfinished Business
            2780, // Lords of the Realm II
            2781, // Lords of the Realm II : Siege Pack
            1133, // 1995 Bedlam (avec Star Wars : Galactic Battlegrounds)
            3430, // Fort Boyard : La Légende
            5733, // Des chiffres et des lettres
            5687, // Risk : La Conquête du monde
            5717, // Eradicator
            15208, // The Rise & Rule of Ancient Empires
            1728, // 1996 Versailles : Complot à la Cour du Roi Soleil
            296, // 1996 Command & Conquer : Alerte Rouge
            1682, // 1997 Command & Conquer : Alerte Rouge : Missions Taïga
            1681, // 1997 Command & Conquer : Alerte Rouge : Missions M.A.D.
            450, // 1996 Exhumed
            295, // 1996 Civilization II
            14931, // Civilization II : Conflicts in Civilization
            14932, // Civilization II : Fantastic Worlds
            636, // 1996 Quake
            2628, // Quake Mission Pack N°1 : Scourge of Armagon
            2629, // Quake Mission Pack N°2 : Dissolution of Eternity
            15822, // Malice
            15827, // Shrak
            15824, // Q!Zone
            15886, // Titanic: The Lost Mission
            271, // 1997 Titanic : une aventure hors du Temps
            765, // 1996 WarCraft II : Tides of Darkness (Les mar�es des t�n�bres)
            1157, // 1997 WarCraft II : Beyond the dark portal (Derri�re la porte noir)
            6095, // Warcraft II : Tides of Darkness : The Next 70 Levels
            417, // 1996 Duke Nukem 3D
            1181, // Duke Nukem 3D : DukeZone II
            1179, // Duke Nukem 3D : Duke Caribbean : Life's a Beach
            1177, // Duke Nukem 3D : Duke Out in D.C.
            1180, // Duke Nukem 3D : Duke Xtreme
            1178, // Duke Nukem 3D : Nuclear Winter
        ],

        1997 => [
            11371, // Safecracker
            17555, // TOCA Touring Car Championship
            19972, // Les Vacances de Camisole
            19980, // Interstate '76
            16044, // Balls of Steel
            16036, // Micro Machines V3
            15211, // Barbie : Salon de beauté
            13951, // Hacx : Twitch'n Kill
            13556, // Le piège diabolique
            20051, // Battle Isle 3 : L'Ombre de l'Empereur
            13549, // Mecanoid 97
            16035, // Oui-Oui au pays des jouets
            15969, // War Wind II: Human Onslaught
            20065, // Perfect Assassin
            20062, // Pax Imperia: Eminent Domain
            20081, // Pingu : Le CD-ROM des petits pingouins
            13429, // Mots croisés & autres jeux de mots
            19383, // Outlaws
            19384, // Outlaws: Handful of Missions
            20013, // Civil War Generals 2
            13389, // Perfect Weapon
            6375, // Men in Black : The Game
            10597, // The fourth generation
            6360, // Légendes souterraines
            6301, // Othello
            13034, // Le mystère XIII
            4078, // Treasure Hunter : Chasseur de trésors
            3898, // Time Warriors
            15419, // Pac PC 2
            5799, // KROM : Le Grand Country d'en haut
            3387, // Atomic Bomberman
            2458, // Creatures
            4149, // Close Combat : Un pont trop loin
            10587, // Pandemonium 2
            3384, // ADI 2.7 - Géographie CE2-3ème
            2087, // Lomax
            2764, // X-com : Apocalypse
            5829, // Conquest Earth
            1827, // Koala Lumpur
            5775, // Offensive
            1640, // Le Jardin Magique
            1828, // La galère des étoiles
            15367, // La Princesse Dragonne
            1820, // Armed and delirious : Dementia
            480, // 1996 Hardline
            15986, // La Maison Hantée
            15227, // Guts 'n' Garters : Les espions du futur
            536, // 1997 La cité des enfants perdus
            1822, // Blackstone Chronicles
            388, // 1997 Cyclone : cité des âmes perdues
            642, // 1996 RAMA
            1819, // Ark of Time
            1695, // 1997 Byzantine
            1141, // 1997 Claw
            2125, // SPY Fox 1 : Opération Milkshake
            3748, // Tintin : Le Temple du Soleil
            10520, // Dracula : Le guerrier des Carpates
            3669, // Les Mystères de Louxor
            776, // 1997 Worms 2
            1641, // Flipper Mania
            15985, // L'incroyable aventure sous-marine de Timmy
            780, // 1997 Zork Grand Inquisiteur
            1781, // Saban's Iznogoud
            10480, // Impérialisme : À la conquête du monde
            651, // 1997 Rising Lands
            1696, // 1997 Wing Commander Prophecy
            767, // 1997 Warlords 3 : Reign Of Heroes
            538, // 1997 Lands of Lore 2 : Les Gardiens de la Destinée
            347, // 1997 Blade Runner
            429, // 1996 Ecstatica II
            725, // 1997 The Feeble files
            3800, // Operation Panzer
            13955, // Les 101 dalmatiens : Livre animé interactif
            555, // 1997 Les 9 Destins De Valdo
            10481, // La panthère rose : Passeport pour le danger
            15998, // An Elder Scrolls Legend: Battlespire
            4877, // Frogger
            13957, // Need For Speed II
            1835, // SPQR : L'heure la plus sombre de l'Empire
            565, // 1997 Lords of Magic
            1638, // 3D Ultra Pinball : Le Continent Perdu
            382, // 1997 Croisades : conspiration au royaume d'Orient
            715, // 1997 Take No Prisoners
            15393, // Toutankhamon
            1109, // 1998 Seven Kingdoms
            6351, // Boggle
            15372, // Livre animé interactif : Hercule
            3232, // Oddworld : L'Odyssée d'Abe
            15384, // Apocalypse : La Tenture du Château d'Angers
            15376, // Le roi des fjords
            10497, // Dessinez c'est Disney
            15215, // Rayman Eveil
            15979, // Robinson Crusoé
            1229, // 1997 Dark Colony
            3526, // Dark Colony: The Council War
            2613, // The Reap
            2059, // Fallout
            6303, // The Feeble Files
            19208, // Star Wars Jedi Knight: Dark Forces II
            19209, // Star Wars Jedi Knight: Mysteries of the Sith
            3411, // Microshaft Winblows 98
            1171, // 1997 Grand Thieft Auto
            413, // 1997 Dragon Lore 2 : Le Cœur de l'Homme Dragon
            2134, // Turok : Dinosaur Hunter
            264, // 1997 Le Voleur d'Esprit
            389, // 1997 Dark Earth
            728, // 1997 The Last Express
            352, // 1997 Blood Omen : Legacy of Kain
            562, // 1997 Little Big Adventure 2
            1166, // Pod : Planet of Death
            1837, // Imperium Galactica
            5531, // Carmageddon
            17557, // Carmageddon: Splat Pack
            5510, // Tomb Raider II
            15995, // Tomb Raider 2 : Le Masque d'or
            16049, // MDK
            15243, // Monopoly Star Wars
            1153, // 1997 Hexen II : le commencement de la fin
            2010, // Hexen II : Portal of Praevus
            280, // 1997 Diablo
            281, // 1997 Diablo Hellfire
            400, // 1997 Démons et manants
            459, // 1997 Forsaken
            571, // 1997 Machine Hunter
            2515, // Independance Day
            2624, // Chasm : The Rift
            1987, // Les Chevaliers de Baphomet : Les Boucliers de Quetzalcoatl
            2456, // Atlantis
            2610, // Sub Culture
            19928, // L'IneXpliqué
            15220, // Aventures sur l'île Lego
            15364, // Cluedo
            10763, // Total Annihilation
            10764, // Total Annihilation : Contre-attaque
            15883, // Total Annihilation : Batailles Stratégiques
            6353, // Touché-Coulé
            421, // 1997 Dungeon Keeper
            648, // 1997 Resident Evil
            13939, // Au coeur des planètes
            6908, // Warcraft Adventures: Lord of the Clans
            38, // Theme Hospital
            2668, // Postal
            2669, // Postal : Special Delivery
            1151, // Egypte : 1156 avant Jésus-Christ, l'énigme de la tombe royale
            350, // 1997 Blood
            637, // 1997 Quake II
            15932, // Quake II Mission Pack: The Reckoning
            15933, // Quake II Mission Pack: Ground Zero
            15934, // Quake II Netpack I: Extremities
            15821, // Juggernaut: The new story
            15829, // Zaero
            11742, // Streets of SimCity
            1125, // 1997 Redneck Rampage
            647, // 1997 Redneck Rampage: Suckin' Grits on Route 66
            646, // 1998 Redneck Rampage Rides Again
            1, // 1997 Age of Empires
            5768, // Conquests of the Ages
            8, // 1998 Age of Empires : l'�mergence de Rome
            20088, // Age of Empires : 2ème édition
        ],

        1998 => [
            20011, // Barbie détective dans Le Mystère de la Fête Foraine
            13967, // Wreckin Crew
            20045, // L'Affaire Zak le chien
            20046, // Les Galaxiens
            20082, // Les Aventures de Robby le Robot : Mission Code de la Route
            20083, // Les Aventures de Robby le Robot : L'Énigme du Sphinx
            20086, // L'Anniversaire d'Arthur
            1630, // Remington Top Shot
            10638, // Wallace et Gromit, le jeu
            13417, // 14 jeux de cartes
            3751, // Ubik
            17553, // Plane Crazy
            17554, // Ultim@te Race Pro
            6372, // Glover
            1830, // Liath
            6354, // Queen : The Eye
            15811, // Descent to Undermountain
            1625, // Creatures 2
            3407, // Shogo : Mobile Armor Division
            3714, // M.A.X. 2 — Mechanized Assault & Exploration
            16032, // Fais ton histoire ! Mulan
            3743, // Big Bug Bang : Le Retour de Commander Blood
            15368, // Le 3e Millénaire
            15221, // Wing Commander : Secret Ops
            2108, // Simon the Sorcerer's Pinball
            1202, // Pingus
            10765, // Kitchenette
            13039, // La panthère rose 2 : Destination mystère
            354, // Bubble Kids
            1669, // 1995 A IV Network$
            483, // 1998 Heart of Darkness
            1746, // 1998 L'album secret de l'oncle Ernest
            2495, // Gangsters : Le Crime Organisé
            15347, // Monster Truck Madness 2
            13030, // The house of the dead
            2478, // 1001 pattes
            1142, // Railroad Tycoon II
            1825, // Hopkins FBI
            1736, // Meurtre au Manoir
            284, // 1998 Might and Magic VI : Le mandat céleste
            622, // 1998 Pocahontas
            598, // 1998 Of Light and Darkness - The Prophecy
            719, // 1998 Tex Murphy : Overseer
            644, // 1998 Reah - Face the Unknown
            659, // 1998 Scotland Yard
            371, // 1998 Comanche 3
            466, // 1997 G-Police
            1697, // 1997 Pilgrim : par le Book et par l'Epée
            378, // 1998 Conflict Freespace : The Great War
            1698, // 1998 Dune 2000
            330, // 1998 Anno 1602 : A la Conquête d'un Nouveau Monde
            15807, // Anno 1602 : Nouvelles Îles, Nouvelle Aventures
            2603, // Shadow Master
            346, // 1998 Black Dahlia
            10494, // Le cinquième élément
            13958, // O.D.T
            13954, // Le maître des éléments
            13953, // Last Bronx
            1149, // 1998 Missing In Action
            5792, // Police Quest : SWAT 2
            487, // 1998 Hexplore
            5774, // Enemy Zero
            13961, // Powerslide
            3231, // Oddworld : L'Exode d'Abe
            15385, // Laura et le secret du diamant
            15404, // Gast
            13942, // Croc : Legend of the Gobbos
            744, // 1998 The X-Files - Le Jeu
            2491, // Comanche Gold
            2734, // Morpheus
            560, // 1998 Liberation Day
            1158, // 1998 Dark Vengeance
            1434, // 1998 Final Fantasy VII
            496, // 1998 Incoming
            19997, // Briques 3D
            2483, // BattleZone
            16037, // Les océans et la vie marine
            15381, // Mission Soleil
            19210, // Die by the Sword
            19211, // Die By the Sword: Limb From Limb
            1699, // 1998 Small Soldiers : Squad Commander
            456, // 1998 Fallout 2
            592, // 1998 Nightlong : Union City Conspiracy
            3292, // Duke Nukem : Time To Kill
            5170, // Rainbow Six
            15951, // Tom Clancy's Rainbow Six Mission Pack: Eagle Watch
            761, // 1998 Vikings
            2482, // The Journeyman Project 3 : L'héritage du temps
            2493, // Deathtrap Dungeon
            2630, // NAM
            10582, // Airborn 101 : Juin 44, la 101ème en Normandie
            6369, // Flight Simulator 98
            16048, // Marine Malice : Le mystère du coquillage volé
            15366, // Lego Loco
            1700, // 1998 Red Jack - la revanche des pirates
            15219, // Il était une fois... trois contes
            15214, // Trespasser
            1724, // 1998 Chine : Intrigue dans la Cit� Interdite
            10505, // Graine de génie : Géographie & Espace 9-12 ans
            2432, // Mech Commander
            2025, // Caesar III
            3412, // O.D.T.
            13037, // S.C.A.R.S.
            13940, // Babe : Le cochon devenu berger
            2600, // Populous : A l'Aube de la Création
            2735, // Warhammer : Dark Omen
            15597, // Need for Speed III : Poursuite infernale
            16002, // The Settlers III
            16003, // The Settlers III: Quest Of The Amazons
            2240, // Blood II : The Chosen
            2048, // Commandos : Derrière les lignes ennemies
            304, // 1998 SiN (avec Blair Witch II ou dans disque externe)
            2109, // SiN : Wages of Sin
            1637, // Trivial Pursuit : Édition CD-ROM
            13555, // King's Quest : Masque d'éternité
            1635, // Lego Jeu d'échecs
            1169, // 1998 Les Visiteurs : le jeu
            476, // 1998 Grim Fandango
            658, // 1998 Sanitarium
            2362, // Heretic II
            2608, // Sonic R
            2809, // Unreal
            2810, // Unreal Mission Pack : Return to Na Pali
            3236, // Destin : Le jeu de la vie
            20049, // Le Petit Prince
            706, // 1998 Starship Titanic
            739, // 1998 The Curse of Monkey Island
            472, // 1998 Get Medieval
            671, // 1998 Shadow Warrior (dans HypraNews)
            1947, // Shadow Warrior : Twin Dragon
            1948, // Shadow Warrior : Wanton Destruction
            1949, // Starcraft
            1433, // 1999 Starcraft : Brood War
            5817, // Les animaux dangereux
            1107, // 1998 Half-Life
            15816, // Half-Life: Uplink
            1192, // 1999 Half-Life : Opposing Forces
            1941, // 2000 Half-Life : Frontline Force
            1305, // 2001 Half-Life : Blue Shift
            1942, // 2003 Half-Life : Invasion
            6415, // Half-Life : Afraid of monsters : Director's cut
        ],

        1999 => [
            19981, // Deo Gratias
            19761, // Bugdom
            15970, // X: Beyond the Frontier
            20055, // Oui-Oui en route vers l'école
            20085, // Le monde secret du Père Noël
            20006, // Cendrillon : Le Conte Interactif
            20087, // Les aventures de Pong Pong : Au royaume des animaux
            20015, // Les Aventures de Pong Pong : Le Monde Perdu
            20044, // Lapin Malin : L'île aux Pirates
            20027, // Winnie l'Ourson et l'arbre à miel
            13552, // Millennium Racer : Y2K Fighters
            13435, // Rubik's games
            13426, // SuperQuiz
            13428, // Water panic
            11384, // Virus
            10589, // Ar'Kritz the Intruder
            10508, // La 6ème piste
            10639, // MechWarrior 3
            10507, // Sabrina, l'apprentie sorcière
            15947, // SWAT 3: Close Quarters Battle
            3260, // Homeworld
            6352, // Agent Armstrong
            3509, // South Park : the game
            3474, // Physikus
            13548, // Isabelle
            1717, // Étranges Disparitions !!!
            6359, // Panzer General 3D : Assault
            634, // Push'n Go
            6297, // Urban Chaos
            15976, // Interstate '82
            1708, // Pouce-Pouce: Voyage dans le temps
            1707, // Marine Malice : Le mystère du ranch aux cochons
            1718, // Le secret des mayas
            1163, // Frozen World
            1195, // Brick Blaster
            1639, // Invasion au country de Kellogg's
            3404, // Gruntz
            13547, // Speed Demons
            1618, // Mah-Jong II
            1629, // Puzzle Bobble 2
            2112, // Legacy of Kain : Soul Reaver
            1831, // Opéra Fatal
            1665, // Excessive Speed
            2222, // Darkstone
            3587, // Creatures 3
            3715, // Land of Lore III
            15232, // Driver
            1642, // Le Petit Dinosaure : La vallée de l'aventure
            1161, // Expendable
            1150, // 1999 Le gardien des t�n�bres
            1677, // 1999 Le fabuleux voyage de l'oncle Ernest
            2016, // Resident Evil 2
            5523, // Alpha Centauri
            5524, // Alpha Centauri : Alien Crossfire
            1747, // 1999 Faust : les sept jeux de l'�me
            2067, // Heroes of Might and Magic III
            15924, // Heroes of Might and Magic III: Armageddon's Blade
            2074, // Heroes of Might and Magic III : The Shadow of Death
            15925, // Heroes of Might and Magic III: Horn Of The Abyss
            2631, // WWII GI
            3163, // Warzone 2100
            13943, // Cydonia : Mars : The First Manned Mission
            15375, // Itacante : La cité des robots
            15361, // The House of the Dead 2
            751, // Top Words
            1726, // Venise
            2602, // Shadow Man
            3561, // Super Taxi Driver
            1651, // 1999 Worms Armageddon
            2089, // Might and Magic VII : Pour le Sang et l'Honneur
            15405, // Mia : Le mystère du chapeau perdu
            2490, // Civilization : Call to Power
            1650, // SimCity 3000
            11383, // Viper Racing
            13946, // Disney : Arcade en folie
            1489, // 1999 The Longest Journey
            1939, // Commandos : Le sens du devoir
            2611, // System Shock 2
            6371, // Pong
            15999, // The Elder Scrolls Adventures: Redguard
            15229, // Paris 1313 : Le disparu de Notre-Dame
            2861, // Lego Creator
            1140, // 1999 Turok II : les Graines du Mal
            1427, // 1999 Discworld Noir
            1610, // Rollcage
            15230, // J'ai trouvé ! Le manoir hanté
            1628, // 1999 Theme Park World
            2060, // Gabriel Knight 3 : Énigme en Country Cathare
            1281, // 1999 Atlantis II
            2503, // Chroniques de la Lune Noire
            5788, // Saga : Rage of the Vikings
            13828, // LEGO Rock Raiders
            1604, // Lego Racers
            2607, // Silver
            2500, // Hidden & Dangerous
            2501, // Hidden & Dangerous : Devil's Bridge
            1108, // Baldur's Gate
            1964, // Baldur's Gate : La légende de l'île perdue
            668, // 1999 Seven Kingdoms 2 : The Fryhtan Wars
            393, // 1999 Dark Project : La Guilde des Voleurs
            2671, // Rollercoaster Tycoon
            2672, // RollerCoaster Tycoon : Nouvelles Attractions
            2673, // RollerCoaster Tycoon : Loopings en Folie
            3425, // L'amerzone
            3420, // Corsairs
            2811, // Unreal Tournament
            1940, // Grand Theft Auto : London 1969
            1833, // Ring : l'anneau des Nibelungen
            1298, // Need for Speed : Conduite en état de liberté
            2103, // Rayman 2 : The Great Escape
            1193, // Revenant
            3719, // Tonic Trouble
            2090, // Riven : La Suite de Myst
            16059, // Total Annihilation: Kingdoms
            2101, // Rainbow Six : Rogue Spear
            15820, // Rainbow Six: Rogue Spear: Urban Operations
            4825, // Rainbow Six : Covert Ops Essentials
            16931, // Rainbow Six: Rogue Spear: Black Thorn
            13032, // Spirit of Speed 1937
            5511, // Tomb Raider III : Les aventures de Lara Croft
            15996, // Tomb Raider 3 : Le dernier artefact
            3473, // Tomb Raider : La Révélation Finale
            297, // 1999 Command & Conquer - Soleil de Tibérium
            298, // 2000 Command & Conquer - Soleil de Tibérium : Mission Hydre
            2586, // La Roue du Temps
            3122, // Pharaon
            3123, // La Reine du Nil : Cléopâtre
            20039, // Disney : Le Retour des Méchants
            16050, // Monopoly Junior
            475, // 1999 Grand Theft Auto II (avec Half-Life : Opposing Forces et avec Pitch Black)
            2502, // Hype : The Time Quest
            1725, // 1999 Aztec : Malédiction au coeur de la cité d'or
            638, // 1999 Quake III Arena
            1989, // Quake 3 Team Arena
            631, // 1999 Prince of Persia 3D
            1429, // 1999 Dungeon Keeper II
            3233, // Drakan : Order Of The Flame
            9, // 1999 Age of Empires II : le temps des rois
            10, // 2000 Age of Empires II : les conquérants
            6411, // Age of Empires II : Forgotten Empires
            13833, // Age of Empires II : Rise of the Rajas
        ],

        2000 => [
            19763, // Kiss: Psycho Circus - The Nightmare Child
            19762, // Flying Heroes
            15239, // Airport Inc.
            16054, // MDK 2
            20007, // Barbie sauve les animaux
            19965, // Contes de fées magique : Barbie princesse
            20012, // Winnie l'Ourson : Premiers Pas
            20008, // Les Saisons de Petit Ours Brun
            20047, // Franklin la Tortue va à l'école
            13415, // Wetrix
            13941, // Blair Witch : Volume I : Rustin Parr
            15398, // Blair Witch : Volume II : La Légende de Coffin Rock
            20059, // Blair Witch : Volume III : Le Conte d'Elly Kedward
            15990, // Taxi 2 : Le jeu
            15369, // Le club des 5 joue et gagne
            13963, // Project IGI
            15834, // Pacific Warriors: Air Combat Action
            13965, // Sheep
            20025, // Richesses du monde
            20021, // Justine et la pierre de feu
            3132, // Risk II
            13035, // L'Alchimiste, l'élixir de la longue vie
            15216, // Wild Wild West : The Steel Assassin
            15876, // Star Trek Voyager: Elite Force
            15877, // Star Trek Voyager : Elite Force Expansion
            20003, // Le Club des 5 et le trésor de l'île
            16023, // Minuit fantôme
            1627, // Les Sims
            1624, // Les Sims : Ça vous change la vie
            15353, // Les Sims : Surprise-partie
            1621, // Les Sims : Et plus si affinités...
            1623, // Les Sims : En Vacances
            15354, // Les Sims : Entre chiens et chats
            15355, // Les Sims : Superstar
            15356, // Les Sims : Abracadabra
            707, // Storm Blaster
            5773, // The Crystal Key
            10590, // Pandora's box
            686, // Snowman's Land
            1164, // Lost in Time
            1743, // Pac-Man voyage dans le temps
            1706, // SPY Fox 2 : Opération Robot-Expo
            15378, // Princesse Sissi et Tempête
            1705, // Pyjama Sam : Héros du Goûter
            15346, // Artus contre le démon du musée
            1727, // Monet : Le mystère de l'orangerie
            15406, // Le pays des pierres magiques
            1162, // Freeze Ball
            504, // Jungle Color
            1978, // Dark Project II : L'Age de Métal
            1643, // Mon ami Koo
            1608, // Start-Up
            15234, // Les trois mondes de Flipper & Lopaka
            1612, // Resident Evil 3 : Nemesis
            1676, // 2000 L'�le myst�rieuse de l'oncle Ernest
            3749, // 2000 Mais o� se cache Carmen Sandiego ?
            19764, // Yoco : Le Mystère des fruits disparus
            15225, // Panique au manoir
            20004, // La Légende du prophète et de l'assassin
            1191, // 2000 Le Ma�tre de l'Olympe : Zeus
            3330, // Zeus : Poseidon
            2601, // Rollcage : Stage II
            2156, // Stupid Invaders
            15395, // The Typing of the Dead
            1648, // Bubble Bobble Nostalgie
            1679, // 2000 Louvre : L'Ultime Malediction
            1722, // 2000 Gift : le cadeau des �toiles
            477, // 2000 Ground Control
            2009, // Ground Control : Dark Conspiracy
            1712, // 2000 Genesys
            404, // 2000 Donald Couak Attack ?*!
            13945, // Demolition Racer
            3300, // Homeworld Cataclysm
            13938, // Astérix : La bataille des Gaules
            2775, // Dracula 2 : Le Dernier Sanctuaire
            15223, // Ronya, fille de brigand
            1227, // 2000 Deus Ex
            381, // 2000 Crimson Skies
            15386, // Sethi et la couronne d'Egypte
            1678, // 2000 Pomp�i : la Col�re du Volcan
            1785, // 2000 Hitman : Nom de code 47
            4846, // Gorky 17
            1279, // 2000 Odyss�e : Sur les traces d'Ulysse
            554, // 2000 Lemmings Revolution
            3026, // Planescape : Torment
            2615, // Theocracy
            2489, // BattleZone II : Combat Commander
            2466, // Necronomicon : L'aube des ténèbres
            3022, // Imperium Galactica II : Alliances
            3234, // Metal Gear Solid
            2032, // Metal Gear Solid : Integral
            5778, // Les Chevaliers d'Arthur
            2110, // Soldier of Fortune
            2612, // The Devil Inside
            5512, // Tomb Raider : Sur les Traces de Lara Croft
            20029, // Super Puzzle Bobble
            20032, // Arcatera : La Confrérie des ombres
            3208, // Age of Wonders
            3250, // Call To Power II
            5796, // Cultures
            3761, // Warlords Battlecry
            10611, // Ultima IX : Ascension
            2057, // Diablo II
            2058, // Diablo II : Lord of Destruction
            15411, // Rome : Le testament de César
            1411, // 2000 Command and Conquer : Alerte Rouge II
            1368, // 2001 Command and Conquer : Alerte Rouge II : La Revanche de Yuri
            1307, // 2000 La Machine � Voyager dans le Temps
            20060, // Jeu de Tarot
            2022, // Baldur's Gate II : Shadows of Amn
            2023, // Baldur's Gate II : Throne of Bhaal
            678, // 2000 Shogun : Total War
            1636, // Cluedo : Le jeu des grands détectives
            1152, // 2000 �gypte II : la Proph�tie d'H�liopolis
            740, // 2000 Escape from Monkey Island
            1265, // 2000 No One Lives Forever
            2129, // Sudden Strike
            2130, // Sudden Strike : Forever
            15409, // Gunman Chronicles
            2599, // Nox
            20119, // Icewind Dale
            20120, // Icewind Dale: Heart of Winter
            20121, // Icewind Dale: Heart of Winter: Trials of the Luremaster
            1388, // 2000 Vampire : La Mascarade - Redemption
        ],

        2001 => [
            19957, // P'tit Louis : Quand l'aventure s'en mêle...
            19922, // Moop & Dreadly : Le Trésor de l'île Bing-Bong
            19924, // Plume au pays des tigres
            13416, // Pinball collection II
            19955, // Qui a croqué la Lune ?
            20017, // Doo Wap : Voyage au Temps des Dinosaures
            20020, // Abracadabra la grenouille se change en roi
            20078, // Franklin la Tortue et le club secret
            20073, // Mia : La Machine à remonter le temps
            1720, // La Vallée des Rois : Une Aventure de Sir Johnson
            20031, // Justine et l'île au fruits rouges
            20035, // Max et le château hanté
            20042, // Zax: The Alien Hunter
            20026, // Le Club des 5 en péril
            710, // Super Casse-Tête
            1611, // IL-2 Sturmovik
            19954, // J'ai trouvé ! La Chasse au Trésor
            19214, // Frank Herbert's Dune
            19215, // Pool of Radiance: Ruins of Myth Drannor
            1168, // 2001 Une faim de loup
            20000, // Scooby-Doo! : Le Secret du Sphinx
            5584, // Startopia
            1501, // 2001 Spider-man Movie
            16021, // Frogger 2 : La revanche de Swampy
            1609, // Lucky Luke : La Fièvre de L'Ouest
            1499, // 2001 Soul Reaver II
            1299, // 2001 Tintin Objectif Aventure
            15350, // Waterloo : La dernière bataille de Napoléon
            3332, // Adibou présente la Magie
            20010, // Shadow of Memories
            2594, // Mech Commander 2
            1451, // 2001 Myst III - Exile
            1606, // Casino Tycoon
            5455, // The Sting!
            5802, // Planète des Singes
            1443, // 2001 Edge of Chaos : Independence War II
            2126, // SPY Fox 3 : Opération Ozone
            1409, // 2001 Atlantide : l'Empire Perdu : L'�preuve du Feu
            1702, // 2001 Alfred Hitchcock : The final cut
            3362, // Adibou et l'Ombre Verte
            1931, // Empereur : La bataille pour Dune
            15988, // Sethi et le sorcier inca
            3670, // Les 102 dalmatiens : À la rescousse
            16940, // Delta Force: Land Warrior
            3373, // Astérix Maxi-Delirium
            652, // 2001 Road To India
            3391, // 2001 Worms World Party
            1739, // 2001 Arabian Nights
            16024, // Scooby-Doo! : Poursuite dans la ville fantôme
            19963, // Scooby-Doo! Le Mystère du château hanté
            19216, // Throne of Darkness
            1318, // 2001 Gangsters II
            1399, // 2001 Aquanox
            19217, // Codename: Outbreak
            465, // 2001 Fur Fighters
            15989, // Panique à Mickey Ville
            16936, // Kohan: Immortal Sovereigns
            1301, // 2001 Les Visiteurs : La Relique de Sainte Rolande
            1121, // 2001 Commandos II
            1729, // 2001 Versailles II : Le Testament
            735, // 2001 The Mystery of the Druids
            1348, // 2001 Ghost Recon
            19219, // Tom Clancy's Ghost Recon: Desert Siege
            2757, // Toon Car
            15884, // Theme Park Inc.
            1308, // 2001 Chevaliers d'Arthur : Chapitre II : Le Secret de Merlin
            1507, // 2001 Arcanum
            1120, // 2001 Black & White
            15370, // Black & White : L'Île aux créatures
            2104, // Rayman M
            19765, // Yoco et la course au trésor
            5803, // Project Eden
            5772, // Schizm
            5814, // Evil Twin : Cyprien's Chronicles
            3290, // Desperados : Wanted Dead or Alive
            16004, // The Settlers IV
            16005, // The Settlers IV: Mission CD
            16006, // The Settlers IV : Les Troyens et l'élixir de puissance
            16007, // The Settlers IV: The New World
            287, // 2001 Lego Racers 2
            323, // 2001 Aliens versus Predator 2
            3126, // Gothic
            19213, // Les Trois Royaumes : Le Destin du dragon
            1122, // 2001 Tristan et le Myst�re du Dragon
            1123, // 2001 Harry Potter � l'�cole des sorciers
            1226, // 2001 Stronghold
            2457, // Atlantis III : Le Nouveau Monde
            1338, // 2001 Operation Flashpoint : Cold War Crisis
            1337, // 2001 Operation Flashpoint : Red Hammer
            1339, // 2002 Operation Flashpoint : Resistance
            669, // 2001 Severance : Blade of Darkness
            2014, // Alone in the Dark : The New Nightmare
            15905, // Etherlords
            1692, // Zoo Tycoon
            5387, // Zoo Tycoon : Dinosaur Digs
            5388, // Zoo Tycoon : Marine Mania
            1132, // 2001 Star Wars : Galactic Battlegrounds
            1366, // 2002 Star Wars : Galactic Battlegrounds : Clone Campaigns
            2133, // Tropico
            16079, // Tropico: Paradise Island
            1458, // 2001 S.W.I.N.E.
            1159, // 2001 American McGee's Alice
            1194, // 2001 Battle Realms
            2141, // Battle Realms : Winter of the Wolf
            1190, // 2001 Max Payne
            1976, // Cossacks : European Wars
            1373, // 2001 Cossacks : The Art of War
            2007, // Cossacks : Back to War
            1117, // 2001 Red Faction
            2002, // Loch Ness
            1312, // 2001 Serious Sam : Premier contact
            432, // 2001 Empire Earth (apparemment en double)
            433, // 2002 Empire Earth : l'art de conqu�rir
            1483, // 2001 Retour au Ch�teau Wolfenstein
        ],

        2002 => [
            16039, // Rock Manager
            20061, // Barbie Détective 2 : Les Vacances Mystérieuses
            19999, // Barbie : Belle au Bois Dormant
            20072, // Lapin Malin : Sauvons les Étoiles !
            13414, // Megacity challenge
            20016, // Le Jardin des Malices
            20022, // L'Œil du cyclone
            20030, // Forestia Revoltozoo
            13427, // Quiz lite
            1115, // BlotterGeist
            19971, // Le Voyage autour du monde de Paddington
            19987, // P'tit Louis contre le Dr Blick
            3698, // Le club des Trouvetout CE2 : la cité perdue
            3342, // Grandia II
            2266, // Napoléon
            15246, // Équipe Actimel contre les Mégakrasses
            1731, // La Boîte à Bidules de l'Oncle Ernest
            529, // 2002 L'île diabolique
            5585, // Army Men : RTS
            15968, // Warrior Kings
            3431, // Le Maillon Faible
            1111, // Bubble Shooter
            1380, // 2002 Civilization III
            1414, // 2002 Civilization III : Play the World
            1387, // 2003 Civilization III : Conquests
            747, // 2002 Tom & Jerry Sèment la Pagaille
            537, // La Planète au Trésor
            1615, // Crazy Taxi
            587, // 2002 Monopoly 2003
            2883, // Les Enquêtes de Nancy Drew : Ghost Dogs of Moon Lake
            1126, // 2002 Hitman II : Silent Assassin
            1412, // 2002 Thorgal : La Mal�diction d'Odin
            1506, // 2002 James Bond 007 - Nightfire
            1425, // 2002 Conflict : Desert Storm
            3343, // Global Operations
            1622, // Rollercoaster Tycoon 2
            2727, // Rollercoaster Tycoon 2 : Wacky Worlds
            2728, // Rollercoaster Tycoon 2 : Time Twister
            1649, // Worms Blast
            3432, // Le jeu d'aventure du Routard
            1430, // 2002 Empereur : l'Empire du Milieu
            2661, // Divine Divinity
            1680, // 2002 J�rusalem : les trois chemins de la ville Sainte
            1452, // 2002 Mobile Forces
            1386, // 2002 Dino Island
            688, // 2002 Soldiers of Anarchy
            1607, // Beach Life
            1172, // 2002 Harry Potter et la chambre des secrets
            675, // 2002 Sherlock Holmes : Le Mystère de la Momie
            532, // 2002 L'Oeil du Kraken
            1602, // Carnivores : Cityscape
            1344, // 2002 Age of Wonders II : the Wizard's Throne
            1362, // 2002 Heroes of Might and Magic IV
            15921, // Heroes of Might and Magic IV: The Gathering Storm
            15922, // Heroes of Might and Magic IV: Winds of War
            1400, // 2002 Sudden Strike II
            726, // 2002 The Gladiators
            16390, // Post Mortem
            547, // 2002 Le secret du Nautilus
            20084, // Zanzarah : La Légende des deux mondes
            1110, // 2002 Medal of Honor : D�barquement Alli�
            1496, // 2002 Medal of Honor : D�barquement Alli� : En formation
            2088, // Medal of Honor : Débarquement Allié : L'Offensive
            1398, // 2002 Stronghold Crusader
            1407, // 2002 The Thing
            5797, // Cultures 2 : Les Portes d'Asgard
            1309, // 2002 Gorasul : L'H�ritage du Dragon
            1341, // 2002 The Elder Scrolls III : Morrowind
            2132, // The Elder Scrolls III : Tribunal
            2131, // The Elder Scrolls III : Bloodmoon
            2912, // Capitalism II
            572, // 2002 Mafia
            1284, // 2002 Syberia
            1173, // 2002 Command and Conquer : Renegade
            1505, // 2002 Iron Storm
            1336, // 2002 Tactical Ops : Assault on Terror
            1346, // 2002 Soldier of Fortune II : Double Helix
            1693, // 2002 Jedi Knight 2
            1620, // Hotel Giant
            1983, // Blood Omen 2
            2647, // Gore
            16069, // Warlords Battlecry II
            3588, // Disciples : Sacred Lands
            3169, // Cluedo Chronicles : Le Masque Fatal
            3228, // Die Hard : Piège de Cristal
            19220, // War and Peace: 1796–1815
            19221, // Celtic Kings: Rage of War
            3569, // Disciples II : Dark Prophecy
            3570, // Disciples II : Guardians Of The Light
            3571, // Disciples II : Servant of the Dark
            3572, // Disciples II : Rise of the Elves
            3527, // Europa Universalis II
            1453, // 2002 Robin Hood : La L�gende de Sherwood
            1406, // 2002 Unreal Tournament 2003
            1333, // 2002 Battlefield 1942
            1381, // 2003 Battlefield 1942 : La Campagne d'Italie
            1376, // 2003 Battlefield 1942 : Arsenal Secret
            1340, // 2002 Arx Fatalis
            1176, // 2002 Duke Nukem : Projet Manhattan
            20123, // Icewind Dale II
            1384, // 2002 Medieval : Total War
            5739, // Medieval : Total War : Viking Invasion
            1199, // 2002 Serious Sam : Second contact
            1154, // 2002 American Conquest
            1382, // 2003 American Conquest : Fight Back
            14346, // American Conquest : Divided Nation
            1134, // 2002 No One Lives Forever II : le C.R.I.M.E. est �ternel
            1175, // 2002 Grand Theft Auto III
            1511, // 2002 WarCraft III : le R�gne du Chaos
            1512, // 2003 WarCraft III : le Tr�ne de Glace
            293, // 2002 Age of Mythology
            1482, // 2003 Age of Mythology : les Titans
            294, // 2003 Age of Mythology : The Golden Gift
            13195, // Age of Mythology : Tale of the Dragon
            1277, // 2002 Dungeon Siege
            1278, // 2003 Dungeon Siege : les l�gendes d'Aranna
        ],

        2003 => [
            16038, // Barbie Lac des Cygnes : La Forêt Enchantée
            19118, // Manhunt
            19985, // Unka : Le Roi des animaux
            20067, // Naxette a disparu !
            19986, // Les Aventures du Petit Corbeau : Le Tricycle d'Eddie
            20036, // Snoopy : Où est passée la couverture, Charlie Brown ?
            20040, // Republic: The Revolution
            20054, // Le Secret de l'Alchimiste
            19964, // Les Voyages de Balthazar
            1616, // Tetris 4000
            3414, // MTV Celebrity Deathmatch
            15987, // Les aventures de Nax et Oyo : Naxette a disparu !
            3409, // Moon Tycoon
            3366, // Pure Pinball
            3368, // Hard Rock Casino
            3293, // Les aventures de Porcinet
            708, // Super Bloc Mania 3D
            709, // Super Bubble Pop
            1732, // Le Bidulo Trésor de l'Oncle Ernest
            1466, // 2003 Tron 2.0
            1675, // 2003 Le temple perdu de l'oncle Ernest
            3386, // Space Colony
            20079, // Harry Potter : Coupe du monde de quidditch
            300, // 2003 Pax Romana
            1389, // 2003 Marine Sharpshooter
            3530, // Cold Zero : The Last Stand
            2449, // Far West
            2238, // Galactic Civilizations
            15831, // Hearts of Iron
            748, // 2003 Tomb Raider : L'Ange des Ténèbres
            2903, // Starsky & Hutch
            19923, // Le Mystère des Clés Perdues
            15955, // Victoria : Un empire en construction 1836-1920
            2003, // Turok Evolution
            1626, // SimCity 4
            15938, // SimCity 4: Rush Hour
            2107, // Silent Hill 2 : Director's Cut
            493, // 2003 In Memoriam
            544, // 2003 Le Monde de Nemo
            653, // 2003 RoboCop
            779, // 2003 Zapper
            15394, // The House of the Dead III
// 2003 Les experts
            1723, // 2003 Aquatic Tycoon
            3251, // Homeworld 2
            15961, // Warlords IV: Heroes of Etheria
            1493, // 2003 Freelancer
            1500, // 2003 Pirates des Cara�bes
            1721, // 2003 Un voisin d'enfer !
            753, // 2003 UFO : Aftermath
            640, // 2003 Railroad Tycoon 3
            1396, // Papyrus : Le défi des pharaons
            696, // 2003 Splinter Cell
            365, // 2003 Chariots of War
            762, // 2003 Voyage au Centre de la Terre
            1463, // 2003 Apocalyptica
            1741, // 2003 Tortuga : Pirates et Flibustiers
            1464, // 2003 Blood Rayne
            1189, // 2003 Vietcong (disque 1 en double)
            1174, // 2004 Vietcong : First Alpha (disque 1 en double)
            1475, // 2003 Tony Tough and the night of Roasted Moths
            1345, // 2003 XIII
            390, // 2003 Dark Fall : Rencontres avec l'Au-Delà
            1492, // 2003 Freedom Fighters
            1403, // 2003 Uru : Ages Beyond Myst
            15952, // Uru: To D'ni
            15953, // Uru: The Path of the Shell
            1490, // 2003 Devastation
            1354, // 2003 Les Chevaliers de Baphomet : Le Manuscrit de Voynich
            1701, // 2003 Halo
            1428, // 2003 Europa 1400 : les Marchands du Moyen-�ge
            643, // 2003 Rayman 3 : Hoodlum Havoc
            19975, // Need for Speed: Underground
            1423, // 2003 Delta Force : Black Hawk Down
            1431, // 2004 Delta Force : Black Hawk Down : Team Sabre
            1404, // 2003 Tropico II : La baie des pirates
            1709, // Chaser
            679, // 2003 Silent Hill 3
            700, // 2003 Star Trek : Elite Force II
            1816, // Warhammer 40.000 : Fire Warrior
            1343, // 2003 Le Seigneur des Anneaux : Le Retour du Roi
            1363, // 2003 Hidden and Dangerous II
            1351, // 2004 Hidden and Dangerous II : Sabre Squadron
            701, // 2003 Jedi Knight : Jedi Academy
            702, // 2003 Star Wars : Knights of the Old Republic
            1379, // 2003 Age of Wonders : Shadow Magic
            376, // 2003 Conflict : Desert Storm II
            364, // 2003 Chaos Legion
            721, // 2003 The Black Mirror
            1353, // 2003 Contract J.A.C.K.
            1294, // 2003 Le Temple du Mal Elementaire
            1358, // 2003 Beyond Good & Evil
            1446, // 2003 IGI II : Covert Strike
            624, // 2003 Postal 2 : Share the Pain
            3429, // Praetorians
            3422, // Aménophis : La Résurrection
            3594, // World War II : Frontline Command
            1293, // 2003 Salammb�
            19153, // Saints Row IV
            1391, // 2003 Warrior Kings : Battles
            367, // 2003 Chrome
            13486, // Le Seigneur des Anneaux : La Guerre de l'Anneau
            360, // 2003 Call of Duty
            1349, // 2004 Call of Duty : La Grande Offensive
            564, // 2003 Lords of Everquest
            374, // 2003 Command & Conquer - Generals
            1984, // Command & Conquer : Generals : Heure H
            2034, // Motus
            329, // 2003 Anno 1503 : le Nouveau Monde
            16371, // Anno 1503 : Trésors, Monstres & Pirates
            516, // 2003 Jurassic Park : Operation Genesis
            2015, // Rainbow Six 3 : Raven Shield
            641, // Rainbow Six 3 : Athena Sword
            15817, // Rainbow Six 3: Iron Wrath
            645, // 2003 Red Faction II
            375, // 2003 Commandos 3 : Destination Berlin
            2371, // Gladiator : Sword of Vengeance
            3215, // Impossible Creatures
            1361, // 2003 SpellForce : The Order Of Dawn
            694, // 2004 SpellForce : The Breath of Winter
            15945, // SpellForce: Shadow of the Phoenix
            655, // 2003 Runaway - A road adventure
            2813, // Unreal II : The Awakening
            478, // 2003 Grand Theft Auto III : Vice City
            2380, // The Simpsons : Hit & Run
            1497, // 2003 Prince of Persia : les sables du temps
            1188, // 2003 Blitzkrieg
            1447, // 2004 Blitzkrieg : Burning Horizon
            1405, // 2004 Blitzkrieg : Rolling Thunder
            4119, // Neverwinter Nights
            591, // 2003 Neverwinter Nights : Shadows of Undrentide
            1450, // 2004 Neverwinter Nights : Hordes of the Underdark
            4120, // Neverwinter Nights : Kingmaker
            1468, // 2003 Max Payne II
            1439, // 2003 Enclave
            1486, // 2003 Empires : l'aube d'un monde nouveau
            1494, // 2003 Nosferatu
            1369, // 2003 Rise of Nations
            1359, // 2004 Rise of Nations : Thrones & Patriots
        ],

        2004 => [
            20053, // Barbie : Cœur de princesse
            3449, // Oui-Oui : La grande fête du country des jouets
            3744, // Maniac Mansion Deluxe
            3365, // Monstres & Cie : Atelier de jeux
            260, // 2004 Tony Hawk's Underground 2
            3528, // Fire Department 2
            3369, // Frère des Ours
            20005, // Boule & Bill : Au Voleur !!!
            20037, // Les Aventures d'Arthur : À la découverte de la Nature
            3024, // Adibou : L'orgue fantastique
            3161, // Titeuf : Méga Compet'
            3189, // Adibou et les voleurs d'énergie
            15974, // Shellshock: Nam '67
            570, // 2004 Luxor : Amun Rising
            1674, // Big Bang Bidule ! chez l'Oncle Ernest
            316, // 2004 Alias
            3269, // X² : The Threat
            2725, // Adibou Sciences & Nature : L'île volante
            258, // 2004 Garfield
            3023, // Adibou : Le Royaume Hocus Pocus
            1730, // 2004 La statuette maudite de l'oncle Ernest
            279, // 2004 Trivial Pursuit Déjanté
            274, // 2004 Hitman Contracts
            468, // 2004 Gadget & Gadgetinis
            286, // 2004 Space Rangers
            1719, // 2004 Les Indestructibles
            2135, // Un Voisin d'Enfer ! 2 : En Vacances
            2111, // Legacy of Kain : Defiance
            1357, // 2004 Leisure Suit Larry : Magna Cum Laude
            1710, // 2004 Ground Control II : Operation Exodus
            3019, // Les Sims 2
            3180, // Les Sims 2 : Kit Joyeux Noël
            12999, // Kult : Heretic Kingdoms
            3361, // Jeanne d'Arc
            689, // 2004 Soldner : Secret Wars
            2820, // Worms Forts : Etat de Siege
            1355, // 2004 Les Experts : Meurtres � Las Vegas
            1200, // 2004 Warhammer 40000 - Dawn of War
            2004, // Warhammer 40.000 : Dawn of War : Winter Assault
            1401, // 2006 Warhammer 40000 : Dawn of War : Dark Crusade
            2005, // Warhammer 40.000 : Dawn of War : Soulstorm
            1456, // 2004 Nexus - The Jupiter Accident
            1735, // 2004 New York Police Judiciaire - �pisode 2 : Quitte ou Double
            409, // 2004 Dora l'exploratrice - Les aventures de Sakado
            407, // 2004 Dora l'exploratrice : Les animaux de la Jungle
            1435, // 2004 Aura : la L�gende des Mondes Parall�les
            2372, // In Memoriam : La treizième Victime
            3129, // Star Wars Battlefront
            288, // 2004 Need for Speed Underground 2
            1933, // Alpha Black Zero : Intrepid Protocol
            763, // 2004 War ! Age of Imperialism
            600, // 2004 Operation Air Assault
            1432, // 2004 Gang Land
            607, // 2004 Pacific Warriors II : Dogfight!
            302, // 2004 Men of Valor
            1408, // 2004 Myst IV : Revelation
            391, // 2004 Dark Fall 2 : Le Phare
            335, // 2004 Astérix & Obélix XXL
            309, // 2004 A.I.M. : Artificial Intelligence Machines
            1476, // Joint Operations : Typhoon Rising
            3417, // Crazy Taxi 3
            1124, // 2004 Splinter Cell : Pandora Tomorrow
            1410, // 2004 The Moment of Silence
            3585, // Spider-Man 2
            1367, // 2004 Teenage Mutant Ninja Turtles
            1454, // 2004 Obscure
            595, // 2004 Nitro Family
            1282, // 2004 Desert Thunder
            1364, // 2004 Dark Project : Deadly Shadows
            680, // 2004 Silent Hill 4 : The Room
            1371, // 2004 Deus Ex : Invisible War
            1378, // 2004 Chicago 1930
            1325, // 2004 Armed and Dangerous
            2660, // Beyond Divinity
            1413, // 2004 Kohan II : Kings of War
            1402, // 2004 The Westerner
            2683, // Rollercoaster Tycoon 3
            2684, // Rollercoaster Tycoon 3 : Délires Aquatiques
            2685, // Rollercoaster Tycoon 3 : Distractions Sauvages
            13134, // Warlords Battlecry III
            2801, // Sherlock Holmes : La boucle d'argent
            1440, // 2004 Harry Potter et le prisonnier d'Azkaban
            341, // 2004 Battlefield Vietnam
            1716, // 2004 Syberia II
            18369, // The Bard's Tale
            398, // 2004 Dead Man's Hand
            1474, // 2004 Le Seigneur des Anneaux : La Bataille pour la Terre du Milieu
            525, // 2004 Knights of the Temple
            610, // 2004 Perimeter
            3327, // FlatOut
            15833, // Lords of the Realm III
            19721, // Evil Genius
            1225, // 2004 �gypte III : le destin de Rams�s
            1488, // 2004 Medal of Honor : Batailles du Pacifique
            1442, // 2004 Etherlords II
            1390, // 2004 Axis & Allies
            1449, // 2004 Vampire : The Masquerade : Bloodlines
            1485, // 2004 Afrika Korps Vs Desert Rats
            629, // 2004 Prince Of Persia : L'�me Du Guerrier
            1393, // 2004 Castle Strike
            1360, // 2004 Unreal Tournament 2004
            608, // 2004 Painkiller
            3438, // Zoo Tycoon 2
            3439, // Zoo Tycoon 2 : Especes en Danger
            3440, // Zoo Tycoon 2 : Aventure Africaine
            2885, // Zoo Tycoon 2 : Marine Mania
            16068, // Zoo Tycoon 2: Dino Danger
            3441, // Zoo Tycoon 2 : Animaux Disparus
            1502, // 2004 Rome - Total War
            654, // 2005 Rome : Total War : Barbarian Invasion
            2106, // Rome : Total War : Alexander
            1467, // 2004 Sid Meier's Pirates!
            1352, // 2004 Codename : Panzers : Phase 1
            285, // 2004 Sacred
            15939, // Sacred Plus
            15940, // Sacred Underworld
            1480, // 2004 Far Cry
            1462, // 2004 Alexandre
            666, // 2004 Doom III
            1461, // 2005 Doom III - Resurrection of Evil
            2653, // Doom 3 : The Dark Mod
            5679, // Doom 3 : The Lost Mission
            1481, // 2004 Half-Life II
            3951, // Half-Life 2 : Lost Coast
            1944, // Half-Life 2 : Dino D-Day
            3728, // Half-Life 2 : WW1
            3729, // Half-Life 2 : 1187
        ],

        2005 => [
            13421, // 200% jeux : Jeux de lettres
            13425, // Objectif jeux : jeux de cartes
            13431, // Family fun
            3385, // CT Special Forces : Fire for Effect
            3427, // Dora l'Exploratrice : Au Country des Contes de Fées
            19988, // Cédric : Chen a disparu !
            3376, // Lugaru HD
            3190, // GIGN Anti-Terror Force
            3006, // Lapin Malin Lecture
            2779, // The Movies
            3175, // Adibou'chou fête son anniversaire
            5618, // Teenage Mutant Ninja Turtles : Mutant Melee
            15396, // Le Club des 5 : Le mystère des catacombes
            1713, // Papyrus : Le secret de la cité perdue
            1714, // Papyrus : La malédiction de Seth
            3539, // The Settlers : L'Héritage des Rois
            16008, // The Settlers : L'Héritage des Rois : Expansion Disc
            16009, // The Settlers : L'Héritage des Rois : Legends Expansion Disc
            289, // 2005 Powerdrome
            3370, // Questions pour un Champion 2006
            5147, // Urgences
            576, // 2005 Martin Mystere : Operation Dorian Gray
            1444, // 2005 Gun Metal
            3031, // Worms 4 : Mayhem
            1477, // 2005 Hearts of Iron II
            2666, // Hearts of Iron II : Doomsday
            4366, // Hearts of Iron II : Doomsday : Armageddon
            380, // 2005 Crime Life : Gang Wars
            361, // 2005 Caméra Café : Le Jeu
            259, // 2005 Project Freedom
            1310, // 2005 Shattered Union
            1205, // Smart Paintball
            3471, // Sniper Elite
            3521, // 1944 : Campagne des Ardennes
            312, // 2005 Adibou et le secret de Paziral
            743, // 2005 The Suffering
            3278, // Dynasty Warriors 4 : Hyper
            408, // 2005 Dora l'exploratrice : Les aventures de la Cité Perdue
            734, // 2005 The Matrix : Path of Neo
            265, // 2005 Les Experts : Miami
            3807, // Chrome Specforce
            2743, // Ultimate Spider-Man
            1945, // King Kong
            617, // 2005 Playboy : The Mansion
            3591, // Gun
            3181, // 7 Sins
            18366, // Resident Evil 4
            3127, // Gothic II
            3128, // Gothic II : La Nuit des Corbeaux
            2026, // Civilization IV
            2027, // Civilization IV : Warlords
            2028, // Civilization IV : Beyond the Sword
            704, // 2005 Star Wars : Republic Commando
            1326, // 2005 StillLife
            431, // 2005 Emergency 3
            1327, // 2005 Myst V : End of Ages
            377, // 2005 Conflict : Global Storm
            1377, // 2005 Lego Star Wars : Le Jeu Vid�o
            660, // 2005 Scrapland
            1436, // 2005 Driv3r
            16374, // UFO: Aftershock
            1331, // 2005 Black and White II
            1459, // 2006 Black and White II - le combat des Dieux
            1734, // 2005 New York Police Judiciaire : Jeu, Set et Meurtre
            333, // 2005 Astérix & Obélix XXL 2 : Mission Las Vegum
            2128, // Stronghold 2
            1737, // Voyage au Coeur de la Lune
            282, // 2005 Dungeon Lords
            481, // 2005 Harry Potter et la Coupe de Feu
            462, // 2005 Frontal Attack 2
            3475, // Vietcong 2
            336, // 2005 Au Coeur de Lascaux
            661, // 2005 Second Sight
            633, // 2005 Project : Snowblind
            1455, // 2005 Total Overdose
            485, // 2005 Heroes of the Pacific
            1347, // 2005 Cold Fear
            720, // 2005 The Bard's Tale
            2096, // Postal 2 : Apocalypse Weekend
            1738, // 2005 Retour sur l'�le myst�rieuse
            353, // 2005 Boiling Point : Road to Hell
            1465, // 2005 Constantine
            64, // 2005 Agatha Christie : Devinez qui ?
            3586, // Fable : The Lost Chapters
            703, // 2005 Star Wars : Knights of the Old Republic II : The Sith Lords
            650, // 2005 Rising Kingdoms
            1375, // 2005 Fahrenheit
            303, // 2005 OpenArena
            1711, // Earth 2160
            1374, // 2005 Starship Troopers
            5454, // Questions pour un Champion Junior
            1342, // 2005 Area 51
            1365, // 2005 Brothers in Arms : Earned in Blood
            609, // 2005 Pariah
            1283, // 2005 Forgotten Realms : Demon Stone
            722, // 2005 The Chronicles of Riddick : Escape from Butcher Bay - Developer's Cut
            1460, // 2005 Sudeki
            1479, // 2005 Les Enfants du Nil
            1395, // 2005 Quake IV
            299, // 2005 Imperial Glory
            1397, // 2005 Cossacks II : Napoleonic Wars
            1504, // 2005 SWAT 4
            2094, // SWAT 4 : The Stetchkov Syndicate
            2124, // Splinter Cell Chaos Theory
            1445, // 2005 F.E.A.R.
            1424, // 2006 F.E.A.R. : Extraction Point
            455, // 2007 F.E.A.R. : Perseus Mandate
            2297, // Need for Speed : Most Wanted
            2365, // Dragonshard
            1495, // 2005 Prince of Persia : les deux royaumes
            1330, // 2005 Act of War - Direct Action
            1332, // 2006 Act of War : High Treason
            1491, // 2005 Brothers in Arms : Road to Hill 30
            1487, // 2005 Grand Theft Auto III : San Andreas
            3017, // Star Wars Battlefront II
            1335, // 2005 Battlefield II
            442, // 2005 Empire Earth II
            443, // 2006 Empire Earth II : The Art of Supremacy
            1508, // 2005 Armies of Exigo
            1356, // 2005 Call of Duty II
            2024, // Blitzkrieg 2
            665, // 2005 Serious Sam II
            290, // 2005 Age of Empires III
            291, // 2006 Age of Empires III - The War Chiefs
            292, // 2007 Age of Empires III : The Asian Dynasties
            1478, // 2005 Dungeon Siege II
            1457, // 2006 Dungeon Siege II - Broken World
        ],

        2006 => [
            3556, // Desperate Housewives
            13440, // Monster Trux Extreme : Offroad Edition
            3515, // 25 to Life
            3408, // JTF : Joint Task Force
            3326, // X3 : Reunion
            20066, // La Grande Aventure : Mission Antarctique
            3578, // Moorhuhn : Pilote de Chasse
            3002, // Sid Meier's Railroads !
            3547, // Lula 3D
            3553, // Darkstar One
            3253, // Bionicle Heroes
            14892, // FlatOut 2
            3212, // Les Enquêtes de Nancy Drew : Le Mystère de l'Horloge
            3983, // L'Age de Glace 2
            15907, // Mystery Case Files : Huntsville
            3221, // CivCity : Rome
            3137, // Star Wars : Empire at War
            3033, // Star Wars : Empire at War : Forces of Corruption
            3005, // Mahjong Towers Eternity
            3277, // X-Men : Le Jeu Officiel
            327, // Amazing Mahjongg 3D
            1683, // Babel Deluxe
            469, // 2006 Gene Troopers
            656, // 2006 Safecracker : Expert en Cambriolage
            3499, // Billy Hatcher and the Giant Egg
            1334, // 2006 Barrow Hill : Le Cercle Maudit
            2819, // Fire Department 3
            4933, // Les enquêtes de Nancy Drew : Danger au coeur de la mode
            3502, // Reservoir Dogs
            526, // 2006 Knights of the Temple II
            1383, // 2006 Agatha Christie : Le Crime de l'Orient Express
            1733, // 2006 Da Vinci Code
            3213, // Just Cause
            3235, // Ghost Recon Advanced Warfighter
            3546, // Faces of War
            203, // Rayman contre les Lapins Crétins
            6178, // Tom Clancy's Splinter Cell: Double Agent
            16377, // Red Orchestra: Ostfront 41-45
            16403, // Warhammer: Mark of Chaos
            766, // 2008 Warhammer : Mark of Chaos : Battle March
            359, // 2006 Caesar IV
            1503, // 2006 Le Parrain
            1509, // 2006 Ankh
            1510, // 2006 Ankh II - le Coeur d'Osiris
            3259, // Dark Messiah of Might and Magic
            2369, // Mortal Kombat Armageddon
            2875, // Commandos Strike Force
            15902, // Dead Rising
            2046, // Rainbow Six Vegas
            16000, // The Settlers II: 10th Anniversary
            16001, // The Settlers II: The Next Generation: Vikings
            1372, // 2006 Lego Star Wars II : La Trilogie originale
            1311, // 2006 SiN Episodes : Emergence
            331, // 2006 Anno 1701
            3525, // Anno 1701 : La Malédiction du Dragon
            1426, // 2006 Cap sur l'�le au tr�sor
            2100, // Rainbow Six : Lockdown
            674, // 2006 Sherlock Holmes : La Nuit des Sacrifiés
            488, // 2006 Hitman : Blood Money
            662, // 2006 Secret Files : Tunguska
            557, // 2006 Les Chevaliers de Baphomet : Les Gardiens du Temple de Salomon
            278, // 2006 Scrabble Edition 2007
            741, // 2006 The Secrets of Da Vinci : Le Manuscrit Interdit
            463, // 2006 Full Spectrum Warrior : Ten Hammers
            583, // Méga Quiz
            15901, // Call of juarez
            2098, // The Elder Scrolls IV : Oblivion
            2113, // The Elder Scrolls IV : Knights of the Nine
            2114, // The Elder Scrolls IV : Oblivion : The Shivering Isles
            3248, // Le Seigneur des Anneaux : La Bataille pour la Terre du Milieu II
            3217, // Le Seigneur des Anneaux : La Bataille pour la Terre du Milieu II : L'Avènement du Roi-Sorcier
            1975, // Cossacks II : Battle for Europe
            2127, // Stronghold Legends
            3359, // Emergency 4
            3281, // In Memoriam : Le Dernier Rituel
            2370, // Dreamfall : The Longest Journey
            2047, // Command & Conquer 3 : Les Guerres du Tibérium
            2035, // Command & Conquer 3 : La Fureur de Kane
            1385, // 2006 Les Dents de la Mer
            1329, // 2006 Blood Rayne II
            16937, // Sam & Max : Saison 1 : Sauvez le monde
            3381, // Sam & Max : Saison 2 : Au-Delà du Temps et de l'Espace
            16938, // Sam & Max: Saison 3: The Devil's Playhouse
            2072, // Heroes of Might and Magic V
            2073, // Heroes of Might and Magic V : Hammers of Fate
            2075, // Heroes of Might and Magic V : Tribes of the East
            3287, // Tomb Raider Legend
            3559, // Marc Ecko's Getting up : Contents under Pressure
            1350, // 2006 Battlefield 2142
            340, // 2007 Battlefield 2142 : Northern Strike
            1498, // 2006 Company of Heroes
            2049, // Company of Heroes : Opposing Fronts
            2055, // Company of Heroes : Tales of Valor
            1484, // 2006 Age Of Pirates : Caribbean Tale
            2761, // Prey
            3507, // Titan Quest
            15879, // Titan Quest: Immortal Throne
            14887, // Titan Quest Ragnarök
            3283, // Western Commandos : La Revanche de Cooper
            681, // 2006 Silverfall
            2726, // Silverfall : Earth Awakening
            1394, // 2006 Condemned : Criminal Origins
            283, // 2006 Gothic 3
            2068, // Runaway 2 : The Dream of the Turtle
            1370, // 2006 Medieval II : Total War
            5738, // Medieval II : Total War Kingdoms
            2091, // Neverwinter Nights 2
            2011, // Neverwinter Nights 2 : Storm of Zehir
            2122, // SpellForce 2 : Shadow Wars
            2123, // SpellForce 2 : Dragon Storm
            4094, // SpellForce 2 : Faith in Destiny
            2105, // Rise of Nations : Rise of Legends
            1264, // 2006 Half-Life II : Episode One
        ],

        2007 => [
            2037, // Another World : Édition spéciale 15ème anniversaire
            2722, // Jewel Quest Solitaire
            3226, // Mini car racing
            20068, // 3-D Ultra Pinball : Le Grand Huit
            2723, // Jewel Quest Solitaire II
            3032, // Hospital Tycoon
            5877, // El Dorado Quest
            3197, // The Settlers : Bâtisseurs d'Empire
            3538, // The Settlers : Bâtisseurs d'Empire : Le Royaume de l'Est
            3220, // Les Enquêtes de Nancy Drew : Dernier Train pour Blue Moon Canyon
            2212, // Luxor 2
            4161, // Les enquêtes de Nancy Drew : La créature de Kapu Cave
            306, // 2007 Warpath
            419, // 2007 Dunes of War
            2657, // Dora l'exploratrice : Les 3 petits cochons
            17984, // Helldorado
            15927, // Tomb Raider: Anniversary
            5758, // Space Empires V
            4209, // Heroes of Hellas
            3294, // Blacksite
            342, // 2007 Bee Movie : Le Jeu
            3480, // Un week-end tranquille à Capri
            517, // 2007 Kane & Lynch : Dead Men
            676, // 2007 Sherlock Holmes contre Arsène Lupin
            625, // 2007 Power Rangers : Super Legends
            1448, // 2007 Ratatouille
            5855, // Aura 2 : Les Anneaux Sacrés
            3818, // Little Busters
            3595, // Shadowrun
            3295, // Death to Spies
            503, // Jeux de lettres 3D
            589, // Nain jaune
            1271, // Les Experts : Morts Programmées
            3582, // Stranglehold
            3258, // World in Conflict
            3268, // World in Conflict : Soviet Assault
            3202, // Penumbra : Overture
            268, // Penumbra : Black Plague
            3203, // Penumbra : Requiem
            2795, // Berlin 1943 : Les Secrets de l'Operation Wintersun
            2662, // Tortuga : Two Treasures
            1990, // Attack on Pearl Harbor
            3500, // Lego Star Wars : La Saga Complète
            446, // 2007 Escape from Paradise City
            530, // 2007 L'Ile Noyée
            451, // 2007 Experience 112
            3537, // Overlord
            15828, // The Treasures of Montezuma
            3274, // Lost Planet : Extreme Condition
            5536, // Lost Planet : Extreme Condition : Colonies Edition
            3455, // Jade Empire : Special Edition
            3246, // Hellgate : London
            276, // 2006 Kyodai Mahjongg 2006
            3702, // War Front : Turning Point
            15825, // Season Match
            3200, // S.T.A.L.K.E.R. : Shadow Of Chernobyl
            3245, // S.T.A.L.K.E.R. : Clear Sky
            3168, // S.T.A.L.K.E.R. : Call of Pripyat
            2211, // Medal of Honor : Airborne
            2097, // Sudden Strike 3 : Arms for Victory
            313, // 2007 Agatha Christie : Meurtre Au Soleil
            613, // 2007 Pirates des Caraïbes : Jusqu'au Bout du Monde
            16375, // UFO : Afterlight
            1715, // Cléopâtre : Le destin d'une reine
            2056, // Defcon
            3214, // Gears of War
            2066, // Harry Potter et l'Ordre du Phénix
            1392, // 2007 La Mal�diction de Judas
            3529, // City Life : Edition 2008
            3325, // Obscure II
            1953, // TimeShift
            5804, // Cradle of Rome
            2065, // Halo 2
            267, // 2007 Nostradamus : La Dernière Prophétie
            1328, // 2007 ArmA : Armed Assault
            2253, // Europa Universalis III
            759, // 2007 Unreal Tournament III
            457, // 2007 Fantasy Wars
            445, // 2007 Enemy Territory : Quake Wars
            2093, // Bioshock
            2428, // Call of Duty 4 : Modern Warfare
            3592, // Loki
            16391, // Two Worlds
            2808, // Crysis
            2373, // Supreme Commander
            2431, // Supreme Commander : Forged Alliance
            1437, // Half-Life 2 : Episode Two
            444, // 2007 Empire Earth III
        ],

        2008 => [
            13422, // Compil' 100% détente
            366, // Cheggers Party Quiz
            3434, // Le Dragon des mers : la dernière légende
            3374, // Building & Co : L'Architecte C'Est Vous !
            3336, // I-Fluid
            3267, // Hell's Kitchen : The Video Game
            3210, // Peggle
            3410, // Sins of a Solar Empire
            3377, // Dimensity
            3340, // Chuzzle
            3173, // Aqua Bubble
            3134, // Real Stories : Mission Equitation : Chevauchée vers l'Orient
            3011, // Warriors Orochi
            3257, // Iron Man
            3405, // 30 millions d'amis : Mon refuge pour animaux
            3004, // Mythic mahjong
            11373, // Zak McKracken : Between time and space
            2719, // Dora l'Exploratrice : Au Country des Friandises
            3239, // Art of Murder : FBI Confidential
            3124, // Cluedo Classic
            580, // Maxi Casse-Tête 3D
            275, // 2008 Crazy Machines 2
            3328, // Les Sims : Histoires de Naufragés
            277, // 2008 Raid Express
            3389, // Chocolatier
            1146, // Objectif Tarot
            2033, // Stronghold Crusader Extreme
            491, // 2008 Igor : Le Jeu
            531, // 2008 The Incredible Hulk
            3034, // Devil May Cry 4
            405, // 2008 Donkey Xote
            2595, // The Settlers : Rise of Cultures
            2363, // Age of Pirates 2 : City of Abandoned Ships
            566, // 2008 Lost
            3216, // The Club
            2821, // Bully : Scholarship Edition
            3016, // Luxor Mahjong
            1270, // Lego Indiana Jones : La trilogie originale
            2099, // Rainbow Six Vegas 2
            2019, // Ankh : Le Tournoi des Dieux
            667, // 2008 Seven Kingdoms : Conquest
            396, // 2008 Darkness Within : A la Poursuite de Loath Nolder
            593, // 2008 Nikopol : La Foire aux Immortels
            2436, // Slingo Quest
            3207, // Mallette de Jeux
            718, // Tests de culture générale
            2287, // Hotel Giant 2
            15826, // Season Match 2
            2731, // Spider-Man : Le Règne des Ombres
            546, // 2008 Le Pic Rouge
            2029, // Civilization IV : Colonization
            2826, // Femme Actuelle Jeux : Aquitania
            2475, // Sacred 2 : Fallen Angel
            2476, // Sacred 2 : Fallen Angel - Ice & Blood
            401, // 2008 Diabolik : The Original Sin
            611, // 2008 Perry Rhodan : Le Mythe des Illochim
            5753, // Mercenaries 2 : L'Enfer des Favelas
            3484, // World War One : La Grande Guerre 14-18
            411, // 2008 Dracula 3 : La Voie du Dragon
            492, // 2008 Imperium Romanum
            449, // 2008 Everlight : Le Pouvoir des Elfes
            425, // 2008 Dynasty Warriors 6
            3448, // Age of Conan : Hyborian Adventures
            3554, // Conflict : Denied Ops
            3628, // Manhunt 2
            599, // 2008 Officers
            2451, // Need for Speed Undercover
            2018, // Alone in the Dark
            588, // 2008 Mozart : Le Dernier Secret
            448, // 2008 Europa Universalis : Rome
            2642, // Tomb Raider Underworld
            3599, // MySims
            3279, // Street Fighter : The Balance Edition
            308, // 2008 A Vampyre Story
            2745, // Rise of the Argonauts
            2733, // Dead Space
            2849, // Ancient Quest of Saqqarah
            3413, // Defense Grid : The Awakening
            3301, // Turok
            3247, // Lego Batman : Le Jeu Vidéo
            3222, // Spore
            3192, // Spore Aventures Galactiques
            3237, // Bejeweled 2
            3244, // Mass Effect
            2250, // The Witcher : Enhanced Edition
            2092, // Assassin's Creed
            1977, // Brothers in Arms : Hell's Highway
            1263, // Turning Point : Fall Of Liberty
            3364, // So Blonde
            2438, // Fallout 3
            4010, // Fallout 3 : Operation Anchorage
            4011, // Fallout 3 : The Pitt
            4012, // Fallout 3 : Broken Steel
            4013, // Fallout 3 : Point Lookout
            3242, // Fallout 3 : Mothership Zeta
            3536, // Crysis Warhead
            518, // 2008 King's Bounty : The Legend
            5757, // King's Bounty : Armored Princess
            2430, // Call of Duty : World at War
            2020, // Command & Conquer : Alerte Rouge 3
            2000, // Command & Conquer : Alerte Rouge 3 : La Révolte
            3580, // Mount & Blade
            3573, // Mount & Blade : Warband
            2855, // Mount & Blade : With Fire and Sword
            5657, // Left 4 Dead
            758, // 2008 Universe at War : Earth Assault
            3331, // Space Siege
            337, // 2008 Avencast : Rise of the Mage
            1985, // Far Cry 2
            2646, // Grand Theft Auto IV
            3320, // Grand Theft Auto : Episodes from Liberty City
        ],

        2009 => [
            3415, // Vertigo
            1127, // 30+ Free Patience
            3470, // Mystery Case Files : Prime Suspects
            15908, // Mystery Case Files : Ravenhearst
            15971, // Zeno Clash
            15909, // Mystery Case Files : Madame Fate
            15910, // Mystery Case Files : Retour à Ravenhearst
            3378, // Crayon Physics Deluxe
            4210, // Heroes of Hellas 2 : Olympia
            4053, // Pahelika : Secret Legends
            3337, // Arabesque
            4096, // Jewel Quest Mysteries : Curse of the Emerald Tear
            3211, // Peggle Nights
            3199, // Jewel Quest Solitaire III
            4196, // Mahjongg Artifacts
            5806, // The Maw
            5948, // Pharaoh's mystery
            5949, // Le Mystère du Titanic
            3135, // Adiboud'Chou soigne les animaux
            3176, // Adiboud'Chou à la Campagne
            3177, // Adiboud'Chou à la Mer
            3504, // Zombie Driver
            3255, // Wings of Prey
            3335, // Flock!
            3517, // Battlestations : Pacific
            3476, // Overlord II
            65, // L'Âge de glace 3 : Le Temps des dinosaures
            3296, // Burn Zombie Burn
            3191, // Trine
            3575, // Vampire Hunters
            3035, // Terminator Renaissance
            3329, // Dreamkiller
            2814, // Settlement : Le Colosse
            2374, // La voleuse de l'ombre
            2296, // Cid the Dummy
            2829, // Où est Charlie ? Le Voyage Fantastique
            2224, // Jewel Quest : Mysteries Bundle
            18099, // Brütal Legend
            2441, // Elven Legacy
            3566, // UNO : Undercover
            2518, // Adiboud'Chou sur la Banquise
            123, // S.O.S. Fantômes : Le Jeu Vidéo
            3020, // Les Enquêtes de Nancy Drew : Le Château Hanté de Malloy
            3147, // Codename : Panzers : Cold War
            3162, // Tale of a Hero
            3302, // Mission G
            3027, // Les Sims 3
            3262, // Les sims 3 : Destination Aventure
            3261, // Les Sims 3 : Ambitions
            3146, // Les Sims 3 : Vitesse Ultime ! Kit
            3156, // Les Sims 3 : Accès VIP
            3230, // Les Sims 3 : Générations
            3637, // Les Sims 3 : Animaux & Cie
            3767, // Les Sims 3 : Jardin de Style Kit
            3816, // Les Sims 3 : Showtime
            4182, // Jewel Match 2
            3155, // Braid
            3148, // Là-Haut
            73, // James Cameron's Avatar : The Game
            3858, // Star Wars : The Clone Wars : Les Héros de la République
            663, // 2009 Secret Files 2 : Puritas Cordis
            3184, // Wanted : Les Armes du Destin
            2823, // Féerie de joyaux 2
            2791, // The Saboteur
            2674, // Still Life 2
            2377, // Bejeweled Twist 3
            3297, // ShellShock 2 : Blood Trails
            2760, // Prototype
            1961, // Agatha Christie : La Maison du Péril
            775, // 2009 World of Goo
            584, // 2009 Memento Mori
            223, // 2009 Sudoku Ball : Detective
            3433, // The Humans : Nos Ancêtres
            3273, // Mini Ninjas
            3049, // Operation Flashpoint : Dragon Rising
            3437, // L'Esprit du Loup
            2724, // Luxor : Quest for the Afterlife
            3282, // Eufloria
            2656, // 20.000 lieues sous les mers : Capitaine Nemo
            3563, // Star Wars : Le Pouvoir de la Force : Ultimate Sith Edition
            3229, // Bionic Commando
            3201, // Scorpion Disfigured
            2282, // Delta Force : Xtreme 2
            2434, // Ceville
            2618, // ArmA II
            3185, // ArmA II : Operation Arrowhead
            3479, // Arma II : Reinforcement
            3508, // White Gold : War in Paradise
            2157, // Anno 1404
            2040, // Anno 1404 Venise
            3275, // Call of Juarez : Bound in Blood
            2474, // Dark Sector
            2477, // Tom Clancy's EndWar
            3574, // Silent Hill : Homecoming
            577, // 2009 Mata Hari
            3186, // Lego Indiana Jones 2 : L'Aventure Continue
            777, // 2009 X-Blades
            2737, // SAW
            2252, // Drakensang : L'Oeil Noir
            2280, // Tropico 3
            2729, // F.E.A.R. 2 : Project Origin
            2730, // F.E.A.R. 2 : Reborn
            2286, // Saints Row 2
            3193, // Wallace & Gromit's Grand Adventures - Episode 1 : Fright of the Bumblebees
            3194, // Wallace & Gromit's Grand Adventures - Episode 2 : The Last Resort
            3195, // Wallace & Gromit's Grand Adventures - Episode 3 : Muzzled!
            3196, // Wallace & Gromit's Grand Adventures - Episode 4 : The Bogey Man
            3584, // Ken le survivant
            3446, // Jewel Quest : The Sleepless Star
            2450, // Le Parrain 2
            3272, // Sherlock Holmes contre Jack l'Eventreur
            3576, // Velvet Assassin
            2664, // Hearts of Iron III
            2665, // Hearts of Iron III : Semper Fi
            2782, // Retour sur l'Ile Mystérieuse 2 : La destinée de Mina
            1986, // Harry Potter et le Prince de sang-mêlé
            2452, // X-Men Origins : Wolverine
            2153, // The Lapins Crétins : La Grosse Aventure
            2889, // Painkiller Resurrection
            2796, // Painkiller Redemption
            3324, // Resident Evil 5
            2095, // Empire : Total War
            2030, // Runaway : A Twist of Fate
            2787, // Street Fighter IV
            5718, // Le Seigneur des Anneaux : L'Age des Conquêtes
            3989, // The Last Remnant
            3133, // Gyromancer
            16378, // Red Faction: Guerrilla
            1422, // Serious Sam HD : The First Encounter
            2677, // Dragon Age : Origins
            3125, // Dragon Age : Origins - Awakening
            2604, // Batman Arkham Asylum
            1898, // Warhammer 40.000 : Dawn of War II
            3143, // Warhammer 40.000 : Dawn of War II : Chaos Rising
            2585, // Warhammer 40.000 : Dawn of War II : Retribution
            2390, // Borderlands
            3166, // Borderlands : L'île des Zombies du Dr. Ned
            3165, // Borderlands : Emeute dans l'Underdome de Mad Moxxi
            3164, // Borderlands : L'Armurerie Secrète du Général Knoxx
            3157, // Borderlands : Nouvelle Révolution
            2435, // Call of Duty : Modern Warfare 2
            3198, // Cryostasis : Sleep of Reason
            3182, // Left 4 Dead 2
            14894, // Halo Wars
            3333, // Demigod
            2292, // Torchlight
            3510, // The Chronicles of Riddick : Assault on Dark Athena
            2800, // Risen
            3375, // Grand Ages : Rome
            3589, // Grand Ages : Rome - Reign of Augustus
            2054, // Wolfenstein
        ],

        2010 => [
            3518, // Beat Hazard
            5878, // The Lost Inca Prophecy
            3419, // Max and the Magic Marker
            3564, // Terrorist Takedown 3
            3339, // Pouce-Pouce découvre le cirque
            3341, // Pouce-Pouce entre dans course
            15911, // Mystery Case Files : Dire Grove
            3334, // Colony Defense
            3209, // Mr. Puzzle
            3505, // Shank
            2996, // Samurai II : Vengeance
            3139, // Building Simulator
            3503, // Blood Bowl : Edition Légendaire
            2899, // World Mosaics 3 : Fairy Tales
            5563, // Inquisitor
            2830, // Robin Hood
            2828, // Dr. House
            3179, // Blood Stone 007
            6100, // Spirit of Wandering
            3839, // I'm not alone
            5876, // La malédiction de sang
            3187, // Alice au Country des merveilles
            2910, // Sniper : Ghost Warrior
            2955, // Drawn : La Tour d'Iris
            2946, // Grand Galop
            2947, // Dead Meets Lead
            3506, // Raiponce
            2884, // Nancy Drew : Les Dossiers Secrets : Malédiction à Hollywood
            2887, // Nancy Drew : Les dossiers secrets n°2 : Beauté sous tension !
            3545, // Drakensang : The River of Time
            3298, // Mahjongg Dimensions Deluxe
            2825, // Aladin et la Lampe Merveilleuse
            3013, // Gothic 4 : Arcania
            2824, // Alice au Country des Merveilles : L'incroyable Aventure
            4095, // Venetica
            2291, // Shades of Death : Royal Blood
            2289, // Cities XL 2011
            3172, // BlazBlue : Calamity Trigger
            2440, // Worms Reloaded
            2678, // Arsenal of Democracy
            2758, // Shadow of The Colossus
            2170, // Epoch Wars
            213, // Shrek 4 : Il était une Fin
            2062, // Spider-Man Dimensions
            3436, // The Guild 2 : Renaissance
            2882, // Les Enquêtes de Nancy Drew : Kidnapping aux Bahamas
            2904, // Les Enquêtes de Nancy Drew : Panique à Waverly Academy
            2031, // MahJong Suite
            2230, // Cursed House
            3243, // Twin Sector
            1965, // Puzzle dimension
            3367, // Transformers : La Guerre pour Cybertron
            1187, // Redrum 2 : Les Crimes du Docteur Fraud
            2277, // Fiction Fixers : The Curse of Oz
            1183, // Mystery of the Earl
            1184, // Golden Trails : The new western rush
            1196, // Dr Jekyll & Mr Hyde
            3416, // Rise of Prussia
            3363, // Heroes of Kalevala
            3206, // Machinarium
            3288, // Disciples III : Renaissance
            19156, // Castlevania: Lords of Shadow
            3121, // The Settlers 7 : À l'aube d'un nouveau royaume
            2043, // L'exorciste
            3289, // Kane & Lynch 2 : Dog Days
            2357, // Le destin de Marie-Antoinette
            3454, // Dark Void
            2366, // Les Trois Mousquetaires
            2520, // Chicken Invaders 4 : Ultimate Omelette
            4932, // Alien Breed : Impact
            4930, // Alien Breed 2 : Assault
            2862, // Civilization V
            4066, // Civilization V : Gods & Kings
            15619, // Civilization V: Brave New World
            2649, // Lara Croft et le gardien de la lumière
            3271, // 1912 : Titanic Mystery
            3028, // Mass Effect 2
            3483, // Mass Effect 2 : Arrival
            3012, // R.U.S.E.
            2519, // Enigmes & Objets Cachés : Shutter Island
            2359, // Énigmes & Objets Cachés : The Tudors
            3388, // Numen : Contest of Heroes
            2840, // Battlefield : Bad Company 2
            2732, // Tron Evolution
            2488, // Dead Rising 2
            3631, // Dead Rising 2 : Off the Record
            2444, // Just Cause 2
            2298, // Men of War : Red Tide
            2772, // Men of War : Assault Squad
            2776, // Splinter Cell Conviction
            3285, // Darksiders
            2053, // Mafia II
            3188, // Mafia II : Joe's Adventures
            5994, // Mafia II : Jimmy's Vendetta
            5995, // Mafia II : Mafia II : The Betrayal of Jimmy
            2041, // Rulers of Nations : Geo Political Simulator 2
            1970, // Ankh : The lost treasures
            1267, // Brico Party : Les As du Bricolage
            1962, // Dark Tales : Le Chat Noir
            1980, // The Ball
            2264, // Voodoo Dice
            1993, // Les experts : Complot à Las Vegas
            2446, // Alter Ego
            2115, // Dark Fall : Lost Souls
            2283, // Farming Simulator 2011
            2790, // Lost Planet 2
            2361, // Need for Speed : Hot Pursuit
            3390, // Ghost Pirates of Vooju Island
            3418, // Australia Zoo Quest
            3174, // Singularity
            5762, // Les Chroniques de Sadwick : The Whispered World
            3025, // Metro 2033
            3612, // Monkey Island : Édition spéciale Collection
            2447, // Black Mirror II
            2439, // Star Wars : Le Pouvoir de la Force II
            3183, // Lego Harry Potter : Années 1 à 4
            2856, // Gray Matter
            2038, // Harry Potter et les Reliques de la Mort - Première Partie
            15973, // Harry Potter et les Reliques de la Mort : Deuxième Partie
            3472, // Lost Horizon
            3276, // Command & Conquer 4 : Le Crépuscule du Tiberium
            1995, // Victoria II
            16014, // Victoria II: A House Divided
            16015, // Victoria II: Heart of Darkness
            3008, // Les trésors de Montezuma 2
            3299, // Alpha Protocol
            3254, // Darkest of Days
            3406, // Puzzle Hero
            3447, // Serious Sam HD : The Second Encounter
            194, // Prince of Persia : Les Sables oubliés
            2001, // History : Great Battles Medieval
            2376, // Supreme Commander 2
            2703, // Aliens vs Predator
            3001, // Battle vs Chess
            3009, // Puzzle Quest 2
            2445, // Lionheart : King's Crusade
            2686, // Assassin's Creed II
            1932, // Two Worlds II
            1266, // Medal of Honor
            2680, // Divinity II : Ego Draconis
            15412, // Divinity II : Flames of Vengeance
            2718, // Call of Duty : Black Ops
            2459, // Starcraft II : Wings of Liberty
            2717, // Bioshock 2
            3842, // Fallout New Vegas
            3843, // Fallout New Vegas : Dead Money
            3010, // Fallout New Vegas : Honest Hearts
            3844, // Fallout New Vegas : Old World Blues
            3619, // Fallout New Vegas : Lonesome Road
            3846, // Fallout New Vegas : Gun Runners' Arsenal
            3845, // Fallout New Vegas : Courier's Stash
            2667, // Napoléon : Total War
        ],

        2011 => [
            3227, // Hamilton's Great Adventure
            3321, // Yars' Revenge
            4195, // Snow Queen Mahjong
            3641, // Dungeon Defenders
            4123, // Esprits Criminels
            4211, // Heroes Of Hellas 3 : Athens
            3338, // Lost in Time : The Clockwork Tower
            5710, // Ghost Whisperer
            4117, // Portal 2
            3630, // Orcs Must Die !
            3543, // Derrick : Meurtre dans un parterre de fleurs
            3648, // Secrets of the Dark : Le démon des Ombres
            4104, // Jewel Quest Mysteries 2 : Trail of the Midnight Heart
            4076, // Jewel Quest Mysteries III : La Septième Porte
            4063, // Amerzone : Part 1
            4068, // Amerzone : Part 2
            4059, // Amerzone Part 3
            3291, // Puzzle Expedition
            3159, // Ancients of Ooga
            3140, // Rio : Course de plage
            3170, // Le roi du feu
            5798, // Costume Quest
            3171, // Section 8 : Prejudice
            3482, // Trapped Dead
            3270, // L'Inquisiteur
            4097, // Herofy
            3768, // Jungle Kartz
            3820, // Wasteland Angel
            3891, // Rock of Ages
            3680, // Jane's Advanced Strike Fighters
            3136, // Luxor HD
            2852, // Spring Bonus
            3003, // Star Raiders
            2972, // Columbus : Le fantôme de la pierre mystérieuse
            3583, // Sengoku
            4690, // Limbo
            3956, // Marie-Antoinette et les disciples de Loki
            3403, // Luxor Adventures
            3524, // From Dust
            3160, // Les trésors de Montezuma 3
            2898, // Fortix 2
            2853, // Farmscapes
            2911, // Faery : Legends of Avalon
            2850, // Age of Japan
            2858, // Cave Quest
            2857, // Anne's Dream World
            2851, // Pantheon
            2827, // Hoard
            2773, // IL-2 Sturmovik : Cliffs of Dover
            2738, // Crazy Machines Elements
            3513, // Pirates of Black Cove
            3360, // Gemini Rue
            2448, // Dreamcast Collection
            3691, // Two Worlds II : Castle Defense
            3654, // Les Nouvelles Enquêtes de Nancy Drew : Secrets Mortels
            3653, // Renegade Ops
            3501, // Anomaly : Warzone Earth
            2767, // Crysis 2
            2739, // Risk : Factions
            2368, // Dying For Daylight
            2364, // Dream Day First Home
            3609, // OIO the Game
            3682, // Explodemon!
            5386, // Payday : The Heist
            2442, // Back to the Future : Episode 102 : Get Tannen!
            3481, // Back to the Future : The Game - Episode 3 : Citizen Brown
            2290, // Jodie Drake and the World in Peril
            2288, // L'héritage secret : Une aventure Kate Brooks
            2836, // Cargo!
            3256, // Hunted : The Demon's Forge
            2697, // Ghostbusters : Sanctum of Slime
            3674, // Odysseus : Le retour d'Ulysse
            3676, // Bastion
            2285, // Angry Birds
            2834, // Amnesia : The Dark Descent
            3249, // The Tiny Bang Story
            2832, // World Mosaics 4
            2284, // Mystery Case Files - 13th Skull
            2349, // Ancient Spirits : Columbus' Legacy
            3817, // Hard Reset
            2794, // Dr. Jekyll & Mr. Hyde : The Strange Case
            3519, // Bigfoot : Chasing Shadows
            3516, // Akhra : Les trésors
            3167, // Operation Flashpoint : Red River
            2945, // BRINK
            2797, // Nightmare on the Pacific
            2437, // Les Enquêtes de Nancy Drew : Chasseurs de Tornades
            3000, // Drakensang : Phileasson's Secret
            2721, // Blake et Mortimer : La Malédiction des Trente Deniers
            2655, // Battle : Los Angeles
            3660, // Sonic Generations
            3632, // Might and Magic Heroes VI
            5853, // Might & Magic Heroes VI : Shades of Darkness
            3021, // The Witcher 2 : Assassins of Kings
            2453, // Season Match 3 : La Malédiction de Crow
            3443, // Cars 2
            13506, // Treasure Adventure Game
            13507, // To the Moon
            15832, // King Arthur: The Role-Playing Wargame
            3667, // King Arthur : Fallen Champions
            3603, // Serious Sam : Double D
            3795, // L'Empire Aztèque
            2831, // Jewel Match 3
            2948, // DarkSpore
            2681, // Les Sims Medieval
            3562, // Les Sims Medieval : Nobles & Pirates
            2639, // Magicka
            2443, // Cities in Motion
            3617, // Worms Ultimate Mayhem
            3639, // Cities XL 2012
            3596, // Warhammer 40.000 : Space Marine
            3565, // Call of Juarez : The Cartel
            5756, // Sanctum
            3498, // Red Faction Armageddon
            3633, // Red Faction Armageddon : Path to War
            3018, // The Next BIG Thing
            3286, // Lego Star Wars III : The Clone Wars
            2720, // Homefront
            2454, // A New Beginning
            3608, // Deus Ex : Human Revolution
            3689, // Serious Sam III : BFE
            4034, // Red Orchestra 2 : Heroes of Stalingrad
            2900, // Lego Pirates des Caraïbes : Le Jeu Vidéo
            5522, // Anno 2070
            15870, // Anno 2070 : En eaux profondes
            4033, // A Game of Thrones Genesis
            2675, // Black Mirror III : The Final Chapter
            3568, // Dead Island
            5790, // Tropico 4
            3836, // Tropico 4 : Modern Times
            3678, // L.A. Noire
            2379, // Bulletstorm
            3238, // Luxor : 5th Passage
            3601, // Might & Magic : Clash of Heroes
            4240, // F.3.A.R.
            3711, // Stronghold 3
            3709, // Postal III
            3645, // Disney Universe
            3693, // Lego Harry Potter : Années 5 à 7
            2433, // Dexter : the game
            5805, // Cradle of Rome 2
            2375, // Dive to the Titanic
            3627, // Rage
            2348, // Garshasp
            4071, // Trine 2
            16011, // Trine 2 : La menace gobeline
            5849, // Saints Row : The Third
            4058, // Battlefield 3
            4249, // Fable III
            5781, // Tiny Token Empires
            3688, // Batman Arkham City
            14893, // Batman Arkham City : Harley Quinn's Revenge
            2654, // Total War : Shogun 2
            3835, // Total War : Shogun 2 : La Fin des Samouraïs
            2663, // Assassin's Creed : Brotherhood
            2614, // Dragon Age II
            3240, // Dragon Age II : Le Prince Exilé
            3241, // Dragon Age II : Le Palais des Perles Noires
            3634, // Dragon Age II : Mark of the Assassin
            3690, // Assassin's Creed : Revelations
            3442, // Duke Nukem Forever
            3616, // The Cursed Crusade
            3514, // Alice : Retour au Pays de la Folie
            2658, // Dungeons
            3606, // Dungeons : The Dark Lord
            2698, // Dead Space 2
            19337, // The Legend of Zelda: Mystery of Solarus DX
            19339, // The Legend of Zelda: Mystery of Solarus XD
            19341, // The Legend of Zelda XD2: Mercuris Chess
            3671, // The Elder Scrolls V : Skyrim
            4934, // The Elder Scrolls V : Skyrim - Dawnguard
            5857, // The Elder Scrolls V : Skyrim - Hearthfire
            5856, // The Elder Scrolls V : Skyrim - Dragonborn
            3397, // Dungeon Siege III
            4783, // Dungeon Siege III : Treasures of the Sun
        ],

        2012 => [
            5764, // Adam's Venture : Episode 3 : Revelations
            5526, // Angry Birds Space
            15912, // Mystery Case Files : Terreur à Ravenhearst
            4052, // Les Tuniques Bleues : Nord vs Sud
            5650, // Jewel Quest IV : Heritage
            5964, // Solitaire Mystery : Stolen Power
            5682, // Hidden Mysteries : Return to Titanic
            5535, // The Expendables 2 Videogame
            5100, // Unmechanical
            4890, // Orcs Must Die! 2
            4720, // Foreign Legion: Multi Massacre
            4144, // Ravensburger Puzzle Selection
            5729, // Jewel Quest Mysteries : L'Oracle d'Ur
            5840, // Arabesque 2 : Retour a Cabot Cove
            5765, // Monument Builders : Notre-Dame de Paris
            4197, // Monument Builders : Tour Eiffel
            3838, // Monument Builders : Titanic
            5674, // Worms Revolution
            4061, // Vessel
            5568, // The Amazing Spider-Man
            4184, // Royal Détective: Le seigneur des statues
            4098, // L'Age de Glace 4 : La Dérive des Continents - Jeux de l'Arctique !
            4179, // Totem Quest
            4060, // Qui veut gagner des millions ? Editions spéciales
            3962, // Squids
            3790, // Team Hot Wheels : Drift
            3957, // Mermaid Adventures : La Perle Magique
            3840, // Zooloretto
            3789, // The Princess Case : A Royal Scoop
            15997, // Secret Files 3
            3889, // Botanicula
            3955, // Les trésors de la compagnie des Indes orientales
            3929, // Blades of Time
            3982, // Alchemist's Apprentice
            3987, // Game of Thrones : Le Trône de Fer
            3848, // Shoot Many Robots
            5726, // Giana Sisters : Twisted Dreams
            5534, // Hitman Absolution
            5779, // Anna
            5691, // Carrier Command : Gaea Mission
            3997, // Resident Evil : Operation Raccoon City
            3988, // Toy Soldiers
            3949, // Jewel Quest 6 : le Dragon de Saphir
            4130, // Hoodwink
            3930, // Binary Domain
            3805, // Depth Hunter
            10329, // The book of unwritten tales : Critter chronicles
            3985, // World's Greatest Places Mahjong
            4143, // Les aventures de Mary Ann : Les pirates de la Chance
            5736, // 007 Legends
            5850, // Rebelle
            5690, // Viking : Battle for Asgard
            4164, // Endless Space
            5092, // Sleeping Dogs
            5810, // Les Secrets du Titanic 1912 - 2012
            5686, // Of Orcs and Men
            5752, // DeadLight
            13204, // Borderlands 2
            4160, // Iron Front : Liberation 1944
            3815, // Deep Black : Reloaded
            3809, // Painkiller : Recurring Evil
            5794, // Sniper Elite V2
            15942, // Sniper Elite: Nazi Zombie Army
            6105, // Sniper Elite : Nazi Zombie Army 2
            4128, // Alan Wake
            4106, // Ghost Recon : Future Soldier
            4689, // Prototype 2
            4077, // Cradle of Egypt
            4782, // Wargame : European Escalation
            5701, // Medal of Honor : Warfighter
            5263, // Darksiders II
            3993, // Max Payne 3
            3935, // King Arthur II : The Role-playing Wargame
            3730, // King Arthur II : The Role-playing Wargame : Dead Legions
            5740, // Dishonored
            14889, // Dishonored : Dunwall City Trials
            14890, // Dishonored : La lame de Dunwall
            14888, // Dishonored : Les sorcières de Brigmore
            3933, // Risen 2 : Dark Waters
            3828, // Mass Effect 3
            5825, // Far Cry 3
            3833, // Diablo III
            5702, // Assassin's Creed III
            3804, // Les Royaumes d'Amalur : Reckoning
            5809, // Les Royaumes d'Amalur : Reckoning - La Légende de Kel le Mort
            14891, // Les royaumes d'Amalur : Reckoning : Les dents de Naros
        ],

        2013 => [
            16060, // The Bureau: XCOM Declassified
            16061, // The Bureau : XCOM Declassified - Hangar 6 R&D
            5845, // Zeno Clash II
            5782, // Papo & Yo
            10574, // Starpoint Gemini 2
            5843, // Dark
            5818, // Omerta : City of Gangsters
            5841, // Dracula 4 : L'Ombre du Dragon
            5838, // Star Trek
            15913, // Mystery Case Files : Shadow Lake
            15914, // Mystery Case Files : Fate's Carnival
            15937, // Rayman Legends
            15928, // Les Chevaliers de Baphomet : La Malédiction du Serpent
            19155, // Castlevania: Lords of Shadow - Mirror of Fate HD
            5842, // Evoland
            5868, // Lumen - Tome 1 : Bemko & Ezechiel
            6099, // Mayan Prophecies : Le Bateau Fantôme
            6153, // Deadfall Adventures
            15903, // Far Cry 3: Blood Dragon
            5801, // Anomaly 2
            13488, // Game Dev Tycoon
            14896, // Batman Arkham Origins
            14897, // Batman Arkham Origins : Un Cœur de Glace
            13451, // Brothers : A tale of two sons
            6323, // Battlefield 4
            5780, // Crysis 3
            6238, // Assassin's Creed IV : Black Flag
            13460, // Grand Theft Auto V
            13521, // Total War : Rome II
        ],

        2014 => [
            10555, // Industry Empire
            6383, // Max : The Curse of Brotherhood
            15915, // Mystery Case Files : Dire Grove, Forêt Sacrée
            10533, // Cargo 3
            10472, // Randal's Monday
            9547, // Ancient Space
            9552, // Thief
            9546, // Defense Grid 2
            9293, // Sacred 3
            6472, // The treasures of Montezuma 4
            10478, // The binding of Isaac : Rebirth
            10532, // Lara Croft and the temple of Osiris
            15602, // Metal Gear Solid V: Ground Zeroes
            15612, // RollerCoaster Tycoon World
            6376, // Bound by Flame
            10509, // Borderlands The Pre-Sequel !
            9544, // Battle Academy 2: Eastern Front
            16942, // Dreamfall Chapters: The Longest Journey
            19161, // Resident Evil 4: Ultimate HD Edition
            19911, // Gabriel Knight: Sins of the Fathers - 20th Anniversary
            10665, // Les Sims 4
            11350, // Les Sims 4 : Détente au spa
            10666, // Les Sims 4 : Au travail
            11747, // Civilization : Beyond Earth
            15948, // Stronghold Crusader II
            10689, // Stronghold Crusader II : The Princess and the Pig
            6290, // LEGO La grande aventure : Le jeu vidéo
            18068, // Castlevania: Lords of Shadow 2
            10732, // Massive Chalice
            13196, // Tropico 5
            10866, // The Talos Principle
            6271, // Assassin's Creed : Liberation HD
            9541, // Watch_Dogs
            9542, // Watch_Dogs : Bad Blood
            10324, // Alien : Isolation
            13452, // Ultra Street Fighter IV
            4203, // Murder on the Titanic
            15867, // Age of Wonders III
            15868, // Age of Wonders III: Golden Realms
            15869, // Age of Wonders III: Eternal Lords
            14643, // Dark Souls II
            14646, // Dark Souls II : Crown of the Sunken King
            14645, // Dark Souls II : Crown of the Old Iron King
            14644, // Dark Souls II : Crown of the Ivory King
            14647, // Dark Souls II : Scholar of the First Sin
            11761, // Dragon Age Inquisition
            10479, // Far Cry 4
            10551, // Assassin's Creed Unity
        ],

        2015 => [
            17899, // Blood Bowl II
            10736, // D4
            15916, // Mystery Case Files : La clé de Ravenhearst
            15917, // Mystery Case Files : Ravenhearst : La Révélation
            16012, // Trine 3: The Artifacts of Power
            11755, // Evoland II
            10708, // Color Guardians
            13203, // Titan Souls
            10868, // Dark Romance: Le Coeur de la Bête
            13609, // Layers of Fear
            10601, // Grim Fandango Remastered
            10580, // Saints Row : Gat out of hell
            10602, // Grey Goo
            14880, // Cities XXL
            17898, // Warhammer Quest
            15929, // Might & Magic Heroes VII
            10758, // Lego Jurassic World
            13516, // The curse of Issyos
            10624, // Evolve
            17900, // Mordheim: City of the Damned
            17901, // Warhammer: The End Times - Vermintide
            13041, // Battle of Empires : 1914-1918
            11749, // Grand Ages : Medieval
            10684, // Dying Light
            10737, // Europa Universalis IV : Common Sense
            14886, // Rise of the Tomb Raider
            10712, // Mortal Kombat X
            15900, // Anno 2205
            16372, // King's Quest: The Complete Collection
            11796, // Colonial Conquest
            19338, // The Legend of Zelda: Return of the Hylian SE
            13626, // Fallout 4
            13627, // Fallout 4 : Automatron
            13628, // Fallout 4 : Wasteland Workshop
            13629, // Fallout 4 : Far Harbor
            13630, // Fallout 4 : Contraptions Workshop
            15835, // Assassin’s Creed Syndicate
            15836, // Assassin's Creed Syndicate : Jack l'Éventreur
            15837, // Assassin's Creed Syndicate : Le dernier Maharaja
            10720, // The Witcher 3 : Wild Hunt
            10685, // Dungeons 2
            11748, // Total War : Attila
            18039, // Tales of Zestiria
        ],

        2016 => [
            13540, // RWBY : Grimm Eclipse
            15918, // Mystery Case Files : Heure Funeste
            14911, // Planet Explorers
            16070, // WATCH_DOGS 2
            16071, // Watch Dogs 2 : Conditions Humaines
            16072, // Watch Dogs 2: No Compromise
            13539, // Furi
            13391, // UnderDread
            13453, // Endless Legend : Shifters
            15520, // Dishonored 2
            15592, // Tyranny
            15593, // Tyranny: Bastard's Wound
            14815, // World of Final Fantasy
            12451, // Tales of Berseria
            14882, // Mafia III
            14884, // Mafia III : Faster, Baby !
            14885, // Mafia III : La Hache de Guerre
            14883, // Mafia III : Le signe des temps
            15703, // Total War: Warhammer
            15904, // Far Cry Primal
            14648, // Dark Souls III
            14649, // Dark Souls III : Ashes of Ariandel
            14650, // Dark Souls III : The Ringed City
            14767, // Shadow Warrior 2
        ],

        2017 => [
            15919, // Mystery Case Files : Le Voile Noir
            14903, // Halo Wars 2
            19340, // The Legend of Zelda: Book of Mudora
            16067, // Zoo Tycoon: Ultimate Animal Collection
        ],

        2018 => [
            19700, // The Shadows of Sergoth
            15687, // Ghost of A Tale
        ],

        2019 => [
            19335, // Le Défi de Zeldo : Chapitre 1 : La Revanche du Bingo
            19336, // Le Défi de Zeldo - Chapitre 2 : La Tour des Souvenirs
            19325, // Tales of Vesperia: Definitive Edition
        ],

        2020 => [
            20050, // Yarntown
        ],

        2021 => [
            19699, // Le Voyage de Nephi
            20124, // Ocean's Heart
        ],
    ];

    /**
     * The platform IDs to exclude.
     *
     * @var int[]
     */
    const EXCLUDED_PLATFORMS = [
        1, 2, 5, 10, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34, 35, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,
    ];

    /**
     * all the elements IDs to exclude.
     *
     * @var int[]
     */
    const EXCLUDED_ELEMENTS = [
        /* Parents */
        7, // Age of Empires : Édition Collector
        6377, // Age of Mythology : Extended Edition
        5744, // Baldur's Gate : Enhanced Edition
        1971, // Divinity II : The Dragon Knight Saga
        15390, // Dracula Trilogy
        1144, // Final Doom
        3952, // Half-Life : Final Pack
        20122, // Icewind Dale: Enhanced Edition
        2154, // Les Sims : 8 en 1
        19332, // Manhunt Collection
        4121, // Neverwinter Nights Diamond
        3204, // Penumbra Collection
        2670, // Postal : Classic and Uncut
        15935, // Quake II: Quad Damage
        15941, // Sacred Gold
        14876, // SPORE Collection
        16939, // Sam & Max: Anthology
        17685, // SpellForce 2: Anniversary Edition
        712, // Syndicate Plus
        16010, // The Settlers: History Collection
        16013, // Trine Trilogy
        16080, // Tropico Reloaded
        16376, // UFO Triolgy
        2812, // Unreal Anthology
        15954, // Uru: The Complete Chronicles

        /* Children */
        406, // Doom
        1306, // Half-Life : Counter-Strike

        /* Other platforms */
        1160, // Bubble Bobble
        5444, // Gex 3D : Enter the Gecko
        5449, // Hercule
        5401, // Legend of Foresia : La Contrée Interdite
        3280, // Mortal Kombat Trilogy
        17413, // Out Run
        5360, // The Incredible Hulk : The Pantheon Saga
        1136, // The Lost Vikings
        5032, // Timon & Pumbaa s'éclatent dans la Jungle
        5514, // Toy Story 2 : Buzz l'Eclair à la rescousse

        /* Multiplayer */
        3252, // Battlefield Heroes
        1943, // Half-Life : Natural Selection
        2347, // Rift : Planes of Telara
    ];

    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    private $entityAdministratorManager;

    /**
     * Constructor.
     *
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     */
    public function __construct(EntityAdministratorManager $entityAdministratorManager)
    {
        $this->entityAdministratorManager = $entityAdministratorManager;
    }

    /**
     * Get the available years.
     *
     * @return int[] The years
     */
    public function getYears(): array
    {
        return array_keys(self::ELEMENTS);
    }

    /**
     * Get the elements.
     *
     * @param int $year The year
     *
     * @return \App\Entity\Element\Element[] The elements
     */
    public function getByYear(int $year): array
    {
        $elementIds = self::ELEMENTS[$year];
        $elements = $this->entityAdministratorManager->get(Element::class)->findBy([
           QueryBuilder::WHERE_IN => ['id' => $elementIds],
       ]);

        // Sort elements according to the element list
        usort($elements, function (Element $element1, Element $element2) use ($elementIds) {
            return array_search($element1->getId(), $elementIds, true) <=> array_search($element2->getId(), $elementIds, true);
        });

        return $elements;
    }

    /**
     * Get the forgotten elements.
     *
     * @return array The forgotten elements
     */
    public function getOublies(): array
    {
        $qb = $this->entityAdministratorManager->get(VideoGame::class)->getRepository()->createQueryBuilder('element');

        $qb
            ->leftJoin('element.platforms', 'platform')
            ->where($qb->expr()->andX(
                $qb->expr()->notIn('element', ':excludedElements'),
                $qb->expr()->orX(
                    $qb->expr()->isNull('platform.id'),
                    $qb->expr()->notIn('platform', ':excludedPlatforms')
                )
            ))
            ->setParameter('excludedElements', array_merge($this->getAllElementIds(), self::EXCLUDED_ELEMENTS))
            ->setParameter('excludedPlatforms', self::EXCLUDED_PLATFORMS)
            ->orderBy('element.id')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * Get all IDs.
     *
     * @return int[] The IDs
     */
    private function getAllElementIds(): array
    {
        $ids = [];

        foreach (self::ELEMENTS as $yearElementIds) {
            $ids = array_merge($ids, $yearElementIds);
        }

        return $ids;
    }
}
