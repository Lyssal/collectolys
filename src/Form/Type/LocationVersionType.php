<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type;

use App\Entity\Location;
use App\Entity\LocationVersion;
use App\Form\Type\File\IconType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The LocationVersion subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class LocationVersionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'startDate',
                null,
                [
                    'label' => 'date.start',
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'endDate',
                null,
                [
                    'label' => 'date.end',
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'name',
                null,
                [
                    'label' => 'name',
                ]
            )
            ->add(
                'emoji',
                null,
                [
                    'label' => 'emoji',
                ]
            )
            ->add(
                'icon',
                IconType::class,
                [
                    'label' => 'icon',
                    'path' => Location::ICON_PATH,
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => LocationVersion::class,
            ])
        ;
    }
}
