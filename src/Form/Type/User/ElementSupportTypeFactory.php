<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\User;

use App\Entity\Element\Element;
use App\Entity\Element\Type\Audio;
use App\Entity\Element\Type\Video;
use App\Form\Type\User\ElementSupport\AudioType;
use App\Form\Type\User\ElementSupport\VideoType;

/**
 * This class permits to get the form type for a user element support.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementSupportTypeFactory
{
    /**
     * Get the form class.
     *
     * @param \App\Entity\Element\Element $element The element
     *
     * @return string The form class
     */
    public static function getTypeClass(Element $element): string
    {
        if ($element instanceof Audio) {
            return AudioType::class;
        }

        if ($element instanceof Video) {
            return VideoType::class;
        }

        return ElementSupportType::class;
    }
}
