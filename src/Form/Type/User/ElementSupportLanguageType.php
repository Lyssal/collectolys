<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\User;

use App\Doctrine\Manager\LanguageManager;
use App\Entity\Language;
use App\Entity\Type;
use App\Entity\User\ElementSupportLanguage;
use App\Enum\LanguageSourceEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The ElementSupportLanguage subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementSupportLanguageType extends AbstractType
{
    /**
     * The language manager.
     *
     * @var \App\Doctrine\Manager\LanguageManager
     */
    private $languageManager;

    /**
     * Constructor.
     *
     * @param \App\Doctrine\Manager\LanguageManager $languageManager The language manager
     */
    public function __construct(LanguageManager $languageManager)
    {
        $this->languageManager = $languageManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \App\Entity\Type $type */
        $type = $options['type'];
        $langues = $this->languageManager->findAll();

        $builder
            ->add('language', null, [
                'label' => 'language',
                'choices' => $langues,
                'preferred_choices' => [$this->languageManager->findOneById(Language::DEFAULT)],
            ])
            ->add('position', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'collection-position',
                ],
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($type) {
                $form = $event->getForm();
                /** @var \App\Entity\User\ElementSupportLanguage $elementSupportLanguage */
                $elementSupportLanguage = $event->getData();

                $form
                    ->add('sources', ChoiceType::class, [
                        'label' => 'default_language_sources',
                        'required' => true,
                        'choices' => array_flip(LanguageSourceEnum::VALUES),
                        'data' => null !== $elementSupportLanguage ? $elementSupportLanguage->getSources() : $type->getDefaultLanguageSources(),
                        'multiple' => true,
                    ])
                ;
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user_elementsupportlanguage';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ElementSupportLanguage::class,
            ])
            ->setRequired('type')
            ->setAllowedTypes('type', Type::class)
        ;
    }
}
