<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\User\ElementSupport;

use App\Entity\User\ElementSupport;
use App\Enum\AudioCodecEnum;
use App\Enum\VideoCodecEnum;
use App\Form\Type\User\ElementSupportType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The ElementSupport\Video subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class VideoType extends ElementSupportType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('videoBitrate', null, [
                'label' => 'video.bitrate',
            ])
            ->add('audioBitrate', null, [
                'label' => 'audio.bitrate',
            ])
            ->add('videoCodec', ChoiceType::class, [
                'label' => 'video.codec',
                'required' => false,
                'choices' => array_flip(VideoCodecEnum::VALUES),
            ])
            ->add('audioCodec', ChoiceType::class, [
                'label' => 'audio.codec',
                'required' => false,
                'choices' => array_flip(AudioCodecEnum::VALUES),
            ])
            ->add('resolutionWidth')
            ->add('resolutionHeight')
            ->add('threeDimensional', null, [
                'label' => 'three_dimensional',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefaults([
                'data_class' => ElementSupport\Video::class,
            ])
        ;
    }
}
