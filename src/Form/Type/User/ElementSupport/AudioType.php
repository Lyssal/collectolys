<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\User\ElementSupport;

use App\Entity\User\ElementSupport;
use App\Enum\AudioCodecEnum;
use App\Form\Type\User\ElementSupportType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The ElementSupport\Audio subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class AudioType extends ElementSupportType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('bitrate', null, [
                'label' => 'bitrate',
            ])
            ->add('codec', ChoiceType::class, [
                'label' => 'codec',
                'required' => false,
                'choices' => array_flip(AudioCodecEnum::VALUES),
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefaults([
                'data_class' => ElementSupport\Audio::class,
            ])
        ;
    }
}
