<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\User;

use App\Entity\User\UserElement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

/**
 * The UserElement subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class UserElementType extends AbstractType
{
    public function __construct(private RouterInterface $router)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \App\Entity\User\UserElement $userElement */
        $userElement = $builder->getData();

        $builder
            ->setAction($this->router->generate('user_userelement_edit', [
                'element' => $userElement->getElement()->getId(),
            ]))
            ->add('note', ChoiceType::class, [
                'label' => 'note',
                'required' => false,
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    '10' => 10,
                ],
            ])
            ->add('wish', null, [
                'label' => 'wish.add',
            ])
            ->add('comment', null, [
                'label' => 'comment',
                'attr' => [
                    'rows' => '4',
                ],
            ])
            ->add('uses', CollectionType::class, [
                'label' => 'uses',
                'entry_type' => ElementUseType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-grid' => 'true',
                ],
            ])
            ->add('supports', CollectionType::class, [
                'label' => 'supports',
                'entry_type' => ElementSupportTypeFactory::getTypeClass($userElement->getElement()),
                'entry_options' => [
                    'type' => $userElement->getElement()->getType(),
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_name' => '__parent_name__',
                'by_reference' => false,
                'error_bubbling' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-grid' => 'true',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => UserElement::class,
            ])
        ;
    }
}
