<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\User;

use App\Doctrine\Manager\Support\SupportManager;
use App\Doctrine\Manager\User\ElementStorageManager;
use App\Entity\Type;
use App\Entity\User\ElementSupport;
use App\Enum\ConditionEnum;
use Lyssal\Doctrine\Orm\QueryBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

/**
 * The ElementSupport subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ElementSupportType extends AbstractType
{
    /**
     * The security service.
     *
     * @var \Symfony\Component\Security\Core\Security
     */
    private $security;

    /**
     * The support manager.
     *
     * @var \App\Doctrine\Manager\Support\SupportManager
     */
    private $supportManager;

    /**
     * The element storage manager.
     *
     * @var \App\Doctrine\Manager\User\ElementStorageManager
     */
    private $elementStorageManager;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Security\Core\Security        $security              The security service
     * @param \App\Doctrine\Manager\Support\SupportManager     $supportManager        The support manager
     * @param \App\Doctrine\Manager\User\ElementStorageManager $elementStorageManager The element storage manager
     */
    public function __construct(Security $security, SupportManager $supportManager, ElementStorageManager $elementStorageManager)
    {
        $this->security = $security;
        $this->supportManager = $supportManager;
        $this->elementStorageManager = $elementStorageManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \App\Entity\Type $type */
        $type = $options['type'];
        $storages = $this->elementStorageManager->findBy(['user' => $this->security->getUser()]);
        $supports = $this->supportManager->findBy([
            'type' => $type,
        ], null, null, null, [
            QueryBuilder::INNER_JOINS => ['types' => 'type'],
        ]);

        $builder
            ->add('storage', null, [
                'label' => 'storage',
                'choices' => $storages,
            ])
            ->add('storageBackup', null, [
                'label' => 'storage_backup',
                'choices' => $storages,
            ])
            ->add('support', null, [
                'label' => 'support',
                'choices' => $supports,
            ])
            ->add('version', null, [
                'label' => 'version',
            ])
            ->add('condition', ChoiceType::class, [
                'label' => 'condition',
                'required' => false,
                'choices' => array_flip(ConditionEnum::VALUES),
            ])
            ->add('comment', null, [
                'label' => 'comment',
                'attr' => [
                    'rows' => '4',
                ],
            ])
            ->add('webSource', UrlType::class, [
                'label' => 'web_source',
                'required' => false,
            ])
            ->add('languages', CollectionType::class, [
                'label' => 'languages',
                'entry_type' => ElementSupportLanguageType::class,
                'entry_options' => [
                    'type' => $type,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-collection-positionable' => '',
                    'data-grid' => 'true',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user_elementsupport';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('type')
            ->setAllowedTypes('type', Type::class)
            ->setDefaults([
                'data_class' => ElementSupport::class,
            ])
        ;
    }
}
