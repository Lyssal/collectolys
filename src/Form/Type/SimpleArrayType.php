<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type;

use App\Form\DataTransformer\SimpleArrayToTextTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * A simple array in a textarea.
 */
final class SimpleArrayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addViewTransformer(new SimpleArrayToTextTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Fix for Easyadmin
        $resolver->setDefaults([
            'allow_add' => null,
            'allow_delete' => null,
            'delete_empty' => null,
            'entry_options' => null,
            'entry_type' => null,
        ]);
    }

    public function getParent(): string
    {
        return TextareaType::class;
    }
}
