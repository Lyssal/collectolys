<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Entity\Element\Element;
use App\Entity\Genre;
use App\Entity\Keyword;
use App\Entity\Location;
use App\Entity\Type;
use App\Entity\Universe;
use App\Form\Type\File\ImageType;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

/**
 * The Element form.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ElementType extends AbstractType
{
    /**
     * The router.
     *
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    protected $entityAdministratorManager;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Routing\RouterInterface                    $router                     The router
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     */
    public function __construct(RouterInterface $router, EntityAdministratorManager $entityAdministratorManager)
    {
        $this->router = $router;
        $this->entityAdministratorManager = $entityAdministratorManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \App\Entity\Element\Element $element */
        $element = $builder->getData();
        $locations = $this->entityAdministratorManager->get(Location::class)->findAll();
        $genres = $this->entityAdministratorManager->get(Genre::class)->findBy(['type' => $element->getType()]);
        $universes = $this->entityAdministratorManager->get(Universe::class)->findAll();

        if (null !== $element->getId()) {
            $builder
                ->setAction($this->router->generate('element_element_edit', ['element' => $element->getId()]))
            ;
        } else {
            $builder
                ->setAction($this->router->generate('element_element_add', ['type' => $element->getType()->getId()]))
            ;
        }

        $builder
            ->add('type', null, [
                'label' => 'element_type',
                'choice_label' => 'elementName',
                'disabled' => true,
            ])
            ->add('names', CollectionType::class, [
                'label' => 'names',
                'entry_type' => ElementNameType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-collection-positionable' => '',
                    'data-grid' => 'true',
                ],
            ])
            ->add('description', null, [
                'label' => 'description',
            ])
            ->add('content', TextareaType::class, [
                'label' => 'content',
                'attr' => [
                    'data-ckeditor' => 'true',
                ],
            ])
            ->add('illustrationRecto', ImageType::class, [
                'label' => 'cover.recto',
                'required' => false,
                'path' => Element::IMAGES_PATH,
            ])
            ->add('illustrationVerso', ImageType::class, [
                'label' => 'cover.verso',
                'required' => false,
                'path' => Element::IMAGES_PATH,
            ])
            ->add('firstSubElements', CollectionType::class, [
                'label' => 'sub_elements',
                'entry_type' => SubElementType::class,
                'data' => $element->getFirstSubElements(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'property_path' => 'subElements',
                'block_prefix' => 'first_sub_elements',
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-collection-positionable' => '',
                    'data-grid' => 'true',
                ],
            ])
            ->add('orderedSubElements', null, [
                'label' => 'ordered_list',
                'required' => false,
            ])
            ->add('universes', null, [
                'label' => 'universes',
                'choices' => $universes,
            ])
            ->add('genres', null, [
                'label' => 'genres',
                'choices' => $genres,
            ])
            ->add('origins', null, [
                'label' => 'origins',
                'choices' => $locations,
            ])
            ->add('dates', CollectionType::class, [
                'label' => 'dates',
                'entry_type' => ElementDateType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-grid' => 'true',
                ],
            ])
            ->add('companies', CollectionType::class, [
                'label' => 'companies',
                'entry_type' => ElementCompanyType::class,
                'entry_options' => [
                    'type' => $element->getType(),
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-collection-positionable' => '',
                    'data-grid' => 'true',
                ],
            ])
            ->add('people', CollectionType::class, [
                'label' => 'people',
                'entry_type' => ElementPersonType::class,
                'entry_options' => [
                    'type' => $element->getType(),
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-collection-positionable' => '',
                    'data-grid' => 'true',
                ],
            ])
            ->add('prices', CollectionType::class, [
                'label' => 'prices',
                'entry_type' => ElementPriceType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-grid' => 'true',
                ],
            ])
            ->add('illustrations', CollectionType::class, [
                'label' => 'illustrations',
                'entry_type' => ElementIllustrationType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
                'attr' => [
                    'class' => 'symfony-collection',
                    'data-collection-positionable' => '',
                    'data-grid' => 'true',
                ],
            ])
        ;

        $this
            ->addSetFields($builder, $element, null !== $element->getElementSet() ? [$element->getElementSet()] : [])
            ->addParentsField($builder, $element, $element->getParents()->toArray())
            ->addChildrenField($builder, $element, $element->getChildren()->toArray())
            ->addReferencesField($builder, $element, $element->getReferences()->toArray())
            ->addPreviousElementField($builder, $element, null !== $element->getPreviousElement() ? [$element->getPreviousElement()] : [])
            ->addSimilarElementField($builder, $element, $element->getSimilarElements()->toArray())
            ->addKeywordsField($builder, $element, $element->getKeywords()->toArray())
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'manageAutocompleteFields'])
        ;
    }

    /**
     * Manage the autocomplete fields and the user choices after submission.
     *
     * @param \Symfony\Component\Form\FormEvent $event The form event
     */
    public function manageAutocompleteFields(FormEvent $event): void
    {
        $form = $event->getForm();
        /** @var \App\Entity\Element\Element $element */
        $element = $form->getData();
        /** @var array $submittedElement */
        $submittedElement = $event->getData();

        $this
            ->manageSetField($form, $element, $submittedElement)
            ->manageParentsField($form, $element, $submittedElement)
            ->manageChildrenField($form, $element, $submittedElement)
            ->manageReferencesField($form, $element, $submittedElement)
            ->managePreviousElementField($form, $element, $submittedElement)
            ->manageSimilarElementsField($form, $element, $submittedElement)
            ->manageKeywordsField($form, $element, $submittedElement)
        ;
    }

    /**
     * Add the set fields.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addSetFields($form, Element $element, array $choices): self
    {
        $form
            ->add('set', null, [
                'label' => 'set.is',
            ])
            ->add('setAggregated', null, [
                'label' => 'set.aggregated',
            ])
            ->add('setDisplayInElementTitle', null, [
                'label' => 'display_in_element_title',
            ])
            ->add('setElementName', null, [
                'label' => 'element_name',
            ])
        ;

        $this->addElementSetField($form, $element, $choices);

        $form
            ->add('setSpecialIssue', null, [
                'label' => 'special_issue',
            ])
            ->add('numberInSet', null, [
                'label' => 'number',
            ])
            ->add('positionInSet', null, [
                'label' => 'position',
            ])
        ;

        return $this;
    }

    /**
     * Add the element set field.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addElementSetField($form, Element $element, array $choices): self
    {
        $form
            ->add('elementSet', null, [
                'label' => 'set',
                'choices' => $choices,
                'attr' => [
                    'data-autocomplete-url' => $this->getAutocompleteUrl('element_set_autocomplete', $element->getType()),
                ],
            ])
        ;

        return $this;
    }

    /**
     * Add the parents field.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addParentsField($form, Element $element, array $choices): self
    {
        $form
            ->add('parents', null, [
                'label' => 'parents',
                'by_reference' => false,
                'choices' => $choices,
                'attr' => [
                    'data-autocomplete-url' => $this->getAutocompleteUrl('element_element_autocomplete', $element->getType(), $element->getId()),
                ],
            ])
        ;

        return $this;
    }

    /**
     * Add the children field.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addChildrenField($form, Element $element, array $choices): self
    {
        $form
            ->add('children', null, [
                'label' => 'children',
                'by_reference' => false,
                'choices' => $choices,
                'attr' => [
                    'data-autocomplete-url' => $this->getAutocompleteUrl('element_element_autocomplete', $element->getType(), $element->getId()),
                ],
            ])
        ;

        return $this;
    }

    /**
     * Add the keywords field.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addKeywordsField($form, Element $element, array $choices): self
    {
        $form
            ->add('keywords', null, [
                'label' => 'keywords',
                'by_reference' => false,
                'choices' => $choices,
                'attr' => [
                    'data-autocomplete-url' => $this->getAutocompleteUrl('keyword_autocomplete'),
                ],
            ])
        ;

        return $this;
    }

    /**
     * Add the references field.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addReferencesField($form, Element $element, array $choices): self
    {
        $form
            ->add('references', null, [
                'label' => 'references',
                'choices' => $choices,
                'attr' => [
                    'data-autocomplete-url' => $this->getAutocompleteUrl('element_element_autocomplete', null, $element->getId()),
                ],
            ])
        ;

        return $this;
    }

    /**
     * Add the previous element field.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addPreviousElementField($form, Element $element, array $choices): self
    {
        $form
            ->add('previousElement', null, [
                'label' => 'element.previous',
                'choices' => $choices,
                'attr' => [
                    'data-autocomplete-url' => $this->getAutocompleteUrl('element_element_autocomplete', $element->getType(), $element->getId()),
                ],
            ])
        ;

        return $this;
    }

    /**
     * Add the similar elements field.
     *
     * @param \Symfony\Component\Form\FormInterface|\Symfony\Component\Form\FormBuilderInterface $form    The form
     * @param \App\Entity\Element\Element                                                        $element The element
     * @param array                                                                              $choices The choices
     *
     * @return $this Self
     */
    private function addSimilarElementField($form, Element $element, array $choices): self
    {
        $form
            ->add('similarElements', null, [
                'label' => 'elements.similar',
                'choices' => $choices,
                'attr' => [
                    'data-autocomplete-url' => $this->getAutocompleteUrl('element_element_autocomplete', $element->getType(), $element->getId()),
                ],
            ])
        ;

        return $this;
    }

    /**
     * Get the autocomplete URL for EntityType.
     *
     * @param string           $route    The autocomplete route
     * @param \App\Entity\Type $type     The type
     * @param int|null         $exceptId The ID to except
     *
     * @return string The URL
     */
    private function getAutocompleteUrl(string $route, ?Type $type = null, ?int $exceptId = null): string
    {
        $routeParameters = [];

        if (null !== $type) {
            $routeParameters['type'] = $type->getId();
        }

        if (null !== $exceptId) {
            $routeParameters['except'] = $exceptId;
        }

        return $this->router->generate($route, $routeParameters);
    }

    /**
     * Manage the parents field after submission.
     *
     * @param \Symfony\Component\Form\FormInterface $form             The form
     * @param \App\Entity\Element\Element           $element          The element
     * @param array                                 $submittedElement The submitted element
     *
     * @return \App\Form\Type\Element\ElementType Self
     */
    private function manageSetField(FormInterface $form, Element $element, array $submittedElement): self
    {
        $sets = [];

        if (\array_key_exists('elementSet', $submittedElement)) {
            $setId = $submittedElement['elementSet'];
            $set = $this->entityAdministratorManager->get(Element::class)->findOneById($setId);

            if (null !== $set) {
                $sets = [$set];
            }
        }

        return $this->addElementSetField($form, $element, $sets);
    }

    /**
     * Manage the parents field after submission.
     *
     * @param \Symfony\Component\Form\FormInterface $form             The form
     * @param \App\Entity\Element\Element           $element          The element
     * @param array                                 $submittedElement The submitted element
     *
     * @return \App\Form\Type\Element\ElementType Self
     */
    private function manageParentsField(FormInterface $form, Element $element, array $submittedElement): self
    {
        if (\array_key_exists('parents', $submittedElement)) {
            $parentIds = $submittedElement['parents'];
            $parents = $this->entityAdministratorManager->get(Element::class)->findBy(['id' => $parentIds]);
        } else {
            $parents = [];
        }

        return $this->addParentsField($form, $element, $parents);
    }

    /**
     * Manage the children field after submission.
     *
     * @param \Symfony\Component\Form\FormInterface $form             The form
     * @param \App\Entity\Element\Element           $element          The element
     * @param array                                 $submittedElement The submitted element
     *
     * @return \App\Form\Type\Element\ElementType Self
     */
    private function manageChildrenField(FormInterface $form, Element $element, array $submittedElement): self
    {
        if (\array_key_exists('children', $submittedElement)) {
            $childrenIds = $submittedElement['children'];
            $children = $this->entityAdministratorManager->get(Element::class)->findBy(['id' => $childrenIds]);
        } else {
            $children = [];
        }

        return $this->addChildrenField($form, $element, $children);
    }

    /**
     * Manage the references field after submission.
     *
     * @param \Symfony\Component\Form\FormInterface $form             The form
     * @param \App\Entity\Element\Element           $element          The element
     * @param array                                 $submittedElement The submitted element
     *
     * @return \App\Form\Type\Element\ElementType Self
     */
    private function manageReferencesField(FormInterface $form, Element $element, array $submittedElement): self
    {
        if (\array_key_exists('references', $submittedElement)) {
            $referenceIds = $submittedElement['references'];
            $references = $this->entityAdministratorManager->get(Element::class)->findBy(['id' => $referenceIds]);

            $this->addReferencesField($form, $element, $references);
        }

        return $this;
    }

    /**
     * Manage the previous element field after submission.
     *
     * @param \Symfony\Component\Form\FormInterface $form             The form
     * @param \App\Entity\Element\Element           $element          The element
     * @param array                                 $submittedElement The submitted element
     *
     * @return \App\Form\Type\Element\ElementType Self
     */
    private function managePreviousElementField(FormInterface $form, Element $element, array $submittedElement): self
    {
        if (\array_key_exists('previousElement', $submittedElement)) {
            $previousElementId = $submittedElement['previousElement'];
            $previousElement = $this->entityAdministratorManager->get(Element::class)->findBy(['id' => $previousElementId]);

            if (null === $previousElement) {
                return $this;
            }

            $this->addPreviousElementField($form, $element, [$previousElement]);
        }

        return $this;
    }

    /**
     * Manage the similar elements field after submission.
     *
     * @param \Symfony\Component\Form\FormInterface $form             The form
     * @param \App\Entity\Element\Element           $element          The element
     * @param array                                 $submittedElement The submitted element
     *
     * @return \App\Form\Type\Element\ElementType Self
     */
    private function manageSimilarElementsField(FormInterface $form, Element $element, array $submittedElement): self
    {
        if (\array_key_exists('similarElements', $submittedElement)) {
            $similarIds = $submittedElement['similarElements'];
            $similars = $this->entityAdministratorManager->get(Element::class)->findBy(['id' => $similarIds]);
        } else {
            $similars = [];
        }

        return $this->addSimilarElementField($form, $element, $similars);
    }

    /**
     * Manage the keywords field after submission.
     *
     * @param \Symfony\Component\Form\FormInterface $form             The form
     * @param \App\Entity\Element\Element           $element          The element
     * @param array                                 $submittedElement The submitted element
     *
     * @return \App\Form\Type\Element\ElementType Self
     */
    private function manageKeywordsField(FormInterface $form, Element $element, array $submittedElement): self
    {
        if (\array_key_exists('keywords', $submittedElement)) {
            $keywordsIds = $submittedElement['keywords'];
            $keywords = $this->entityAdministratorManager->get(Keyword::class)->findBy(['id' => $keywordsIds]);
        } else {
            $keywords = [];
        }

        return $this->addKeywordsField($form, $element, $keywords);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Element::class,
            ])
        ;
    }
}
