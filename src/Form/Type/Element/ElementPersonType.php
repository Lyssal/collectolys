<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Doctrine\Manager\Person\PersonManager;
use App\Doctrine\Manager\Person\PersonRoleManager;
use App\Entity\Element\ElementPerson;
use App\Entity\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The ElementPerson subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementPersonType extends AbstractType
{
    /**
     * The person manager.
     *
     * @var \App\Doctrine\Manager\Person\PersonManager
     */
    private $personManager;

    /**
     * The person rolenmanager.
     *
     * @var \App\Doctrine\Manager\Person\PersonRoleManager
     */
    private $personRoleManager;

    /**
     * Constructor.
     *
     * @param \App\Doctrine\Manager\Person\PersonManager     $personManager     The person manager
     * @param \App\Doctrine\Manager\Person\PersonRoleManager $personRoleManager The person role manager
     */
    public function __construct(PersonManager $personManager, PersonRoleManager $personRoleManager)
    {
        $this->personManager = $personManager;
        $this->personRoleManager = $personRoleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \App\Entity\Type $type */
        $type = $options['type'];
        $roles = $this->personRoleManager->findBy(['type' => $type]);

        $builder
            ->add('role', null, [
                'label' => 'role',
                'choices' => $roles,
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'addPersonField'])
            ->add('details', null, [
                'label' => 'details',
            ])
            ->add(
                'highlighted',
                null,
                [
                    'label' => 'highlighted',
                ]
            )
            ->add('position', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'collection-position',
                ],
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, [$this, 'setPerson'])
        ;
    }

    /**
     * Add the person fields.
     *
     * @param \Symfony\Component\Form\FormEvent $event The form event
     */
    public function addPersonField(FormEvent $event)
    {
        $form = $event->getForm();
        /** @var \App\Entity\Element\ElementPerson|null $elementPerson */
        $elementPerson = $event->getData();

        $form
            ->add('firstName', null, [
                'label' => 'first_name',
                'mapped' => false,
                'data' => null !== $elementPerson ? $elementPerson->getFirstName() : null,
            ])
            ->add('lastName', null, [
                'label' => 'last_name',
                'mapped' => false,
                'data' => null !== $elementPerson ? $elementPerson->getLastName() : null,
            ])
        ;
    }

    /**
     * Set the person.
     *
     * @param \Symfony\Component\Form\FormEvent $event The form event
     */
    public function setPerson(FormEvent $event)
    {
        $form = $event->getForm();
        /** @var \App\Entity\Element\ElementPerson $elementPerson */
        $elementPerson = $event->getData();

        $firstName = $form->get('firstName')->getData();
        $lastName = $form->get('lastName')->getData();

        if (null !== $firstName) {
            $person = $this->personManager
                ->getOrCreateByFirstNameAndLastName($firstName, $lastName);

            $elementPerson->setPerson($person);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'element_person';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('type')
            ->setAllowedTypes('type', Type::class)
            ->setDefaults([
                'data_class' => ElementPerson::class,
                'error_bubbling' => false,
                'error_mapping' => [
                    'person' => 'firstName',
                ],
            ])
        ;
    }
}
