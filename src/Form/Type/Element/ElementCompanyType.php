<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Entity\Company\Company;
use App\Entity\Company\CompanyRole;
use App\Entity\Element\ElementCompany;
use App\Entity\Type;
use Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

/**
 * The ElementCompany subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementCompanyType extends AbstractType
{
    /**
     * The router.
     *
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * The entity administrator manager.
     *
     * @var \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager
     */
    private $entityAdministratorManager;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Routing\RouterInterface                    $router                     The router
     * @param \Lyssal\Doctrine\OrmBundle\Manager\EntityAdministratorManager $entityAdministratorManager The entity administrator manager
     */
    public function __construct(RouterInterface $router, EntityAdministratorManager $entityAdministratorManager)
    {
        $this->router = $router;
        $this->entityAdministratorManager = $entityAdministratorManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \App\Entity\Type $type */
        $type = $options['type'];
        $roles = $this->entityAdministratorManager->get(CompanyRole::class)->findBy(['type' => $type]);

        $builder
            ->add('role', null, [
                'label' => 'role',
                'choices' => $roles,
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'addCompanyField'])
            ->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'addCompanyField'])
            ->add(
                'precisions',
                null,
                [
                    'label' => 'precisions',
                ]
            )
            ->add(
                'highlighted',
                null,
                [
                    'label' => 'highlighted',
                ]
            )
            ->add('position', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'collection-position',
                ],
            ])
        ;
    }

    /**
     * Add the company field.
     *
     * @param \Symfony\Component\Form\FormEvent $event The form event
     *
     * @throws \Doctrine\ORM\Mapping\MappingException
     */
    public function addCompanyField(FormEvent $event)
    {
        $form = $event->getForm();
        $elementCompany = $event->getData();
        $company = null;

        if (\is_array($elementCompany)) { // Pre submit
            if (isset($elementCompany['company'])) {
                $companyId = $elementCompany['company'];
                $company = $this->entityAdministratorManager->get(Company::class)->findOneById($companyId);
            }
        } elseif (null !== $elementCompany) { // Pre set data
            $company = $elementCompany->getCompany();
        }
        $companies = null !== $company ? [$company] : [];

        $form
            ->add('company', null, [
                'label' => 'company',
                'choices' => $companies,
                'attr' => [
                    'data-autocomplete-url' => $this->router->generate('company_company_autocomplete'),
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'element_company';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('type')
            ->setAllowedTypes('type', Type::class)
            ->setDefaults([
                'data_class' => ElementCompany::class,
                'error_bubbling' => false,
            ])
        ;
    }
}
