<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Doctrine\Manager\LanguageManager;
use App\Entity\Element\ElementName;
use App\Entity\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The ElementName subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementNameType extends AbstractType
{
    /**
     * The Language manager.
     *
     * @var \App\Doctrine\Manager\LanguageManager
     */
    private $languageManager;

    /**
     * Constructor.
     *
     * @param \App\Doctrine\Manager\LanguageManager $languageManager The Language manager
     */
    public function __construct(LanguageManager $languageManager)
    {
        $this->languageManager = $languageManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
                'required' => true,
            ])
            ->add('subname', null, [
                'label' => 'subname',
                'required' => false,
            ])
            ->add('language', null, [
                'label' => 'language',
                'choices' => $this->languageManager->findAll(),
                'preferred_choices' => [$this->languageManager->findOneById(Language::DEFAULT)],
            ])
            ->add('precisions', null, [
                'label' => 'precisions',
            ])
            ->add('position', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'collection-position',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'element_name';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ElementName::class,
            ])
        ;
    }
}
