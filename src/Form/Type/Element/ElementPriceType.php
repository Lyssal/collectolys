<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Doctrine\Manager\CurrencyManager;
use App\Doctrine\Manager\LocationManager;
use App\Entity\Element\ElementPrice;
use App\Security\UserInformations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The ElementPrice subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementPriceType extends AbstractType
{
    /**
     * @var \App\Security\UserInformations The user informations
     */
    private $userInformations;

    /**
     * The location manager.
     *
     * @var \App\Doctrine\Manager\LocationManager
     */
    private $locationManager;

    /**
     * The currency manager.
     *
     * @var \App\Doctrine\Manager\CurrencyManager
     */
    private $currencyManager;

    /**
     * Constructor.
     *
     * @param \App\Security\UserInformations        $userInformations The user informations
     * @param \App\Doctrine\Manager\LocationManager $locationManager  The location manager
     * @param \App\Doctrine\Manager\CurrencyManager $currencyManager  The currency manager
     */
    public function __construct(UserInformations $userInformations, LocationManager $locationManager, CurrencyManager $currencyManager)
    {
        $this->userInformations = $userInformations;
        $this->locationManager = $locationManager;
        $this->currencyManager = $currencyManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locations = $this->locationManager->findAll();
        $currencies = $this->currencyManager->findAll();

        $builder
            ->add('location', null, [
                'label' => 'location',
                'choices' => $locations,
                'preferred_choices' => [$this->userInformations->getLocation()],
            ])
            ->add('price', null, [
                'label' => 'price',
            ])
            ->add('currency', null, [
                'label' => 'currency',
                'choices' => $currencies,
                'preferred_choices' => [$this->userInformations->getCurrency()],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'element_price';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ElementPrice::class,
                'error_bubbling' => false,
            ])
        ;
    }
}
