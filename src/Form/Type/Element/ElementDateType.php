<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Doctrine\Manager\LocationManager;
use App\Entity\Element\ElementDate;
use App\Security\UserInformations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The ElementDate subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementDateType extends AbstractType
{
    /**
     * @var \App\Security\UserInformations The user informations
     */
    private $userInformations;

    /**
     * The location manager.
     *
     * @var \App\Doctrine\Manager\LocationManager
     */
    private $locationManager;

    /**
     * Constructor.
     *
     * @param \App\Security\UserInformations        $userInformations The user informations
     * @param \App\Doctrine\Manager\LocationManager $locationManager  The location manager
     */
    public function __construct(UserInformations $userInformations, LocationManager $locationManager)
    {
        $this->userInformations = $userInformations;
        $this->locationManager = $locationManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $locations = $this->locationManager->findAll();

        $builder
            ->add('location', null, [
                'label' => 'location',
                'choices' => $locations,
                'preferred_choices' => [$this->userInformations->getLocation()],
            ])
            ->add('date', null, [
                'label' => 'date',
                'widget' => 'single_text',
                'attr' => [
                    'data-element-date-date' => '',
                ],
            ])
            ->add('year', null, [
                'label' => 'year',
                'required' => true,
            ])
            ->add(
                'precisions',
                null,
                [
                    'label' => 'precisions',
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'element_date';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => ElementDate::class,
                'error_bubbling' => false,
            ])
        ;
    }
}
