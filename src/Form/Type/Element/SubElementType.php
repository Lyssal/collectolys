<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Entity\Element\SubElement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The sub-elements subform.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class SubElementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])
            ->add('position', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'collection-position',
                ],
            ])
            ->add('orderedChildren', null, [
                'label' => 'ordered_list',
                'required' => false,
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $subElement = $event->getData();

                if (null === $subElement) {
                    $event->setData(new SubElement());
                }

                $this->manageChildrenField($event->getForm(), $subElement);
            })
        ;
    }

    /**
     * Add the children field if needed.
     *
     * @param \Symfony\Component\Form\FormInterface $form       The sub-element form
     * @param \App\Entity\Element\SubElement|null   $subElement The sub-element entity
     */
    private function manageChildrenField(FormInterface $form, ?SubElement $subElement): void
    {
        $form
            ->add('children', SubElementChildrenType::class, [
                'label' => false,
                'attr' => [
                    'rows' => null !== $subElement && $subElement->getChildren()->count() > 0 ? $subElement->getChildren()->count() : 5,
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'element_subelement';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => SubElement::class,
                'error_bubbling' => false,
            ])
        ;
    }
}
