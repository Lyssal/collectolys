<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element;

use App\Form\DataTransformer\Element\SubElementsToTextareaTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * The sub-element children textarea.
 */
final class SubElementChildrenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new SubElementsToTextareaTransformer())
        ;
    }

    public function getParent()
    {
        return TextareaType::class;
    }
}
