<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element\Type;

use App\Entity\Element\Type\Book;
use App\Form\Type\Element\ElementType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The book element form.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class BookType extends ElementType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('pageCount', null, [
                'label' => 'page_count',
            ])
            ->add('isbn', null, [
                'label' => 'ISBN',
                'attr' => ['maxlength' => '17'],
            ])
            ->add('minAge', null, [
                'label' => 'age.from',
                'block_prefix' => 'age',
            ])
            ->add('maxAge', null, [
                'label' => 'age.to',
                'block_prefix' => 'age',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Book::class,
            ])
        ;
    }
}
