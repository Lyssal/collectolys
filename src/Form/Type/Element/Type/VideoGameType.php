<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element\Type;

use App\Entity\Element\Type\VideoGame;
use App\Entity\Platform;
use App\Enum\MultiplayerEnum;
use App\Form\Type\Element\ElementType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The video game element form.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class VideoGameType extends ElementType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $platforms = $this->entityAdministratorManager->get(Platform::class)->findAll();
        parent::buildForm($builder, $options);

        $builder
            ->add('platforms', null, [
                'label' => 'platforms',
                'choices' => $platforms,
            ])
            ->add('cheating', TextareaType::class, [
                'label' => 'cheating',
                'attr' => [
                    'data-ckeditor' => 'true',
                ],
            ])
            ->add('multiplayer', ChoiceType::class, [
                'label' => 'multiplayer',
                'required' => false,
                'choices' => array_flip(MultiplayerEnum::VALUES),
                'multiple' => true,
            ])
            ->add('maxPlayerCount', null, [
                'label' => 'max_player_count',
            ])
            ->add('minAge', null, [
                'label' => 'age.from',
                'block_prefix' => 'age',
            ])
            ->add('maxAge', null, [
                'label' => 'age.to',
                'block_prefix' => 'age',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => VideoGame::class,
            ])
        ;
    }
}
