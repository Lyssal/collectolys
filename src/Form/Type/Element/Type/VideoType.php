<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element\Type;

use App\Entity\Element\Type\Video;
use App\Enum\ColorEnum;
use App\Enum\VideoTypeEnum;
use App\Form\Type\Element\ElementType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The video element form.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class VideoType extends ElementType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('videoType', ChoiceType::class, [
                'label' => 'video.type',
                'choices' => array_flip(VideoTypeEnum::VALUES),
            ])
            ->add('duration', null, [
                'label' => 'duration',
                'widget' => 'single_text',
            ])
            ->add('color', ChoiceType::class, [
                'label' => 'color',
                'required' => false,
                'choices' => array_flip(ColorEnum::VALUES),
            ])
            ->add('minAge', null, [
                'label' => 'age.from',
                'block_prefix' => 'age',
            ])
            ->add('maxAge', null, [
                'label' => 'age.to',
                'block_prefix' => 'age',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Video::class,
            ])
        ;
    }
}
