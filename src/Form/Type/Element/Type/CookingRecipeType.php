<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\Element\Type;

use App\Entity\Element\Type\CookingRecipe;
use App\Form\Type\Element\ElementType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The cooking recipe form.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class CookingRecipeType extends ElementType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('preparationTime', null, [
                'label' => 'preparation_time',
                'widget' => 'single_text',
            ])
            ->add('personCount', null, [
                'label' => 'person_count',
            ])
            ->add('ingredients', null, [
                'label' => 'ingredients',
                'attr' => [
                    'rows' => '10',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => CookingRecipe::class,
            ])
        ;
    }
}
