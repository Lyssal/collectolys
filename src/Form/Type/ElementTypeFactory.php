<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type;

use App\Entity\Element\Element;
use App\Entity\Element\Type\Audio;
use App\Entity\Element\Type\Book;
use App\Entity\Element\Type\CookingRecipe;
use App\Entity\Element\Type\Periodical;
use App\Entity\Element\Type\Software;
use App\Entity\Element\Type\Video;
use App\Entity\Element\Type\VideoGame;
use App\Form\Type\Element\ElementType;
use App\Form\Type\Element\Type\AudioType;
use App\Form\Type\Element\Type\BookType;
use App\Form\Type\Element\Type\CookingRecipeType;
use App\Form\Type\Element\Type\PeriodicalType;
use App\Form\Type\Element\Type\SoftwareType;
use App\Form\Type\Element\Type\VideoGameType;
use App\Form\Type\Element\Type\VideoType;

/**
 * This class permits to get the form type for an Element.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
final class ElementTypeFactory
{
    /**
     * Get the form class.
     *
     * @param \App\Entity\Element\Element $element The element
     *
     * @return string The form class
     */
    public static function getTypeClass(Element $element): string
    {
        if ($element instanceof Audio) {
            return AudioType::class;
        }

        if ($element instanceof Video) {
            return VideoType::class;
        }

        if ($element instanceof Software) {
            return SoftwareType::class;
        }

        if ($element instanceof VideoGame) {
            return VideoGameType::class;
        }

        if ($element instanceof Book) {
            return BookType::class;
        }

        if ($element instanceof Periodical) {
            return PeriodicalType::class;
        }

        if ($element instanceof CookingRecipe) {
            return CookingRecipeType::class;
        }

        return ElementType::class;
    }
}
