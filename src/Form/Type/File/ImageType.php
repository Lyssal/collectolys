<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\File;

use App\Entity\File\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The Image form.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class ImageType extends FileType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver
            ->setDefaults([
                'data_class' => Image::class,
            ])
        ;
    }
}
