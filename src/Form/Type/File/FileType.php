<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Type\File;

use App\Entity\File\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType as SymfonyFileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * The File form.
 *
 * @category Form
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
abstract class FileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityClass = $options['data_class'];
        $path = (string) $options['path'];

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($entityClass, $path) {
                $this->initFile($event, $entityClass, $path);

                /** @var \App\Entity\File\File $file */
                $file = $event->getData();
                /** @var \Symfony\Component\Form\FormInterface $form */
                $form = $event->getForm();

                $this->addFileField($form, $file);
            })
        ;
    }

    /**
     * Create the empty file.
     *
     * @param \Symfony\Component\Form\FormEvent $event       The form event
     * @param string                            $entityClass The entity class
     * @param string                            $path        The file path
     */
    private function initFile(FormEvent $event, string $entityClass, string $path): void
    {
        /** @var \App\Entity\File\File $file */
        $file = $event->getData();

        if (null === $file) {
            $file = new $entityClass();
            $file->setPath($path);
            $event->setData($file);
        }
    }

    /**
     * Add the file field.
     *
     * @param \Symfony\Component\Form\FormInterface $form The form
     * @param \App\Entity\File\File                 $file The file
     */
    private function addFileField(FormInterface $form, File $file): void
    {
        $required = $form->isRequired() && null === $file->getFilename();

        $form
            ->add('uploadedFile', SymfonyFileType::class, [
                'label' => false,
                'required' => $required,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('path')
        ;
    }
}
