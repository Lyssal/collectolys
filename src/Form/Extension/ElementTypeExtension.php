<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\Extension;

use App\Entity\Element\Element;
use Lyssal\EntityBundle\Appellation\AppellationManager;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Use the Appellation for Element choices.
 */
final class ElementTypeExtension extends AbstractTypeExtension
{
    /**
     * @var \Lyssal\EntityBundle\Appellation\AppellationManager
     */
    private $appellationManager;

    public function __construct(AppellationManager $appellationManager)
    {
        $this->appellationManager = $appellationManager;
    }

    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        if (isset($options['class']) && Element::class === $options['class']) {
            /**
             * @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice
             */
            foreach ($view->vars['choices'] as $choice) {
                $choice->label = $this->appellationManager->appellation($choice->data);
            }
        }
    }

    public static function getExtendedTypes(): iterable
    {
        return [ChoiceType::class];
    }
}
