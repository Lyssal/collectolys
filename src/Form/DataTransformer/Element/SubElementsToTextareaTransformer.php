<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\DataTransformer\Element;

use App\Entity\Element\SubElement;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Transform the sub-element collection into a simple textarea.
 */
class SubElementsToTextareaTransformer implements DataTransformerInterface
{
    /**
     * @var \Doctrine\Common\Collections\Collection|\App\Entity\Element\SubElement[]
     */
    private $subElements;

    public function transform($subElements): ?string
    {
        if (!is_iterable($subElements)) {
            return null;
        }

        $this->subElements = $subElements;
        $subElementsText = '';
        /** @var \App\Entity\Element\SubElement $subElement */
        foreach ($subElements as $subElement) {
            $subElementsText .= $subElement->getName()."\n";
        }

        return trim($subElementsText);
    }

    public function reverseTransform($subElementsText): Collection
    {
        if (empty($subElementsText)) {
            return new ArrayCollection();
        }

        $subElementNames = explode("\n", trim($subElementsText));
        $subElements = new ArrayCollection();

        foreach ($subElementNames as $i => $subElementName) {
            $modifyExistingSubElement = $this->subElements->containsKey($i);
            // We reuse existing sub-elements to not remove and create same elements just after
            $subElement = $modifyExistingSubElement ? $this->subElements->get($i) : new SubElement();

            $subElement->setName(trim($subElementName));
            $subElement->setPosition($i);

            $subElements->add($subElement);
        }

        return $subElements;
    }
}
