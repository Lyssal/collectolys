<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * To display simple values of an array in a textarea.
 */
final class SimpleArrayToTextTransformer implements DataTransformerInterface
{
    /**
     * @param array $json
     *
     * @return string
     */
    public function transform($json)
    {
        if (empty($json)) {
            return '';
        }

        return implode("\n", $json);
    }

    /**
     * @param string $json
     *
     * @return array
     */
    public function reverseTransform($json)
    {
        if (empty($json)) {
            return [];
        }

        return array_map(
            function (string $id) {
                return (int) $id;
            },
            explode("\n", $json),
        );
    }
}
