<?php

/*
 * This file is part of a Lyssal project.
 *
 * @copyright Rémi Leclerc
 */

namespace App\EventSubscriber;

use App\Element\Sorter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event to manager the sort of the element lists.
 *
 * @category Event
 *
 * @author    Rémi Leclerc
 * @copyright 2019 Rémi Leclerc
 * @license   MIT https://opensource.org/licenses/MIT
 *
 * @see https://gitlab.com/Lyssal/collectolys
 */
class SortSubscriber implements EventSubscriberInterface
{
    /**
     * @var \App\Element\Sorter The sorter service
     */
    private $sorter;

    /**
     * Constructor.
     *
     * @param \App\Element\Sorter $sorter The sorter service
     */
    public function __construct(Sorter $sorter)
    {
        $this->sorter = $sorter;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['processSort'],
            ],
        ];
    }

    /**
     * Process the chosen sort.
     *
     * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event The event
     */
    public function processSort(RequestEvent $event): void
    {
        $this->sorter->setByRequest($event->getRequest());
    }
}
