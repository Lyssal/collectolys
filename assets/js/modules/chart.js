import 'chart.js/dist/Chart.min.css';
const Chart = require('chart.js/dist/Chart.min');

/**
 * Search and initialize the charts.
 *
 * @param {HTMLElement} container The HTML container
 */
function init(container) {
    const charts = container.querySelectorAll('[data-chart-config]');

    Object.values(charts).forEach((chart) => {
        const config = JSON.parse(chart.dataset.chartConfig);

        new Chart(chart, config);
    });
}

init(document);

export { init };
