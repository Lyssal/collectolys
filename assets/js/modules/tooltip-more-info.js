const $ = require('jquery');

$('body').on('show.zf.tooltip', '[data-tooltip]', function(e) {
    const link = e.currentTarget;

    if (link.dataset.tooltipUrl) {
        fetch(link.dataset.tooltipUrl)
            .then((response) => response.text())
            .then((html) => {
                const tooltipTarget = document.getElementById(link.getAttribute("aria-describedby"));

                tooltipTarget.innerHTML = html;
                $(link).foundation('show');
            })
        ;

        delete link.dataset.tooltipUrl;
    }
});
