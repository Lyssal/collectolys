require('jvectormap-next/jquery-jvectormap.css');

require('jquery-mousewheel');
const $ = require('jquery');
require('jvectormap-next')($);
$.fn.vectorMap('addMap', 'world_mill', require('jvectormap-content/world-mill'));

function init(container) {
  const mappemonde = container.querySelector("[data-preview=\"jvectormap-next\"]");

  if (mappemonde === null) {
    return;
  }

  const $mappemonde = $(mappemonde),
    regions = JSON.parse(mappemonde.dataset.regions),
    darkColor = mappemonde.dataset.color;
  let vectormapRegions = {};

  for (const [code, location] of Object.entries(regions)) {
    vectormapRegions[code.toUpperCase()] = location['count'];
  }

  $mappemonde.vectorMap({
    map: 'world_mill',
    showTooltip: true,
    zoomOnScroll: false,
    series: {
      regions: [{
        values: vectormapRegions,
        scale: ['#ffffff', darkColor],
      }]
    },
    backgroundColor: 'transparent',
    onRegionTipShow: function(e, el, code){
      $(el).css('display', "block");
      if (typeof vectormapRegions[code] !== "undefined") {
        el.html(regions[code.toLowerCase()]['name']+' ('+vectormapRegions[code]+')');
      }
    }
  });
}

init(document);

export { init };
