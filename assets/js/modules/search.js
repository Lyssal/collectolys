const advancedSearchContainer = document.getElementById('advanced-search'),
    advancedSearchButton = document.getElementById('advanced-search-open'),
    advancedSearchCheckboxes = advancedSearchContainer.querySelectorAll('input[type="checkbox"], input[type="radio"]'),
    submitButton = document.getElementById('search-valid');

// Click to display the advanced search window
advancedSearchButton.addEventListener('click', function () {
    advancedSearchContainer.classList.toggle('open');
});

// Init the displying of the chosen options
Object.values(advancedSearchCheckboxes).forEach(function (checkbox) {
    checkbox.addEventListener('change', function () {
        processCheckboxDesign();
    });
});

// When we submit the search we close the advanced search window
submitButton.addEventListener('click', function () {
    if (advancedSearchContainer.classList.contains('open')) {
        advancedSearchContainer.classList.remove('open');
    }
});

/**
 * Function which manage the displaying of the chosen options.
 */
function processCheckboxDesign() {
    Object.values(advancedSearchCheckboxes).forEach(function (checkbox) {
        const label = checkbox.closest('label');

        if (checkbox.checked && !label.classList.contains('chosen')) {
            label.classList.add('chosen');
        } else if (!checkbox.checked && label.classList.contains('chosen')) {
            label.classList.remove('chosen');
        }
    });
}

processCheckboxDesign();
