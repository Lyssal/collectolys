function init (container) {
    const togglers = container.querySelectorAll('[data-toggler]');

    togglers.forEach(function (toggler) {
        const targetSelector = toggler.dataset.toggler;

        toggler.addEventListener('click', function () {
            const targets = document.querySelectorAll(targetSelector);

            targets.forEach(function (target) {
                const display = target.style.display;

                target.style.display = display !== 'none' ? 'none' : 'block';
            });
        });
    });
}

init(document);

export { init };
