import '@lyssal/ajax-page-loader/lib/ajax-page-loader.css';
// import AjaxPageLoader from '@lyssal/ajax-page-loader/lib/ajax-page-loader.amd';

import AjaxPageLoader from '@lyssal/ajax-page-loader/src/js/ajax-page-loader';
require('foundation-sites');

const $ = require('jquery');
const elementForm = require('../pages/element-form');
const select2 = require('../../modules/select2/select2');
const symfonyCollection = require('./symfony-collection');
const lightbox = require('../../modules/lightbox/lightbox');
const editor = require('../../modules/editor/editor');
const toggler = require('../modules/toggler');
const chart = require('./chart');
const mappemonde = require('./mappemonde');

let ajaxPageLoader = new AjaxPageLoader('a[href]:not([data-ajax="false"]):not([target="_blank"]), button[type="submit"]:not([data-ajax="false"])'),
    currentTabId = null;

ajaxPageLoader.setDefaultTarget('#page');

ajaxPageLoader.setBeforeAjaxLoadingEvent((ajaxLink) => {
    saveCurrentTab();
});

ajaxPageLoader.setAfterAjaxLoadingEvent((ajaxLink) => {
    openCurrentTab();

    const newPage = ajaxLink.getAttribute('data-ajax-new-page', 'true');

    if (newPage === 'true') {
        // Change the browser tab title
        const url = ajaxLink.getUrl();
        global.history.pushState({}, null, url);

        // Update the tab title
        const titleElement = document.querySelector('h1');
        document.title = (null !== titleElement ? titleElement.textContent + ' - ' : '') + 'CollectoLys';
    }
});

ajaxPageLoader.setBeforeContentSettingEvent((ajaxLink) => {
    if (!ajaxLink.element.classList.contains('element-menu__icon')) {
        // Go to the top of the page
        global.window.scrollTo(0, 0);
    }
});

ajaxPageLoader.setAfterContentSettingEvent((ajaxLink) => {
    const target = ajaxLink.getTargetElement();

    // Called before foundation because foundation move the modal with illustrations out of the target element
    lightbox.init(target);
    $(target).foundation();
    elementForm.setYearWhenDateChange(target);
    select2.initSelects2(target);
    symfonyCollection.init(target);
    editor.init(target);
    toggler.init(target);
    chart.init(target);
    mappemonde.init(target);

    if (target.querySelector('[data-type]') !== null) {
        document.querySelector('body').dataset.type = target.querySelector('[data-type]').dataset.type;
    }
});

// As we dynamically change the URL, we refresh the page when the user use back / forward buttons
window.addEventListener('popstate', function(event) {
    window.location.href = window.location.pathname;
}, false);

function saveCurrentTab() {
    const currentTab = document.querySelector('.tabs-panel.is-active');

    if (currentTab !== null) {
        currentTabId = currentTab.id;
    } else {
        currentTabId = null;
    }
}

function openCurrentTab() {
    if (currentTabId !== null) {
        const currentTab = document.getElementById(currentTabId);

        if (currentTab !== null) {
            $('#element-tabs').foundation('selectTab', $(currentTab));
        }
    }
}
