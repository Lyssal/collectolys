require('jquery-ui/ui/widgets/sortable');
require('symfony-collection');

const $ = require('jquery');
const elementForm = require('../pages/element-form');
const select2 = require('../../modules/select2/select2');

/**
 * Search and initialize the collections.
 *
 * @param {HTMLElement} container The HTML container
 */
function init(container) {
  const collections = container.querySelectorAll('.symfony-collection');

  Object.values(collections).forEach((collection) => {
    initCollection(collection);
  });
}

/**
 * Init the collection.
 *
 * @param {HTMLElement} collection The collection
 */
function initCollection(collection)  {
  const isGrid = processGrid(collection),
    isPositionable = collection.hasAttribute('data-collection-positionable');

  $(collection).collection({
    add: (isGrid ? '<div class="cell small-2">' : '') + '<button type="button" class="button success"><em class="mdi mdi-plus"></em></button>' + (isGrid ? '</div>' : ''),
    remove: '',
    position_field_selector: '.collection-position',
    allow_up: isPositionable,
    allow_down: isPositionable,
    up: '',
    down: '',
    add_at_the_end: true,
    after_add: function(collection, element) {
      elementForm.setYearWhenDateChange(element.get(0));
      select2.initSelects2(element);
      init(collection.get(0));
    },
  });
}

/**
 * Add the grid classes to the collection.
 *
 * @param {HTMLElement} collection The collection
 * @returns {boolean} If it is a grid
 */
function processGrid(collection) {
  const isGrid = typeof collection.dataset.grid !== 'undefined' && collection.dataset.grid === 'true';

  if (isGrid) {
    collection.classList.add('grid-x', 'grid-padding-x');
  }

  return isGrid;
}

init(document);

export { init };
