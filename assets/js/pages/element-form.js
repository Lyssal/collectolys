/**
 * When a user set a date, the year is automatically set.
 */
function setYearWhenDateChange(container) {
    const elementDateFields = container.querySelectorAll('[data-element-date-date]');

    Object.values(elementDateFields).forEach(function (dateField) {
        dateField.addEventListener('change', function () {
            const date = dateField.value;

            if ('' !== date) {
                const yearFieldId = dateField.id.substr(0, dateField.id.length - 4) + 'year',
                    year = date.substr(0, 4);

                document.getElementById(yearFieldId).value = year;
            }
        });
    });
}

setYearWhenDateChange(document);

export { setYearWhenDateChange };
