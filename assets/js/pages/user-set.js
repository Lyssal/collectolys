if (document.getElementById("user-sets") !== null) {
  const $ = require('jquery');

  $("body").on("click", "[data-user-set-target]", (e) => {
    displayUserSet(e.currentTarget.dataset.userSetTarget);
  });
}

function displayUserSet(userSetId) {
  document.getElementById("user-sets").querySelectorAll('[data-user-set]').forEach(function (userSetElement) {
    userSetElement.style.display = userSetElement.dataset.userSet !== userSetId ? 'none' : 'block';
  });
}
