require('@mdi/font/scss/materialdesignicons.scss');

require('../modules/foundation/foundation');
require('../modules/lightbox/lightbox');
require('../modules/select2/select2');
require('../modules/editor/editor');

require('./modules/toggler');
require('./modules/search');
require('./modules/symfony-collection');
require('./modules/ajax-page-loader');
require('./modules/tooltip-more-info');
require('./modules/mappemonde');

require('./pages/element-form');
require('./pages/user-set');

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require('jquery');
