var $ = require('jquery');
require('select2/dist/js/select2.min.js');
require('select2/dist/js/i18n/fr');

initSelects2(document);

/**
 * Init all the select2.
 *
 * @param {HTMLElement} container The HTML container
 */
function initSelects2(container)  {
  const selects = getSelects(document);

  Object.values(selects).forEach((select) => {
    initSelect2(select);
  });
}

/**
 * Get the fields to select2.
 *
 * @param {HTMLElement} container The HTML container
 *
 * @returns {HTMLSelectElement[]} The selects
 */
function getSelects(container) {
  return container.querySelectorAll('select');
}

/**
 * Init the select2.
 *
 * @param {HTMLSelectElement} select The select field
 */
function initSelect2(select)  {
  if (select.style.display !== 'none') {
    const allowClear = !select.multiple && !select.required;
    let options = {
      theme: 'foundation',
      placeholder: '',
      allowClear: allowClear,
      width: '100%'
    };

    if (select.hasAttribute('data-autocomplete-url')) {
      const autocompleteUrl = select.dataset.autocompleteUrl;

      options.minimumInputLength = 2;
      options.ajax = {
        url: autocompleteUrl,
        dataType: 'json',
        language: App.locale,
        delay: 500,
        data: function (params) {
          return { 'query': params.term, 'page': params.page };
        },
        processResults: function (data, params) {
          return {
            results: data.results,
            pagination: {
              more: data.has_next_page
            }
          };
        }
      };
    }

    $(select).select2(options);
  }
}

export { initSelects2 };
