const $ = require('jquery');
global.jQuery = $;

require('simplelightbox/dist/simple-lightbox.min');

/**
 * Search and initialize the lightboxes.
 *
 * @param {HTMLElement} container The HTML container
 */
function init(container) {
    const images = container.getElementsByClassName('lightbox');

    if (images.length > 0) {
        $(images).simpleLightbox();
    }
}

init(document);

export { init };
