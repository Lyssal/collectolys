const $ = require('jquery');

require('trumbowyg');
require('trumbowyg/dist/langs/fr.min');
require('trumbowyg/dist/ui/sass/trumbowyg.scss');

$.trumbowyg.svgPath = '/build/trumbowyg/icons.svg';

function init(container) {
    const editors = container.querySelectorAll('[data-ckeditor="true"]');

    // Update the textarea
    Object.values(editors).forEach(function (editor) {
        $(editor).trumbowyg({
            lang: 'fr',
            btns: [
                ['viewHTML'],
                ['customFormatting'],
                ['strong', 'em'],
                ['superscript', 'subscript'],
                ['link'],
                ['insertImage'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
            ],
            btnsDef: {
                customFormatting: {
                    dropdown: ['p', 'blockquote', 'h2', 'h3', 'h4'],
                    ico: 'p'
                }
            },
            autogrow: true,
        });
    });
}

init(document);

export { init };
